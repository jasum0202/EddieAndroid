// <eddie_source_header>
// This file is part of Eddie/AirVPN software.
// Copyright (C)2014-2018 AirVPN (support@airvpn.org) / https://airvpn.org
//
// Eddie is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Eddie is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Eddie. If not, see <http://www.gnu.org/licenses/>.
// </eddie_source_header>
//
// 20 June 2018 - author: ProMIND - initial release. (a tribute to the 1859 Perugia uprising occurred on 20 June 1859 and in memory of those brave inhabitants who fought for the liberty of Perugia)

package org.airvpn.eddie;

import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import org.w3c.dom.Document;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity implements NetworkStatusListener, EddieEventListener
{
    public static final int ACTIVITY_RESULT_FILE_CHOOSER = 1000;
    public static final int ACTIVITY_RESULT_SETTINGS = 1001;

    public static final int QUICK_CONNECT_FRAGMENT = 0;
    public static final int SERVER_LIST_FRAGMENT = 1;
    public static final int CONNECT_OPENVPN_PROFILE_FRAGMENT = 2;
    public static final int CONNECTION_INFO_FRAGMENT = 3;

    private AirVPNUser airVPNUser = null;
    private AirVPNManifest airVPNManifest = null;

    private int currentReconnectionRetry = 0;
    private boolean userRequestedDisconnection = false;

    private SupportTools supportTools = null;
    private EddieLogger eddieLogger = null;

    private SettingsManager settingsManager = null;

    private QuickConnectFragment quickConnectFragment = null;
    private ConnectAirVPNServerFragment connectAirVPNServerFragment = null;
    private ConnectOpenVpnProfileFragment connectOpenVpnProfileFragment = null;
    private ConnectionInfoFragment connectionInfoFragment = null;

    private static VPNManager vpnManager = null;
    private EddieEvent eddieEvent = null;
    private Toolbar toolbar = null;
    private TabLayout tabLayout = null;
    private ViewPager viewPager = null;
    private ViewPagerAdapter viewPagerAdapter = null;
    private DrawerLayout drawer = null;
    private ActionBarDrawerToggle drawerToggle = null;
    private NavigationView navigationView = null;
    private NetworkStatusReceiver networkStatusReceiver = null;
    private Menu appMenu = null;
    private MenuItem menuItemLogout = null;

    private Timer timerAirVPNManifestRefresh = null;

    private static VPN.Status lastVpnStatus = VPN.Status.UNKNOWN;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        supportTools = new SupportTools(this);
        settingsManager = new SettingsManager(this);

        String language = settingsManager.getSystemApplicationLanguage();

        if(!language.isEmpty())
        {
            supportTools.setLocale(language);

            EddieLogger.info("Application language overridden to %s", language);
        }

        setContentView(R.layout.main_activity_layout);

        if(vpnManager == null)
            vpnManager = new VPNManager(this);
        else
            vpnManager.setContext(this);

        eddieLogger = new EddieLogger();

        eddieLogger.init(this);

        networkStatusReceiver = new NetworkStatusReceiver(this);
        networkStatusReceiver.addListener(this);

        eddieEvent = new EddieEvent();
        eddieEvent.addListener(this);

        this.registerReceiver(networkStatusReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));

        toolbar = (Toolbar)findViewById(R.id.toolbar);

        toolbar.setTitle("");

        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        viewPager = (ViewPager)findViewById(R.id.viewpager);

        viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());

        quickConnectFragment = new QuickConnectFragment();
        connectAirVPNServerFragment = new ConnectAirVPNServerFragment();
        connectOpenVpnProfileFragment = new ConnectOpenVpnProfileFragment();
        connectionInfoFragment = new ConnectionInfoFragment();

        viewPagerAdapter.addFragment(quickConnectFragment, getResources().getString(R.string.fragment_quick_connect_cap));
        viewPagerAdapter.addFragment(connectAirVPNServerFragment, getResources().getString(R.string.fragment_server_connect_cap));
        viewPagerAdapter.addFragment(connectOpenVpnProfileFragment, getResources().getString(R.string.fragment_openvpn_connect_cap));
        viewPagerAdapter.addFragment(connectionInfoFragment, getResources().getString(R.string.fragment_connection_info_cap));

        viewPager.setAdapter(viewPagerAdapter);

        quickConnectFragment.setVpnManager(vpnManager);
        quickConnectFragment.setConnectButton();

        connectAirVPNServerFragment.setVpnManager(vpnManager);

        connectOpenVpnProfileFragment.setVpnManager(vpnManager);
        connectOpenVpnProfileFragment.setConnectButton();

        connectionInfoFragment.setVpnManager(vpnManager);
        connectionInfoFragment.hideConnectionStatus();

        tabLayout = (TabLayout)findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

        drawer = (DrawerLayout)findViewById(R.id.drawer_layout);
        drawerToggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(drawerToggle);
        drawerToggle.syncState();

        navigationView = (NavigationView)findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener()
        {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item)
            {
                navigationViewItemSelected(item);

                return false;
            }
        });

        airVPNUser = new AirVPNUser(this);
        airVPNManifest = new AirVPNManifest(this);
        timerAirVPNManifestRefresh = null;

        appMenu = navigationView.getMenu();

        menuItemLogout = appMenu.findItem(R.id.nav_logout);

        if(airVPNUser.isUserValid())
            menuItemLogout.setEnabled(true);
        else
            menuItemLogout.setEnabled(false);

        importOpenVPNProfile(getIntent());
    }

    @Override
    public void onSaveInstanceState(Bundle outState)
    {
        if(quickConnectFragment != null && quickConnectFragment.isAdded())
            getSupportFragmentManager().putFragment(outState,"quickConnectFragment", quickConnectFragment);

        if(connectAirVPNServerFragment != null && connectAirVPNServerFragment.isAdded())
            getSupportFragmentManager().putFragment(outState,"connectAirVPNServerFragment", connectAirVPNServerFragment);

        if(connectOpenVpnProfileFragment != null && connectOpenVpnProfileFragment.isAdded())
            getSupportFragmentManager().putFragment(outState,"connectOpenVpnProfileFragment", connectOpenVpnProfileFragment);

        if(connectionInfoFragment != null && connectionInfoFragment.isAdded())
            getSupportFragmentManager().putFragment(outState,"connectionInfoFragment", connectionInfoFragment);

        super.onSaveInstanceState(outState);
    }

    @Override
    public void onRestoreInstanceState(Bundle inState)
    {
        super.onRestoreInstanceState(inState);

        quickConnectFragment = (QuickConnectFragment)getSupportFragmentManager().getFragment(inState,"quickConnectFragment");
        connectAirVPNServerFragment = (ConnectAirVPNServerFragment)getSupportFragmentManager().getFragment(inState,"connectAirVPNServerFragment");
        connectOpenVpnProfileFragment = (ConnectOpenVpnProfileFragment)getSupportFragmentManager().getFragment(inState,"connectOpenVpnProfileFragment");
        connectionInfoFragment = (ConnectionInfoFragment)getSupportFragmentManager().getFragment(inState,"connectionInfoFragment");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        switch(requestCode)
        {
            case ACTIVITY_RESULT_SETTINGS:
            {
                if(resultCode == RESULT_OK)
                {
                    supportTools.setAppIsVisible(true);

                    VPN.Status currentConnectionStatus = VPN.getConnectionStatus();

                    if(currentConnectionStatus == VPN.Status.CONNECTED || currentConnectionStatus == VPN.Status.PAUSED_BY_USER || currentConnectionStatus == VPN.Status.PAUSED_BY_SYSTEM || currentConnectionStatus == VPN.Status.LOCKED)
                        supportTools.infoDialog(R.string.settings_changed, true);
                }
            }
            break;

            case VPNManager.VPN_REQUEST_CODE:
            {
                if(vpnManager != null)
                    vpnManager.handleActivityResult(requestCode, resultCode, data);
            }
            break;

            default:
            {
            }
            break;
        }
    }

    @Override
    public void onStart()
    {
        super.onStart();

        supportTools.setAppIsVisible(true);
    }

    @Override
    public void onStop()
    {
        super.onStop();

        supportTools.setAppIsVisible(false);
    }

    @Override
    public void onPause()
    {
        super.onPause();

        supportTools.setAppIsVisible(false);
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();

        networkStatusReceiver.removeListener(this);

        this.unregisterReceiver(networkStatusReceiver);

        eddieEvent.removeListener(this);

        supportTools.setAppIsVisible(false);

        if(VPN.getConnectionStatus() == VPN.Status.CONNECTED && connectionInfoFragment != null)
            connectionInfoFragment.stopVPNStats();
    }

    @Override
    protected void onResume()
    {
        super.onResume();

        supportTools.setAppIsVisible(true);

        if(connectionInfoFragment != null)
            connectionInfoFragment.updateConnectionData(VPN.getVpnConnectionData());
    }

    @Override
    public void onBackPressed()
    {
        if(drawer.isDrawerOpen(GravityCompat.START))
            drawer.closeDrawer(GravityCompat.START);
        else
            super.onBackPressed();
    }

    public void setVpnManager(VPNManager v)
    {
        vpnManager = v;

        if(quickConnectFragment != null)
            quickConnectFragment.setVpnManager(vpnManager);

        if(connectAirVPNServerFragment != null)
            connectAirVPNServerFragment.setVpnManager(vpnManager);

        if(connectOpenVpnProfileFragment != null)
            connectOpenVpnProfileFragment.setVpnManager(vpnManager);

        if(connectionInfoFragment != null)
            connectionInfoFragment.setVpnManager(vpnManager);
    }

    protected void navigationViewItemSelected(MenuItem menuItem)
    {
        drawer.closeDrawers();

        switch(menuItem.getItemId())
        {
            case R.id.nav_quick_connect:
            {
                if(viewPager != null)
                    viewPager.setCurrentItem(QUICK_CONNECT_FRAGMENT, true);
            }
            break;

            case R.id.nav_server_list:
            {
                if(viewPager != null)
                    viewPager.setCurrentItem(SERVER_LIST_FRAGMENT, true);
            }
            break;

            case R.id.nav_openvpn_connect:
            {
                if(viewPager != null)
                    viewPager.setCurrentItem(CONNECT_OPENVPN_PROFILE_FRAGMENT, true);
            }
            break;

            case R.id.nav_connection_info:
            {
                if(viewPager != null)
                    viewPager.setCurrentItem(CONNECTION_INFO_FRAGMENT, true);
            }
            break;

            case R.id.nav_log:
            {
                Intent logActivityIntent = new Intent(this, LogActivity.class);

                logActivityIntent.putExtra("ViewMode", LogActivity.MODE_LISTVIEW);

                startActivity(logActivityIntent);
            }
            break;

            case R.id.nav_settings:
            {
                Intent settingsActivityIntent = new Intent(this, SettingsActivity.class);

                startActivityForResult(settingsActivityIntent, ACTIVITY_RESULT_SETTINGS);
            }
            break;

            case R.id.nav_logout:
            {
                airVPNUser.logout();
            }
            break;

            case R.id.nav_about:
            {
                Intent aboutActivityIntent = new Intent(this, WebViewerActivity.class);

                aboutActivityIntent.putExtra("icon", android.R.drawable.ic_menu_info_details);
                aboutActivityIntent.putExtra("title", getResources().getString(R.string.about_title));
                aboutActivityIntent.putExtra("uri", getResources().getString(R.string.local_about_page));
                aboutActivityIntent.putExtra("html", "");

                startActivity(aboutActivityIntent);
            }
            break;

            case R.id.nav_website:
            {
                Intent airVPNWebSiteIntent = new Intent(this, WebViewerActivity.class);

                airVPNWebSiteIntent.putExtra("icon", android.R.drawable.ic_menu_compass);
                airVPNWebSiteIntent.putExtra("title", getResources().getString(R.string.eddie));
                airVPNWebSiteIntent.putExtra("uri", getResources().getString(R.string.eddie_url));
                airVPNWebSiteIntent.putExtra("html", "");

                startActivity(airVPNWebSiteIntent);
            }
            break;
        }
    }

    private void importOpenVPNProfile(Intent intent)
    {
        if(intent == null)
            return;

        Uri intentUri = intent.getData();

        if(intentUri == null)
            return;

        connectOpenVpnProfileFragment.loadExternalProfile(intentUri);

        viewPager.setCurrentItem(CONNECT_OPENVPN_PROFILE_FRAGMENT, true);
    }

    private void updateConnectionStatus(VPN.Status vpnStatus, String message)
    {
        String serverDescription = "";
        HashMap<String, String> profileInfo = null;

        if(vpnStatus == VPN.Status.CONNECTED || vpnStatus == VPN.Status.NOT_CONNECTED || vpnStatus == VPN.Status.CONNECTION_REVOKED_BY_SYSTEM)
            supportTools.dismissConnectionProgressDialog();

        if(vpnStatus == lastVpnStatus)
            return;

        if(vpnStatus != VPN.Status.UNKNOWN)
        {
            profileInfo = VPN.getProfileInfo();

            if(profileInfo != null)
            {
                if(!profileInfo.get("description").equals(""))
                    serverDescription = String.format("AirVPN %s (%s)", profileInfo.get("description"), profileInfo.get("server"));
                else
                    serverDescription = profileInfo.get("server");
            }
            else
                serverDescription = "";

            switch(vpnStatus)
            {
                case CONNECTED:
                {
                    if(profileInfo != null && profileInfo.containsKey("server") == true)
                        supportTools.infoDialog(String.format(Locale.getDefault(), getResources().getString(R.string.connection_success), serverDescription, NetworkStatusReceiver.getNetworkDescription()), false);

                    if(VPN.getConnectionMode() == VPN.ConnectionMode.OPENVPN_PROFILE || VPN.getConnectionMode() == VPN.ConnectionMode.BOOT_CONNECT)
                        settingsManager.setSystemLastProfileIsConnected(true);
                    else
                        settingsManager.setSystemLastProfileIsConnected(false);

                    viewPager.setCurrentItem(CONNECTION_INFO_FRAGMENT, true);

                    menuItemLogout.setEnabled(false);

                    currentReconnectionRetry = 0;
                    userRequestedDisconnection = false;

                    VPN.setPendingProfileInfo(null);
                    VPN.setPendingOpenVpnProfile("");
                    VPN.setPendingProgressMessage("");
                }
                break;

                case NOT_CONNECTED:
                case CONNECTION_REVOKED_BY_SYSTEM:
                case CONNECTION_ERROR:
                {
                    HashMap<String, String> pendingProfileInfo = VPN.getPendingProfileInfo();
                    String pendingOpenVpnProfile = VPN.getPendingOpenVpnProfile();
                    VPN.ConnectionMode connectionMode = VPN.ConnectionMode.UNKNOWN;

                    settingsManager.setSystemLastProfileIsConnected(false);

                    if(airVPNUser.isUserValid())
                        menuItemLogout.setEnabled(true);
                    else
                        menuItemLogout.setEnabled(false);

                    VPN.setProfileInfo(null);

                    if(pendingProfileInfo != null)
                    {
                        VPN.setProfileInfo(pendingProfileInfo);

                        try
                        {
                            int mode = Integer.parseInt(pendingProfileInfo.get("mode"));

                            connectionMode = VPN.getConnectionMode(mode);
                        }
                        catch(NumberFormatException e)
                        {
                            connectionMode = VPN.ConnectionMode.UNKNOWN;
                        }

                        switch(connectionMode)
                        {
                            case AIRVPN_SERVER:
                            {
                                VPN.setConnectionMode(VPN.ConnectionMode.AIRVPN_SERVER);

                                VPN.setConnectionModeDescription(getResources().getString(R.string.conn_type_airvpn_server));

                                VPN.setUserProfileDescription(airVPNUser.getCurrentProfile());

                                VPN.setUserName(airVPNUser.getUserName());
                            }
                            break;

                            case OPENVPN_PROFILE:
                            {
                                VPN.setConnectionMode(VPN.ConnectionMode.OPENVPN_PROFILE);

                                VPN.setConnectionModeDescription(getResources().getString(R.string.conn_type_openvpn_profile));

                                VPN.setUserProfileDescription("");

                                VPN.setUserName("");
                            }
                            break;

                            default:
                            {
                                return;
                            }
                        }

                        supportTools.showConnectionProgressDialog(VPN.getPendingProgressMessage());

                        startConnection(pendingOpenVpnProfile);

                        VPN.setPendingProfileInfo(null);
                        VPN.setPendingOpenVpnProfile("");
                        VPN.setPendingProgressMessage("");
                    }
                    else
                    {
                        switch(vpnStatus)
                        {
                            case NOT_CONNECTED:
                            {
                                supportTools.infoDialog(String.format(Locale.getDefault(), getResources().getString(R.string.connection_disconnected), serverDescription), false);
                            }
                            break;

                            case CONNECTION_REVOKED_BY_SYSTEM:
                            {
                                supportTools.infoDialog(R.string.connection_revoked, false);
                            }
                            break;

                            case CONNECTION_ERROR:
                            {
                                supportTools.infoDialog(R.string.connection_error, true);

                                VPN.setConnectionStatus(VPN.Status.NOT_CONNECTED);
                            }
                            break;

                            default:
                            {
                                supportTools.infoDialog(String.format(Locale.getDefault(), getResources().getString(R.string.connection_disconnected), serverDescription), false);
                            }
                            break;
                        }
                    }
                }
                break;

                case CONNECTING:
                {
                }
                break;

                case DISCONNECTING:
                {
                    userRequestedDisconnection = true;
                }
                break;

                case PAUSED_BY_USER:
                case PAUSED_BY_SYSTEM:
                {
                    supportTools.infoDialog(getResources().getString(R.string.connection_paused), false);

                    settingsManager.setSystemLastProfileIsConnected(true);
                }
                break;

                case LOCKED:
                {
                    settingsManager.setSystemLastProfileIsConnected(true);
                }
                break;

                default:
                {
                    settingsManager.setSystemLastProfileIsConnected(false);
                }
                break;
            }

            lastVpnStatus = vpnStatus;
        }
    }


    private void startConnection(String openVPNProfile)
    {
        if(vpnManager == null)
        {
            EddieLogger.error("MainActivity.startConnection(): vpnManager is null");

            return;
        }

        if(openVPNProfile.equals(""))
            return;

        vpnManager.clearProfile();

        vpnManager.setProfile(openVPNProfile);

        String profileString = settingsManager.getOvpn3CustomDirectives().trim();

        if(profileString.length() > 0)
            vpnManager.addProfileString(profileString);

        vpnManager.start();
    }

    private void vpnAuthFailed(OpenVPNEvent oe)
    {
        supportTools.waitInfoDialog(R.string.conn_auth_failed);

        try
        {
            vpnManager.stop();
        }
        catch(Exception e)
        {
            EddieLogger.error("MainActivity.vpnAuthFailed() exception: %s", e.getMessage());
        }

        supportTools.dismissConnectionProgressDialog();
    }

    private void startAirVPNManifestRefresh()
    {
        if(!AirVPNUser.isUserValid())
            return;

        int updateInterval = 0;

        if(airVPNManifest != null)
            airVPNManifest = new AirVPNManifest(this);

        if(timerAirVPNManifestRefresh != null)
            timerAirVPNManifestRefresh.cancel();

        timerAirVPNManifestRefresh = new Timer();

        updateInterval = airVPNManifest.getNextUpdateIntervalMinutes() * 60000; // Minutes to milliseconds

        timerAirVPNManifestRefresh.schedule(new TimerTask()
        {
            @Override
            public void run()
            {
                airVPNManifest.refreshManifestFromAirVPN();
            }
        }, updateInterval, updateInterval);
    }

    public void stopAirVPNManifestRefresh()
    {
        if(timerAirVPNManifestRefresh != null)
            timerAirVPNManifestRefresh.cancel();

        timerAirVPNManifestRefresh = null;
    }

    // Eddie events

    public void onVpnConnectionDataChanged(final OpenVPNConnectionData connectionData)
    {
    }

    public void onVpnStatusChanged(final VPN.Status status, final String message)
    {
        Runnable runnable = new Runnable()
        {
            @Override
            public void run()
            {
                updateConnectionStatus(status, message);
            }
        };

        supportTools.runOnUiActivity(this, runnable);
    }

    public void onVpnAuthFailed(final OpenVPNEvent oe)
    {
        Runnable runnable = new Runnable()
        {
            @Override
            public void run()
            {
                vpnAuthFailed(oe);
            }
        };

        supportTools.runOnUiActivity(this, runnable);
    }

    public void onVpnError(final OpenVPNEvent oe)
    {
        if(oe.type == OpenVPNEvent.DISCONNECTED && !userRequestedDisconnection)
        {
            EddieLogger.info("WARNING: Unexpected OpenVPN disconnection detected.");

            VPN.setConnectionStatus(VPN.Status.NOT_CONNECTED);

            eddieEvent.onVpnStatusChanged(VPN.Status.NOT_CONNECTED, getResources().getString(VPN.descriptionResource(VPN.Status.NOT_CONNECTED)));

            if(settingsManager.isVPNReconnectEnabled() && settingsManager.isVPNLockEnabled() == false)
            {
                int maxReconnectionRetries = 0;

                try
                {
                    maxReconnectionRetries = Integer.parseInt(settingsManager.getVPNReconnectionRetries());
                }
                catch (NumberFormatException e)
                {
                    maxReconnectionRetries = 0;
                }

                if(currentReconnectionRetry < maxReconnectionRetries)
                {
                    currentReconnectionRetry++;

                    EddieLogger.info("Trying to reconnect VPN. Attempt %d out of %d", currentReconnectionRetry, maxReconnectionRetries);

                    vpnManager.start();
                }
                else
                    EddieLogger.info("WARNING: Maximum reconnection attempts reached.");
            }
        }
    }

    public void onMasterPasswordChanged()
    {
    }

    public void onAirVPNLogin()
    {
        Runnable runnable = new Runnable()
        {
            @Override
            public void run()
            {
                String logoutMenuItemTitle = getResources().getString(R.string.menu_logout_cap);

                logoutMenuItemTitle += " " + airVPNUser.getUserName();

                menuItemLogout.setEnabled(true);

                menuItemLogout.setTitle(logoutMenuItemTitle);
            }
        };

        supportTools.runOnUiActivity(this, runnable);

        if(AirVPNUser.isUserValid())
            startAirVPNManifestRefresh();
    }

    public void onAirVPNLoginFailed()
    {
        Runnable runnable = new Runnable()
        {
            @Override
            public void run()
            {
                menuItemLogout.setEnabled(false);

                menuItemLogout.setTitle(getResources().getString(R.string.menu_logout_cap));
            }
        };

        supportTools.runOnUiActivity(this, runnable);
    }

    public void onAirVPNLogout()
    {
        Runnable runnable = new Runnable()
        {
            @Override
            public void run()
            {
                menuItemLogout.setEnabled(false);

                menuItemLogout.setTitle(getResources().getString(R.string.menu_logout_cap));
            }
        };

        supportTools.runOnUiActivity(this, runnable);

        stopAirVPNManifestRefresh();
    }

    public void onAirVPNUserDataChanged()
    {
    }

    public void onAirVPNUserProfileReceived(Document document)
    {
    }

    public void onAirVPNUserProfileDownloadError()
    {
    }

    public void onAirVPNUserProfileChanged()
    {
    }

    public void onAirVPNManifestReceived(Document document)
    {
    }

    public void onAirVPNManifestDownloadError()
    {
    }

    public void onAirVPNManifestChanged()
    {
        if(airVPNManifest == null)
            return;

        ArrayList<AirVPNManifest.Message> messages = airVPNManifest.getMessages();
        AirVPNManifest.Message m = null;
        String htmlDocument = "";
        int nMessage = 0;
        long utcTimestamp = System.currentTimeMillis() / 1000;

        if(messages != null && messages.size() > 0)
        {
            htmlDocument = "<html><body><br>\n";

            for(int i = 0; i < messages.size(); i++)
            {
                m = messages.get(i);

                if(!m.isShown() && utcTimestamp >= m.getFromUTCTimeTS() && utcTimestamp <= m.getToUTCTimeTS())
                {
                    if(i > 0)
                        htmlDocument += "<br><br><hr><br><br>\n";

                    htmlDocument += "<div>\n";

                    if(!m.getHtml().isEmpty())
                        htmlDocument += m.getHtml();
                    else
                    {
                        htmlDocument += "<font style='font-size:20px'>";
                        htmlDocument += m.getText() + "<br><br>";

                        htmlDocument += "<a href='" + m.getUrl() + "'>" + m.getLink() + "</a></font><br>";
                    }

                    htmlDocument += "</div>\n";

                    airVPNManifest.setMessageShown(i);

                    nMessage++;
                }

            }

            htmlDocument += "</body></html>\n";

            if(nMessage > 0)
            {
                EddieLogger.info("Displayed %d manifest's messages", nMessage);

                Intent messageIntent = new Intent(this, WebViewerActivity.class);

                messageIntent.putExtra("icon", android.R.drawable.ic_menu_info_details);
                messageIntent.putExtra("title", getResources().getString(R.string.eddie));
                messageIntent.putExtra("uri", "");
                messageIntent.putExtra("html", htmlDocument);

                startActivity(messageIntent);
            }
        }
    }

    public void onAirVPNIgnoredDocumentRequest()
    {
    }

    public void onCancelConnection()
    {
        Runnable runnable = new Runnable()
        {
            @Override
            public void run()
            {
                try
                {
                    vpnManager.stop();
                }
                catch(Exception e)
                {
                    supportTools.infoDialog(e.getMessage(), true);
                }
            }
        };

        supportTools.runOnUiActivity(this, runnable);
    }

    // NetworkStatusReceiver

    public void onNetworkStatusNotAvailable()
    {
    }

    public void onNetworkStatusConnected()
    {
    }

    public void onNetworkStatusIsConnecting()
    {
    }

    public void onNetworkStatusIsDisonnecting()
    {
    }

    public void onNetworkStatusSuspended()
    {
    }

    public void onNetworkStatusNotConnected()
    {
    }

    public void onNetworkTypeChanged()
    {
    }
}
