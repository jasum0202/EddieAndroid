// <eddie_source_header>
// This file is part of Eddie/AirVPN software.
// Copyright (C)2014-2018 AirVPN (support@airvpn.org) / https://airvpn.org
//
// Eddie is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Eddie is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Eddie. If not, see <http://www.gnu.org/licenses/>.
// </eddie_source_header>
//
// 5 September 2018 - author: ProMIND - initial release. (a tribute to the 1859 Perugia uprising occurred on 20 June 1859 and in memory of those brave inhabitants who fought for the liberty of Perugia)

package org.airvpn.eddie;

import android.app.Activity;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class WebViewerActivity extends Activity
{
    private int iconResource = 0;
    private String title = "";
    private String uri = "";
    private String html = "";

    private LinearLayout llMainLayout = null;

    private ImageView imgIcon = null;
    private TextView txtTitle = null;
    private Button btnBack = null;
    private WebView webView = null;

    private SupportTools supportTools = null;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.webviewer_activity_layout);

        supportTools = new SupportTools(this);

        Bundle extras = getIntent().getExtras();

        iconResource = extras.getInt("icon");
        title = extras.getString("title");
        uri = extras.getString("uri");
        html = extras.getString("html");

        llMainLayout = (LinearLayout)findViewById(R.id.main_layout);

        imgIcon = (ImageView)findViewById(R.id.icon);
        txtTitle = (TextView)findViewById(R.id.title);
        btnBack = (Button)findViewById(R.id.btn_back);

        if(iconResource > 0)
            imgIcon.setImageDrawable(getDrawable(iconResource));

        txtTitle.setText(title);

        btnBack.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                if(webView != null)
                {
                    if(webView.canGoBack())
                        webView.goBack();
                    else
                        WebViewerActivity.this.finish();
                }
            }
        });

        btnBack.setOnFocusChangeListener(new View.OnFocusChangeListener()
        {
            @Override
            public void onFocusChange(View v, boolean hasFocus)
            {
                if(!btnBack.isEnabled())
                    return;

                if(hasFocus)
                    btnBack.setBackgroundResource(R.drawable.arrow_back_focus);
                else
                    btnBack.setBackgroundResource(R.drawable.arrow_back);
            }
        });

        btnBack.setAccessibilityDelegate(new View.AccessibilityDelegate()
        {
            @Override
            public void sendAccessibilityEvent(View host, int eventType)
            {
                super.sendAccessibilityEvent(host, eventType);

                btnBack.setContentDescription(getString(R.string.accessibility_go_back));
            }
        });

        webView = (WebView)findViewById(R.id.webview);

        webView.setClickable(true);
        webView.getSettings().setJavaScriptEnabled(true);

        CookieManager.getInstance().setAcceptThirdPartyCookies(webView, true);

        webView.setWebViewClient(new EddieWebViewClient());

        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setDisplayZoomControls(false);

        if(uri != null & !uri.isEmpty())
            webView.loadUrl(uri);
        else if(html != null & !html.isEmpty())
            webView.loadDataWithBaseURL(null, html, "text/html", "UTF-8", null);
        else
            this.finish();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);

        webView.saveState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState)
    {
        super.onRestoreInstanceState(savedInstanceState);

        if(savedInstanceState != null)
            webView.restoreState(savedInstanceState);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig)
    {
        llMainLayout.removeView(webView);

        super.onConfigurationChanged(newConfig);

        llMainLayout.addView(webView);
    }

    @Override
    public void onBackPressed()
    {
        this.finish();
    }

    private class EddieWebViewClient extends WebViewClient
    {
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon)
        {
            super.onPageStarted(view, url, favicon);

            if(!WebViewerActivity.this.isDestroyed())
                supportTools.showProgressDialog(getResources().getString(R.string.loading_page_wait));
        }

        public void onPageFinished(WebView view, String url)
        {
            super.onPageFinished(view, url);

            if(!WebViewerActivity.this.isDestroyed())
                supportTools.dismissProgressDialog();
        }
    }
}
