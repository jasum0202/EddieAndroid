// <eddie_source_header>
// This file is part of Eddie/AirVPN software.
// Copyright (C)2014-2018 AirVPN (support@airvpn.org) / https://airvpn.org
//
// Eddie is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Eddie is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Eddie. If not, see <http://www.gnu.org/licenses/>.
// </eddie_source_header>
//
// 20 June 2018 - author: ProMIND - initial release. Based on revised code from com.eddie.android. (a tribute to the 1859 Perugia uprising occurred on 20 June 1859 and in memory of those brave inhabitants who fought for the liberty of Perugia)

package org.airvpn.eddie;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.net.ConnectivityManager;
import android.net.VpnService;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;

import java.util.ArrayList;
import java.util.Locale;

import static android.app.Activity.RESULT_OK;

public class VPNManager implements MessageHandlerListener, ServiceConnection
{
    public static final int VPN_REQUEST_CODE = 3000;

    private Context appContext = null;
    private boolean vpnReady = false;
    private boolean vpnBound = false;
    private Messenger serviceMessenger = null;
    private Messenger clientMessenger = null;
    private String vpnProfile = "";
    private ArrayList<String> vpnProfileStrings = new ArrayList<String>();
    private SupportTools supportTools = null;
    private SettingsManager settingsManager = null;
    private EddieLogger eddieLogger = null;
    private EddieEvent eddieEvent = null;
    private ArrayList<String> localIPv4Routes = null, localIPv6Routes = null;

    public VPNManager(Context context)
    {
        setContext(context);
    }

    public void setContext(Context context)
    {
        appContext = context;

        eddieLogger = new EddieLogger();

        eddieLogger.init(context);

        eddieEvent = new EddieEvent();

        supportTools = new SupportTools(appContext);

        settingsManager = new SettingsManager(appContext);

        localIPv4Routes = supportTools.getLocalIPs(false);
        localIPv6Routes = supportTools.getLocalIPs(true);
    }

    protected void finalize() throws Throwable
    {
        try
        {
            if(vpnBound)
                unbindService(true);
        }
        catch(Exception e)
        {
            EddieLogger.error("VPNManager.finalize(): unbindService() exception: ", e);
        }
        finally
        {
            try
            {
                super.finalize();
            }
            catch(Exception e)
            {
                EddieLogger.error("VPNManager.finalize(): super() exception: ", e);
            }
        }
    }

    public boolean isReady()
    {
        return vpnReady;
    }

    public void setProfile(String profile)
    {
        vpnProfile = profile;
    }

    public void addProfileString(String str)
    {
        if(str.isEmpty())
            return;

        vpnProfileStrings.add(str);
    }

    public void clearProfile()
    {
        vpnProfile = "";

        vpnProfileStrings.clear();
    }

    public boolean start()
    {
        String logEntry;

        logEntry = "VPN Lock is ";

        if(settingsManager.isVPNLockEnabled())
            logEntry += "enabled";
        else
            logEntry += "disabled";

        EddieLogger.info(logEntry);

        logEntry = "Local networks are ";

        if(settingsManager.areLocalNetworksExcluded() && NetworkStatusReceiver.getNetworkType() != NetworkStatusReceiver.NetworkType.MOBILE)
            logEntry += "exempted from";
        else
            logEntry += "tunneled into";

        logEntry += " the VPN";

        if(settingsManager.areLocalNetworksExcluded() && NetworkStatusReceiver.getNetworkType() == NetworkStatusReceiver.NetworkType.MOBILE)
            logEntry += " (Device is currently connected to mobile network therefore local networks are tunneled by default)";

        EddieLogger.info(logEntry);

        if(!settingsManager.getAirVPNCipher().equals(SettingsManager.AIRVPN_CIPHER_SERVER))
        {
            logEntry = "Selected cipher: " + settingsManager.getAirVPNCipher();

            EddieLogger.info(logEntry);
        }

        try
        {
            handleActivityStart();

            return true;
        }
        catch(Exception e)
        {
            EddieLogger.error("VPNManager.start() exception: ", e);

            onStatusChanged(VPN.Status.UNKNOWN, e.getMessage());
        }

        return false;
    }

    public void stop()
    {
        try
        {
            onStatusChanged(VPN.Status.DISCONNECTING, appContext.getResources().getString(VPN.descriptionResource(VPN.Status.DISCONNECTING)));

            sendStopMessage();

            handleActivityStop();
        }
        catch(Exception e)
        {
            EddieLogger.error("VPNManager.stop() exception: ", e);

            onStatusChanged(VPN.Status.UNKNOWN, e.getMessage());
        }
    }

    public boolean pause()
    {
        try
        {
            sendPauseMessage();

            return true;
        }
        catch(Exception e)
        {
            EddieLogger.error("VPNManager.pause() exception: ", e);

            onStatusChanged(VPN.Status.UNKNOWN, e.getMessage());
        }

        return false;
    }

    public boolean resume()
    {
        try
        {
            sendResumeMessage();

            return true;
        }
        catch(Exception e)
        {
            EddieLogger.error("VPNManager.resume() exception: ", e);

            onStatusChanged(VPN.Status.UNKNOWN, e.getMessage());
        }

        return false;
    }

    public void handleActivityResult(int requestCode, int resultCode, Intent data)
    {
        if(requestCode != VPN_REQUEST_CODE)
            return;

        if(resultCode == RESULT_OK)
            doBindService();
    }

    public void handleActivityStart()
    {
        bindService();
    }

    public void handleActivityStop()
    {
        unbindService(true);
    }

    private Intent createServiceIntent()
    {
        Intent vpnServiceIntent = new Intent(appContext, VPNService.class);

        return vpnServiceIntent;
    }

    private synchronized void onStatusChanged(VPN.Status status, String message)
    {
        VPN.setConnectionStatus(status);

        if(eddieEvent != null)
            eddieEvent.onVpnStatusChanged(status, message);
    }

    public void onNullBinding(ComponentName name)
    {
        EddieLogger.error("VPNManager.onNullBinding(): Cannot bind VPN service to app: %s", name.toString());

        vpnBound = false;

        VPN.setConnectionStatus(VPN.Status.NOT_CONNECTED);

        if(eddieEvent != null)
            eddieEvent.onVpnStatusChanged(VPN.getConnectionStatus(), appContext.getResources().getString(VPN.descriptionResource(VPN.getConnectionStatus())));
    }

    public void onBindingDied(ComponentName name)
    {
        EddieLogger.error("VPNManager.onBindingDied(): Binding died during VPN prepare: %s", name.toString());

        vpnBound = false;

        VPN.setConnectionStatus(VPN.Status.NOT_CONNECTED);

        if(eddieEvent != null)
            eddieEvent.onVpnStatusChanged(VPN.getConnectionStatus(), appContext.getResources().getString(VPN.descriptionResource(VPN.getConnectionStatus())));
    }

    public void onServiceConnected(ComponentName name, IBinder service)
    {
        vpnBound = true;

        clientMessenger = new Messenger(new MessageHandler(this));
        serviceMessenger = new Messenger(service);

        vpnReady = true;

        sendBindMessage(true);

        sendStartMessage();
    }

    public void onServiceDisconnected(ComponentName name)
    {
        vpnBound = false;
    }

    private void sendStartMessage()
    {
        appContext.startService(createServiceIntent());

        sendMessage(Message.obtain(null, VPNService.MSG_START), createProfileBundle());
    }

    private Bundle createProfileBundle()
    {
        String routeSpec[], routeIP;
        long maskIP;

        if(vpnProfile.equals(""))
            return null;

        String profile = "";

        // Exclude local networks from VPN

        if(settingsManager.areLocalNetworksExcluded() && NetworkStatusReceiver.getNetworkType() != NetworkStatusReceiver.NetworkType.MOBILE)
        {
            for(String route : localIPv4Routes)
            {
                routeSpec = route.split("/");

                try
                {
                    maskIP = 0xffffffffL << (32 - Integer.parseInt(routeSpec[1]));
                }
                catch(NumberFormatException e)
                {
                    maskIP = 0xffffffffL;
                }

                routeIP = supportTools.longToIP(supportTools.IPToLong(routeSpec[0]) & maskIP);

                vpnProfileStrings.add(String.format("route %s %s net_gateway", routeIP, supportTools.longToIP(maskIP)));
            }
        }

        if(vpnProfileStrings.size() == 0)
            profile = vpnProfile + "\n";
        else
        {
            boolean directivesAdded = false;

            vpnProfile = vpnProfile.replace("\r\n", "\n");

            String[] lines = vpnProfile.split("\n");

            for(String curLine : lines)
            {
                if(directivesAdded == false && !curLine.equals("") && curLine.charAt(0) == '<')
                {
                    for(String profileString : vpnProfileStrings)
                        profile += profileString + "\n";

                    directivesAdded = true;
                }

                profile += curLine + "\n";
            }
        }

        Bundle data = new Bundle();

        data.putString(VPNService.PARAM_PROFILE, profile.replace("\r\n", "\n"));

        return data;
    }

    private void sendStopMessage()
    {
        sendMessage(Message.obtain(null, VPNService.MSG_STOP), null);
    }

    private void sendPauseMessage()
    {
        sendMessage(Message.obtain(null, VPNService.MSG_PAUSE), null);
    }

    private void sendResumeMessage()
    {
        sendMessage(Message.obtain(null, VPNService.MSG_RESUME), null);
    }

    private void sendBindMessage(boolean bind)
    {
        sendMessage(Message.obtain(null, VPNService.MSG_BIND, bind ? VPNService.MSG_BIND_ARG_ADD : VPNService.MSG_BIND_ARG_REMOVE, 0), null);
    }

    private void sendMessage(Message msg, Bundle payload)
    {
        if(serviceMessenger == null)
        {
            supportTools.infoDialog(String.format(Locale.getDefault(), appContext.getResources().getString(R.string.conn_cannot_start_vpnservice)), true);

            EddieLogger.error("VPNManager.sendMessage(): serviceMessenger is null");

            return;
        }

        msg.replyTo = clientMessenger;

        if(payload != null)
            msg.setData(payload);

        try
        {
            serviceMessenger.send(msg);
        }
        catch(RemoteException e)
        {
            EddieLogger.error("VPNManager.sendMessage() exception: %s", e);
        }
    }

    private void bindService()
    {
        Intent vpnServiceIntent = VpnService.prepare(appContext.getApplicationContext());

        if(vpnServiceIntent != null)
        {
            Activity activity = (Activity)appContext;

            if(activity != null)
                activity.startActivityForResult(vpnServiceIntent, VPN_REQUEST_CODE);
            else
                EddieLogger.error("VPNManager.bindService(): Failed to cast Context to Activity");
        }
        else
            handleActivityResult(VPN_REQUEST_CODE, Activity.RESULT_OK, null);
    }

    private void doBindService()
    {
        try
        {
            if(!appContext.bindService(createServiceIntent(), this, Context.BIND_AUTO_CREATE))
            {
                throw new Exception("VPNManager.doBindService(): Failed to bind service");
            }
        }
        catch(Exception e)
        {
            onStatusChanged(VPN.Status.UNKNOWN, e.getMessage());
        }
    }

    private void unbindService(boolean unbind)
    {
        if(clientMessenger != null)
        {
            if(unbind)
                sendBindMessage(false);

            clientMessenger = null;
        }

        if(vpnBound)
        {
            try
            {
                if(unbind)
                    appContext.unbindService(this);
            }
            catch(Exception e)
            {
                EddieLogger.warning("VPNManager.unbindService(): service unbound");

                vpnBound = false;
            }

            vpnBound = false;
        }

        if(serviceMessenger != null)
            serviceMessenger = null;

        vpnReady = false;
    }

    public synchronized void onMessage(Message msg)
    {
        if(msg == null)
            return;

        switch(msg.what)
        {
            case VPNService.MSG_STATUS:
            {
                VPN.Status status = VPN.getStatus(msg.arg1);

                onStatusChanged(status, msg.getData().getString(VPNService.MESSAGE_TEXT));
            }
            break;

            case VPNService.MSG_REVOKE:
            {
                unbindService(true);
            }
            break;
        }
    }
}
