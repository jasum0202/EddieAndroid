// <eddie_source_header>
// This file is part of Eddie/AirVPN software.
// Copyright (C)2014-2018 AirVPN (support@airvpn.org) / https://airvpn.org
//
// Eddie is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Eddie is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Eddie. If not, see <http://www.gnu.org/licenses/>.
// </eddie_source_header>
//
// 13 July 2019 - author: ProMIND - initial release. (a tribute to the 1859 Perugia uprising occurred on 20 June 1859 and in memory of those brave inhabitants who fought for the liberty of Perugia)

package org.airvpn.eddie;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class AirVPNServerGroup
{
    private HashMap<Integer, ServerGroup> serverGroupDatabase;

    public AirVPNServerGroup()
    {
        serverGroupDatabase = new HashMap<Integer, ServerGroup>();
    }

    boolean add(ServerGroup s)
    {
        boolean success = true;

        if(serverGroupDatabase == null)
            return false;

        for(Map.Entry<Integer, ServerGroup> map : serverGroupDatabase.entrySet())
        {
            if(s.getGroup() == map.getKey())
                success = false;
        }

        if(!success)
            return false;

        serverGroupDatabase.put(s.getGroup(), s);

        return success;
    }

    public boolean getGroupIPv4Support(int group)
    {
        boolean result = false;

        ServerGroup serverGroup = serverGroupDatabase.get(group);

        if(serverGroup != null)
            result = serverGroup.isIPv4Supported();

        return result;
    }

    public boolean getGroupIPv6Support(int group)
    {
        boolean result = false;

        ServerGroup serverGroup = serverGroupDatabase.get(group);

        if(serverGroup != null)
            result = serverGroup.isIPv6Supported();

        return result;
    }

    public boolean getGroupSupportCheck(int group)
    {
        boolean result = false;

        ServerGroup serverGroup = serverGroupDatabase.get(group);

        if(serverGroup != null)
            result = serverGroup.isCheckSupported();

        return result;
    }

    public ArrayList<Integer> getGroupTlsCiphers(int group)
    {
        ArrayList<Integer> result = null;

        ServerGroup serverGroup = serverGroupDatabase.get(group);

        if(serverGroup != null)
            result = serverGroup.getTlsCiphers();

        return result;
    }

    public ArrayList<Integer> getGroupTlsSuiteCiphers(int group)
    {
        ArrayList<Integer> result = null;

        ServerGroup serverGroup = serverGroupDatabase.get(group);

        if(serverGroup != null)
            result = serverGroup.getTlsSuiteCiphers();

        return result;
    }

    public ArrayList<Integer> getGroupDataCiphers(int group)
    {
        ArrayList<Integer> result = null;

        ServerGroup serverGroup = serverGroupDatabase.get(group);

        if(serverGroup != null)
            result = serverGroup.getDataCiphers();

        return result;
    }

    public void reset()
    {
        serverGroupDatabase.clear();
    }

    public class ServerGroup
    {
        private int group = -1;
        private boolean ipv4Support;
        private boolean ipv6Support;
        private boolean checkSupport;
        private ArrayList<Integer> tlsCiphers;
        private ArrayList<Integer> tlsSuiteCiphers;
        private ArrayList<Integer> dataCiphers;

        ServerGroup()
        {
            group = -1;
            ipv4Support = false;
            ipv6Support = false;
            checkSupport = false;
            tlsCiphers = null;
            tlsSuiteCiphers = null;
            dataCiphers = null;
        }

        public void setGroup(int code)
        {
            group = code;
        }

        public int getGroup()
        {
            return group;
        }

        public void setIPv4Support(boolean s)
        {
            ipv4Support = s;
        }

        public boolean isIPv4Supported()
        {
            return ipv4Support;
        }

        public void setIPv6Support(boolean s)
        {
            ipv6Support = s;
        }

        public boolean isIPv6Supported()
        {
            return ipv6Support;
        }

        public void setCheckSupport(boolean s)
        {
            checkSupport = s;
        }

        public boolean isCheckSupported()
        {
            return checkSupport;
        }

        public void setTlsCiphers(ArrayList<Integer> a)
        {
            tlsCiphers = a;
        }

        public ArrayList<Integer> getTlsCiphers()
        {
            return tlsCiphers;
        }

        public void setTlsSuiteCiphers(ArrayList<Integer> a)
        {
            tlsSuiteCiphers = a;
        }

        public ArrayList<Integer> getTlsSuiteCiphers()
        {
            return tlsSuiteCiphers;
        }

        public void setDataCiphers(ArrayList<Integer> a)
        {
            dataCiphers = a;
        }

        public ArrayList<Integer> getDataCiphers()
        {
            return dataCiphers;
        }

        public boolean hasTlsCipher(int c)
        {
            return tlsCiphers.contains(c);
        }

        public boolean hasTlsSuiteCipher(int c)
        {
            return tlsSuiteCiphers.contains(c);
        }

        public boolean hasDataCipher(int c)
        {
            return dataCiphers.contains(c);
        }
    }
}
