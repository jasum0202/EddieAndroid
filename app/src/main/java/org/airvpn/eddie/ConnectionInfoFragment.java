// <eddie_source_header>
// This file is part of Eddie/AirVPN software.
// Copyright (C)2014-2018 AirVPN (support@airvpn.org) / https://airvpn.org
//
// Eddie is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Eddie is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Eddie. If not, see <http://www.gnu.org/licenses/>.
// </eddie_source_header>
//
// 3 October 2018 - author: ProMIND - initial release. (a tribute to the 1859 Perugia uprising occurred on 20 June 1859 and in memory of those brave inhabitants who fought for the liberty of Perugia)

package org.airvpn.eddie;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.w3c.dom.Document;

import java.util.HashMap;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

public class ConnectionInfoFragment extends Fragment implements NetworkStatusListener, EddieEventListener
{
    private final int FRAGMENT_ID = 5004;

    private SupportTools supportTools = null;
    private static VPNManager vpnManager = null;
    private NetworkStatusReceiver networkStatusReceiver = null;
    private EddieEvent eddieEvent = null;

    private Button btnDisconnect = null, btnPauseConnection = null, btnResumeConnection = null, btnLog = null;
    private TextView txtVpnStatus = null, txtNetworkStatus = null;
    private TextView txtCdConnectionType = null, txtCdUserProfile = null, txtCdUser = null, txtCdServerName = null;
    private TextView txtCdServerHost = null, txtCdServerPort = null, txtCdServerProto = null, txtCdServerIp = null;
    private TextView txtCdVpnIp4 = null, txtCdVpnIp6 = null, txtCdGwIp4 = null, txtCdGwIp6 = null, txtCdTunnelName = null;
    private TextView txtCdCipherName = null, txtCdDigest = null;
    private TextView txtStatsInTotal = null, txtStatsInRate = null;
    private TextView txtStatsMaxInRate = null, txtSessionTime = null, txtTotalTime = null;
    private TextView txtStatsOutTotal = null, txtStatsOutRate = null, txtStatsMaxOutRate = null;
    private LinearLayout llVpnNotConnected = null, llVpnInfoLayout = null;
    private LinearLayout llConnectionData = null, llVpnStats = null;

    private Timer vpnStatTimer = null;
    private OpenVPNTransportStats vpnStats = null;
    private boolean isVisible = false;

    public void setVpnManager(VPNManager v)
    {
        vpnManager = v;
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        supportTools = new SupportTools(getActivity());

        networkStatusReceiver = new NetworkStatusReceiver(getContext());
        networkStatusReceiver.addListener(this);

        eddieEvent = new EddieEvent();
        eddieEvent.addListener(this);
    }

    @Override
    public void onStart()
    {
        super.onStart();

        isVisible = true;
    }

    @Override
    public void onStop()
    {
        super.onStop();

        isVisible = false;

        stopVPNStats();
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();

        networkStatusReceiver.removeListener(this);

        eddieEvent.removeListener(this);

        isVisible = false;

        stopVPNStats();
    }

    @Override
    public void onResume()
    {
        super.onResume();

        isVisible = true;

        if(VPN.getConnectionStatus() == VPN.Status.CONNECTED)
        {
            startVPNStats();

            updateConnectionData(VPN.getVpnConnectionData());
        }
        else
        {
            stopVPNStats();

            llConnectionData.setVisibility(View.GONE);
        }

        if(txtNetworkStatus != null)
        {
            if(NetworkStatusReceiver.getNetworkStatus() == NetworkStatusReceiver.Status.CONNECTED)
                txtNetworkStatus.setText(String.format(Locale.getDefault(), getResources().getString(R.string.conn_status_connected), NetworkStatusReceiver.getNetworkDescription()));
            else
                txtNetworkStatus.setText(getResources().getString(R.string.conn_status_not_available));
        }

        if(VPN.getConnectionStatus() == VPN.Status.CONNECTED || VPN.getConnectionStatus() == VPN.Status.CONNECTING)
        {
            if(btnPauseConnection != null)
                btnPauseConnection.setVisibility(View.VISIBLE);

            if(btnResumeConnection != null)
                btnResumeConnection.setVisibility(View.GONE);
        }

        if(VPN.getConnectionStatus() == VPN.Status.PAUSED_BY_USER || VPN.getConnectionStatus() == VPN.Status.PAUSED_BY_SYSTEM)
        {
            if(btnPauseConnection != null)
                btnPauseConnection.setVisibility(View.GONE);

            if(btnResumeConnection != null)
                btnResumeConnection.setVisibility(View.VISIBLE);
        }

        if(NetworkStatusReceiver.getNetworkStatus() == NetworkStatusReceiver.Status.CONNECTED)
        {
            if(btnPauseConnection != null)
                SupportTools.enableButton(btnPauseConnection, true);

            if(btnResumeConnection != null)
                SupportTools.enableButton(btnResumeConnection, true);
        }
        else
        {
            if(btnPauseConnection != null)
                SupportTools.enableButton(btnPauseConnection, false);

            if(btnResumeConnection != null)
                SupportTools.enableButton(btnResumeConnection, false);
        }
    }

    @Override
    public void onPause()
    {
        super.onPause();

        isVisible = false;

        stopVPNStats();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View fragmentView = inflater.inflate(R.layout.fragment_connection_info_layout, container, false);

        if(fragmentView != null)
        {
            btnDisconnect = (Button)fragmentView.findViewById(R.id.disconnect_btn);
            btnPauseConnection = (Button)fragmentView.findViewById(R.id.pause_connection_btn);
            btnResumeConnection = (Button)fragmentView.findViewById(R.id.resume_connection_btn);
            btnLog = (Button)fragmentView.findViewById(R.id.log_btn);

            btnDisconnect.setOnClickListener(new View.OnClickListener()
            {
                public void onClick(View arg0)
                {
                    final Runnable uiRunnable = new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            stopConnection();
                        }
                    };

                    supportTools.runOnUiActivity(getActivity(), uiRunnable);
                }
            });

            btnPauseConnection.setOnClickListener(new View.OnClickListener()
            {
                public void onClick(View arg0)
                {
                    final Runnable uiRunnable = new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            pauseConnection();

                            stopVPNStats();
                        }
                    };

                    supportTools.runOnUiActivity(getActivity(), uiRunnable);
                }
            });

            btnResumeConnection.setOnClickListener(new View.OnClickListener()
            {
                public void onClick(View arg0)
                {
                    resumeConnection();

                    startVPNStats();
                }
            });

            btnLog.setOnClickListener(new View.OnClickListener()
            {
                public void onClick(View arg0)
                {
                    Intent logActivityIntent = new Intent(getContext(), LogActivity.class);

                    logActivityIntent.putExtra("ViewMode", LogActivity.MODE_LISTVIEW);

                    startActivity(logActivityIntent);
                }
            });

            llVpnNotConnected = (LinearLayout)fragmentView.findViewById(R.id.vpn_not_connected_layout);

            txtVpnStatus = (TextView)fragmentView.findViewById(R.id.vpn_connection_status);
            txtVpnStatus.setText(getResources().getString(VPN.descriptionResource(VPN.getConnectionStatus())));

            txtNetworkStatus = (TextView)fragmentView.findViewById(R.id.network_connection_status);

            if(NetworkStatusReceiver.getNetworkStatus() == NetworkStatusReceiver.Status.CONNECTED)
                txtNetworkStatus.setText(String.format(Locale.getDefault(), getResources().getString(R.string.conn_status_connected), NetworkStatusReceiver.getNetworkDescription()));
            else
                txtNetworkStatus.setText(getResources().getString(R.string.conn_status_not_available));

            llVpnInfoLayout = (LinearLayout)fragmentView.findViewById(R.id.vpn_info_layout);

            llConnectionData = (LinearLayout)fragmentView.findViewById(R.id.connection_data_layout);
            txtCdConnectionType = (TextView)fragmentView.findViewById(R.id.cd_connection_type);
            txtCdUserProfile = (TextView)fragmentView.findViewById(R.id.cd_user_profile);
            txtCdUser = (TextView)fragmentView.findViewById(R.id.cd_user);
            txtCdServerName = (TextView)fragmentView.findViewById(R.id.cd_server_name);
            txtCdServerHost = (TextView)fragmentView.findViewById(R.id.cd_server_host);
            txtCdServerPort = (TextView)fragmentView.findViewById(R.id.cd_server_port);
            txtCdServerProto = (TextView)fragmentView.findViewById(R.id.cd_server_proto);
            txtCdServerIp = (TextView)fragmentView.findViewById(R.id.cd_server_ip);
            txtCdVpnIp4 = (TextView)fragmentView.findViewById(R.id.cd_vpn_ip4);
            txtCdVpnIp6 = (TextView)fragmentView.findViewById(R.id.cd_vpn_ip6);
            txtCdGwIp4 = (TextView)fragmentView.findViewById(R.id.cd_gw_ip4);
            txtCdGwIp6 = (TextView)fragmentView.findViewById(R.id.cd_gw_ip6);
            txtCdTunnelName = (TextView)fragmentView.findViewById(R.id.cd_tunnel_name);
            txtCdCipherName = (TextView)fragmentView.findViewById(R.id.cd_cipher_name);
            txtCdDigest = (TextView)fragmentView.findViewById(R.id.cd_digest);

            llVpnStats = (LinearLayout)fragmentView.findViewById(R.id.vpn_stats_layout);

            txtStatsInTotal = (TextView)fragmentView.findViewById(R.id.stats_in_total);
            txtStatsInRate = (TextView)fragmentView.findViewById(R.id.stats_in_rate);
            txtStatsMaxInRate = (TextView)fragmentView.findViewById(R.id.stats_max_in_rate);
            txtStatsOutTotal = (TextView)fragmentView.findViewById(R.id.stats_out_total);
            txtStatsOutRate = (TextView)fragmentView.findViewById(R.id.stats_out_rate);
            txtStatsMaxOutRate = (TextView)fragmentView.findViewById(R.id.stats_max_out_rate);
            txtSessionTime = (TextView)fragmentView.findViewById(R.id.stats_session_time);
            txtTotalTime = (TextView)fragmentView.findViewById(R.id.stats_total_time);

            VPN.Status status = VPN.getConnectionStatus();

            switch(status)
            {
                case CONNECTING:
                case CONNECTED:
                case PAUSED_BY_USER:
                case PAUSED_BY_SYSTEM:
                case LOCKED:
                {
                    showConnectionStatus(getResources().getString(VPN.descriptionResource(status)));
                }
                break;

                case DISCONNECTING:
                case NOT_CONNECTED:
                case CONNECTION_REVOKED_BY_SYSTEM:
                case UNKNOWN:
                {
                    hideConnectionStatus();
                }
                break;

                default:
                {
                    hideConnectionStatus();
                }
                break;
            }
        }

        return fragmentView;
    }

    private void stopConnection()
    {
        if(!supportTools.confirmationDialog(R.string.conn_confirm_disconnection))
            return;

        try
        {
            vpnManager.stop();
        }
        catch(Exception e)
        {
            EddieLogger.error("ConnectionInfoFragment.stopConnection() exception: %s", e.getMessage());
        }
    }

    private void pauseConnection()
    {
        if(VPN.getConnectionStatus() == VPN.Status.CONNECTED && btnPauseConnection.isEnabled())
        {
            vpnManager.pause();

            btnPauseConnection.setVisibility(View.GONE);

            btnResumeConnection.setVisibility(View.VISIBLE);
        }
    }

    private void resumeConnection()
    {
        if((VPN.getConnectionStatus() == VPN.Status.PAUSED_BY_USER || VPN.getConnectionStatus() == VPN.Status.PAUSED_BY_SYSTEM) && btnResumeConnection.isEnabled())
        {
            vpnManager.resume();

            btnResumeConnection.setVisibility(View.GONE);

            btnPauseConnection.setVisibility(View.VISIBLE);
        }
    }

    public void showConnectionStatus(String vpnStatusDescription)
    {
        if(llVpnNotConnected != null)
            llVpnNotConnected.setVisibility(View.GONE);

        if(llVpnInfoLayout != null)
            llVpnInfoLayout.setVisibility(View.VISIBLE);

        if(llVpnStats != null)
            llVpnStats.setVisibility(View.VISIBLE);

        if(llConnectionData != null)
            llConnectionData.setVisibility(View.VISIBLE);

        if(supportTools.isNetworkConnectionActive() && (VPN.getConnectionStatus() == VPN.Status.CONNECTED || VPN.getConnectionStatus() == VPN.Status.CONNECTING))
        {
            if(btnPauseConnection != null)
                btnPauseConnection.setVisibility(View.VISIBLE);

            if(btnResumeConnection != null)
                btnResumeConnection.setVisibility(View.GONE);
        }

        if(supportTools.isNetworkConnectionActive() && (VPN.getConnectionStatus() == VPN.Status.PAUSED_BY_USER || VPN.getConnectionStatus() == VPN.Status.PAUSED_BY_SYSTEM))
        {
            if(btnPauseConnection != null)
                btnPauseConnection.setVisibility(View.GONE);

            if(btnResumeConnection != null)
                btnResumeConnection.setVisibility(View.VISIBLE);
        }

        if(txtVpnStatus != null)
            txtVpnStatus.setText(vpnStatusDescription);

        updateConnectionData(VPN.getVpnConnectionData());

        showVpnStats();
    }

    public void hideConnectionStatus()
    {
        if(llVpnNotConnected != null)
            llVpnNotConnected.setVisibility(View.VISIBLE);

        if(llVpnInfoLayout != null)
            llVpnInfoLayout.setVisibility(View.GONE);

        if(llVpnStats != null)
            llVpnStats.setVisibility(View.GONE);

        if(llConnectionData != null)
            llConnectionData.setVisibility(View.GONE);
    }

    public void updateConnectionData(OpenVPNConnectionData connectionData)
    {
        if(llConnectionData == null)
            return;

        if(connectionData != null)
        {
            HashMap<String, String> profileInfo = VPN.getProfileInfo();

            llConnectionData.setVisibility(View.VISIBLE);

            txtCdConnectionType.setText(VPN.getConnectionModeDescription());

            if(VPN.getConnectionMode() != VPN.ConnectionMode.OPENVPN_PROFILE)
                txtCdUserProfile.setText(VPN.getUserProfileDescription());
            else
            {
                if(profileInfo != null)
                    txtCdUserProfile.setText(profileInfo.get("name"));
                else
                    txtCdUserProfile.setText("");
            }

            if(!VPN.getUserName().isEmpty())
                txtCdUser.setText(VPN.getUserName());
            else
                txtCdUser.setText(connectionData.user);

            if(profileInfo != null)
                txtCdServerName.setText(profileInfo.get("description"));
            else
                txtCdServerName.setText("");

            txtCdServerHost.setText(connectionData.serverHost);
            txtCdServerPort.setText(connectionData.serverPort);
            txtCdServerProto.setText(connectionData.serverProto);
            txtCdServerIp.setText(connectionData.serverIp);
            txtCdVpnIp4.setText(connectionData.vpnIp4);
            txtCdVpnIp6.setText(connectionData.vpnIp6);
            txtCdGwIp4.setText(connectionData.gw4);
            txtCdGwIp6.setText(connectionData.gw6);
            txtCdTunnelName.setText(connectionData.tunName);
            txtCdCipherName.setText(VPN.getCipherName());
            txtCdDigest.setText(VPN.getDigest());
        }
        else
            llConnectionData.setVisibility(View.GONE);
    }

    public void enableDisconnectButton(boolean enabled)
    {
        if(btnDisconnect != null)
            SupportTools.enableButton(btnDisconnect, enabled);
    }

    public void startVPNStats()
    {
        int updateInterval = VPNService.STATS_UPDATE_INTERVAL_SECONDS * 1000;

        if(vpnStatTimer != null)
            vpnStatTimer.cancel();

        if(llVpnStats != null)
            llVpnStats.setVisibility(View.VISIBLE);

        vpnStatTimer = new Timer();

        vpnStatTimer.schedule(new TimerTask()
        {
            @Override
            public void run()
            {
                 if(isVisible)
                    updateVPNStats();
            }
        }, updateInterval, updateInterval);
    }

    public void stopVPNStats()
    {
        if(llVpnStats != null)
            llVpnStats.setVisibility(View.GONE);

        if(vpnStatTimer != null)
            vpnStatTimer.cancel();

        vpnStatTimer = null;
    }

    private void updateVPNStats()
    {
        if(getActivity() != null)
        {
            Runnable runnable = new Runnable()
            {
                @Override
                public void run()
                {
                    showVpnStats();
                }
            };

            supportTools.runOnUiActivity(getActivity(), runnable);
        }
    }

    private void showVpnStats()
    {
        if(llVpnStats == null)
            return;

        llVpnStats.setVisibility(View.VISIBLE);

        vpnStats = VPN.getVpnTransportStats();

        if(txtStatsInTotal != null && vpnStats != null)
            txtStatsInTotal.setText(supportTools.formatDataVolume(vpnStats.bytesIn));

        if(txtStatsInRate != null)
            txtStatsInRate.setText(supportTools.formatTransferRate(VPN.getInRate()));

        if(txtStatsMaxInRate != null)
            txtStatsMaxInRate.setText(supportTools.formatTransferRate(VPN.getMaxInRate()));

        if(txtStatsOutTotal != null && vpnStats != null)
            txtStatsOutTotal.setText(supportTools.formatDataVolume(vpnStats.bytesOut));

        if(txtStatsOutRate != null)
            txtStatsOutRate.setText(supportTools.formatTransferRate(VPN.getOutRate()));

        if(txtStatsMaxOutRate != null)
            txtStatsMaxOutRate.setText(supportTools.formatTransferRate(VPN.getMaxOutRate()));

        if(txtSessionTime != null)
            txtSessionTime.setText(VPN.getSessionTime());

        if(txtTotalTime != null)
            txtTotalTime.setText(VPN.getTotalConnectionTime());
    }

    // Eddie events

    public void onVpnConnectionDataChanged(final OpenVPNConnectionData connectionData)
    {
        Runnable runnable = new Runnable()
        {
            @Override
            public void run()
            {
                updateConnectionData(connectionData);
            }
        };

        supportTools.runOnUiActivity(getActivity(), runnable);
    }

    public void onVpnStatusChanged(final VPN.Status status, final String message)
    {
        Runnable runnable = new Runnable()
        {
            @Override
            public void run()
            {
                String vpnStatusDescription = "";

                enableDisconnectButton((status == VPN.Status.CONNECTING) || (status == VPN.Status.CONNECTED) || (status == VPN.Status.PAUSED_BY_USER) || (status == VPN.Status.PAUSED_BY_SYSTEM) || (status == VPN.Status.LOCKED));

                if(!vpnManager.isReady())
                    vpnStatusDescription = getResources().getString(R.string.conn_status_initialize);
                else
                    vpnStatusDescription = message;

                switch(status)
                {
                    case CONNECTED:
                    {
                        startVPNStats();

                        showConnectionStatus(vpnStatusDescription);
                    }
                    break;

                    case PAUSED_BY_USER:
                    case PAUSED_BY_SYSTEM:
                    case LOCKED:
                    {
                        stopVPNStats();

                        showConnectionStatus(vpnStatusDescription);
                    }
                    break;

                    case NOT_CONNECTED:
                    case CONNECTION_REVOKED_BY_SYSTEM:
                    {
                        stopVPNStats();

                        hideConnectionStatus();
                    }
                    break;

                    default:
                    {
                        stopVPNStats();

                        hideConnectionStatus();
                    }
                    break;
                }
            }
        };

        supportTools.runOnUiActivity(getActivity(), runnable);
    }

    public void onVpnAuthFailed(final OpenVPNEvent oe)
    {
        Runnable runnable = new Runnable()
        {
            @Override
            public void run()
            {
                enableDisconnectButton(false);

                showConnectionStatus(getResources().getString(R.string.conn_auth_failed));
            }
        };

        supportTools.runOnUiActivity(getActivity(), runnable);
    }

    public void onVpnError(final OpenVPNEvent oe)
    {
    }

    public void onMasterPasswordChanged()
    {
    }

    public void onAirVPNLogin()
    {
    }

    public void onAirVPNLoginFailed()
    {
    }

    public void onAirVPNLogout()
    {
    }

    public void onAirVPNUserDataChanged()
    {
    }

    public void onAirVPNUserProfileReceived(Document document)
    {
    }

    public void onAirVPNUserProfileDownloadError()
    {
    }

    public void onAirVPNUserProfileChanged()
    {
    }

    public void onAirVPNManifestReceived(Document document)
    {
    }

    public void onAirVPNManifestDownloadError()
    {
    }

    public void onAirVPNManifestChanged()
    {
    }

    public void onAirVPNIgnoredDocumentRequest()
    {
    }

    public void onCancelConnection()
    {
    }

    // NetworkStatusReceiver

    public void onNetworkStatusNotAvailable()
    {
        if(txtNetworkStatus != null)
            txtNetworkStatus.setText(getResources().getString(R.string.conn_status_not_available));

        if(btnPauseConnection != null)
            SupportTools.enableButton(btnPauseConnection, false);

        if(btnResumeConnection != null)
            SupportTools.enableButton(btnResumeConnection, false);
    }

    public void onNetworkStatusConnected()
    {
        if(txtNetworkStatus != null)
            txtNetworkStatus.setText(String.format(Locale.getDefault(), getResources().getString(R.string.conn_status_connected), NetworkStatusReceiver.getNetworkDescription()));

        if(btnPauseConnection != null)
            SupportTools.enableButton(btnPauseConnection, true);

        if(btnResumeConnection != null)
            SupportTools.enableButton(btnResumeConnection, true);
    }

    public void onNetworkStatusIsConnecting()
    {
        if(txtNetworkStatus != null)
            txtNetworkStatus.setText(getResources().getString(R.string.conn_status_not_available));

        if(btnPauseConnection != null)
            SupportTools.enableButton(btnPauseConnection, false);

        if(btnResumeConnection != null)
            SupportTools.enableButton(btnResumeConnection, false);
    }

    public void onNetworkStatusIsDisonnecting()
    {
        if(txtNetworkStatus != null)
            txtNetworkStatus.setText(getResources().getString(R.string.conn_status_not_available));

        if(btnPauseConnection != null)
            SupportTools.enableButton(btnPauseConnection, false);

        if(btnResumeConnection != null)
            SupportTools.enableButton(btnResumeConnection, false);
    }

    public void onNetworkStatusSuspended()
    {
        if(txtNetworkStatus != null)
            txtNetworkStatus.setText(getResources().getString(R.string.conn_status_not_available));

        if(btnPauseConnection != null)
            SupportTools.enableButton(btnPauseConnection, false);

        if(btnResumeConnection != null)
            SupportTools.enableButton(btnResumeConnection, false);
    }

    public void onNetworkStatusNotConnected()
    {
        if(txtNetworkStatus != null)
            txtNetworkStatus.setText(getResources().getString(R.string.conn_status_disconnected));

        if(btnPauseConnection != null)
            SupportTools.enableButton(btnPauseConnection, false);

        if(btnResumeConnection != null)
            SupportTools.enableButton(btnResumeConnection, false);
    }

    public void onNetworkTypeChanged()
    {
        if(txtNetworkStatus != null)
            txtNetworkStatus.setText(getResources().getString(R.string.conn_status_disconnected));

        if(btnPauseConnection != null)
            SupportTools.enableButton(btnPauseConnection, false);

        if(btnResumeConnection != null)
            SupportTools.enableButton(btnResumeConnection, false);
    }
}
