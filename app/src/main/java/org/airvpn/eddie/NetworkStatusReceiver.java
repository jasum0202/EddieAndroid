// <eddie_source_header>
// This file is part of Eddie/AirVPN software.
// Copyright (C)2014-2018 AirVPN (support@airvpn.org) / https://airvpn.org
//
// Eddie is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Eddie is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Eddie. If not, see <http://www.gnu.org/licenses/>.
// </eddie_source_header>
//
// 4 September 2018 - author: ProMIND - initial release.

package org.airvpn.eddie;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import java.util.HashSet;

public class NetworkStatusReceiver extends BroadcastReceiver
{
	public enum Status
    {
        UNKNOWN,
        NOT_AVAILABLE,
        CONNECTED,
        IS_CONNECTING,
        IS_DISCONNECTING,
        SUSPENDED,
        NOT_CONNECTED
    }

    public enum NetworkType
    {
        UNKNOWN,
        MOBILE,
        WIFI,
        ETHERNET,
        BLUETOOTH
    }

	private static Status networkStatus = Status.UNKNOWN;
	private static Status currentNetworkStatus = Status.UNKNOWN;
    private NetworkInfo networkInfo = null;
    private ConnectivityManager manager = null;
    private static NetworkType networkType = NetworkType.UNKNOWN;
    private static NetworkType currentNetworkType = NetworkType.UNKNOWN;
    private static String networkDescription = "";
    private static String networkTypeName = "";

    private static HashSet<NetworkStatusListener> registeredListeners = null;

    public NetworkStatusReceiver()
    {
        this(null);
    }

    public NetworkStatusReceiver(Context context)
    {
        if(registeredListeners == null)
            registeredListeners = new HashSet<NetworkStatusListener>();

        if(context == null)
            return;

        manager = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);

        try
        {
            networkInfo = manager.getActiveNetworkInfo();

            networkType = getConnectionType(manager);

            networkDescription = getFullNetworkDescription(networkInfo);
            networkTypeName = networkInfo.getTypeName();
        }
        catch(NullPointerException e)
        {
            networkType = NetworkType.UNKNOWN;

            networkDescription = "";
            networkTypeName = "";

            networkInfo = null;
        }

        if(networkInfo != null)
        {
            NetworkInfo.State connectionStatus = networkInfo.getState();

            switch(connectionStatus)
            {
                case CONNECTED:
                {
                    networkStatus = Status.CONNECTED;
                }
                break;

                case DISCONNECTED:
                {
                    networkStatus = Status.NOT_CONNECTED;
                }
                break;

                case CONNECTING:
                {
                    networkStatus = Status.IS_CONNECTING;
                }
                break;

                case DISCONNECTING:
                {
                    networkStatus = Status.IS_DISCONNECTING;
                }
                break;

                case SUSPENDED:
                {
                    networkStatus = Status.SUSPENDED;
                }
                break;

                case UNKNOWN:
                {
                    networkStatus = Status.UNKNOWN;
                }
                break;

                default:
                {
                    networkStatus = Status.NOT_AVAILABLE;
                }
                break;
            }
        }
    }

    public boolean addListener(NetworkStatusListener listener)
    {
        if(listener == null || registeredListeners == null)
            return false;

        if(registeredListeners.contains(listener))
            return false;

        registeredListeners.add(listener);

        notifyState(listener);

        return true;
    }

    public boolean removeListener(NetworkStatusListener listener)
    {
        if(listener == null || registeredListeners == null)
            return false;

        if(!registeredListeners.contains(listener))
            return false;

        registeredListeners.remove(listener);

        return true;
    }

    @Override
    public void onReceive(Context context, Intent intent)
    {
        if(context == null || intent == null)
            return;

        manager = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);

        if(manager == null)
            return;

        networkType = getConnectionType(manager);

        networkInfo = manager.getActiveNetworkInfo();

        if(networkInfo != null)
        {
            NetworkInfo.State connectionStatus = networkInfo.getState();

            networkDescription = getFullNetworkDescription(networkInfo);
            networkTypeName = networkInfo.getTypeName();

            switch(connectionStatus)
            {
                case CONNECTED:
                {
                    networkStatus = Status.CONNECTED;
                }
                break;

                case DISCONNECTED:
                {
                    networkStatus = Status.NOT_CONNECTED;
                }
                break;

                case CONNECTING:
                {
                    networkStatus = Status.IS_CONNECTING;
                }
                break;

                case DISCONNECTING:
                {
                    networkStatus = Status.IS_DISCONNECTING;
                }
                break;

                case SUSPENDED:
                {
                    networkStatus = Status.SUSPENDED;
                }
                break;

                case UNKNOWN:
                {
                    networkStatus = Status.UNKNOWN;
                }
                break;

                default:
                {
                    networkStatus = Status.UNKNOWN;
                }
                break;
            }
        }
        else
        {
            networkStatus = Status.NOT_AVAILABLE;
            networkDescription = "";
            networkTypeName = "";
        }

        if(networkType != currentNetworkType || networkStatus != currentNetworkStatus)
            notifyStateToAll();

        currentNetworkType = networkType;
        currentNetworkStatus = networkStatus;
    }

    private NetworkType getConnectionType(ConnectivityManager connectivityManager)
    {
        NetworkType connectionType = NetworkType.UNKNOWN;

        if(connectivityManager == null)
            return NetworkType.UNKNOWN;

        NetworkInfo activeNetwork = connectivityManager.getActiveNetworkInfo();

        if(activeNetwork != null)
        {
            switch(activeNetwork.getType())
            {
                case ConnectivityManager.TYPE_MOBILE:
                case ConnectivityManager.TYPE_MOBILE_DUN:
                {
                    connectionType = NetworkType.MOBILE;
                }
                break;

                case ConnectivityManager.TYPE_WIFI:
                {
                    connectionType = NetworkType.WIFI;
                }
                break;

                case ConnectivityManager.TYPE_ETHERNET:
                {
                    connectionType = NetworkType.ETHERNET;
                }
                break;

                case ConnectivityManager.TYPE_BLUETOOTH:
                {
                    connectionType = NetworkType.BLUETOOTH;
                }
                break;

                default:
                {
                    connectionType = NetworkType.UNKNOWN;
                }
                break;
            }
        }
        else
            connectionType = NetworkType.UNKNOWN;

        return connectionType;
    }

    private void notifyStateToAll()
    {
        for(NetworkStatusListener listener : registeredListeners)
            notifyState(listener);
    }

    private void notifyState(NetworkStatusListener listener)
    {
        if(listener == null)
            return;

        if(currentNetworkType != networkType)
            listener.onNetworkTypeChanged();

        switch(networkStatus)
        {
            case NOT_AVAILABLE:
            {
                listener.onNetworkStatusNotAvailable();
            }
            break;

            case CONNECTED:
            {
                listener.onNetworkStatusConnected();
            }
            break;

            case IS_CONNECTING:
            {
                listener.onNetworkStatusIsConnecting();
            }
            break;

            case IS_DISCONNECTING:
            {
                listener.onNetworkStatusIsDisonnecting();
            }
            break;

            case SUSPENDED:
            {
                listener.onNetworkStatusSuspended();
            }
            break;

            case NOT_CONNECTED:
            {
                listener.onNetworkStatusNotConnected();
            }
            break;

            default:
            {
                listener.onNetworkStatusNotAvailable();
            }
            break;
        }
    }

    private String getFullNetworkDescription(NetworkInfo networkInfo)
    {
        if(networkInfo == null)
            return "";

        String description = networkInfo.getTypeName();
        String extrainfo = networkInfo.getExtraInfo();
        String subtype = networkInfo.getSubtypeName();

        if(extrainfo != null && !extrainfo.isEmpty())
            description += " " + extrainfo;

        if(subtype != null && !subtype.isEmpty())
            description += " (" + subtype + ")";

        return description;
    }

    public static Status getNetworkStatus()
    {
        return networkStatus;
    }

    public static boolean isNetworkConnected()
    {
        return (networkStatus == Status.CONNECTED);
    }

    public static NetworkType getNetworkType()
    {
        return networkType;
    }

    public static String getNetworkTypeName()
    {
        return networkTypeName;
    }

    public static String getNetworkDescription()
    {
        return networkDescription;
    }
}
