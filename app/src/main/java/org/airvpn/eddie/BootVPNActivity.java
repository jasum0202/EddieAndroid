// <eddie_source_header>
// This file is part of Eddie/AirVPN software.
// Copyright (C)2014-2018 AirVPN (support@airvpn.org) / https://airvpn.org
//
// Eddie is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Eddie is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Eddie. If not, see <http://www.gnu.org/licenses/>.
// </eddie_source_header>
//
// 13 January 2019 - author: ProMIND - initial release. (a tribute to the 1859 Perugia uprising occurred on 20 June 1859 and in memory of those brave inhabitants who fought for the liberty of Perugia)

package org.airvpn.eddie;

import android.app.Activity;
import android.os.Bundle;
import android.widget.Toast;

import java.util.HashMap;
import java.util.Locale;

public class BootVPNActivity extends Activity
{
    private VPNManager vpnManager = null;
    private MainActivity mainActivity = null;
    private SettingsManager settingsManager = null;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        settingsManager = new SettingsManager(this);

        if(!settingsManager.isSystemRestoreLastProfile())
        {
            EddieLogger.debug("BootVPNActivity.onCreate(): SystemRestoreLastProfile option is disabled");

            finish();

            return;
        }

        if(!settingsManager.isSystemLastProfileIsConnected())
        {
            EddieLogger.debug("BootVPNActivity.onCreate(): SystemLastProfileIsConnected option is false");

            finish();

            return;
        }

        EddieLogger.info("Trying to restore last connected profile before boot");

        vpnManager = new VPNManager(this);

        mainActivity = new MainActivity();
        mainActivity.setVpnManager(vpnManager);

        String lastProfile = settingsManager.getLastOpenVPNProfile();

        HashMap<String, String> profileInfo = settingsManager.getSystemLastProfileInfo();

        if(profileInfo == null)
            profileInfo = new HashMap<String, String>();

        profileInfo.put("mode", String.format(Locale.getDefault(), "%d", VPN.connectionModeToInt(VPN.ConnectionMode.BOOT_CONNECT)));
        profileInfo.put("name", "boot_connect");
        profileInfo.put("profile", "boot_connect");
        profileInfo.put("status", "ok");
        profileInfo.put("description", String.format("%s (boot)", profileInfo.get("server")));

        for(int i=0; i < 2; i++)
            Toast.makeText(this, String.format(getResources().getString(R.string.boot_last_connected_openvpn_profile), profileInfo.get("server")), Toast.LENGTH_LONG).show();

        VPN.setProfileInfo(profileInfo);

        VPN.setConnectionMode(VPN.ConnectionMode.BOOT_CONNECT);

        VPN.setConnectionModeDescription(getResources().getString(R.string.conn_type_boot_connect));

        VPN.setUserProfileDescription("");

        VPN.setUserName("");

        if(lastProfile.isEmpty())
        {
            EddieLogger.debug("BootVPNActivity.onCreate(): lastProfile is empty");

            finish();

            return;
        }

        vpnManager.clearProfile();

        vpnManager.setProfile(lastProfile);

        String profileString = settingsManager.getOvpn3CustomDirectives().trim();

        if(profileString.length() > 0)
            vpnManager.addProfileString(profileString);

        vpnManager.start();

        finish();
    }
}
