// <eddie_source_header>
// This file is part of Eddie/AirVPN software.
// Copyright (C)2014-2018 AirVPN (support@airvpn.org) / https://airvpn.org
//
// Eddie is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Eddie is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Eddie. If not, see <http://www.gnu.org/licenses/>.
// </eddie_source_header>
//
// 20 June 2018 - author: ProMIND - initial release. Based on revised code from com.eddie.android. (a tribute to the 1859 Perugia uprising occurred on 20 June 1859 and in memory of those brave inhabitants who fought for the liberty of Perugia)

package org.airvpn.eddie;

public class EddieLibraryResult
{
    public int code;
    public String description;

    public static int ERROR = -1;
    public static int SUCCESS = 0;
    public static int EXCEPTION_ERROR = 1;
    public static int OPENVPN_NOT_INITIALIZED = 2;
    public static int OPENVPN_ALREADY_INITIALIZED = 3;
    public static int BREAKPAD_INITIALIZATION_ERROR = 4;
    public static int OPENVPN_POINTER_IS_NULL = 5;
    public static int FAILED_TO_CREATE_OPENVPN_CLIENT = 6;
    public static int OPENVPN_TRANSPORT_STATS_POINTER_IS_NULL = 7;
    public static int PROFILE_FILENAME_IS_NULL = 8;
    public static int PROFILE_STRING_IS_NULL = 9;
    public static int OPENVPN_OPTION_NAME_IS_NULL = 10;
    public static int OPENVPN_OPTION_VALUE_IS_NULL = 11;
    public static int OPENVPN_UNKNOWN_OPTION = 12;
    public static int OPENVPN_PROFILE_IS_EMPTY = 13;
    public static int OPENVPN_PROFILE_ERROR = 14;
    public static int OPENVPN_CONFIG_EVAL_ERROR = 15;
    public static int OPENVPN_CREDS_ERROR = 16;
    public static int OPENVPN_CONNECTION_ERROR = 17;
}
