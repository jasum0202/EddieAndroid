// <eddie_source_header>
// This file is part of Eddie/AirVPN software.
// Copyright (C)2014-2018 AirVPN (support@airvpn.org) / https://airvpn.org
//
// Eddie is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Eddie is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Eddie. If not, see <http://www.gnu.org/licenses/>.
// </eddie_source_header>
//
// 20 June 2018 - author: ProMIND - initial release. Based on revised code from com.eddie.android. (a tribute to the 1859 Perugia uprising occurred on 20 June 1859 and in memory of those brave inhabitants who fought for the liberty of Perugia)

package org.airvpn.eddie;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.res.ResourcesCompat;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class PackageChooserActivity extends Activity
{
    public static final String PARAM_PACKAGES = "PACKAGES";
    public static final String CHOOSER_TITLE = "TITLE";

    private SupportTools supportTools = null;

    private ArrayList<ApplicationItem> applicationList = new ArrayList<ApplicationItem>();
    private ApplicationsListAdapter applicationListAdapter = null;

    private Typeface typeface = null;

    private ListView applicationsListView = null;
    private TextView txtTitle = null;
    private LinearLayout llProgressSpinner = null;

    private String title = "", paramPackages = "";

    private class ApplicationItem
    {
        private ApplicationInfo appInfo = null;
        private boolean selected = false;
        private Drawable icon = null;
        private String name = "";
        private String description = "";

        public ApplicationItem(Context context, ApplicationInfo info)
        {
            appInfo = info;
            description = appInfo.loadLabel(context.getPackageManager()).toString();
        }

        public ApplicationInfo getAppInfo()
        {
            return appInfo;
        }

        public void setAppInfo(ApplicationInfo a)
        {
            appInfo = a;
        }

        public boolean isSelected()
        {
            return selected;
        }

        public void setSelected(boolean s)
        {
            selected = s;
        }

        public Drawable getIcon()
        {
            return icon;
        }

        public void setIcon(Drawable i)
        {
            icon = i;
        }

        public String getName()
        {
            return name;
        }

        public void setName(String s)
        {
            name = s;
        }

        public String getDescription()
        {
            return description;
        }

        public void setDescription(String s)
        {
            description = s;
        }
    }

    private class ApplicationsListAdapter extends BaseAdapter
    {
        private Activity activity = null;
        private ArrayList<ApplicationItem> appItems = null;

        private class ListViewHolder
        {
            public ImageView itemIcon;
            public CheckBox itemCheckbox;
            public TextView itemName;
            public TextView itemDescription;
        }

        public ApplicationsListAdapter(Activity a, ArrayList<ApplicationItem> items)
        {
            activity = a;

            appItems = items;
        }

        public void dataSet(ArrayList<ApplicationItem> appList)
        {
            appItems = appList;

            notifyDataSetChanged();
        }

        @Override
        public int getCount()
        {
            int entries = 0;

            if(appItems != null)
                entries = appItems.size();

            return entries;
        }

        @Override
        public ApplicationItem getItem(int position)
        {
                return appItems.get(position);
        }

        @Override
        public long getItemId(int position)
        {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent)
        {
            ApplicationItem item = appItems.get(position);

            ListViewHolder listViewHolder = null;

            if(convertView != null)
                listViewHolder = (ListViewHolder)convertView.getTag();

            if(listViewHolder == null)
            {
                listViewHolder = new ListViewHolder();

                convertView = activity.getLayoutInflater().inflate(R.layout.package_chooser_item, null);

                listViewHolder.itemIcon = (ImageView)convertView.findViewById(R.id.packages_picker_item_icon);
                listViewHolder.itemCheckbox = (CheckBox)convertView.findViewById(R.id.packages_picker_item_selection);
                listViewHolder.itemDescription = (TextView)convertView.findViewById(R.id.packages_picker_item_description);
                listViewHolder.itemName = (TextView)convertView.findViewById(R.id.packages_picker_item_name);

                convertView.setTag(listViewHolder);
            }

            listViewHolder.itemIcon.setImageDrawable(item.getIcon());

            listViewHolder.itemCheckbox.setChecked(item.isSelected());
            listViewHolder.itemCheckbox.setClickable(false);
            listViewHolder.itemCheckbox.setSelected(false);

            listViewHolder.itemDescription.setText(item.getDescription());
            listViewHolder.itemDescription.setTypeface(typeface);

            listViewHolder.itemName.setText(item.getName());
            listViewHolder.itemName.setTypeface(typeface);

            return convertView;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        supportTools = new SupportTools(this);

        Bundle bundleExtras = getIntent().getExtras();

        title = bundleExtras.getString(CHOOSER_TITLE);
        paramPackages = bundleExtras.getString(PARAM_PACKAGES);

        typeface = ResourcesCompat.getFont(this, R.font.default_font);

        initUI();

        new ApplicationListLoader().execute();
    }

    @Override
    public void onBackPressed()
    {
        Intent resultIntent = new Intent(this, PackageChooserActivity.class);
        resultIntent.putExtra(PARAM_PACKAGES, getSelectedApplicationList());

        setResult(RESULT_OK, resultIntent);

        finish();
    }

    private void initUI()
    {
        setContentView(R.layout.package_chooser_activity_layout);

        txtTitle = (TextView)findViewById(R.id.chooser_title);
        applicationsListView = (ListView)findViewById(R.id.packages_picker_applications);
        llProgressSpinner = (LinearLayout)findViewById(R.id.llProgressSpinner);

        txtTitle.setText(title);
        txtTitle.setTypeface(typeface);

        applicationListAdapter = new ApplicationsListAdapter(this, applicationList);

        applicationsListView.setAdapter(applicationListAdapter);

        applicationListAdapter.notifyDataSetChanged();

        applicationsListView.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
           @Override
           public void onItemClick(AdapterView<?> parent, View view, int position, long id)
           {
               applicationList.get(position).setSelected(!applicationList.get(position).isSelected());

               if(applicationListAdapter != null)
                   applicationListAdapter.notifyDataSetChanged();
           }
        });
    }

    private String getSelectedApplicationList()
    {
        String selectedApplications = "";

        for(ApplicationItem app : applicationList)
        {
            if(app.isSelected())
            {
                if(selectedApplications.length() > 0)
                    selectedApplications += SettingsManager.DEFAULT_SPLIT_SEPARATOR;

                selectedApplications += app.getName();
            }
        }

        return selectedApplications;
    }

    private void loadApplications(String appList)
    {
        ArrayList<String> selectedPackages = new ArrayList<String>();
        String eddiePackage = getApplicationContext().getPackageName();

        if(appList != null)
        {
            String[] valArray = appList.split(SettingsManager.DEFAULT_SPLIT_SEPARATOR);

            for(String item : valArray)
                selectedPackages.add(item);
        }

        applicationList.clear();

        List<ApplicationInfo> applications = getPackageManager().getInstalledApplications(PackageManager.GET_META_DATA);

        for(ApplicationInfo app : applications)
        {
            ApplicationItem item = new ApplicationItem(this, app);

            if(!app.packageName.equals(eddiePackage))
            {
                item.setName(app.packageName);
                item.setSelected(selectedPackages.contains(app.packageName));

                item.setIcon(app.loadIcon(getPackageManager()));

                applicationList.add(item);
            }
        }

         Collections.sort(applicationList, new Comparator<ApplicationItem>()
         {
             public int compare(ApplicationItem a, ApplicationItem b)
             {
                 return a.getDescription().compareToIgnoreCase(b.getDescription());
             }
         });
    }

    private class ApplicationListLoader extends AsyncTask<Void, Void, Void>
    {
        @Override
        protected void onPreExecute()
        {
            llProgressSpinner.setVisibility(View.VISIBLE);
        }

        @Override
        protected Void doInBackground(Void... p)
        {
            Runnable runnable = new Runnable()
            {
                @Override
                public void run()
                {
                    loadApplications(paramPackages);

                    applicationListAdapter.dataSet(applicationList);

                    llProgressSpinner.setVisibility(View.GONE);
                }
            };

            supportTools.runOnUiActivity(PackageChooserActivity.this, runnable);

            return null;
        }

        @Override
        protected void onPostExecute(Void param)
        {
        }
    }
}
