// <eddie_source_header>
// This file is part of Eddie/AirVPN software.
// Copyright (C)2014-2018 AirVPN (support@airvpn.org) / https://airvpn.org
//
// Eddie is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Eddie is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Eddie. If not, see <http://www.gnu.org/licenses/>.
// </eddie_source_header>
//
// 20 June 2018 - author: ProMIND - initial release. Based on revised code from com.eddie.android. (a tribute to the 1859 Perugia uprising occurred on 20 June 1859 and in memory of those brave inhabitants who fought for the liberty of Perugia)

package org.airvpn.eddie;

import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.os.ParcelFileDescriptor;
import android.system.OsConstants;

import java.util.ArrayList;
import java.util.Locale;

public class VPNContext
{
    private VPNService.Builder vpnServiceBuilder = null;
    private ParcelFileDescriptor fileDescriptor = null;
    private SettingsManager settingsManager = null;
    private EddieLogger eddieLogger = null;

    private boolean blockIPv4 = false;
    private boolean blockIPv6 = false;
    private boolean customDNS = false;
    private boolean hasDNS = false;
    private boolean forceMTU = false;

    public VPNContext(VPNService vpnService)
    {
        settingsManager = new SettingsManager(vpnService);

        vpnServiceBuilder = vpnService.new Builder();

        vpnServiceBuilder.setConfigureIntent(vpnService.createConfigIntent());

        eddieLogger = new EddieLogger();

        eddieLogger.init(vpnService);

        initDNS();

        try
        {
            initMTU();
        }
        catch(Exception e)
        {
            EddieLogger.error("VPNContext(): Error while executing initMTU() Exception: %s", e);
        }

        try
        {
            initApplications();
        }
        catch(Exception e)
        {
            EddieLogger.error("VPNContext(): Error while executing initMTU() Exception: %s", e);
        }
    }

    protected void finalize() throws Throwable
    {
        try
        {
            cleanup();
        }
        catch(Exception e)
        {
            EddieLogger.error("VPNContext.finalize(): cleanup() Exception: %s", e);
        }
        finally
        {
            try
            {
                super.finalize();
            }
            catch(Exception e)
            {
                EddieLogger.error("VPNManager.finalize(): super() Exception: %s", e);
            }
        }
    }

    public VPNService.Builder getBuilder()
    {
        return vpnServiceBuilder;
    }

    private void initDNS()
    {
        if(!settingsManager.isSystemDNSOverrideEnable())
            return;

        if(settingsManager.isSystemDNSOverrideEnable())
        {
            ArrayList<String> dnsCustom = settingsManager.getSystemDNSCustomList();

            if(dnsCustom.size() == 0)
                return;

            customDNS = true;

            for(String dns : dnsCustom)
            {
                try
                {
                    doAddDNS(dns);
                }
                catch(Exception e)
                {
                    EddieLogger.error("VPNContext.addDNSServer(): Cannot add DNS %s - Exception: %s", dns, e);
                }
            }
        }
        else
            customDNS = false;
    }

    private void initMTU() throws Exception
    {
        String customMtu = settingsManager.getSystemCustomMTU().trim();

        if(customMtu.isEmpty())
            return;

        try
        {
            int mtu;

            try
            {
                mtu = Integer.parseInt(customMtu);
            }
            catch(NumberFormatException e)
            {
                mtu = 0;
            }

            if(mtu > 0)
            {
                vpnServiceBuilder.setMtu(mtu);

                forceMTU = true;
            }
        }
        catch(Exception e)
        {
            throw new Exception(String.format(Locale.getDefault(), "VPNContext.initMTU(): Invalid mtu option '%s': '%s'", customMtu, e.getMessage()));
        }
    }

    private void initApplications() throws Exception
    {
        ArrayList<String> applicationsList = settingsManager.getSystemApplicationFilterList();

        if(applicationsList.size() == 0)
            return;

        String filterType = settingsManager.getSystemApplicationFilterType();

        if(filterType.equals(SettingsManager.SYSTEM_OPTION_APPLICATION_FILTER_TYPE_NONE))
            return;

        if(filterType.equals(SettingsManager.SYSTEM_OPTION_APPLICATION_FILTER_TYPE_WHITELIST))
        {
            // Only the specified applications will be inside the tunnel

            for(String app : applicationsList)
            {
                EddieLogger.debug(String.format(Locale.getDefault(), "VPNContext.initApplications(): Adding '%s' to whitelisted applications. Traffic and data will be encapsuleted inside the tunnel.", app));

                try
                {
                    vpnServiceBuilder.addAllowedApplication(app);
                }
                catch(PackageManager.NameNotFoundException e)
                {
                    EddieLogger.error("VPNContext.initApplications(): Allowed application %s not found - Exception: %s", app, e);
                }
            }
        }
        else if(filterType.equals(SettingsManager.SYSTEM_OPTION_APPLICATION_FILTER_TYPE_BLACKLIST))
        {
            // The specified applications will be outside the tunnel

            for(String app : applicationsList)
            {
                EddieLogger.debug(String.format(Locale.getDefault(), "VPNContext.initApplications(): Adding '%s' to blacklisted applications. Traffic and data will be outside of the tunnel control.", app));

                try
                {
                    vpnServiceBuilder.addDisallowedApplication(app);
                }
                catch(PackageManager.NameNotFoundException e)
                {
                    EddieLogger.error("VPNContext.initApplications() Disallowed application %s not found - Exception: %s", app, e);
                }
            }
        }
        else
        {
            throw new Exception(String.format(Locale.getDefault(), "VPNContext.initApplications(): Unknown application's filter type '%s'", filterType));
        }
    }

    public ParcelFileDescriptor establish() throws Exception
    {
        ensureRoutes();

        ensureDNS();

        if(vpnServiceBuilder == null)
            throw new Exception("VPNContext.establish(): vpnServiceBuilder is null");

        if(fileDescriptor != null)
            throw new Exception("VPNContext.establish(): fileDescriptor is already initialized");

        fileDescriptor = vpnServiceBuilder.establish();

        return fileDescriptor;
    }

    private void cleanup()
    {
        fileDescriptor = null;

        vpnServiceBuilder = null;
    }

    private void ensureDNS()
    {
        if(!settingsManager.isSystemDNSOverrideEnable())
            return;

        if(hasDNS)
            return;     // At least one DNS has been added, do not check for alternative

        if(settingsManager.isSystemDNSOverrideEnable())
        {
            ArrayList<String> dnsCustom = settingsManager.getSystemDNSCustomList();

            for(String dns : dnsCustom)
            {
                try
                {
                    doAddDNS(dns);
                }
                catch (Exception e)
                {
                    EddieLogger.error("VPNContext.ensureDNS(): Cannot add DNS %s - Exception: %s", dns, e);
                }
            }
        }
    }

    private void ensureRoutes()
    {
        if(blockIPv4 == false)
        {
            vpnServiceBuilder.allowFamily(OsConstants.AF_INET);

            // Routes all IPV4 traffic inside the tunnel

            if(!settingsManager.areLocalNetworksExcluded() || NetworkStatusReceiver.getNetworkType() == NetworkStatusReceiver.NetworkType.MOBILE)
                vpnServiceBuilder.addRoute("0.0.0.0", 0);
        }

        if(blockIPv6 == false)
        {
            vpnServiceBuilder.allowFamily(OsConstants.AF_INET6);

            // Routes all IPV6 traffic inside the tunnel

            if(!settingsManager.areLocalNetworksExcluded() || NetworkStatusReceiver.getNetworkType() == NetworkStatusReceiver.NetworkType.MOBILE)
                vpnServiceBuilder.addRoute("::", 0);
        }
    }

    private void doAddDNS(String address) throws Exception
    {
        address = address.trim();

        if(address.isEmpty())
            throw new Exception("Invalid DNS server");

        EddieLogger.debug("VPNContext.doAddDNS(): Adding DNS server '%s'", address);

        vpnServiceBuilder.addDnsServer(address);

        hasDNS = true;
    }

    public void addDNSServer(String address, boolean ipv6)
    {
        if(customDNS)
            EddieLogger.debug("VPNContext.addDNSServer(): Custom DNS. Address %s will be skipped", address);
        else
        {
            try
            {
                doAddDNS(address);
            }
            catch(Exception e)
            {
                EddieLogger.error("VPNContext.addDNSServer(): Cannot add DNS %s - Exception: %s", address, e);
            }
        }
    }

    public void setBlockIPv6(boolean block)
    {
        blockIPv6 = block;
    }

    public void setMTU(int mtu)
    {
        if(forceMTU)
            EddieLogger.debug("VPNContext.setBlockIPv6(): MTU forced. Value %d will be skipped", mtu);
        else
            vpnServiceBuilder.setMtu(mtu);
    }
}
