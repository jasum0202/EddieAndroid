// <eddie_source_header>
// This file is part of Eddie/AirVPN software.
// Copyright (C)2014-2018 AirVPN (support@airvpn.org) / https://airvpn.org
//
// Eddie is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Eddie is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Eddie. If not, see <http://www.gnu.org/licenses/>.
// </eddie_source_header>
//
// 20 June 2018 - author: ProMIND - initial release. Based on revised code from com.eddie.android. (a tribute to the 1859 Perugia uprising occurred on 20 June 1859 and in memory of those brave inhabitants who fought for the liberty of Perugia)

package org.airvpn.eddie;

// Android.Content.Intent.ActionScreenOn, Android.Content.Intent.ActionScreenOff cannot be handled statically but they must be registered dynamically at runtime

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import java.util.HashMap;
import java.util.Locale;

public class EddieBroadcastReceiver extends BroadcastReceiver
{
    private EddieLogger eddieLogger = null;

    @Override
    public void onReceive(Context context, Intent intent)
    {
        String action = intent.getAction();

        eddieLogger = new EddieLogger();
        eddieLogger.init(context);

        if(action.equals(Intent.ACTION_BOOT_COMPLETED))
        {
            EddieLogger.debug("EddieBroadcastReceiver.onReceive(): Received 'ACTION_BOOT_COMPLETED'");
            EddieLogger.debug("EddieBroadcastReceiver.onReceive(): Trying to launch BootVPNActivity");

            Intent startVPNActivityIntent = new Intent(context, BootVPNActivity.class);

            startVPNActivityIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            context.startActivity(startVPNActivityIntent);
        }
        else
            EddieLogger.error(String.format(Locale.getDefault(), "EddieBroadcastReceiver.onReceive(): Received unhandled action '%s'", action));
    }
}
