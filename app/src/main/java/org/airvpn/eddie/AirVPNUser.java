// <eddie_source_header>
// This file is part of Eddie/AirVPN software.
// Copyright (C)2014-2018 AirVPN (support@airvpn.org) / https://airvpn.org
//
// Eddie is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Eddie is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Eddie. If not, see <http://www.gnu.org/licenses/>.
// </eddie_source_header>
//
// 10 October 2018 - author: ProMIND - initial release. (a tribute to the 1859 Perugia uprising occurred on 20 June 1859 and in memory of those brave inhabitants who fought for the liberty of Perugia)

package org.airvpn.eddie;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.NodeList;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

public class AirVPNUser implements NetworkStatusListener, EddieEventListener
{
    private final String AIRVPN_USER_DATA_FILE_NAME = "AirVPNUser.dat";
    private final String AIRVPN_USER_PROFILE_FILE_NAME = "AirVPNUserProfile.dat";
    private final String AIRVPN_USER_DATA = "AirVPNUser";
    private final String AIRVPN_USER_NAME_ITEM = "UserName";
    private final String AIRVPN_USER_PASSWORD_ITEM = "Password";
    private final String AIRVPN_USER_CURRENT_PROFILE = "CurrentProfile";

    public enum CheckPasswordMode
    {
        ASK_PASSWORD,
        USE_CURRENT_PASSWORD
    }

    public enum UserProfileType
    {
        NOT_SET,
        PROCESSING,
        STORED,
        FROM_SERVER
    }

    public enum UserIpCountryStatus
    {
        NOT_SET,
        PROCESSING,
        SET
    }

    public class UserKey
    {
        private String name;
        private String certificate;
        private String privateKey;

        public String getName()
        {
            return name;
        }

        public void setName(String n)
        {
            name = n;
        }

        public String getCertificate()
        {
            return certificate;
        }

        public void setCertificate(String c)
        {
            certificate = c;
        }

        public String getPrivateKey()
        {
            return privateKey;
        }

        public void setPrivateKey(String pk)
        {
            privateKey = pk;
        }
    }

    private Context appContext = null;
    private static Document airVPNUserProfileDocument = null;

    private SupportTools supportTools = null;
    private SettingsManager settingsManager = null;
    private static NetworkStatusReceiver networkStatusReceiver = null;
    private static EddieEvent eddieEvent = null;

    private DocumentBuilderFactory documentBuilderFactory = null;
    private DocumentBuilder documentBuilder = null;
    private static Document userAirVPNLoginDocument = null;

    private Button btnOk = null;
    private Button btnCancel = null;
    private EditText edtUserName = null;
    private EditText edtUserPassword = null;
    private CheckBox chkRememberMe = null;
    boolean loginResult = false;

    AlertDialog.Builder dialogBuilder = null;
    AlertDialog passwordDialog = null;
    AlertDialog loginDialog = null;
    private Handler dialogHandler = null;
    private EditText edtKey = null;
    String editValue = "";

    private static String masterPassword = "";
    private static String airVPNUserName = "";
    private static String airVPNUserPassword = "";
    private static String airVPNUserCurrentProfile = "";

    private static UserProfileType userProfileType = UserProfileType.NOT_SET;
    private static UserIpCountryStatus userIpCountryStatus = UserIpCountryStatus.NOT_SET;

    private static boolean userIsValid;
    private static boolean userLoginFailed;
    private static String expirationDate;
    private static Date airVPNSubscriptionExpirationDate;
    private static int daysToExpiration;
    private static String certificateAuthorityCertificate;
    private static String tlsAuthKey;
    private static String tlsCryptKey;
    private static String sshKey;
    private static String sshPpk;
    private static String sslCertificate;
    private static HashMap<String, UserKey> userKey;

    private static String userIP = "";
    private static String userCountry = "";
    private static float userLatitude = 0;
    private static float userLongitude = 0;

    public AirVPNUser(Context c)
    {
        appContext = c;

        supportTools = new SupportTools(appContext);
        settingsManager = new SettingsManager(appContext);

        if(networkStatusReceiver == null)
        {
            networkStatusReceiver = new NetworkStatusReceiver(appContext);
            networkStatusReceiver.addListener(this);
        }

        if(eddieEvent == null)
        {
            eddieEvent = new EddieEvent();
            eddieEvent.addListener(this);
        }

        if(userIpCountryStatus == UserIpCountryStatus.NOT_SET)
        {
            if(supportTools.isNetworkConnectionActive() == true && (VPN.getConnectionStatus() == VPN.Status.NOT_CONNECTED || VPN.getConnectionStatus() == VPN.Status.UNKNOWN))
                new getUserLocation().execute();
        }

        if(userAirVPNLoginDocument == null)
            initializeUserData();

        if(airVPNUserProfileDocument == null)
            loadUserProfile();
    }

    protected void finalize() throws Throwable
    {
        try
        {
            if(networkStatusReceiver != null)
                networkStatusReceiver.removeListener(this);

            if(eddieEvent != null)
                eddieEvent.removeListener(this);
        }
        catch(Exception e)
        {
            EddieLogger.warning("AirVPNUser.finalize() Exception: %s", e);
        }
    }

    private void initializeUserData()
    {
        try
        {
            documentBuilderFactory = DocumentBuilderFactory.newInstance();

            documentBuilder = documentBuilderFactory.newDocumentBuilder();
        }
        catch(ParserConfigurationException e)
        {
            userAirVPNLoginDocument = null;

            return;
        }

        userAirVPNLoginDocument = documentBuilder.newDocument();

        if(userAirVPNLoginDocument != null)
        {
            Element rootElement = userAirVPNLoginDocument.createElement(AIRVPN_USER_DATA);
            userAirVPNLoginDocument.appendChild(rootElement);

            Element user = userAirVPNLoginDocument.createElement(AIRVPN_USER_NAME_ITEM);
            user.setNodeValue("");
            rootElement.appendChild(user);

            Element password = userAirVPNLoginDocument.createElement(AIRVPN_USER_PASSWORD_ITEM);
            password.setNodeValue("");
            rootElement.appendChild(password);

            Element currentProfile = userAirVPNLoginDocument.createElement(AIRVPN_USER_CURRENT_PROFILE);
            currentProfile.setNodeValue("");
            rootElement.appendChild(currentProfile);
        }

        airVPNUserName = "";
        airVPNUserPassword = "";
        airVPNUserCurrentProfile = "";

        initializeUserProfileData();
    }

    private void initializeUserProfileData()
    {
        userIsValid = false;
        userLoginFailed = true;
        expirationDate = "";
        daysToExpiration = 0;
        certificateAuthorityCertificate = "";
        tlsAuthKey = "";
        tlsCryptKey = "";
        sshKey = "";
        sshPpk = "";
        sslCertificate = "";
        userKey = null;
    }

    public void saveUserCredentials()
    {
        setUserLoginDocumentItem(AIRVPN_USER_NAME_ITEM, airVPNUserName);
        setUserLoginDocumentItem(AIRVPN_USER_PASSWORD_ITEM, airVPNUserPassword);
        setUserLoginDocumentItem(AIRVPN_USER_CURRENT_PROFILE, airVPNUserCurrentProfile);
    }

    public void forgetAirVPNCredentials()
    {
        setUserLoginDocumentItem(AIRVPN_USER_NAME_ITEM, "");
        setUserLoginDocumentItem(AIRVPN_USER_PASSWORD_ITEM, "");
        setUserLoginDocumentItem(AIRVPN_USER_CURRENT_PROFILE, "");
    }

    private void saveUserAirLoginDocument()
    {
        if(!masterPassword.isEmpty())
        {
            if(!supportTools.encryptXmlDocumentToFile(userAirVPNLoginDocument, masterPassword, AIRVPN_USER_DATA_FILE_NAME))
                supportTools.infoDialog(R.string.login_user_data_save_error, true);
        }
    }

    public static String masterPassword()
    {
        return masterPassword;
    }

    public static String getUserIP()
    {
        return userIP;
    }

    public static String getUserCountry()
    {
        return userCountry;
    }

    public static float getUserLatitude()
    {
        return userLatitude;
    }

    public static float getUserLongitude()
    {
        return userLongitude;
    }

    public static boolean isUserValid()
    {
        return userIsValid;
    }

    public static Date getExpirationDate()
    {
        return airVPNSubscriptionExpirationDate;
    }

    public static void setExpirationDate(Date e)
    {
        airVPNSubscriptionExpirationDate = e;
    }

    public static int getDaysToExpiration()
    {
        return daysToExpiration;
    }

    public static void setDaysToExpiration(int d)
    {
        daysToExpiration = d;
    }

    public String getExpirationText()
    {
        if(appContext == null)
            return "";

        return String.format(Locale.getDefault(), appContext.getString(R.string.airvpn_subscription_status), daysToExpiration, DateFormat.getDateInstance(DateFormat.LONG, Locale.getDefault()).format(airVPNSubscriptionExpirationDate));
    }

    public static String getCertificateAuthorityCertificate()
    {
        return certificateAuthorityCertificate;
    }

    public static void setCertificateAuthorityCertificate(String c)
    {
        certificateAuthorityCertificate = c;
    }

    public static String getTlsAuthKey()
    {
        return tlsAuthKey;
    }

    public static void setTlsAuthKey(String t)
    {
        tlsAuthKey = t;
    }

    public static String getTlsCryptKey()
    {
        return tlsCryptKey;
    }

    public static void setTlsCryptKey(String t)
    {
        tlsCryptKey = t;
    }

    public static String getSshKey()
    {
        return sshKey;
    }

    public static void setSshKey(String s)
    {
        sshKey = s;
    }

    public static String getSshPpk()
    {
        return sshPpk;
    }

    public static void setSshPpk(String s)
    {
        sshPpk = s;
    }

    public static String getSslCertificate()
    {
        return sslCertificate;
    }

    public static void setSslCertificate(String s)
    {
        sslCertificate = s;
    }

    public static HashMap<String, UserKey> getUserKeys()
    {
        return userKey;
    }

    public static void setUserKeys(HashMap<String, UserKey> u)
    {
        userKey = u;
    }

    public UserKey getUserKey(String name)
    {
        UserKey key = null;

        if(userKey == null)
            return null;

        if(userKey.containsKey(name))
            key = userKey.get(name);

        return key;
    }

    public void setUserKey(String name, UserKey u)
    {
        if(userKey == null)
            userKey = new HashMap<String, UserKey>();

        userKey.put(name, u);
    }

    public void addUserKey(String name, UserKey key)
    {
        if(userKey == null)
            userKey = new HashMap<String, UserKey>();

        if(name.isEmpty() || key == null)
            return;

        userKey.put(name, key);
    }

    public ArrayList<String> getUserKeyNames()
    {
        ArrayList<String> keyNames = new ArrayList<String>();

        if(userKey != null)
        {
            for(String key : userKey.keySet())
                keyNames.add(key);
        }

        return keyNames;
    }

    public static UserProfileType getUserProfileType()
    {
        return userProfileType;
    }

    public boolean checkMasterPassword(CheckPasswordMode checkPasswordMode)
    {
        boolean result = false;
        int pwHashCode = settingsManager.getAirVPNMasterPasswordHashCode();

        if(pwHashCode == -1)
        {
            setMasterPassword();

            if(!masterPassword.isEmpty())
                return true;
            else
                return false;
        }

        if(!masterPassword.isEmpty() && checkPasswordMode == CheckPasswordMode.USE_CURRENT_PASSWORD)
            result = true;
        else
        {
            masterPassword = getMasterPassword(appContext.getResources().getString(R.string.login_enter_master_password));

            if(masterPassword.isEmpty())
                result = false;
            else
            {
                result = true;

                if(eddieEvent != null)
                    eddieEvent.onMasterPasswordChanged();
            }
        }

        return result;
    }

    public void setMasterPassword()
    {
        int pwHashCode = settingsManager.getAirVPNMasterPasswordHashCode();
        String password = "", pwd1 = "x", pwd2 = "";
        boolean valid = false;

        if(masterPassword.isEmpty() && pwHashCode != -1)
        {
            password = getMasterPassword(appContext.getResources().getString(R.string.login_enter_master_password));

            if(password.isEmpty())
                return;

            masterPassword = password;

            if(eddieEvent != null)
                eddieEvent.onMasterPasswordChanged();
        }

        valid = false;

        while(!valid)
        {
            pwd1 = getPasswordDialog(appContext.getResources().getString(R.string.login_enter_new_master_password));

            if(pwd1.isEmpty())
            {
                supportTools.waitInfoDialog(R.string.login_new_master_password_not_set);

                return;
            }

            pwd2 = getPasswordDialog(appContext.getResources().getString(R.string.login_confirm_new_master_password));

            if(pwd2.isEmpty())
            {
                supportTools.waitInfoDialog(R.string.login_new_master_password_not_set);

                return;
            }

            if(!pwd1.equals(pwd2))
                supportTools.waitInfoDialog(R.string.login_master_password_do_not_match);
            else
            {
                masterPassword = pwd1;

                settingsManager.setAirVPNMasterPasswordHashCode(masterPassword.hashCode());

                supportTools.waitInfoDialog(R.string.login_new_master_password_set);

                valid = true;

                supportTools.encryptXmlDocumentToFile(userAirVPNLoginDocument, masterPassword, AIRVPN_USER_DATA_FILE_NAME);

                if(eddieEvent != null)
                    eddieEvent.onMasterPasswordChanged();
            }
        }
    }

    private String getMasterPassword(String dialogTitle)
    {
        String password = "";
        boolean valid = false;
        int pwHashCode = settingsManager.getAirVPNMasterPasswordHashCode();

        while(!valid)
        {
            password = getPasswordDialog(dialogTitle);

            if(!password.isEmpty())
            {
                if(password.hashCode() != pwHashCode)
                    supportTools.waitInfoDialog(R.string.login_incorrect_master_password);
                else
                {
                    valid = true;

                    userAirVPNLoginDocument = supportTools.decryptFileToXmlDocument(AIRVPN_USER_DATA_FILE_NAME, password);

                    if(userAirVPNLoginDocument != null)
                    {
                        airVPNUserName = getUserLoginDocumentItem(AIRVPN_USER_NAME_ITEM);

                        airVPNUserPassword = getUserLoginDocumentItem(AIRVPN_USER_PASSWORD_ITEM);

                        airVPNUserCurrentProfile = getUserLoginDocumentItem(AIRVPN_USER_CURRENT_PROFILE);

                        supportTools.showProgressDialog(String.format(appContext.getResources().getString(R.string.login_to_airvpn), airVPNUserName));

                        loadUserProfile();
                    }
                }
            }
            else
                valid = true;
        }

        return password;
    }

    private String getPasswordDialog(String dialogTitle)
    {
        TextView txtDialogTitle = null;

        btnOk = null;
        btnCancel = null;

        if(appContext == null)
            return "";

        dialogHandler = new Handler(new Handler.Callback()
        {
            @Override
            public boolean handleMessage(Message mesg)
            {
                throw new RuntimeException();
            }
        });

        dialogBuilder = new AlertDialog.Builder(appContext);

        View content = LayoutInflater.from(appContext).inflate(R.layout.edit_option_dialog, null);

        txtDialogTitle = (TextView)content.findViewById(R.id.title);
        edtKey = (EditText)content.findViewById(R.id.key);

        edtKey.setText("");

        edtKey.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD | InputType.TYPE_CLASS_TEXT);

        btnOk = (Button)content.findViewById(R.id.btn_ok);
        btnCancel = (Button)content.findViewById(R.id.btn_cancel);

        supportTools.enableButton(btnOk, false);

        supportTools.enableButton(btnCancel, true);

        btnOk.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View arg0)
            {
                editValue = edtKey.getText().toString();

                passwordDialog.dismiss();

                dialogHandler.sendMessage(dialogHandler.obtainMessage());
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View arg0)
            {
                editValue = "";

                passwordDialog.dismiss();

                dialogHandler.sendMessage(dialogHandler.obtainMessage());
            }
        });

        edtKey.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void afterTextChanged(Editable s)
            {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                supportTools.enableButton(btnOk, !edtKey.getText().toString().isEmpty());
            }
        });

        txtDialogTitle.setText(dialogTitle);

        dialogBuilder.setTitle("");
        dialogBuilder.setView(content);

        passwordDialog = dialogBuilder.create();
        passwordDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        passwordDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);

        if(appContext instanceof Activity)
        {
            if(((Activity)appContext).isFinishing())
                return "";
        }

        passwordDialog.show();

        try
        {
            Looper.loop();
        }
        catch(RuntimeException e)
        {
        }

        return editValue;
    }

    public boolean checkUserLogin()
    {
        if(userIsValid)
            return true;

        if(masterPassword.isEmpty())
        {
            int pwHashCode = settingsManager.getAirVPNMasterPasswordHashCode();

            if(pwHashCode == -1)
                setMasterPassword();
            else
                masterPassword = getMasterPassword(appContext.getResources().getString(R.string.login_enter_master_password));

            if(masterPassword.isEmpty())
                return false;
            else
            {
                if(eddieEvent != null)
                    eddieEvent.onMasterPasswordChanged();
            }
        }

        if(userAirVPNLoginDocument != null && settingsManager.isAirVPNRememberMe() && userLoginFailed == false)
        {
            airVPNUserName = getUserLoginDocumentItem(AIRVPN_USER_NAME_ITEM);

            airVPNUserPassword = getUserLoginDocumentItem(AIRVPN_USER_PASSWORD_ITEM);

            airVPNUserCurrentProfile = getUserLoginDocumentItem(AIRVPN_USER_CURRENT_PROFILE);
        }

        if(airVPNUserName.isEmpty() && airVPNUserPassword.isEmpty())
        {
            if(loginDialog())
            {
                supportTools.showProgressDialog(String.format(appContext.getResources().getString(R.string.login_to_airvpn), airVPNUserName));

                loadUserProfile();
            }
        }
        else
        {
            supportTools.showProgressDialog(String.format(appContext.getResources().getString(R.string.login_to_airvpn), airVPNUserName));

            loadUserProfile();
        }

        return false;
    }

    public boolean logout()
    {
        userIsValid = false;
        userLoginFailed = false;

        EddieLogger.info(String.format("User %s Logged out from AirVPN", airVPNUserName));

        airVPNUserName = "";
        airVPNUserPassword = "";
        airVPNUserCurrentProfile = "";

        forgetAirVPNCredentials();

        eddieEvent.onAirVPNLogout();

        return true;
    }

    public static String getUserName()
    {
        return airVPNUserName;
    }

    public void setUserName(String u)
    {
        airVPNUserName = u;

        if(settingsManager.isAirVPNRememberMe())
            saveUserCredentials();
    }

    public static String getUserPassword()
    {
        return airVPNUserPassword;
    }

    public void setUserPassword(String p)
    {
        airVPNUserPassword = p;

        if(settingsManager.isAirVPNRememberMe())
            saveUserCredentials();
    }

    public static String getCurrentProfile()
    {
        return airVPNUserCurrentProfile;
    }

    public void setCurrentProfile(String p)
    {
        airVPNUserCurrentProfile = p;

        setUserLoginDocumentItem(AIRVPN_USER_CURRENT_PROFILE, p);
    }

    private String getUserLoginDocumentItem(String item)
    {
        if(userAirVPNLoginDocument == null)
            return "";

        return supportTools.getXmlItemValue(userAirVPNLoginDocument, item);
    }

    private void setUserLoginDocumentItem(String item, String value)
    {
        if(userAirVPNLoginDocument == null)
            return;

        NodeList itemList = userAirVPNLoginDocument.getElementsByTagName(item);

        if(itemList != null && itemList.getLength() > 0)
            itemList.item(0).setTextContent(value);
        else
        {
            NodeList nodeList = userAirVPNLoginDocument.getElementsByTagName(AIRVPN_USER_DATA);

            Element element = userAirVPNLoginDocument.createElement(item);

            if(nodeList != null && element != null)
            {
                element.setNodeValue(value);

                nodeList.item(0).appendChild(element);
            }
        }

        saveUserAirLoginDocument();
    }

    private class getUserLocation extends AsyncTask<Void, Boolean, Boolean>
    {
        @Override
        protected void onPreExecute()
        {
        }

        @Override
        protected Boolean doInBackground(Void... p)
        {
            BufferedReader bufferedReader = null;
            StringBuffer httpData = null;
            Document userData = null;

            try
            {
                userIpCountryStatus = UserIpCountryStatus.PROCESSING;

                EddieLogger.info("Requesting user IP and country to AirVPN ipleak.net via secure connection");

                URL url = new URL("https://ipleak.net/xml/");

                HttpURLConnection httpURLConnection = (HttpURLConnection)url.openConnection();
                httpURLConnection.setRequestMethod("GET");
                httpURLConnection.setConnectTimeout(SupportTools.HTTP_CONNECTION_TIMEOUT);
                httpURLConnection.setReadTimeout(SupportTools.HTTP_READ_TIMEOUT);

                if(httpURLConnection.getResponseCode() != HttpURLConnection.HTTP_OK)
                    return false;

                bufferedReader = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()));
                httpData = new StringBuffer();

                String line = "";

                while ((line = bufferedReader.readLine()) != null)
                    httpData.append(line);

                userData = supportTools.stringToXmlDocument(httpData.toString());

                if(userData != null)
                {
                    userIP = supportTools.getXmlItemValue(userData, "ip");

                    userCountry = supportTools.getXmlItemValue(userData, "country_code");

                    try
                    {
                        userLatitude = Float.parseFloat(supportTools.getXmlItemValue(userData, "latitude"));
                    }
                    catch(NumberFormatException e)
                    {
                        userLatitude = 0;
                    }

                    try
                    {
                        userLongitude = Float.parseFloat(supportTools.getXmlItemValue(userData, "longitude"));
                    }
                    catch(NumberFormatException e)
                    {
                        userLongitude = 0;
                    }

                    EddieLogger.info("AirVPN ipleak.net: User IP is %s", userIP);
                    EddieLogger.info("AirVPN ipleak.net: User country is %s", userCountry);
                }
            }
            catch(SocketTimeoutException e)
            {
                EddieLogger.warning("AirVPNUser.getUserLocation() Cannot connect to AirVPN ipleak.net");

                return false;
            }
            catch(ConnectException e)
            {
                EddieLogger.warning("AirVPNUser.getUserLocation() Cannot connect to AirVPN ipleak.net");

                return false;
            }
            catch(Exception e)
            {
                EddieLogger.error("AirVPNUser.getUserLocation() Exception: %s", e);

                return false;
            }
            finally
            {
                if(bufferedReader != null)
                {
                    try
                    {
                        bufferedReader.close();
                    }
                    catch(Exception e)
                    {
                        EddieLogger.error("AirVPNUser.getUserLocation() Exception: %s", e);
                    }
                }
            }

            return true;
        }

        @Override
        protected void onPostExecute(Boolean result)
        {
            if(result)
            {
                eddieEvent.onAirVPNUserDataChanged();

                userIpCountryStatus = UserIpCountryStatus.SET;
            }
            else
            {
                EddieLogger.warning("AirVPNUser.getUserLocation(): Error while getting user's IP and country");

                userIpCountryStatus = UserIpCountryStatus.NOT_SET;
            }
        }
    }

    private boolean loginDialog()
    {
        loginResult = false;

        btnOk = null;
        btnCancel = null;

        dialogHandler = new Handler(new Handler.Callback()
        {
            @Override
            public boolean handleMessage(Message mesg)
            {
                throw new RuntimeException();
            }
        });

        dialogBuilder = new AlertDialog.Builder(appContext);

        View content = LayoutInflater.from(appContext).inflate(R.layout.login_dialog_layout, null);

        edtUserName = (EditText)content.findViewById(R.id.edt_login_username);
        edtUserPassword = (EditText)content.findViewById(R.id.edt_login_password);

        edtUserName.setText("");
        edtUserPassword.setText("");

        btnOk = (Button)content.findViewById(R.id.btn_login);
        btnCancel = (Button)content.findViewById(R.id.btn_cancel_login);

        chkRememberMe = (CheckBox)content.findViewById(R.id.chk_login_remember_me);

        chkRememberMe.setChecked(settingsManager.isAirVPNRememberMe());

        supportTools.enableButton(btnCancel, true);
        supportTools.enableButton(btnOk, false);

        btnOk.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View arg0)
            {
                loginResult = true;

                airVPNUserName = edtUserName.getText().toString();
                airVPNUserPassword = edtUserPassword.getText().toString();

                settingsManager.setAirVPNRememberMe(chkRememberMe.isChecked());

                if(chkRememberMe.isChecked())
                    saveUserCredentials();
                else
                    forgetAirVPNCredentials();

                loginDialog.dismiss();

                dialogHandler.sendMessage(dialogHandler.obtainMessage());
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View arg0)
            {
                loginResult = false;

                loginDialog.dismiss();

                dialogHandler.sendMessage(dialogHandler.obtainMessage());
            }
        });

        edtUserName.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void afterTextChanged(Editable s)
            {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                if(!edtUserName.getText().toString().isEmpty() && !edtUserPassword.getText().toString().isEmpty())
                    supportTools.enableButton(btnOk, true);
                else
                    supportTools.enableButton(btnOk, false);
            }
        });

        edtUserPassword.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void afterTextChanged(Editable s)
            {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                if(!edtUserName.getText().toString().isEmpty() && !edtUserPassword.getText().toString().isEmpty())
                    supportTools.enableButton(btnOk, true);
                else
                    supportTools.enableButton(btnOk, false);
            }
        });

        dialogBuilder.setTitle(appContext.getResources().getString(R.string.login_dialog_title));

        dialogBuilder.setView(content);

        loginDialog = dialogBuilder.create();
        loginDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);

        if(appContext instanceof Activity)
        {
            if(((Activity)appContext).isFinishing())
                return false;
        }

        loginDialog.show();

        try
        {
            Looper.loop();
        }
        catch(RuntimeException e)
        {
        }

        return loginResult;
    }

    private void loadUserProfile()
    {
        if(userProfileType == UserProfileType.PROCESSING)
            return;

        Runnable runnable = new Runnable()
        {
            @Override
            public void run()
            {
                Document userProfileDocument = null;
                File userProfileFile = null;

                if(!masterPassword.isEmpty())
                {
                    if(!airVPNUserName.isEmpty() || !airVPNUserPassword.isEmpty())
                    {
                        if(!AirVPNManifest.getRsaPublicKeyModulus().isEmpty() && !AirVPNManifest.getRsaPublicKeyExponent().isEmpty())
                        {
                            HashMap<String, String> parameters = new HashMap<String, String>();

                            parameters.put("login", airVPNUserName);
                            parameters.put("password", airVPNUserPassword);
                            parameters.put("system", SupportTools.DEVICE_PLATFORM);
                            parameters.put("version", SupportTools.AIRVPN_SERVER_DOCUMENT_VERSION);
                            parameters.put("act", "user");

                            SupportTools.RequestAirVPNDocument userProfileRequest = supportTools.new RequestAirVPNDocument();

                            userProfileRequest.execute(parameters);
                        }

                        return;
                    }
                    else
                    {
                        userProfileFile = new File(appContext.getFilesDir(), AIRVPN_USER_PROFILE_FILE_NAME);

                        if(userProfileFile.exists())
                        {
                            userProfileDocument = supportTools.decryptFileToXmlDocument(AIRVPN_USER_PROFILE_FILE_NAME, masterPassword);

                            userProfileType = UserProfileType.STORED;

                            processXmlUserProfile(userProfileDocument);
                        }
                        else
                        {
                            userProfileDocument = null;

                            userProfileType = UserProfileType.NOT_SET;
                        }
                    }
                }
                else
                    userProfileType = UserProfileType.NOT_SET;
            }
        };

        supportTools.startThread(runnable);
    }

    private void processXmlUserProfile(Document userProfileDocument)
    {
        NodeList nodeList = null;
        NamedNodeMap namedNodeMap = null;
        UserProfileType newUserProfileType = userProfileType;
        AirVPNUser.UserKey localUserKey = null;
        String value, logMessage = "";

        if(userProfileDocument == null)
        {
            EddieLogger.error("AirVPNUser.processXmlUserProfile(): userProfileDocument is null.");

            return;
        }

        newUserProfileType = userProfileType;

        userProfileType = UserProfileType.PROCESSING;

        switch(newUserProfileType)
        {
            case STORED:
            {
                logMessage = "Setting user profile to the locally stored instance";
            }
            break;

            case FROM_SERVER:
            {
                logMessage = "Setting user profile to the instance downloaded from AirVPN server";
            }
            break;

            default:
            {
                logMessage = "User profile is not set.";
            }
            break;
        }

        EddieLogger.info(logMessage);

        if(newUserProfileType == UserProfileType.NOT_SET)
        {
            eddieEvent.onAirVPNUserProfileChanged();

            supportTools.dismissProgressDialog();

            userProfileType = newUserProfileType;

            return;
        }

        initializeUserProfileData();

        // user attributes

        nodeList = userProfileDocument.getElementsByTagName("user");

        if(nodeList != null && nodeList.getLength() > 0)
        {
            namedNodeMap = nodeList.item(0).getAttributes();

            if(namedNodeMap == null)
            {
                EddieLogger.error("AirVPNUser.processXmlUserProfile(): \"user\" node has no attributes");

                initializeUserProfileData();

                eddieEvent.onAirVPNUserProfileChanged();

                supportTools.dismissProgressDialog();

                return;
            }

            String messageAction = supportTools.getXmlItemNodeValue(namedNodeMap, "message_action");

            if(messageAction.isEmpty())
                userIsValid = true;
            else
                userIsValid = false;

            if(!airVPNUserName.equals(supportTools.getXmlItemNodeValue(namedNodeMap, "login")))
            {
                userIsValid = false;

                EddieLogger.error("AirVPNUser.processXmlUserProfile(): user name in profile data does not match to current user");
            }

            expirationDate = supportTools.getXmlItemNodeValue(namedNodeMap, "expirationdate");
            certificateAuthorityCertificate = supportTools.convertXmlEntities(supportTools.getXmlItemNodeValue(namedNodeMap, "ca"));
            tlsAuthKey = supportTools.convertXmlEntities(supportTools.getXmlItemNodeValue(namedNodeMap, "ta"));
            tlsCryptKey = supportTools.convertXmlEntities(supportTools.getXmlItemNodeValue(namedNodeMap, "tls_crypt"));
            sshKey = supportTools.convertXmlEntities(supportTools.getXmlItemNodeValue(namedNodeMap, "ssh_key"));
            sshPpk = supportTools.convertXmlEntities(supportTools.getXmlItemNodeValue(namedNodeMap, "ssh_ppk"));
            sslCertificate = supportTools.convertXmlEntities(supportTools.getXmlItemNodeValue(namedNodeMap, "ssl_crt"));

            if(!expirationDate.isEmpty())
            {
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                Date now = new Date(System.currentTimeMillis());

                try
                {
                    airVPNSubscriptionExpirationDate = format.parse(expirationDate);

                    daysToExpiration = (int)TimeUnit.DAYS.convert(airVPNSubscriptionExpirationDate.getTime() - now.getTime(), TimeUnit.MILLISECONDS);
                }
                catch(ParseException e)
                {
                    airVPNSubscriptionExpirationDate = new Date();

                    daysToExpiration = 0;
                }
            }
            else
                daysToExpiration = 0;

            // user's keys (profiles)

            nodeList = userProfileDocument.getElementsByTagName("key");

            if(nodeList != null)
            {
                for(int position = 0; position < nodeList.getLength(); position++)
                {
                    localUserKey = new AirVPNUser.UserKey();

                    namedNodeMap = nodeList.item(position).getAttributes();

                    if(namedNodeMap != null)
                    {
                        value = supportTools.getXmlItemNodeValue(namedNodeMap, "name");

                        if(!value.isEmpty())
                        {
                            localUserKey.name = value;
                            localUserKey.certificate = supportTools.convertXmlEntities(supportTools.getXmlItemNodeValue(namedNodeMap, "crt"));
                            localUserKey.privateKey = supportTools.convertXmlEntities(supportTools.getXmlItemNodeValue(namedNodeMap, "key"));

                            addUserKey(value, localUserKey);
                        }
                        else
                            EddieLogger.warning("AirVPNUser.processXmlUserProfile(): found unnamed \"key\" in user profile");
                    }
                    else
                        EddieLogger.warning("AirVPNUser.processXmlUserProfile(): found \"key\" node in user profile with no attributes");
                }
            }
            else
                EddieLogger.error("AirVPNUser.processXmlUserProfile(): \"keys\" node not found in manifest");
        }
        else
        {
            EddieLogger.error("AirVPNUser.processXmlUserProfile(): \"user\" node not found in manifest");

            initializeUserProfileData();

            eddieEvent.onAirVPNUserProfileChanged();

            supportTools.dismissProgressDialog();

            return;
        }

        userProfileType = newUserProfileType;

        airVPNUserProfileDocument = userProfileDocument;

        if(eddieEvent != null)
            eddieEvent.onAirVPNUserProfileChanged();

        if(userIsValid)
            eddieEvent.onAirVPNLogin();
        else
            eddieEvent.onAirVPNLoginFailed();

        if(!masterPassword.isEmpty())
        {
            if(!supportTools.encryptXmlDocumentToFile(airVPNUserProfileDocument, AirVPNUser.masterPassword(), AIRVPN_USER_PROFILE_FILE_NAME))
                EddieLogger.error("Error while saving AirVPN user profile to local storage");
        }
    }

    public String getOpenVPNProfile(String profileName, String server, int port, String proto, String tlsMode, String cipher, boolean connectIPv6, boolean createCountryProfile, String countryCode)
    {
        String openVpnProfile = "";

        if(profileName.isEmpty() || !userKey.containsKey(profileName))
        {
            EddieLogger.error(String.format("AirVPNUser.userCertificate(): wrong profile name \"%s\"", profileName));

            return "";
        }

        if(server == null || server.isEmpty())
        {
            EddieLogger.error("AirVPNUser.userCertificate(): server is empty");

            return "";
        }

        if(port < 1 || port > 65535)
        {
            EddieLogger.error(String.format("AirVPNUser.userCertificate(): illegal port number %d", port));

            return "";
        }

        proto = proto.toLowerCase();

        if(!proto.equals("udp") && !proto.equals("tcp") && !proto.equals("udp6") && !proto.equals("tcp6"))
        {
            EddieLogger.error(String.format("AirVPNUser.userCertificate(): illegal prptocol %s", proto));

            return "";

        }

        tlsMode = tlsMode.toLowerCase();

        if(!tlsMode.equals(SettingsManager.AIRVPN_TLS_MODE_AUTH) && !tlsMode.equals(SettingsManager.AIRVPN_TLS_MODE_CRYPT))
        {
            EddieLogger.error(String.format("AirVPNUser.userCertificate(): illegal tls mode %s", tlsMode));

            return "";
        }

        if(createCountryProfile)
        {
            server = countryCode.toLowerCase();

            if(tlsMode.equals(SettingsManager.AIRVPN_TLS_MODE_CRYPT))
                server += "3";
            else
                server += "1";

            if(connectIPv6)
                server += ".ipv6";

            server += ".vpn.airdns.org";
        }

        openVpnProfile = "client\ndev tun\n";

        openVpnProfile += String.format(Locale.getDefault(), "remote %s %d\n", server, port);

        openVpnProfile += "proto " + proto + "\n";

        if(connectIPv6)
            openVpnProfile += "setenv UV_IPV6 yes\n";

        openVpnProfile += "resolv-retry infinite\n" +
                "nobind\n" +
                "persist-key\n" +
                "persist-tun\n" +
                "auth-nocache\n" +
                "verb 3\n";

        if(proto.equals("udp"))
            openVpnProfile += "explicit-exit-notify 5\n";

        openVpnProfile += "remote-cert-tls server\n";

        if(settingsManager.isOvpn3AESCBCCiphersuitesForced())
            openVpnProfile += "cipher AES-256-CBC\n";
        else
        {
            if(cipher.equals(SettingsManager.AIRVPN_CIPHER_SERVER))
                openVpnProfile += "cipher AES-256-GCM\n";
            else
            {
                openVpnProfile += "cipher " + cipher + "\n";
                openVpnProfile += "ncp-disable\n";
            }
        }

        openVpnProfile += "comp-lzo no\n";

        if(tlsMode.equals(SettingsManager.AIRVPN_TLS_MODE_CRYPT))
            openVpnProfile += "auth sha512\n";

        if(tlsMode.equals(SettingsManager.AIRVPN_TLS_MODE_AUTH))
            openVpnProfile += "key-direction 1\n";

        if(certificateAuthorityCertificate.isEmpty())
        {
            EddieLogger.error("AirVPNUser.userCertificate(): Certificate authority certificate (ca) is empty");

            return "";
        }

        openVpnProfile += "<ca>\n" + certificateAuthorityCertificate + "\n</ca>\n";

        UserKey profile = userKey.get(profileName);

        openVpnProfile += "<cert>\n" + profile.certificate + "\n</cert>\n";

        openVpnProfile += "<key>\n" + profile.privateKey + "\n</key>\n";

        if(tlsMode.equals("tls-auth"))
            openVpnProfile += "<tls-auth>\n" + tlsAuthKey + "\n</tls-auth>\n";
        else if(tlsMode.equals("tls-crypt"))
            openVpnProfile += "<tls-crypt>\n" + tlsCryptKey + "\n</tls-crypt>\n";

        return openVpnProfile;
    }

    // Eddie events

    public void onVpnConnectionDataChanged(final OpenVPNConnectionData connectionData)
    {
    }

    public void onVpnStatusChanged(VPN.Status status, String message)
    {
    }

    public void onVpnAuthFailed(final OpenVPNEvent oe)
    {
    }

    public void onVpnError(final OpenVPNEvent oe)
    {
    }

    public void onMasterPasswordChanged()
    {
    }

    public void onAirVPNLogin()
    {
        supportTools.dismissProgressDialog();

        EddieLogger.info(String.format("Logged in to AirVPN as user %s", airVPNUserName));

        userLoginFailed = false;
    }

    public void onAirVPNLoginFailed()
    {
        supportTools.dismissProgressDialog();

        airVPNUserName = "";
        airVPNUserPassword = "";

        EddieLogger.warning(String.format("Failed login to AirVPN as user %s", airVPNUserName));

        userIsValid = false;
        userLoginFailed = true;
    }

    public void onAirVPNLogout()
    {
        userLoginFailed = false;
    }

    public void onAirVPNUserDataChanged()
    {
    }

    public void onAirVPNUserProfileReceived(final Document document)
    {
        Runnable runnable = new Runnable()
        {
            @Override
            public void run()
            {
                userProfileType = UserProfileType.FROM_SERVER;

                processXmlUserProfile(document);
            }
        };

        supportTools.startThread(runnable);
    }

    public void onAirVPNUserProfileDownloadError()
    {
        userIsValid = false;

        supportTools.dismissProgressDialog();

        supportTools.infoDialog(String.format("%s%s", appContext.getResources().getString(R.string.user_profile_download_error), appContext.getResources().getString(R.string.bootstrap_server_error)), true);

        EddieLogger.error("Error while retrieving AirVPN user profile from server");
    }

    public void onAirVPNUserProfileChanged()
    {
    }

    public void onAirVPNManifestReceived(final Document document)
    {
    }

    public void onAirVPNManifestDownloadError()
    {
    }

    public void onAirVPNManifestChanged()
    {
    }

    public void onAirVPNIgnoredDocumentRequest()
    {
        supportTools.dismissProgressDialog();

        if(airVPNUserName.isEmpty() || airVPNUserPassword.isEmpty())
        {
            userIsValid = false;
            userLoginFailed = true;

            eddieEvent.onAirVPNLoginFailed();
        }
    }

    public void onCancelConnection()
    {
    }

    // NetworkStatusReceiver

    public void onNetworkStatusNotAvailable()
    {
    }

    public void onNetworkStatusConnected()
    {
    }

    public void onNetworkStatusIsConnecting()
    {
    }

    public void onNetworkStatusIsDisonnecting()
    {
    }

    public void onNetworkStatusSuspended()
    {
    }

    public void onNetworkStatusNotConnected()
    {
    }

    public void onNetworkTypeChanged()
    {
        if(userIpCountryStatus != UserIpCountryStatus.PROCESSING)
        {
            if(supportTools.isNetworkConnectionActive() == true && (VPN.getConnectionStatus() == VPN.Status.NOT_CONNECTED || VPN.getConnectionStatus() == VPN.Status.UNKNOWN))
                new getUserLocation().execute();
        }
    }
}
