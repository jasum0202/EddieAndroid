// <eddie_source_header>
// This file is part of Eddie/AirVPN software.
// Copyright (C)2014-2018 AirVPN (support@airvpn.org) / https://airvpn.org
//
// Eddie is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Eddie is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Eddie. If not, see <http://www.gnu.org/licenses/>.
// </eddie_source_header>
//
// 4 September 2018 - author: ProMIND - initial release. (a tribute to the 1859 Perugia uprising occurred on 20 June 1859 and in memory of those brave inhabitants who fought for the liberty of Perugia)

package org.airvpn.eddie;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.res.ResourcesCompat;
import android.text.Html;
import android.text.Spanned;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.logging.Level;

public class LogActivity extends Activity
{
    public static final int MODE_LISTVIEW = 1;
    public static final int MODE_WEBVIEW = 2;

    private Typeface typeface = null;
    private ArrayList<String> logEntry = null;
    private ListView listLogView = null;
    private TextView txtTitle = null;
    private WebView webLogView = null;
    private Button btnShare = null;
    private LinearLayout llProgressSpinner = null;
    private LogListAdapter logListAdapter = null;
    private int viewMode = MODE_LISTVIEW;

    private SettingsManager settingsManager = null;
    private EddieLogger eddieLogger = null;

    private enum FormatType
    {
        HTML,
        PLAIN_TEXT
    }

    private enum LogTime
    {
        UTC,
        LOCAL
    }

    private class LogListAdapter extends BaseAdapter
    {
        private Activity activity = null;
        private ArrayList<Spanned> logEntry = null;

        private class ListViewHolder
        {
            public TextView txtLogEntry;
        }

        public LogListAdapter(Activity a, ArrayList<Spanned> entryList)
        {
            activity = a;

            logEntry = entryList;
        }

        public void dataSet(ArrayList<Spanned> entryList)
        {
            logEntry = entryList;

            notifyDataSetChanged();
        }

        @Override
        public boolean isEnabled(int position)
        {
            return false;
        }

        @Override
        public int getCount()
        {
            int entries = 0;

            if(logEntry != null)
                entries = logEntry.size();

            return entries;
        }

        @TargetApi(24)
        @Override
        public Spanned getItem(int position)
        {
            Spanned item = null;

            if(logEntry != null && position < logEntry.size())
                item = logEntry.get(position);
            else
            {
                String txt = "<font color='red'>logEntry is invalid</font>";

                if(Build.VERSION.SDK_INT < Build.VERSION_CODES.N)
                    item = Html.fromHtml(txt);
                else
                    item = Html.fromHtml(txt, Html.FROM_HTML_MODE_LEGACY);
            }

            return item;
        }

        @Override
        public long getItemId(int position)
        {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent)
        {
            ListViewHolder listViewHolder = null;

            if(convertView != null)
                listViewHolder = (ListViewHolder)convertView.getTag();

            if(listViewHolder == null)
            {
                listViewHolder = new ListViewHolder();

                convertView = activity.getLayoutInflater().inflate(R.layout.log_activity_listitem, null);

                listViewHolder.txtLogEntry = (TextView)convertView.findViewById(R.id.log_entry);

                convertView.setTag(listViewHolder);
            }

            listViewHolder.txtLogEntry.setText(logEntry.get(position));
            listViewHolder.txtLogEntry.setTypeface(typeface);

            return convertView;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        Bundle extras = getIntent().getExtras();

        settingsManager = new SettingsManager(this);

        eddieLogger = new EddieLogger();

        eddieLogger.init(this);

        viewMode = extras.getInt("ViewMode");

        typeface = ResourcesCompat.getFont(this, R.font.default_font);

        if(viewMode == MODE_LISTVIEW)
        {
            setContentView(R.layout.log_activity_layout);

            listLogView = (ListView)findViewById(R.id.log);
        }
        else
        {
            setContentView(R.layout.log_activity_weblayout);

            webLogView = (WebView)findViewById(R.id.logwebview);
        }

        txtTitle = (TextView)findViewById(R.id.title);

        txtTitle.setTypeface(typeface);

        btnShare = (Button)findViewById(R.id.btn_share);

        btnShare.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View arg0)
            {
                SimpleDateFormat dateFormatter = null;
                ArrayList<String> exportLog = null;
                String logText = "", logSubject = "";
	            DisplayMetrics appDisplayMetrics = getResources().getDisplayMetrics();

                llProgressSpinner.setVisibility(View.VISIBLE);

                dateFormatter = new SimpleDateFormat("dd MMM yyyy HH:mm:ss");

                dateFormatter.setTimeZone(TimeZone.getTimeZone("GMT"));

                logSubject = String.format(Locale.getDefault(), getResources().getString(R.string.log_subject), dateFormatter.format(new Date()));

                logText = logSubject + "\n\nEddie for Android ";

                logText += String.format(Locale.getDefault(), "%s Version Code %s\n", BuildConfig.VERSION_NAME, BuildConfig.VERSION_CODE);

                logText += "\n\n--- App and device information ---\n\n";

                logText += String.format("Eddie Version Code: %s\n", BuildConfig.VERSION_CODE);
                logText += String.format("Eddie Version Name: %s\n", BuildConfig.VERSION_NAME);
                logText += String.format("%s - %s\n", EddieLibrary.qualifiedName(), EddieLibrary.releaseDate());
                logText += String.format("Eddie Library API level: %s\n\n", EddieLibrary.apiLevel());
                logText += String.format("Architecture: %s\n", EddieLibrary.architecture());
                logText += String.format("Platform: %s\n", EddieLibrary.platform());

                logText += String.format("\nManufacturer: %s\n", Build.MANUFACTURER);
                logText += String.format("Model: %s\n", Build.MODEL);
                logText += String.format("Device: %s\n", Build.DEVICE);
                logText += String.format("Board: %s\n", Build.BOARD);
                logText += String.format("Brand: %s\n", Build.BRAND);
                logText += String.format("Product: %s\n", Build.PRODUCT);
                logText += String.format("Hardware: %s\n", Build.HARDWARE);

                logText += "Supported ABIs:";

                String[] supportedAbis = Build.SUPPORTED_ABIS;

                for(String abi : supportedAbis)
                    logText += " " + abi;

                logText += String.format("\nHost: %s\n", Build.HOST);
                logText += String.format("ID: %s\n", Build.ID);
                logText += String.format("Display: %s\n", Build.DISPLAY);
                logText += String.format(Locale.getDefault(), "Android API Level: %d\n", Build.VERSION.SDK_INT);
                logText += String.format("Android Codename: %s\n", Build.VERSION.CODENAME);
                logText += String.format("Android Version Release: %s\n", Build.VERSION.RELEASE);
                logText += String.format(Locale.getDefault(), "Android Version Incremental: %s\n", Build.VERSION.INCREMENTAL);
                logText += String.format("Fingerprint: %s\n", Build.FINGERPRINT);
                logText += String.format("Type: %s\n", Build.TYPE);
                logText += String.format("Tags: %s\n", Build.TAGS);
                logText += String.format("User: %s\n", Build.USER);
                logText += String.format("Language: %s\n", Locale.getDefault().getLanguage());
                logText += String.format("Rooted device: %s\n", SupportTools.isDeviceRooted());
                logText += String.format("Network: %s\n", NetworkStatusReceiver.getNetworkTypeName());

                logText += String.format(Locale.getDefault(), "Screen Width: %d\n", appDisplayMetrics.widthPixels);
                logText += String.format(Locale.getDefault(), "Screen Height: %d\n", appDisplayMetrics.heightPixels);
                logText += String.format(Locale.getDefault(), "Screen Density: %.0f\n", appDisplayMetrics.density);
                logText += String.format(Locale.getDefault(), "Screen Density DPI: %d\n", appDisplayMetrics.densityDpi);
                logText += String.format(Locale.getDefault(), "Screen XDPI: %.0f\n", appDisplayMetrics.xdpi);
                logText += String.format(Locale.getDefault(), "Screen YDPI: %.0f\n", appDisplayMetrics.ydpi);

                // System memory

                ActivityManager.MemoryInfo memoryInfo = new ActivityManager.MemoryInfo();
                ActivityManager activityManager = (ActivityManager)getSystemService(Context.ACTIVITY_SERVICE);

                try
                {
                    activityManager.getMemoryInfo(memoryInfo);

                    long totalMemory = memoryInfo.totalMem / 1048576L;
                    long memory = memoryInfo.availMem / 1048576L;

                    logText += String.format(Locale.getDefault(), "Total RAM: %d Mb\n", totalMemory);
                    logText += String.format(Locale.getDefault(), "Free RAM: %d Mb\n", memory);

                    memory = memoryInfo.threshold / 1048576L;

                    logText += String.format(Locale.getDefault(), "Threshold Memory: %d Mb\n", memory);
                    logText += String.format("Low Memory: %s\n", memoryInfo.lowMemory);

                    // App memory

                    memory = Runtime.getRuntime().totalMemory() / 1048576L;

                    logText += String.format(Locale.getDefault(), "App Total Memory: %d Mb\n", memory);

                    memory = Runtime.getRuntime().maxMemory() / 1048576L;

                    logText += String.format(Locale.getDefault(), "App Max Memory: %d Mb\n", memory);

                    memory = Runtime.getRuntime().freeMemory() / 1048576L;

                    logText += String.format(Locale.getDefault(), "App Free Memory: %d Mb\n", memory);
                }
                catch(NullPointerException e)
                {
                    memoryInfo = null;
                }

                logText += String.format("Bootloader: %s\n", Build.BOOTLOADER);

                logText += "\n\n--- Settings dump ---\n\n";
                logText += settingsManager.dump();

                logText += "\n\n--- Log dump ---\n\n";

                exportLog = getCurrentLog(FormatType.PLAIN_TEXT, LogTime.UTC);

                if (exportLog == null)
                    return;

                for (String entry : exportLog)
                    logText += entry + "\n";

                llProgressSpinner.setVisibility(View.GONE);

                Intent shareIntent = new Intent(Intent.ACTION_SEND);

                shareIntent.setType("text/plain");
                shareIntent.putExtra(Intent.EXTRA_TITLE, getResources().getString(R.string.log_title));
                shareIntent.putExtra(Intent.EXTRA_SUBJECT, logSubject);
                shareIntent.putExtra(Intent.EXTRA_TEXT, logText);

                startActivity(Intent.createChooser(shareIntent, getResources().getString(R.string.log_share_with)));
            }
        });

        btnShare.setOnFocusChangeListener(new View.OnFocusChangeListener()
        {
            @Override
            public void onFocusChange(View v, boolean hasFocus)
            {
                if(!btnShare.isEnabled())
                    return;

                if(hasFocus)
                    btnShare.setBackgroundResource(R.drawable.share_focus);
                else
                    btnShare.setBackgroundResource(R.drawable.share);
            }
        });

        btnShare.setAccessibilityDelegate(new View.AccessibilityDelegate()
        {
            @Override
            public void sendAccessibilityEvent(View host, int eventType)
            {
                super.sendAccessibilityEvent(host, eventType);

                btnShare.setContentDescription(getString(R.string.accessibility_share_log));
            }
        });

        llProgressSpinner = (LinearLayout)findViewById(R.id.llProgressSpinner);

        new loadLogView().execute();
    }

    @Override
    public void onBackPressed()
    {
        finish();
    }

    private ArrayList<String> getCurrentLog(FormatType formatType, LogTime logTime)
    {
        SimpleDateFormat dateFormatter = null;
        Date logCurrenTimeZone = null;
        Calendar calendar = null;
        ArrayList<String> logItem = null;
        ArrayList<EddieLogger.LogItem> eddieLog = null;
        long utcTimeStamp = 0;
        String log = "", logColor = "", levelDescription = "";

        calendar = Calendar.getInstance();

        dateFormatter = new SimpleDateFormat("dd MMM yyyy HH:mm:ss");

        if(logTime == LogTime.UTC)
            dateFormatter.setTimeZone(TimeZone.getTimeZone("GMT"));
        else
            dateFormatter.setTimeZone(TimeZone.getDefault());

        eddieLog = eddieLogger.getLog();

        if(eddieLog == null)
            return null;

        logItem = new ArrayList<String>();

        for(EddieLogger.LogItem item : eddieLog)
        {
            utcTimeStamp = item.utcUnixTimestamp;

            calendar.setTimeInMillis(utcTimeStamp * 1000);
            logCurrenTimeZone = (Date)calendar.getTime();

            if(item.logLevel == Level.SEVERE)
            {
                logColor = "#ff0000";
                levelDescription = "Error";
            }
            else if(item.logLevel == Level.INFO)
            {
                logColor = "#008000";
                levelDescription = "Info";
            }
            else if(item.logLevel == Level.WARNING)
            {
                logColor = "#ff7f50";
                levelDescription = "Warning";
            }
            else if(item.logLevel == Level.FINE)
            {
                logColor = "#800080";
                levelDescription = "Debug";
            }
            else
            {
                logColor = "#000000";
                levelDescription = item.logLevel.toString();
            }

            switch(formatType)
            {
                case HTML:
                {
                    log = "<font color='#0000ff'>" + dateFormatter.format(logCurrenTimeZone) + "</font>";
                    log += " [<font color='" + logColor + "'>" + levelDescription + "</font>]: <font color='#000000'>";
                    log += item.message.replace("\n", "</font><br>");
                }
                break;

                case PLAIN_TEXT:
                {
                    log = dateFormatter.format(logCurrenTimeZone) + " UTC [" + levelDescription +"] " + item.message;
                }
                break;

                default:
                {
                    log = "";
                }
                break;
            }

            logItem.add(log);
        }

        return logItem;
    }

    private ArrayList<Spanned> getSpannedLog()
    {
        ArrayList<Spanned> spannedList = new ArrayList<Spanned>();

        Spanned logSpanned = null;

        for(String item : logEntry)
        {
            if(Build.VERSION.SDK_INT < Build.VERSION_CODES.N)
                logSpanned = Html.fromHtml(item);
            else
                logSpanned = Html.fromHtml(item, Html.FROM_HTML_MODE_LEGACY);

            spannedList.add(logSpanned);
        }

        return spannedList;
    }

    private void loadLogWebView()
    {
        String logHtml = "";

        for(String entry : logEntry)
            logHtml += "<p>" + entry + "</p>";

        webLogView.loadData(logHtml, "text/html", null);
    }

    private class loadLogView extends AsyncTask<Void, Void, Void>
    {
        @Override
        protected void onPreExecute()
        {
            llProgressSpinner.setVisibility(View.VISIBLE);
        }

        @Override
        protected Void doInBackground(Void... p)
        {
            logEntry = getCurrentLog(FormatType.HTML, LogTime.LOCAL);

            return null;
        }

        @Override
        protected void onPostExecute(Void param)
        {
            if(viewMode == MODE_LISTVIEW)
            {
                logListAdapter = new LogListAdapter(LogActivity.this, getSpannedLog());

                listLogView.setAdapter(logListAdapter);
            }
            else
            {
                webLogView.getSettings().setJavaScriptEnabled(false);

                webLogView.setWebViewClient(new WebViewClient());

                webLogView.getSettings().setBuiltInZoomControls(true);
                webLogView.getSettings().setDisplayZoomControls(false);

                loadLogWebView();
            }

            llProgressSpinner.setVisibility(View.GONE);
        }
    }
}
