// <eddie_source_header>
// This file is part of Eddie/AirVPN software.
// Copyright (C)2014-2018 AirVPN (support@airvpn.org) / https://airvpn.org
//
// Eddie is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Eddie is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Eddie. If not, see <http://www.gnu.org/licenses/>.
// </eddie_source_header>
//
// 20 June 2018 - author: ProMIND - initial release. Based on revised code from com.eddie.android. (a tribute to the 1859 Perugia uprising occurred on 20 June 1859 and in memory of those brave inhabitants who fought for the liberty of Perugia)

package org.airvpn.eddie;

import java.util.HashMap;

public class VPN
{
    private static OpenVPNConnectionData vpnConnectionData = null;
    private static OpenVPNTransportStats vpnTransportStats = null;
    private static Status connectionStatus = Status.UNKNOWN;
    private static ConnectionMode connectionMode = ConnectionMode.UNKNOWN;
    private static long inRate = 0;
    private static long outRate = 0;
    private static long maxInRate = 0;
    private static long maxOutRate = 0;
    private static long sessionTimeSeconds = 0;
    private static long totalConnectionTimeSeconds = 0;
    private static HashMap<String, String> profileInfo = null;
    private static HashMap<String, String> pendingProfileInfo = null;
    private static String openVpnProfile = "";
    private static String pendingOpenVpnProfile = "";
    private static String pendingProgressMessage = "";
    private static String connectionModeDescription = "";
    private static String userProfileDescription = "";
    private static String userName = "";
    private static String cipherName = "";
    private static String digest = "";

    public enum Status
    {
        UNKNOWN,
        CONNECTION_ERROR,
        NOT_CONNECTED,
        DISCONNECTING,
        CONNECTING,
        CONNECTED,
        PAUSED_BY_USER,
        PAUSED_BY_SYSTEM,
        LOCKED,
        CONNECTION_REVOKED_BY_SYSTEM
    }

    public enum ConnectionMode
    {
        UNKNOWN,
        QUICK_CONNECT,
        AIRVPN_SERVER,
        OPENVPN_PROFILE,
        BOOT_CONNECT
    }

    public static Status getStatus(int v)
    {
        Status val;

        switch(v)
        {
            case 0:
            {
                val = Status.UNKNOWN;
            }
            break;

            case 1:
            {
                val = Status.CONNECTION_ERROR;
            }
            break;

            case 2:
            {
                val = Status.NOT_CONNECTED;
            }
            break;

            case 3:
            {
                val = Status.DISCONNECTING;
            }
            break;

            case 4:
            {
                val = Status.CONNECTING;
            }
            break;

            case 5:
            {
                val = Status.CONNECTED;
            }
            break;

            case 6:
            {
                val = Status.PAUSED_BY_USER;
            }
            break;

            case 7:
            {
                val = Status.PAUSED_BY_SYSTEM;
            }
            break;

            case 8:
            {
                val = Status.LOCKED;
            }
            break;

            case 9:
            {
                val = Status.CONNECTION_REVOKED_BY_SYSTEM;
            }
            break;

            default:
            {
                val = Status.UNKNOWN;
            }
            break;

        }

        return val;
    }

    public static int statusToInt(Status v)
    {
        int val;

        switch(v)
        {
            case UNKNOWN:
            {
                val = 0;
            }
            break;

            case CONNECTION_ERROR:
            {
                val = 1;
            }
            break;

            case NOT_CONNECTED:
            {
                val = 2;
            }
            break;

            case DISCONNECTING:
            {
                val = 3;
            }
            break;

            case CONNECTING:
            {
                val = 4;
            }
            break;

            case CONNECTED:
            {
                val = 5;
            }
            break;

            case PAUSED_BY_USER:
            {
                val = 6;
            }
            break;

            case PAUSED_BY_SYSTEM:
            {
                val = 7;
            }
            break;

            case LOCKED:
            {
                val = 8;
            }
            break;

            case CONNECTION_REVOKED_BY_SYSTEM:
            {
                val = 9;
            }
            break;

            default:
            {
                val = 0;
            }
            break;

        }

        return val;
    }

    public static ConnectionMode getConnectionMode(int v)
    {
        ConnectionMode val;

        switch(v)
        {
            case 0:
            {
                val = ConnectionMode.UNKNOWN;
            }
            break;

            case 1:
            {
                val = ConnectionMode.QUICK_CONNECT;
            }
            break;

            case 2:
            {
                val = ConnectionMode.AIRVPN_SERVER;
            }
            break;

            case 3:
            {
                val = ConnectionMode.OPENVPN_PROFILE;
            }
            break;

            default:
            {
                val = ConnectionMode.UNKNOWN;
            }
            break;

        }

        return val;
    }

    public static int connectionModeToInt(ConnectionMode v)
    {
        int val;

        switch(v)
        {
            case UNKNOWN:
            {
                val = 0;
            }
            break;

            case QUICK_CONNECT:
            {
                val = 1;
            }
            break;

            case AIRVPN_SERVER:
            {
                val = 2;
            }
            break;

            case OPENVPN_PROFILE:
            {
                val = 3;
            }
            break;

            case BOOT_CONNECT:
            {
                val = 3;
            }
            break;

            default:
            {
                val = 0;
            }
            break;

        }

        return val;
    }

    public static int descriptionResource(Status s)
    {
        int res = 0;

        switch(s)
        {
            case CONNECTION_ERROR:
            {
                res = R.string.vpn_status_connection_error;
            }
            break;

            case NOT_CONNECTED:
            {
                res = R.string.vpn_status_not_connected;
            }
            break;

            case DISCONNECTING:
            {
                res = R.string.vpn_status_disconnecting;
            }
            break;

            case CONNECTING:
            {
                res = R.string.vpn_status_connecting;
            }
            break;

            case CONNECTED:
            {
                res = R.string.vpn_status_connected;
            }
            break;

            case PAUSED_BY_USER:
            case PAUSED_BY_SYSTEM:
            {
                res = R.string.vpn_status_paused;
            }
            break;

            case LOCKED:
            {
                res = R.string.vpn_status_locked;
            }
            break;

            case CONNECTION_REVOKED_BY_SYSTEM:
            {
                res = R.string.vpn_status_revoked;
            }
            break;

            default:
            {
                res = R.string.vpn_status_not_connected;
            }
            break;
        }

        return res;
    }

    public static void setProfileInfo(HashMap<String, String> p)
    {
        profileInfo = p;
    }

    public static HashMap<String, String> getProfileInfo()
    {
        return profileInfo;
    }

    public static void setOpenVpnProfile(String p)
    {
        openVpnProfile = p;
    }

    public static String getOpenVpnProfile()
    {
        return openVpnProfile;
    }

    public static void setPendingProfileInfo(HashMap<String, String> p)
    {
        pendingProfileInfo = p;
    }

    public static HashMap<String, String> getPendingProfileInfo()
    {
        return pendingProfileInfo;
    }

    public static void setPendingOpenVpnProfile(String p)
    {
        pendingOpenVpnProfile = p;
    }

    public static String getPendingOpenVpnProfile()
    {
        return pendingOpenVpnProfile;
    }

    public static void setPendingProgressMessage(String m)
    {
        pendingProgressMessage = m;
    }

    public static String getPendingProgressMessage()
    {
        return pendingProgressMessage;
    }

    public static void setVpnConnectionData(OpenVPNConnectionData cdata)
    {
        vpnConnectionData = cdata;
    }

    public static OpenVPNConnectionData getVpnConnectionData()
    {
        return vpnConnectionData;
    }

    public static void setVpnTransportStats(OpenVPNTransportStats stats)
    {
        if(stats == null)
            return;

        vpnTransportStats = stats;
    }

    public static OpenVPNTransportStats getVpnTransportStats()
    {
        return vpnTransportStats;
    }

    public static void setInRate(long v)
    {
        inRate = v;
    }

    public static long getInRate()
    {
        return inRate;
    }

    public static void setOutRate(long v)
    {
        outRate = v;
    }

    public static long getOutRate()
    {
        return outRate;
    }

    public static void setMaxInRate(long v)
    {
        maxInRate = v;
    }

    public static long getMaxInRate()
    {
        return maxInRate;
    }

    public static void setMaxOutRate(long v)
    {
        maxOutRate = v;
    }

    public static long getMaxOutRate()
    {
        return maxOutRate;
    }

    public static void setConnectionStatus(Status s)
    {
        connectionStatus = s;
    }

    public static Status getConnectionStatus()
    {
        return connectionStatus;
    }

    public static void setConnectionMode(ConnectionMode m)
    {
        connectionMode = m;
    }

    public static ConnectionMode getConnectionMode()
    {
        return connectionMode;
    }

    public static void resetTotalConnectionTime()
    {
        totalConnectionTimeSeconds = 0;
    }

    public static void resetSessionTime()
    {
        sessionTimeSeconds = 0;
    }

    public static void addSecondsConnectionTime(long s)
    {
        sessionTimeSeconds += s;

        totalConnectionTimeSeconds += s;
    }

    public static long getSessionTimeSeconds()
    {
        return sessionTimeSeconds;
    }

    public static long getTotalConnectionTimeSeconds()
    {
        return totalConnectionTimeSeconds;
    }

    public static String getSessionTime()
    {
        return getFormattedTime(sessionTimeSeconds);
    }

    public static String getTotalConnectionTime()
    {
        return getFormattedTime(totalConnectionTimeSeconds);
    }

    public static void setConnectionModeDescription(String d)
    {
        connectionModeDescription = d;
    }

    public static String getConnectionModeDescription()
    {
        return connectionModeDescription;
    }

    public static void setUserProfileDescription(String p)
    {
        userProfileDescription = p;
    }

    public static String getUserProfileDescription()
    {
        return userProfileDescription;
    }

    public static void setUserName(String u)
    {
        userName = u;
    }

    public static String getUserName()
    {
        return userName;
    }

    public static void setCipherName(String n)
    {
        cipherName = n;
    }

    public static String getCipherName()
    {
        return cipherName;
    }

    public static void setDigest(String d)
    {
        digest = d;
    }

    public static String getDigest()
    {
        return digest;
    }

    private static String getFormattedTime(long s)
    {
        long hours, minutes, seconds;

        hours = s / 3600;
        minutes = (s % 3600) / 60;
        seconds = s % 60;

        return String.format("%02d:%02d:%02d", hours, minutes, seconds);
    }
}
