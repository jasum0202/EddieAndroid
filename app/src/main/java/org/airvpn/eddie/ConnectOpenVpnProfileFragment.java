// <eddie_source_header>
// This file is part of Eddie/AirVPN software.
// Copyright (C)2014-2018 AirVPN (support@airvpn.org) / https://airvpn.org
//
// Eddie is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Eddie is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Eddie. If not, see <http://www.gnu.org/licenses/>.
// </eddie_source_header>
//
// 3 October 2018 - author: ProMIND - initial release. (a tribute to the 1859 Perugia uprising occurred on 20 June 1859 and in memory of those brave inhabitants who fought for the liberty of Perugia)

package org.airvpn.eddie;

import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.ContextMenu;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;

import org.w3c.dom.Document;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

public class ConnectOpenVpnProfileFragment extends Fragment implements NetworkStatusListener, EddieEventListener
{
    private final int FRAGMENT_ID = 5003;

    private static VPNManager vpnManager = null;
    private NetworkStatusReceiver networkStatusReceiver = null;

    private ScrollView scrollView = null;

    private Button btnSelectProfile = null, btnConnectProfile = null;

    private TextView txtOpenVpnProfilesCap = null, txtNoOpenVpnProfiles = null, txtProfileFileName = null;
    private TextView txtServerName = null, txtServerPort = null, txtServerProtocol = null;
    private TextView txtVpnStatus = null, txtNetworkStatus = null;

    private LinearLayout llServerInfo = null;

    private ListView lvOpenVpnProfiles = null;

    private Uri profileUri = null;
    private String profileData = "", currentVpnStatusDescription = "";
    private HashMap<String, String> profileInfo = null;

    private boolean loadExternalUriProfile = false;

    private SupportTools supportTools = null;
    private EddieEvent eddieEvent = null;
    private SettingsManager settingsManager = null;
    private OpenVPNProfileDatabase openVPNProfileDatabase = null;

    public void setVpnManager(VPNManager v)
    {
        vpnManager = v;
    }

    public class ProfileAdapter extends ArrayAdapter<String>
    {
        public ProfileAdapter(Context context, ArrayList<String> profiles)
        {
            super(context, 0, profiles);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent)
        {
            String profile = getItem(position);

            if(convertView == null)
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.openvpn_profile_item, parent, false);

            TextView txtProfile = (TextView)convertView.findViewById(R.id.profile_name);

            txtProfile.setText(profile);

            return convertView;
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        supportTools = new SupportTools(getActivity());
        settingsManager = new SettingsManager(getActivity());

        openVPNProfileDatabase = new OpenVPNProfileDatabase(getActivity());

        networkStatusReceiver = new NetworkStatusReceiver(getContext());
        networkStatusReceiver.addListener(this);

        eddieEvent = new EddieEvent();
        eddieEvent.addListener(this);
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();

        networkStatusReceiver.removeListener(this);

        eddieEvent.removeListener(this);
    }

    @Override
    public void onResume()
    {
        super.onResume();

        if(txtNetworkStatus != null)
        {
            if (NetworkStatusReceiver.getNetworkStatus() == NetworkStatusReceiver.Status.CONNECTED)
                txtNetworkStatus.setText(String.format(Locale.getDefault(), getResources().getString(R.string.conn_status_connected), NetworkStatusReceiver.getNetworkDescription()));
            else
                txtNetworkStatus.setText(getResources().getString(R.string.conn_status_not_available));
        }

        if(txtVpnStatus != null)
            txtVpnStatus.setText(currentVpnStatusDescription);

        if(lvOpenVpnProfiles != null)
            setupProfileListView();

        setConnectButton();
    }

    @Override
    public void setUserVisibleHint(boolean isVisible)
    {
        super.setUserVisibleHint(isVisible);

        if(isVisible && lvOpenVpnProfiles != null)
            setupProfileListView();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View fragmentView = inflater.inflate(R.layout.fragment_openvpn_profile_layout, container, false);

        if(fragmentView != null)
        {
            scrollView = (ScrollView)fragmentView.findViewById(R.id.scroll_view);

            btnSelectProfile = (Button)fragmentView.findViewById(R.id.select_profile_btn);

            btnSelectProfile.setOnClickListener(new View.OnClickListener()
            {
                public void onClick(View arg0)
                {
                    onClickSelectProfileButton();
                }
            });

            txtOpenVpnProfilesCap = (TextView)fragmentView.findViewById(R.id.openvpn_profiles_title);

            txtNoOpenVpnProfiles = (TextView)fragmentView.findViewById(R.id.no_openvpn_profiles);

            txtProfileFileName = (TextView)fragmentView.findViewById(R.id.profile_filename);
            txtProfileFileName.setText(getResources().getString(R.string.conn_no_profile));

            llServerInfo = (LinearLayout)fragmentView.findViewById(R.id.server_info_layout);

            txtServerName = (TextView)fragmentView.findViewById(R.id.profile_server);
            txtServerName.setText("");

            txtServerPort = (TextView)fragmentView.findViewById(R.id.profile_port);
            txtServerPort.setText("");

            txtServerProtocol = (TextView)fragmentView.findViewById(R.id.profile_protocol);
            txtServerProtocol.setText("");

            btnConnectProfile = (Button)fragmentView.findViewById(R.id.connect_profile_btn);

            btnConnectProfile.setOnClickListener(new View.OnClickListener()
            {
                public void onClick(View arg0)
                {
                    if(profileInfo == null)
                    {
                        supportTools.enableButton(btnConnectProfile, false);

                        return;
                    }

                    final Runnable uiRunnable = new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            connectOpenVPNProfile(profileInfo.get("name"));
                        }
                    };

                    supportTools.runOnUiActivity(getActivity(), uiRunnable);
                }
            });

            if(settingsManager.isSystemRestoreLastProfile())
                restoreLastProfile();
            else
            {
                txtProfileFileName.setText(getResources().getString(R.string.conn_no_profile));

                llServerInfo.setVisibility(View.GONE);
            }

            if(currentVpnStatusDescription.isEmpty())
                currentVpnStatusDescription = getResources().getString(VPN.descriptionResource(VPN.getConnectionStatus()));

            txtVpnStatus = (TextView)fragmentView.findViewById(R.id.vpn_connection_status);
            txtVpnStatus.setText(currentVpnStatusDescription);

            txtNetworkStatus = (TextView)fragmentView.findViewById(R.id.network_connection_status);

            if(NetworkStatusReceiver.getNetworkStatus() == NetworkStatusReceiver.Status.CONNECTED)
                txtNetworkStatus.setText(String.format(Locale.getDefault(), getResources().getString(R.string.conn_status_connected), NetworkStatusReceiver.getNetworkDescription()));
            else
                txtNetworkStatus.setText(getResources().getString(R.string.conn_status_not_available));

            lvOpenVpnProfiles = (ListView)fragmentView.findViewById(R.id.openvpn_profiles);

            lvOpenVpnProfiles.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
            {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
                {
                    if(!lvOpenVpnProfiles.hasFocus())
                        return;

                    View itemView = lvOpenVpnProfiles.getChildAt(position);

                    if(itemView == null)
                        return;

                    Display display = getActivity().getWindowManager().getDefaultDisplay();
                    Point size = new Point();

                    display.getSize(size);

                    int ypos = itemView.getTop() + lvOpenVpnProfiles.getTop() - (size.y / 2);

                    scrollView.scrollTo(0, ypos);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent)
                {
                }
            });

            lvOpenVpnProfiles.setOnItemClickListener(new AdapterView.OnItemClickListener()
            {
                @Override
                public void onItemClick(final AdapterView<?>adapter, final View v, final int position, final long id)
                {
                    final Runnable uiRunnable = new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            connectOpenVPNProfile(adapter.getItemAtPosition(position).toString());
                        }
                    };

                    supportTools.runOnUiActivity(getActivity(), uiRunnable);
                }
            });

            registerForContextMenu(lvOpenVpnProfiles);

            setConnectButton();

            setupProfileListView();

            if(loadExternalUriProfile == true)
            {
                selectOpenVPNProfile(profileUri);

                loadExternalUriProfile = false;
            }
        }

        return fragmentView;
    }
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo)
    {
        super.onCreateContextMenu(menu, v, menuInfo);

        if(v.getId() == R.id.openvpn_profiles)
        {
            MenuInflater inflater = getActivity().getMenuInflater();

            inflater.inflate(R.menu.openvpn_profile_menu, menu);

            for(int i = 0; i < menu.size(); i++)
            {
                MenuItem item = menu.getItem(i);

                if(item != null)
                {
                    Intent intent = new Intent();
                    intent.putExtra("fragment_id", FRAGMENT_ID);

                    item.setIntent(intent);
                }
            }
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem menuItem)
    {
        boolean processed = false;

        Intent intent = menuItem.getIntent();

        if(intent != null && intent.getIntExtra("fragment_id", -1) != FRAGMENT_ID)
            return false;

        AdapterView.AdapterContextMenuInfo menuInfo = (AdapterView.AdapterContextMenuInfo) menuItem.getMenuInfo();
        final String listViewItemValue = lvOpenVpnProfiles.getAdapter().getItem(menuInfo.position).toString();

        switch(menuItem.getItemId())
        {
            case R.id.context_menu_connect_openvpn_profile:
            {
                connectOpenVPNProfile(listViewItemValue);

                processed = true;
            }
            break;

            case R.id.context_menu_rename_openvpn_profile:
            {
                Runnable runnable = new Runnable()
                {
                    @Override
                    public void run()
                    {
                        renameOpenVPNProfile(listViewItemValue);
                    }
                };

                supportTools.runOnUiActivity(getActivity(), runnable);

                processed = true;
            }
            break;

            case R.id.context_menu_delete_openvpn_profile:
            {
                Runnable runnable = new Runnable()
                {
                    @Override
                    public void run()
                    {
                        deleteOpenVPNProfile(listViewItemValue);
                    }
                };

                supportTools.runOnUiActivity(getActivity(), runnable);

                processed = true;
            }
            break;
        }

        return processed;
    }

    private void setupProfileListView()
    {
        ArrayList<String> profileList = openVPNProfileDatabase.getProfileNameList(OpenVPNProfileDatabase.SortMode.SORT_NAMES);

        ProfileAdapter profileAdapter = new ProfileAdapter(getActivity(), profileList);

        txtOpenVpnProfilesCap.setText(String.format(Locale.getDefault(), getResources().getString(R.string.openvpn_profiles_cap), profileAdapter.getCount()));

        if(profileAdapter.getCount() > 0)
        {
            lvOpenVpnProfiles.setVisibility(View.VISIBLE);
            txtNoOpenVpnProfiles.setVisibility(View.GONE);

            lvOpenVpnProfiles.setAdapter(profileAdapter);

            expandListViewHeight(lvOpenVpnProfiles);
        }
        else
        {
            lvOpenVpnProfiles.setVisibility(View.GONE);
            txtNoOpenVpnProfiles.setVisibility(View.VISIBLE);
        }
    }

    public void loadExternalProfile(Uri profile)
    {
        profileUri = profile;

        loadExternalUriProfile = true;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if(data != null)
        {
            loadExternalUriProfile = false;

            profileUri = data.getData();

            selectOpenVPNProfile(profileUri);
        }
    }

    private void onClickSelectProfileButton()
    {
        Intent fileChooserIntent = new Intent();

        if(fileChooserIntent != null)
        {
            fileChooserIntent.setAction(Intent.ACTION_GET_CONTENT);
            fileChooserIntent.setType("*/*");

            startActivityForResult(Intent.createChooser(fileChooserIntent, getResources().getString(R.string.conn_select_profile_cap)), MainActivity.ACTIVITY_RESULT_FILE_CHOOSER);
        }
        else
            supportTools.infoDialog(R.string.cannot_open_web_browser, true);
    }

    public void selectOpenVPNProfile(Uri profile)
    {
        if(profile == null)
            return;

        profileInfo = supportTools.getOpenVPNProfile(profile);

        if(profileInfo != null)
        {
            if(profileInfo.get("status").equals("ok"))
            {
                profileData = profileInfo.get("profile");

                if(profileInfo.containsKey("name") == true)
                    txtProfileFileName.setText(profileInfo.get("name"));
                else
                    txtProfileFileName.setText("???");

                profileInfo.put("description", "");

                if(profileInfo.containsKey("server") == true)
                    txtServerName.setText(profileInfo.get("server"));
                else
                    txtServerName.setText("???");

                if(profileInfo.containsKey("port") == true)
                    txtServerPort.setText(profileInfo.get("port"));
                else
                    txtServerPort.setText("???");

                if(profileInfo.containsKey("protocol") == true)
                    txtServerProtocol.setText(profileInfo.get("protocol"));
                else
                    txtServerProtocol.setText("???");

                llServerInfo.setVisibility(View.VISIBLE);

                supportTools.enableButton(btnConnectProfile, true);

                Runnable runnable = new Runnable()
                {
                    @Override
                    public void run()
                    {
                        addOpenVPNProfile(profileInfo, profileData);
                    }
                };

                supportTools.runOnUiActivity(getActivity(), runnable);
            }
            else
            {
                int errMsg = 0;

                if(profileInfo.containsKey("status") == true)
                {
                    if(profileInfo.get("status").equals("not_found"))
                        errMsg = R.string.conn_profile_not_found;
                    else if(profileInfo.get("status").equals("invalid"))
                        errMsg = R.string.conn_profile_is_invalid;
                    else if(profileInfo.get("status").equals("no_permission"))
                        errMsg = R.string.conn_profile_no_permission;

                    supportTools.infoDialog(errMsg, true);
                }

                profileData = "";

                profileInfo.put("mode", String.format(Locale.getDefault(), "%d", VPN.connectionModeToInt(VPN.ConnectionMode.UNKNOWN)));
                profileInfo.put("name", "");
                profileInfo.put("description", "");
                profileInfo.put("server", "");
                profileInfo.put("port", "");
                profileInfo.put("protocol", "");

                txtProfileFileName.setText(getResources().getString(R.string.conn_no_profile));

                llServerInfo.setVisibility(View.GONE);
            }

            saveCurrentProfile();

            VPN.setProfileInfo(profileInfo);

            VPN.setConnectionMode(VPN.ConnectionMode.OPENVPN_PROFILE);

            VPN.setConnectionModeDescription(getResources().getString(R.string.conn_type_openvpn_profile));

            VPN.setUserProfileDescription("");

            VPN.setUserName("");
        }
    }

    private void saveCurrentProfile()
    {
        if(profileInfo == null || profileData.isEmpty())
            return;

        if(!profileData.isEmpty())
            settingsManager.setLastOpenVPNProfile(profileData);

        if(profileInfo != null)
            settingsManager.setSystemLastProfileInfo(profileInfo);
    }

    private void restoreLastProfile()
    {
        profileData = settingsManager.getLastOpenVPNProfile();

        profileInfo = settingsManager.getSystemLastProfileInfo();

        if(profileInfo != null && profileInfo.size() > 0 && !profileData.equals(""))
        {
            profileInfo.put("description", "");

            if(profileInfo.containsKey("name"))
                txtProfileFileName.setText(profileInfo.get("name"));
            else
                txtProfileFileName.setText(getResources().getString(R.string.conn_status_unknown));

            if(profileInfo.containsKey("server"))
                txtServerName.setText(profileInfo.get("server"));
            else
                txtServerName.setText(getResources().getString(R.string.conn_status_unknown));

            if(profileInfo.containsKey("port"))
                txtServerPort.setText(profileInfo.get("port"));
            else
                txtServerPort.setText(getResources().getString(R.string.conn_status_unknown));

            if(profileInfo.containsKey("protocol"))
                txtServerProtocol.setText(profileInfo.get("protocol"));
            else
                txtServerProtocol.setText(getResources().getString(R.string.conn_status_unknown));

            llServerInfo.setVisibility(View.VISIBLE);

            supportTools.enableButton(btnConnectProfile, true);
        }
        else
        {
            txtProfileFileName.setText(getResources().getString(R.string.conn_no_profile));

            llServerInfo.setVisibility(View.GONE);
        }
    }

    private String getOpenVPNProfileName(String profileName)
    {
        boolean nameIsValid = false;
        String newProfileName = profileName;
        int dialogTitle = -1;

        if(profileName == null)
            return "";

        dialogTitle = R.string.openvpn_profile_rename;

        while(!nameIsValid)
        {
            newProfileName = supportTools.getTextOptionDialog(dialogTitle, profileName, SupportTools.EditOption.DO_NOT_ALLOW_EMPTY_FIELD);

            if(!profileName.equals(newProfileName))
            {
                if(openVPNProfileDatabase.exists(newProfileName))
                    dialogTitle = R.string.openvpn_profile_name_exists;
                else
                    nameIsValid = true;
            }
            else
                nameIsValid = true;
        }

        return newProfileName;
    }

    private boolean addOpenVPNProfile(HashMap<String, String> pInfo, String pData)
    {
        boolean profileNameExists = false;
        OpenVPNProfileDatabase.OpenVPNProfile openVPNProfile = null;
        String profileName = "", newProfileName = "";
        int value;
        OpenVPNProfileDatabase.ProtocolType proto = OpenVPNProfileDatabase.ProtocolType.UNKNOWN;

        if(pInfo == null || pInfo.get("name") == null || pData == null || pData.isEmpty())
            return false;

        if(openVPNProfileDatabase == null || pInfo.get("name").isEmpty() || pInfo.get("name").equals("???"))
            return false;

        profileName = pInfo.get("name");

        profileNameExists = openVPNProfileDatabase.exists(profileName);

        if(profileNameExists)
        {
            while(profileNameExists)
            {
                newProfileName = supportTools.getTextOptionDialog(R.string.openvpn_profile_name_exists, profileName, SupportTools.EditOption.DO_NOT_ALLOW_EMPTY_FIELD);

                if(!profileName.equals(newProfileName))
                    profileNameExists = openVPNProfileDatabase.exists(newProfileName);
                else
                    return false;
            }

            pInfo.put("name", newProfileName);
        }

        openVPNProfile = openVPNProfileDatabase.new OpenVPNProfile();

        openVPNProfile.setName(profileInfo.get("name"));
        openVPNProfile.setServer(profileInfo.get("server"));

        try
        {
            value = Integer.parseInt(profileInfo.get("port"));
        }
        catch(NumberFormatException e)
        {
            value = 0;
        }

        openVPNProfile.setPort(value);

        switch(profileInfo.get("protocol").toUpperCase())
        {
            case "UDP":
            {
                proto = OpenVPNProfileDatabase.ProtocolType.UDPv4;
            }
            break;

            case "TCP":
            {
                proto = OpenVPNProfileDatabase.ProtocolType.TCPv4;
            }
            break;

            case "UDP6":
            {
                proto = OpenVPNProfileDatabase.ProtocolType.UDPv6;
            }
            break;

            case "TCP6":
            {
                proto = OpenVPNProfileDatabase.ProtocolType.TCPv6;
            }
            break;
        }

        openVPNProfile.setProtocol(proto);

        openVPNProfile.setProfile(profileData);

        openVPNProfileDatabase.setProfile(profileInfo.get("name"), openVPNProfile);

        setupProfileListView();

        return true;
    }

    private void renameOpenVPNProfile(String profileName)
    {
        if(profileName == null || profileName.isEmpty())
            return;

        String newProfileName = getOpenVPNProfileName(profileName);

        openVPNProfileDatabase.renameProfile(profileName, newProfileName);

        setupProfileListView();
    }

    private void deleteOpenVPNProfile(String profileName)
    {
        if(profileName == null || profileName.isEmpty())
            return;

        if(!supportTools.confirmationDialog(String.format(getResources().getString(R.string.openvpn_profile_delete), profileName)))
            return;

        openVPNProfileDatabase.deleteProfile(profileName);

        setupProfileListView();
    }

    private void connectOpenVPNProfile(String profileName)
    {
        String progressMessage = "";

        VPN.Status currentConnectionStatus = VPN.getConnectionStatus();

        if(profileName == null || profileName.isEmpty() || !NetworkStatusReceiver.isNetworkConnected())
            return;

        OpenVPNProfileDatabase.OpenVPNProfile openVPNProfile = openVPNProfileDatabase.getProfile(profileName);

        if(openVPNProfile == null)
        {
            supportTools.infoDialog(R.string.openvpn_profile_not_found, true);

            return;
        }

        profileData = openVPNProfile.getProfile();

        profileInfo = new HashMap<String, String>();

        profileInfo.put("mode", String.format(Locale.getDefault(), "%d", VPN.connectionModeToInt(VPN.ConnectionMode.OPENVPN_PROFILE)));
        profileInfo.put("name", openVPNProfile.getName());
        profileInfo.put("description", "");
        profileInfo.put("server", openVPNProfile.getServer());
        profileInfo.put("port", String.format("%d", openVPNProfile.getPort()));
        profileInfo.put("protocol", openVPNProfile.getProtocol().toString());

        txtProfileFileName.setText(profileInfo.get("name"));
        txtServerName.setText(profileInfo.get("server"));
        txtServerPort.setText(profileInfo.get("port"));
        txtServerProtocol.setText(profileInfo.get("protocol"));

        progressMessage = String.format(getResources().getString(R.string.conn_try_connection), profileInfo.get("server"), profileInfo.get("protocol"), profileInfo.get("port"));

        if(currentConnectionStatus == VPN.Status.CONNECTING || currentConnectionStatus == VPN.Status.CONNECTED || currentConnectionStatus == VPN.Status.PAUSED_BY_USER || currentConnectionStatus == VPN.Status.PAUSED_BY_SYSTEM || currentConnectionStatus == VPN.Status.LOCKED)
        {
            HashMap<String, String> currentProfile = VPN.getProfileInfo();
            String message = "", serverDescription = "";

            if(currentProfile != null)
            {
                if(currentProfile.containsKey("description") && !currentProfile.get("description").isEmpty())
                    serverDescription = String.format("AirVPN %s (%s)", currentProfile.get("description"), currentProfile.get("server"));
                else
                    serverDescription = currentProfile.get("server");
            }

            message = String.format(Locale.getDefault(), getResources().getString(R.string.airvpn_server_connection_warning), serverDescription);
            message += " ";
            message += String.format(Locale.getDefault(), getResources().getString(R.string.openvpn_profile_connect), profileName);

            if(supportTools.confirmationDialog(message))
            {
                try
                {
                    vpnManager.stop();

                    VPN.setPendingProfileInfo(profileInfo);
                    VPN.setPendingOpenVpnProfile(profileData);
                    VPN.setPendingProgressMessage(progressMessage);

                    return;
                }
                catch(Exception e)
                {
                    EddieLogger.error("ConnectAirVPNServerFragment.connectServer(): vpnManager.stop() exception: %s", e.getMessage());
                }
            }
            else
                return;
        }
        else
        {
            if(!supportTools.confirmationDialog(String.format(Locale.getDefault(), getResources().getString(R.string.openvpn_profile_connect), profileName)))
                return;
        }

        supportTools.showConnectionProgressDialog(progressMessage);

        onStartConnection();
    }

    private void onStartConnection()
    {
        String progressMessage = "";

        VPN.Status currentConnectionStatus = VPN.getConnectionStatus();

        if(profileData.equals(""))
        {
            supportTools.infoDialog(R.string.conn_no_profile_selected, true);

            return;
        }

        saveCurrentProfile();

        VPN.setProfileInfo(profileInfo);

        VPN.setConnectionMode(VPN.ConnectionMode.OPENVPN_PROFILE);

        VPN.setConnectionModeDescription(getResources().getString(R.string.conn_type_openvpn_profile));

        VPN.setUserProfileDescription("");

        VPN.setUserName("");

        try
        {
            startConnection();
        }
        catch(Exception e)
        {
            EddieLogger.error("MainActivity.onStartConnection() exception: %s", e.getMessage());
        }
    }

    public void updateVpnStatus(String vpnStatus)
    {
        if(txtVpnStatus != null)
            txtVpnStatus.setText(vpnStatus);

        currentVpnStatusDescription = vpnStatus;

        setConnectButton();
    }

    private void startConnection()
    {
        if(vpnManager == null)
        {
            EddieLogger.error("ConnectOpenVpnProfileFragment.startConnection(): vpnManager is null");

            return;
        }

        if(profileData.equals(""))
        {
            supportTools.infoDialog(R.string.conn_no_profile_selected, true);

            return;
        }

        EddieLogger.info(String.format(Locale.getDefault(), "Trying to connect user OpenVPN profile"));

        vpnManager.clearProfile();

        vpnManager.setProfile(profileData);

        String profileString = settingsManager.getOvpn3CustomDirectives().trim();

        if(profileString.length() > 0)
            vpnManager.addProfileString(profileString);

        vpnManager.start();
    }

    public void setConnectButton()
    {
        boolean enabled = false;

        if(btnConnectProfile == null)
            return;

        VPN.Status status = VPN.getConnectionStatus();

        switch(status)
        {
            case CONNECTING:
            case DISCONNECTING:
            case PAUSED_BY_USER:
            case PAUSED_BY_SYSTEM:
            case LOCKED:
            {
                enabled = false;
            }
            break;

            case CONNECTED:
            case NOT_CONNECTED:
            case CONNECTION_REVOKED_BY_SYSTEM:
            case UNKNOWN:
            {
                enabled = true;
            }
            break;

            default:
            {
                enabled = true;
            }
            break;
        }

        if(!NetworkStatusReceiver.isNetworkConnected())
            enabled = false;

        supportTools.enableButton(btnConnectProfile, enabled);
    }

    private void expandListViewHeight(ListView listView)
    {
        ListAdapter listAdapter = listView.getAdapter();

        if(listAdapter != null)
        {
            int items = listAdapter.getCount();

            int itemsHeight = 0;

            for(int pos = 0; pos < items; pos++)
            {
                View item = listAdapter.getView(pos, null, listView);

                item.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED), View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED)); // (View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);

                itemsHeight += item.getMeasuredHeight();
            }

            int dividersHeight = listView.getDividerHeight() * (items - 1);

            ViewGroup.LayoutParams params = listView.getLayoutParams();

            params.height = itemsHeight + dividersHeight;

            listView.setLayoutParams(params);

            listView.requestLayout();
        }
    }

    // Eddie events

    public void onVpnConnectionDataChanged(final OpenVPNConnectionData connectionData)
    {
    }

    public void onVpnStatusChanged(final VPN.Status vpnStatus, final String message)
    {
        Runnable runnable = new Runnable()
        {
            @Override
            public void run()
            {
                updateVpnStatus(message);
            }
        };

        supportTools.runOnUiActivity(getActivity(), runnable);
    }

    public void onVpnAuthFailed(final OpenVPNEvent oe)
    {
        Runnable runnable = new Runnable()
        {
            @Override
            public void run()
            {
                setConnectButton();

                updateVpnStatus(getResources().getString(R.string.conn_auth_failed));
            }
        };

        supportTools.runOnUiActivity(getActivity(), runnable);
    }

    public void onVpnError(final OpenVPNEvent oe)
    {
    }

    public void onMasterPasswordChanged()
    {
    }

    public void onAirVPNLogin()
    {
    }

    public void onAirVPNLoginFailed()
    {
    }

    public void onAirVPNLogout()
    {
    }

    public void onAirVPNUserDataChanged()
    {
    }

    public void onAirVPNUserProfileReceived(Document document)
    {
    }

    public void onAirVPNUserProfileDownloadError()
    {
    }

    public void onAirVPNUserProfileChanged()
    {
    }

    public void onAirVPNManifestReceived(Document document)
    {
    }

    public void onAirVPNManifestDownloadError()
    {
    }

    public void onAirVPNManifestChanged()
    {
    }

    public void onAirVPNIgnoredDocumentRequest()
    {
    }

    public void onCancelConnection()
    {
    }

    // NetworkStatusReceiver

    public void onNetworkStatusNotAvailable()
    {
        if(txtNetworkStatus != null)
            txtNetworkStatus.setText(getResources().getString(R.string.conn_status_not_available));

        setConnectButton();
    }

    public void onNetworkStatusConnected()
    {
        if(txtNetworkStatus != null)
            txtNetworkStatus.setText(String.format(Locale.getDefault(), getResources().getString(R.string.conn_status_connected), NetworkStatusReceiver.getNetworkDescription()));

        setConnectButton();
    }

    public void onNetworkStatusIsConnecting()
    {
        if(txtNetworkStatus != null)
            txtNetworkStatus.setText(getResources().getString(R.string.conn_status_not_available));

        setConnectButton();
    }

    public void onNetworkStatusIsDisonnecting()
    {
        if(txtNetworkStatus != null)
            txtNetworkStatus.setText(getResources().getString(R.string.conn_status_not_available));

        setConnectButton();
    }

    public void onNetworkStatusSuspended()
    {
        if(txtNetworkStatus != null)
            txtNetworkStatus.setText(getResources().getString(R.string.conn_status_not_available));

        setConnectButton();
    }

    public void onNetworkStatusNotConnected()
    {
        if(txtNetworkStatus != null)
            txtNetworkStatus.setText(getResources().getString(R.string.conn_status_disconnected));

        setConnectButton();
    }

    public void onNetworkTypeChanged()
    {
        if(txtNetworkStatus != null)
            txtNetworkStatus.setText(getResources().getString(R.string.conn_status_disconnected));

        setConnectButton();
    }
}