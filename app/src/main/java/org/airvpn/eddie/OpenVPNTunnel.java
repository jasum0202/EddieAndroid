// <eddie_source_header>
// This file is part of Eddie/AirVPN software.
// Copyright (C)2014-2018 AirVPN (support@airvpn.org) / https://airvpn.org
//
// Eddie is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Eddie is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Eddie. If not, see <http://www.gnu.org/licenses/>.
// </eddie_source_header>
//
// 20 June 2018 - author: ProMIND - initial release. Based on revised code from com.eddie.android. (a tribute to the 1859 Perugia uprising occurred on 20 June 1859 and in memory of those brave inhabitants who fought for the liberty of Perugia)

package org.airvpn.eddie;

import android.os.ParcelFileDescriptor;

import java.util.EmptyStackException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Stack;

public class OpenVPNTunnel implements VPNTunnelInterface
{
    private VPNService vpnService = null;
    private OpenVPNClient openVPNClient = null;
    private EddieEvent eddieEvent = null;

    private Stack<VPNContext> vpnContext = new Stack<VPNContext>();

    private SettingsManager settingsManager = null;
    private EddieLogger eddieLogger = null;

    private int maxUdpPartialSendErrors = 5;
    private int udpPartialSendErrorCount = 0;

    private boolean isVPNLockEnabled = true;

    private static final String vpnLockDisabledLogWarning = "VPN Lock has been disabled. Best effort leaks prevention explicitly turned off by user. Eddie will ignore this error.";

    public enum VPNAction
    {
        SYSTEM_PAUSE,
        USER_PAUSE,
        SYSTEM_RESUME,
        USER_RESUME,
        LOCK,
        NETWORK_TYPE_CHANGED
    }

    public OpenVPNTunnel(VPNService service)
    {
        vpnService = service;

        settingsManager = new SettingsManager(service);

        eddieLogger = new EddieLogger();

        eddieLogger.init(vpnService);

        udpPartialSendErrorCount = 0;

        eddieEvent = new EddieEvent();
    }

    protected void finalize() throws Throwable
    {
        try
        {
            if(openVPNClient != null)
                openVPNClient.destroy(this);
        }
        catch(Exception e)
        {
            EddieLogger.error("OpenVPNTunnel.finalize(): Exception: ", e);
        }
        finally
        {
            try
            {
                super.finalize();
            }
            catch(Exception e)
            {
                EddieLogger.error("OpenVPNTunnel.finalize(): Exception: ", e);
            }
        }
    }

    public VPNService getService()
    {
        return vpnService;
    }

    public synchronized VPNContext getActiveContext() throws Exception
    {
        VPNContext context = null;

        try
        {
            context = vpnContext.peek();

            if(context == null)
                throw new Exception("internal error (cannot get a valid context)");
        }
        catch(EmptyStackException e)
        {
            context = null;
        }

        return context;
    }

    private boolean onSocketProtect(int socket)
    {
        boolean result = false;

        EddieLogger.debug("OpenVPNTunnel.onSocketProtect(socket: %d)", socket);

        try
        {
            result = vpnService.protect(socket);
        }
        catch(Exception e)
        {
            EddieLogger.error("OpenVPNTunnel.onSocketProtect()", e);
        }

        return result;
    }

    private boolean onTunBuilderNew()
    {
        EddieLogger.debug("OpenVPNTunnel.onTunBuilderNew()");

        try
        {
            return onTunBuilderNewImpl();
        }
        catch(Exception e)
        {
            EddieLogger.error("OpenVPNTunnel.onTunBuilderNew()", e);
        }

        return false;
    }

    private synchronized boolean onTunBuilderNewImpl()
    {
        vpnContext.push(new VPNContext(vpnService));

        return true;
    }

    private boolean onTunBuilderSetLayer(int layer)
    {
        EddieLogger.debug("OpenVPNTunnel.onTunBuilderSetLayer(layer: %d)", layer);

        return true;
    }

    private boolean onTunBuilderSetRemoteAddress(String address, boolean ipv6)
    {
        EddieLogger.debug("OpenVPNTunnel.onTunBuilderSetRemoteAddress(address: %s, ipv6: %s)", address, Boolean.toString(ipv6));

        return true;
    }

    private boolean onTunBuilderAddAddress(String address, int prefix_length, String gateway, boolean ipv6, boolean net30)
    {
        EddieLogger.debug("OpenVPNTunnel.onTunBuilderAddAddress(address: %s, prefix_length: %s, gateway: %s, ipv6: %s, net30: %s)", address, prefix_length, gateway, Boolean.toString(ipv6), Boolean.toString(net30));

        try
        {
            return onTunBuilderAddAddressImpl(address, prefix_length, gateway, ipv6, net30);
        }
        catch(Exception e)
        {
            EddieLogger.error("OpenVPNTunnel.onTunBuilderAddAddress()", e);
        }

        return false;
    }

    private boolean onTunBuilderAddAddressImpl(String address, int prefix_length, String gateway, boolean ipv6, boolean net30)
    {
        try
        {
            getActiveContext().getBuilder().addAddress(address, prefix_length);
        }
        catch(Exception e)
        {
            EddieLogger.error("OpenVPNTunnel.onTunBuilderAddAddressImpl(): cannot add address %s - prefix_length %d", address, prefix_length);

            return false;
        }

        return true;
    }

    private boolean onTunBuilderSetRouteMetricDefault(int metric)
    {
        EddieLogger.debug("OpenVPNTunnel.OnTunBuilderSetRouteMetricDefault(metric: %d)", metric);

        return true;
    }

    private boolean onTunBuilderRerouteGW(boolean ipv4, boolean ipv6, int flags)
    {
        EddieLogger.debug("OpenVPNTunnel.onTunBuildeRerouteGW(ipv4: %s, ipv6: %s, flags: %d)", Boolean.toString(ipv4), Boolean.toString(ipv6), flags);

        return true;
    }

    private boolean onTunBuilderAddRoute(String address, int prefix_length, int metric, boolean ipv6)
    {
        EddieLogger.debug("OpenVPNTunnel.onTunBuilderAddRoute(address: %s, prefix_length: %d, metric: %d, ipv6: %s)", address, prefix_length, metric, Boolean.toString(ipv6));

        try
        {
            return onTunBuilderAddRouteImpl(address, prefix_length, metric, ipv6);
        }
        catch(Exception e)
        {
            EddieLogger.error("OpenVPNTunnel.onTunBuilderAddRoute()", e);
        }

        return false;
    }

    private boolean onTunBuilderAddRouteImpl(String address, int prefix_length, int metric, boolean ipv6)
    {
        try
        {
            getActiveContext().getBuilder().addRoute(address, prefix_length);
        }
        catch(Exception e)
        {
            EddieLogger.error("OpenVPNTunnel.onTunBuilderAddRouteImpl(): cannot add route %s - prefix_length %d - metric %d - ipv6 %s", address, prefix_length, metric, Boolean.toString(ipv6));

            return false;
        }

        return true;
    }

    private boolean onTunBuilderExcludeRoute(String address, int prefix_length, int metric, boolean ipv6)
    {
        EddieLogger.debug("OpenVPNTunnel.onTunBuilderExcludeRoute(address: %s, prefix_length: %d, metric: %d, ipv6: %s)", address, prefix_length, metric, Boolean.toString(ipv6));

        return true;
    }

    private boolean onTunBuilderAddDNSServer(String address, boolean ipv6)
    {
        EddieLogger.debug("OpenVPNTunnel.onTunBuilderAddDNSServer(address: %s, ipv6: %s)", address, Boolean.toString(ipv6));

        try
        {
            getActiveContext().addDNSServer(address, ipv6);

            return true;
        }
        catch(Exception e)
        {
            EddieLogger.error("OpenVPNTunnel.onTunBuilderAddDNSServer()", e);
        }

        return false;
    }

    private boolean onTunBuilderAddSearchDomain(String domain)
    {
        EddieLogger.debug("OpenVPNTunnel.onTunBuilderAddSearchDomain(domain: %s)", domain);

        try
        {
            return onTunBuilderAddSearchDomainImpl(domain);
        }
        catch(Exception e)
        {
            EddieLogger.error("OpenVPNTunnel.onTunBuilderAddSearchDomain()", e);
        }

        return false;
    }

    private boolean onTunBuilderAddSearchDomainImpl(String domain)
    {
        try
        {
            getActiveContext().getBuilder().addSearchDomain(domain);
        }
        catch(Exception e)
        {
            EddieLogger.error("OpenVPNTunnel.onTunBuilderAddSearchDomainImpl()", e);

            return false;
        }

        return true;
    }

    private boolean onTunBuilderSetMTU(int mtu)
    {
        EddieLogger.debug("OpenVPNTunnel.onTunBuilderSetMTU(mtu=%d)", mtu);

        try
        {
            getActiveContext().setMTU(mtu);

            return true;
        }
        catch(Exception e)
        {
            EddieLogger.error("OpenVPNTunnel.onTunBuilderSetMTU()", e);
        }

        return false;
    }

    private boolean onTunBuilderSetSessionName(String name)
    {
        EddieLogger.debug("OpenVPNTunnel.onTunBuilderSetSessionName(name: %s)", name);

        try
        {
            return onTunBuilderSetSessionNameImpl(name);
        }
        catch(Exception e)
        {
            EddieLogger.error("OpenVPNTunnel.onTunBuilderSetSessionName()", e);
        }

        return false;
    }

    private boolean onTunBuilderSetSessionNameImpl(String name)
    {
        try
        {
            getActiveContext().getBuilder().setSession(name);
        }
        catch(Exception e)
        {
            EddieLogger.error("OpenVPNTunnel.onTunBuilderSetSessionNameImpl()", e);

            return false;
        }

        return true;
    }

    private boolean onTunBuilderAddProxyBypass(String bypass_host)
    {
        EddieLogger.debug("OpenVPNTunnel.onTunBuilderAddProxyBypass(bypass_host: %s)", bypass_host);

        return true;
    }

    private boolean onTunBuilderSetProxyAutoConfigUrl(String url)
    {
        EddieLogger.debug("OpenVPNTunnel.onTunBuilderSetProxyAutoConfigUrl(url: %s)", url);

        return true;
    }

    private boolean onTunBuilderSetProxyHttp(String host, int port)
    {
        EddieLogger.debug("OpenVPNTunnel.onTunBuilderSetProxyHttp(host: %s, port: %d)", host, port);

        return true;
    }

    private boolean onTunBuilderSetProxyHttps(String host, int port)
    {
        EddieLogger.debug("OpenVPNTunnel.onTunBuilderSetProxyHttps(host: %s, port: %d)", host, port);

        return true;
    }

    private boolean onTunBuilderAddWinsServer(String address)
    {
        EddieLogger.debug("OpenVPNTunnel.onTunBuilderAddWinsServer(address: %s)", address);

        return true;
    }

    private boolean onTunBuilderSetBlockIPV6(boolean block_ipv6)
    {
        EddieLogger.debug("OpenVPNTunnel.onTunBuilderSetBlockIPV6(block_ipv6: %s)", Boolean.toString(block_ipv6));

        try
        {
            getActiveContext().setBlockIPv6(block_ipv6);

            return true;
        }
        catch(Exception e)
        {
            EddieLogger.error("OpenVPNTunnel.onTunBuilderSetBlockIPV6()", e);
        }

        return false;
    }

    private boolean onTunBuilderSetAdapterDomainSuffix(String name)
    {
        EddieLogger.debug("OpenVPNTunnel.onTunBuilderSetAdapterDomainSuffix(name: %s)", name);

        return true;
    }

    private int onTunBuilderEstablish()
    {
        EddieLogger.debug("OpenVPNTunnel.onTunBuilderEstablish()");

        try
        {
            return onTunBuilderEstablishImpl();
        }
        catch(Exception e)
        {
            EddieLogger.error("OpenVPNTunnel.onTunBuilderEstablish()", e);
        }

        return 0;
    }

    private int onTunBuilderEstablishImpl()
    {
        ParcelFileDescriptor fileDescriptor = null;

        try
        {
            fileDescriptor = getActiveContext().establish();
        }
        catch(Exception e)
        {
            EddieLogger.error("OpenVPNTunnel.onTunBuilderEstablishImpl()", e);

            return 0;
        }

        if(fileDescriptor == null)
        {
            EddieLogger.error("OpenVPNTunnel.onTunBuilderEstablishImpl(): VPNService.Builder.establish() failed");

            return 0;
        }

        vpnService.handleThreadStarted();

        return fileDescriptor.detachFd();
    }


    private boolean onTunBuilderPersist()
    {
        EddieLogger.debug("OpenVPNTunnel.onTunBuilderPersist()");

        return true;
    }

    private void onTunBuilderEstablishLite()
    {
        EddieLogger.debug("OpenVPNTunnel.onTunBuilderEstablishLite()");
    }

    private void onTunBuilderTeardown(boolean disconnect)
    {
        EddieLogger.debug("OpenVPNTunnel.onTunBuilderTeardown(disconnect: %s)", Boolean.toString(disconnect));
    }

    private void onConnectAttach()
    {
        EddieLogger.debug("OpenVPNTunnel.onConnectAttach()");
    }

    private void onConnectPreRun()
    {
        EddieLogger.debug("OpenVPNTunnel.onConnectPreRun()");
    }

    private void onConnectRun()
    {
        EddieLogger.debug("OpenVPNTunnel.onConnectRun()");
    }

    private void onConnectSessionStop()
    {
        EddieLogger.debug("OpenVPNTunnel.onConnectSessionStop()");
    }

    private void onEvent(Object event)
    {
        OpenVPNEvent oe = (OpenVPNEvent)event;
        OpenVPNConnectionData connectionData = (OpenVPNConnectionData)oe.data;

        try
        {
            switch(oe.type)
            {
                case OpenVPNEvent.MESSAGE:
                case OpenVPNEvent.INFO:
                {
                    EddieLogger.info("%s: %s", oe.name, oe.info);

                    if(oe.info.contains("PROTOCOL OPTIONS:"))
                        getProtocolOptions(oe.info);
                }
                break;

                case OpenVPNEvent.WARN:
                {
                    EddieLogger.warning("OpenVPN %s: %s", oe.name, oe.info);
                }
                break;

                case OpenVPNEvent.ERROR:
                case OpenVPNEvent.FATAL_ERROR:
                {
                    if(VPN.getConnectionMode() == VPN.ConnectionMode.QUICK_CONNECT && VPN.getConnectionStatus() == VPN.Status.CONNECTING)
                    {
                        if(quickConnectFailureError(oe.name))
                        {
                            eddieEvent.onVpnError(oe);

                            return;
                        }
                    }

                    if(ignoredError(oe.name))
                    {
                        EddieLogger.warning("OpenVPN %s: %s", oe.name, oe.info);
                    }
                    else
                    {
                        // It seems OpenVPN is having BIG troubles with the connection
                        // In order to prevent worse conditions, try to lock the VPN (in case it is possible)

                        EddieLogger.error("OpenVPN3 Fatal Error %s: %s", oe.name, oe.info);

                        if(VPN.getConnectionStatus() == VPN.Status.UNKNOWN)
                        {
                            vpnService.doChangeStatus(VPN.Status.CONNECTION_ERROR);

                            alertNotification(vpnService.getResources().getString(R.string.connection_error));

                            VPN.setVpnConnectionData(null);

                            eddieEvent.onVpnConnectionDataChanged(null);
                        }
                        else if(isVPNLockEnabled)
                        {
                            networkStatusChanged(VPNAction.LOCK);

                            vpnService.doChangeStatus(VPN.Status.LOCKED);

                            alertNotification(vpnService.getResources().getString(R.string.connection_vpn_error));

                            VPN.setVpnConnectionData(null);

                            eddieEvent.onVpnConnectionDataChanged(null);
                        }
                        else
                            EddieLogger.warning(vpnLockDisabledLogWarning);
                    }
                }
                break;

                case OpenVPNEvent.FORMAL_WARNING:
                {
                    if(VPN.getConnectionMode() == VPN.ConnectionMode.QUICK_CONNECT && VPN.getConnectionStatus() == VPN.Status.CONNECTING)
                    {
                        if(quickConnectFailureError(oe.name))
                        {
                            eddieEvent.onVpnError(oe);

                            return;
                        }
                    }

                    if(ignoredError(oe.name))
                    {
                        EddieLogger.warning("OpenVPN %s: %s", oe.name, oe.info);
                    }
                    else
                    {
                        // It seems OpenVPN is having troubles with the connection
                        // In order to prevent worse conditions, lock the VPN

                        EddieLogger.error("OpenVPN3 %s: %s", oe.name, oe.info);

                        if(VPN.getConnectionStatus() == VPN.Status.UNKNOWN)
                        {
                            vpnService.doChangeStatus(VPN.Status.NOT_CONNECTED);

                            alertNotification(vpnService.getResources().getString(R.string.connection_error));

                            VPN.setVpnConnectionData(null);

                            eddieEvent.onVpnConnectionDataChanged(null);
                        }
                        else if(isVPNLockEnabled)
                        {
                            networkStatusChanged(VPNAction.LOCK);

                            vpnService.doChangeStatus(VPN.Status.LOCKED);

                            alertNotification(vpnService.getResources().getString(R.string.connection_vpn_formal_warning));

                            VPN.setVpnConnectionData(null);

                            eddieEvent.onVpnConnectionDataChanged(null);
                        }
                        else
                            EddieLogger.warning(vpnLockDisabledLogWarning);
                    }
                }
                break;

                case OpenVPNEvent.UDP_PARTIAL_SEND_ERROR:
                {
                    udpPartialSendErrorCount++;

                    if(udpPartialSendErrorCount >= maxUdpPartialSendErrors)
                    {
                        // It seems OpenVPN is having troubles with the UDP connection
                        // In order to prevent worse conditions, lock the VPN

                        EddieLogger.error("OpenVPN3 %s: %s - Reached maximum limit for this error (%d)", oe.name, oe.info, maxUdpPartialSendErrors);

                        if(VPN.getConnectionStatus() == VPN.Status.UNKNOWN)
                        {
                            vpnService.doChangeStatus(VPN.Status.NOT_CONNECTED);

                            alertNotification(vpnService.getResources().getString(R.string.connection_error));

                            VPN.setVpnConnectionData(null);

                            eddieEvent.onVpnConnectionDataChanged(null);
                        }
                        else if(isVPNLockEnabled)
                        {
                            networkStatusChanged(VPNAction.LOCK);

                            vpnService.doChangeStatus(VPN.Status.LOCKED);

                            alertNotification(vpnService.getResources().getString(R.string.connection_vpn_formal_warning));

                            VPN.setVpnConnectionData(null);

                            eddieEvent.onVpnConnectionDataChanged(null);
                        }
                        else
                            EddieLogger.warning(vpnLockDisabledLogWarning);
                    }
                    else
                        EddieLogger.warning("OpenVPN3 %s: %s", oe.name, oe.info);
                }
                break;

                case OpenVPNEvent.AUTH_FAILED:
                {
                    EddieLogger.error("OpenVPN Authentication Failed: %s: %s", oe.name, oe.info);

                    eddieEvent.onVpnAuthFailed(oe);
                }
                break;

                case OpenVPNEvent.TUN_ERROR:
                case OpenVPNEvent.CLIENT_RESTART:
                case OpenVPNEvent.CERT_VERIFY_FAIL:
                case OpenVPNEvent.TLS_VERSION_MIN:
                case OpenVPNEvent.CLIENT_HALT:
                case OpenVPNEvent.CLIENT_SETUP:
                case OpenVPNEvent.CONNECTION_TIMEOUT:
                case OpenVPNEvent.INACTIVE_TIMEOUT:
                case OpenVPNEvent.DYNAMIC_CHALLENGE:
                case OpenVPNEvent.PROXY_NEED_CREDS:
                case OpenVPNEvent.PROXY_ERROR:
                case OpenVPNEvent.TUN_SETUP_FAILED:
                case OpenVPNEvent.TUN_IFACE_CREATE:
                case OpenVPNEvent.TUN_IFACE_DISABLED:
                case OpenVPNEvent.EPKI_ERROR:
                case OpenVPNEvent.EPKI_INVALID_ALIAS:
                {
                    if(oe.type == OpenVPNEvent.CONNECTION_TIMEOUT && VPN.getConnectionMode() == VPN.ConnectionMode.QUICK_CONNECT)
                    {
                        eddieEvent.onVpnError(oe);

                        return;
                    }

                    // These OpenVPN events may cause a fatal error
                    // In order to prevent worse conditions, lock the VPN

                    EddieLogger.error("OpenVPN %s: %s", oe.name, oe.info);

                    if(VPN.getConnectionStatus() == VPN.Status.UNKNOWN)
                    {
                        vpnService.doChangeStatus(VPN.Status.NOT_CONNECTED);

                        alertNotification(vpnService.getResources().getString(R.string.connection_error));

                        VPN.setVpnConnectionData(null);

                        eddieEvent.onVpnConnectionDataChanged(null);
                    }
                    else if(isVPNLockEnabled)
                    {
                        networkStatusChanged(VPNAction.LOCK);

                        vpnService.doChangeStatus(VPN.Status.LOCKED);

                        alertNotification(vpnService.getResources().getString(R.string.connection_vpn_formal_warning));

                        VPN.setVpnConnectionData(null);

                        eddieEvent.onVpnConnectionDataChanged(null);

                        eddieEvent.onVpnError(oe);
                    }
                    else
                        EddieLogger.warning(vpnLockDisabledLogWarning);
                }
                break;

                case OpenVPNEvent.CONNECTED:
                {
                    String serverInfo = "";

                    switch(VPN.getConnectionMode())
                    {
                        case AIRVPN_SERVER:
                        case QUICK_CONNECT:
                        {
                            HashMap<String, String> currentProfile = VPN.getProfileInfo();

                            if(currentProfile != null && currentProfile.containsKey("description"))
                                serverInfo = String.format(" (AirVPN server %s)", currentProfile.get("description"));
                            else
                                serverInfo = String.format(" (AirVPN server)");
                        }
                        break;

                        case OPENVPN_PROFILE:
                        {
                            serverInfo = " (OpenVPN profile)";
                        }
                        break;

                        case BOOT_CONNECT:
                        {
                            serverInfo = " (Boot OpenVPN profile)";
                        }
                        break;

                        default:
                        {
                            serverInfo = "";
                        }
                        break;
                    }

                    EddieLogger.info("CONNECTED to VPN%s\nDefined: %d\nUser: %s\nServer Host: %s\nServer Port: %s\nServer Protocol: %s\nServer IP: %s\nVPN IPv4: %s\nVPN IPv6: %s\nGateway IPv4: %s\nGateway IPv6: %s\nTunnel Name: %s",
                            serverInfo,
                            connectionData.defined,
                            connectionData.user,
                            connectionData.serverHost,
                            connectionData.serverPort,
                            connectionData.serverProto,
                            connectionData.serverIp,
                            connectionData.vpnIp4,
                            connectionData.vpnIp6,
                            connectionData.gw4,
                            connectionData.gw6,
                            connectionData.tunName);

                    VPN.setVpnConnectionData(connectionData);

                    eddieEvent.onVpnConnectionDataChanged(connectionData);

                    udpPartialSendErrorCount = 0;
                }
                break;

                case OpenVPNEvent.TRANSPORT_ERROR:
                case OpenVPNEvent.RELAY_ERROR:
                case OpenVPNEvent.DISCONNECTED:
                {
                    EddieLogger.warning("OpenVPN %s: %s", oe.name, oe.info);

                    if(oe.type == OpenVPNEvent.DISCONNECTED)
                    {
                        VPN.setVpnConnectionData(null);

                        eddieEvent.onVpnConnectionDataChanged(null);

                        eddieEvent.onVpnError(oe);
                    }
                }
                break;

                default:
                {
                    EddieLogger.debug("OpenVPN Event: type: %d, name: %s, info: %s", oe.type, oe.name, oe.info);
                }
                break;
            }
        }
        catch(Exception e)
        {
            EddieLogger.error("OpenVPNTunnel.onEvent() Exception: %s", e);
        }
    }

    private String getEventContent(OpenVPNEvent oe)
    {
        String name = oe.name;
        String info = oe.info;

        if(info.isEmpty())
            return name;

        return name + ": " + info;
    }

    public VPN.Status handleScreenChanged(boolean active)
    {
        if(VPN.getConnectionStatus() == VPN.Status.LOCKED)
            return VPN.getConnectionStatus();

        if(openVPNClient == null)
            return VPN.getConnectionStatus();

        if(active)
        {
            if(VPN.getConnectionStatus() == VPN.Status.PAUSED_BY_SYSTEM)
            {
                EddieLogger.info("Screen is now on. Trying to resume VPN");

                EddieLibraryResult result = openVPNClient.resume(this);

                if(result.code == EddieLibraryResult.SUCCESS)
                {
                    EddieLogger.info("Successfully resumed VPN");

                    VPN.setConnectionStatus(VPN.Status.CONNECTED);
                }
                else
                {
                    EddieLogger.error(String.format(Locale.getDefault(), "Failed to resume VPN. %s", result.description));

                    VPN.setConnectionStatus(VPN.Status.UNKNOWN);

                    VPN.setVpnConnectionData(null);

                    eddieEvent.onVpnConnectionDataChanged(null);
                }
            }
        }
        else
        {
            if(settingsManager.isSystemPauseVpnWhenScreenIsOff() && VPN.getConnectionStatus() == VPN.Status.CONNECTED)
            {
                EddieLogger.info("Screen is now off. Trying to pause VPN");

                EddieLibraryResult result = openVPNClient.pause(this, "Screen is off");

                if(result.code == EddieLibraryResult.SUCCESS)
                {
                    EddieLogger.info("Successfully paused VPN");

                    VPN.setConnectionStatus(VPN.Status.PAUSED_BY_SYSTEM);
                }
                else
                {
                    EddieLogger.error(String.format(Locale.getDefault(), "Failed to pause VPN. %s", result.description));

                    VPN.setConnectionStatus(VPN.Status.NOT_CONNECTED);
                }

                VPN.setVpnConnectionData(null);

                eddieEvent.onVpnConnectionDataChanged(null);
            }
        }

        updateNotification(VPN.getConnectionStatus());

        return VPN.getConnectionStatus();
    }

    public void networkStatusChanged(VPNAction action)
    {
        if(VPN.getConnectionStatus() == VPN.Status.LOCKED || openVPNClient == null)
            return;

        switch(action)
        {
            case USER_RESUME:
            {
                if(VPN.getConnectionStatus() == VPN.Status.PAUSED_BY_USER || VPN.getConnectionStatus() == VPN.Status.PAUSED_BY_SYSTEM)
                {
                    EddieLogger.info("%s. Trying to resume VPN", "User requested to resume VPN");

                    EddieLibraryResult result = openVPNClient.resume(this);

                    if(result.code == EddieLibraryResult.SUCCESS)
                    {
                        EddieLogger.info("Successfully resumed VPN");

                        VPN.setConnectionStatus(VPN.Status.CONNECTED);
                    }
                    else
                    {
                        EddieLogger.error(String.format(Locale.getDefault(), "Failed to resume VPN. %s", result.description));

                        VPN.setConnectionStatus(VPN.Status.UNKNOWN);
                    }
                }
            }
            break;

            case SYSTEM_RESUME:
            {
                if(VPN.getConnectionStatus() == VPN.Status.PAUSED_BY_SYSTEM)
                {
                    EddieLogger.info("%s. Trying to resume VPN", "Network status has changed");

                    EddieLibraryResult result = openVPNClient.resume(this);

                    if(result.code == EddieLibraryResult.SUCCESS)
                    {
                        EddieLogger.info("Successfully resumed VPN");

                        VPN.setConnectionStatus(VPN.Status.CONNECTED);
                    }
                    else
                    {
                        EddieLogger.error(String.format(Locale.getDefault(), "Failed to resume VPN. %s", result.description));

                        VPN.setConnectionStatus(VPN.Status.UNKNOWN);
                    }
                }
            }
            break;

            case USER_PAUSE:
            case SYSTEM_PAUSE:
            case NETWORK_TYPE_CHANGED:
            {
                String pauseReason = "";

                if(action == VPNAction.USER_PAUSE)
                    pauseReason = "User requested to pause VPN";
                else
                    pauseReason = "Network status has changed";

                EddieLogger.info("%s. Trying to pause VPN", pauseReason);

                EddieLibraryResult result = openVPNClient.pause(this, pauseReason);

                if(result.code == EddieLibraryResult.SUCCESS)
                {
                    EddieLogger.info("Successfully paused VPN");

                    if(action == VPNAction.USER_PAUSE)
                        VPN.setConnectionStatus(VPN.Status.PAUSED_BY_USER);
                    else
                        VPN.setConnectionStatus(VPN.Status.PAUSED_BY_SYSTEM);
                }
                else
                {
                    EddieLogger.error(String.format(Locale.getDefault(), "Failed to pause VPN. %s", result.description));

                    VPN.setConnectionStatus(VPN.Status.NOT_CONNECTED);
                }

                VPN.setVpnConnectionData(null);

                eddieEvent.onVpnConnectionDataChanged(null);
            }
            break;

            case LOCK:
            {
                EddieLogger.info("VPN error detected. Locking VPN");

                EddieLibraryResult result = openVPNClient.pause(this, "Lock VPN");

                if(result.code == EddieLibraryResult.SUCCESS)
                {
                    EddieLogger.info("Successfully locked VPN");

                    VPN.setConnectionStatus(VPN.Status.LOCKED);
                }
                else
                {
                    EddieLogger.error(String.format(Locale.getDefault(), "Failed to lock VPN. %s", result.description));

                    VPN.setConnectionStatus(VPN.Status.NOT_CONNECTED);
                }

                VPN.setVpnConnectionData(null);

                eddieEvent.onVpnConnectionDataChanged(null);
            }
            break;
        }

        eddieEvent.onVpnStatusChanged(VPN.getConnectionStatus(), vpnService.getResources().getString(VPN.descriptionResource(VPN.getConnectionStatus())));

        updateNotification(VPN.getConnectionStatus());

        return;
    }

    public synchronized void init() throws Exception
    {
        if(openVPNClient != null)
            throw new Exception("OpenVPNTunnel.init(): Client already initialized");

        openVPNClient = OpenVPNClient.create(this);

        if(openVPNClient == null)
            throw new Exception("OpenVPNTunnel.init(): Failed to create a new OpenVPN client");

        isVPNLockEnabled = settingsManager.isVPNLockEnabled();
    }

    public synchronized OpenVPNTransportStats getTransportStats() throws Exception
    {
        if(openVPNClient == null)
        {
            String errMsg = "OpenVPNTunnel.getTransportStats(): OpenVPN client is not initialized";

            EddieLogger.error(errMsg);

            throw new Exception(errMsg);
        }

        OpenVPNTransportStats stats = openVPNClient.getTransportStats();

        if(stats.resultCode != EddieLibrary.SUCCESS)
        {
            String errMsg = String.format(Locale.getDefault(), "OpenVPNTunnel.getTransportStats(): Failed to get OpenVPN transport stats. %s", stats.resultDescription);

            EddieLogger.error(errMsg);

            throw new Exception(errMsg);
        }

        return stats;
    }

    public void loadProfileFile(String filename)
    {
        try
        {
            loadProfile(filename, false);
        }
        catch(Exception e)
        {
            EddieLogger.error("OpenVPNTunnel.loadProfileFile() Exception: %s", e);
        }
    }

    public void loadProfileString(String profile)
    {
        try
        {
            loadProfile(profile, true);
        }
        catch(Exception e)
        {
            EddieLogger.error("OpenVPNTunnel.loadProfileString() Exception: %s", e);
        }
    }

    private synchronized void loadProfile(String profile, boolean isString) throws Exception
    {
        EddieLibraryResult result;

        if(openVPNClient == null)
            throw new Exception("OpenVPNTunnel.loadProfile(): Client is not initialized");

        if(isString)
        {
            result = openVPNClient.loadProfileString(this, profile);

            if(result.code != EddieLibraryResult.SUCCESS)
            {
                String errMsg = String.format(Locale.getDefault(), "OpenVPNTunnel.loadProfile(): Failed to load profile string. %s", result.description);

                EddieLogger.error(errMsg);

                throw new Exception(errMsg);
            }
        }
        else
        {
            result = openVPNClient.loadProfileFile(this, profile);

            if(result.code != EddieLibrary.SUCCESS)
            {
                String errMsg = String.format(Locale.getDefault(), "OpenVPNTunnel.loadProfile(): Failed to load profile file. %s", result.description);

                EddieLogger.error(errMsg);

                throw new Exception(errMsg);
            }
        }
    }

    private static String toOptionValue(boolean value)
    {
        return value ? "true" : "false";
    }

    private static String toOptionValue(int value)
    {
        return String.format(Locale.getDefault(), "%d", value);
    }

    private void doSetOption(String option, String value) throws Exception
    {
        EddieLibraryResult result = openVPNClient.setOption(this, option, value);

        if(result.code != EddieLibrary.SUCCESS)
        {
            String errMsg = String.format(Locale.getDefault(), "OpenVPNTunnel.doSetOption(): Failed to set option '%s' with value '%s'. %s", option, value, result.description);

            EddieLogger.error(errMsg);

            throw new Exception(errMsg);
        }
    }

    public synchronized void bindOptions() throws Exception
    {
        if(openVPNClient == null)
            throw new Exception("OpenVPNTunnel.bindOptions(): Client is not initialized");

        doSetOption(SettingsManager.OVPN3_OPTION_TLS_MIN_VERSION_NATIVE, settingsManager.getOvpn3TLSMinVersion());
        doSetOption(SettingsManager.OVPN3_OPTION_PROTOCOL_NATIVE, settingsManager.getOvpn3Protocol());
        doSetOption(SettingsManager.OVPN3_OPTION_IPV6_NATIVE, settingsManager.getOvpn3IPV6());
        doSetOption(SettingsManager.OVPN3_OPTION_TIMEOUT_NATIVE, settingsManager.getOvpn3Timeout());
        doSetOption(SettingsManager.OVPN3_OPTION_TUN_PERSIST_NATIVE, toOptionValue(settingsManager.isOvpn3TunPersist()));
        doSetOption(SettingsManager.OVPN3_OPTION_COMPRESSION_MODE_NATIVE, settingsManager.getOvpn3CompressionMode());
        doSetOption(SettingsManager.OVPN3_OPTION_USERNAME_NATIVE, settingsManager.getOvpn3Username());
        doSetOption(SettingsManager.OVPN3_OPTION_PASSWORD_NATIVE, settingsManager.getOvpn3Password());
        doSetOption(SettingsManager.OVPN3_OPTION_SYNCHRONOUS_DNS_LOOKUP_NATIVE, toOptionValue(settingsManager.isOvpn3SynchronousDNSLookup()));
        doSetOption(SettingsManager.OVPN3_OPTION_AUTOLOGIN_SESSIONS_NATIVE, toOptionValue(settingsManager.isOvpn3AutologinSessions()));
        doSetOption(SettingsManager.OVPN3_OPTION_DISABLE_CLIENT_CERT_NATIVE, toOptionValue(settingsManager.isOvpn3DisableClientCert()));
        doSetOption(SettingsManager.OVPN3_OPTION_SSL_DEBUG_LEVEL_NATIVE, settingsManager.getOvpn3SSLDebugLevel());
        doSetOption(SettingsManager.OVPN3_OPTION_PRIVATE_KEY_PASSWORD_NATIVE, settingsManager.getOvpn3PrivateKeyPassword());
        doSetOption(SettingsManager.OVPN3_OPTION_DEFAULT_KEY_DIRECTION_NATIVE, settingsManager.getOvpn3DefaultKeyDirection());
        doSetOption(SettingsManager.OVPN3_OPTION_FORCE_AES_CBC_CIPHERSUITES_NATIVE, toOptionValue(settingsManager.isOvpn3AESCBCCiphersuitesForced()));
        doSetOption(SettingsManager.OVPN3_OPTION_TLS_CERT_PROFILE_NATIVE, settingsManager.getOvpn3TLSCertProfile());

        if(settingsManager.isSystemProxyEnabled())
        {
            doSetOption(SettingsManager.OVPN3_OPTION_PROXY_HOST_NATIVE, settingsManager.getOvpn3ProxyHost());
            doSetOption(SettingsManager.OVPN3_OPTION_PROXY_PORT_NATIVE, settingsManager.getOvpn3ProxyPort());
            doSetOption(SettingsManager.OVPN3_OPTION_PROXY_USERNAME_NATIVE, settingsManager.getOvpn3ProxyUsername());
            doSetOption(SettingsManager.OVPN3_OPTION_PROXY_PASSWORD_NATIVE, settingsManager.getOvpn3ProxyPassword());
            doSetOption(SettingsManager.OVPN3_OPTION_PROXY_ALLOW_CLEARTEXT_AUTH_NATIVE, toOptionValue(settingsManager.isOvpn3ProxyAllowCleartextAuth()));
        }
    }

    public void run()
    {
        OpenVPNClient client = null;

        if(openVPNClient == null)
        {
            String errMsg = "OpenVPNTunnel.run(): OpenVPN client is not initialized";

            EddieLogger.error(errMsg);

            vpnService.handleConnectionError();
        }

        client = openVPNClient;

        EddieLibraryResult result = client.start(this);

        if(result.code != EddieLibrary.SUCCESS)
        {
            String errMsg = String.format(Locale.getDefault(), "OpenVPNTunnel.run(): Failed to start OpenVPN client. %s", result.description);

            EddieLogger.error(errMsg);

            vpnService.handleConnectionError();
        }
    }

    public synchronized void cleanup() throws Exception
    {
        OpenVPNClient client = null;

        if(openVPNClient == null)
        {
            String errMsg = "OpenVPNTunnel.cleanup(): OpenVPN client is not initialized";

            EddieLogger.error(errMsg);

            throw new Exception(errMsg);
        }

        client = openVPNClient;

        openVPNClient = null;

        try
        {
            EddieLibraryResult result = client.stop(this);

            if(result.code != EddieLibrary.SUCCESS)
            {
                String errMsg = String.format(Locale.getDefault(), "OpenVPNTunnel.cleanup(): Failed to stop OpenVPN client. %s", result.description);

                EddieLogger.error(errMsg);

                throw new Exception(errMsg);
            }
        }
        finally
        {
            clearContexts();
        }
    }

    public synchronized void onConnectionDataChanged(OpenVPNConnectionData connectionData)
    {
        EddieLogger.debug("OpenVPNTunnel.onConnectionDataChanged()");
    }

    private synchronized void clearContexts()
    {
        EddieLogger.debug("OpenVPNTunnel.clearContexts()");

        while(vpnContext.size() > 0)
        {
            EddieLogger.debug("OpenVPNTunnel.clearContext(): Disposing context");

            vpnContext.pop();
        }
    }

    private void alertNotification(String message)
    {
        if(message.equals(""))
            return;

        vpnService.alertNotification(message);
    }

    private void updateNotification(VPN.Status status)
    {
        String text, server = "";

        if(status != VPN.Status.CONNECTED && status != VPN.Status.PAUSED_BY_USER && status != VPN.Status.PAUSED_BY_SYSTEM && status != VPN.Status.LOCKED)
            return;

        HashMap<String, String> profileInfo = VPN.getProfileInfo();

        if(profileInfo != null)
        {
            if(VPN.getConnectionMode() == VPN.ConnectionMode.AIRVPN_SERVER || VPN.getConnectionMode() == VPN.ConnectionMode.QUICK_CONNECT)
                server = String.format("AirVPN %s (%s)", profileInfo.get("description"), profileInfo.get("server"));
            else
            {
                if(!profileInfo.get("description").isEmpty())
                    server = profileInfo.get("description");
                else
                    server = profileInfo.get("server");
            }
        }

        text = "";

        if(status == VPN.Status.PAUSED_BY_USER || status == VPN.Status.PAUSED_BY_SYSTEM)
            text += "(" + vpnService.getResources().getString(R.string.vpn_status_paused) + ") ";

        if(status == VPN.Status.LOCKED)
            text += "(" + vpnService.getResources().getString(R.string.vpn_status_locked) + ") ";

        text += String.format(Locale.getDefault(), vpnService.getResources().getString(R.string.notification_text), server);

        if(!NetworkStatusReceiver.getNetworkDescription().equals(""))
            text += " " + String.format(Locale.getDefault(), vpnService.getResources().getString(R.string.notification_network), NetworkStatusReceiver.getNetworkDescription());

        vpnService.updateNotification(text);
    }

    private boolean quickConnectFailureError(String s)
    {
        boolean quickConnectError = false;
        String[] quickConnectErrors = new String[]{ "CONNECTION_TIMEOUT",
                                                    "HANDSHAKE_TIMEOUT",
                                                    "AUTH_FAILED",
                                                    "NETWORK_EOF_ERROR",
                                                    "NETWORK_RECV_ERROR",
                                                    "TRANSPORT_ERROR",
                                                    "TLS_VERSION_MIN",
                                                    "TLS_AUTH_FAIL",
                                                    "CERT_VERIFY_FAIL",
                                                    "PEM_PASSWORD_FAIL",
                                                    "TCP_CONNECT_ERROR",
                                                    "UDP_CONNECT_ERROR"
                                                  };

        for(int i = 0; i < quickConnectErrors.length && quickConnectError == false; i++)
        {
            if(s.equals(quickConnectErrors[i]))
                quickConnectError = true;
        }

        return quickConnectError;
    }

    private boolean ignoredError(String s)
    {
        boolean ignoreWarning = false;
        String[] ignoredKeys = new String[]{ "PKTID_INVALID",
                                             "PKTID_BACKTRACK",
                                             "PKTID_EXPIRE",
                                             "PKTID_REPLAY",
                                             "PKTID_TIME_BACKTRACK"
                                           };

        for(int i = 0; i < ignoredKeys.length && ignoreWarning == false; i++)
        {
            if(s.equals(ignoredKeys[i]))
                ignoreWarning = true;
        }

        return ignoreWarning;
    }

    void getProtocolOptions(String o)
    {
        String[] options = o.split("\n");

        for(String opt : options)
        {
            String[] row = opt.split(":");

            switch(row[0].trim())
            {
                case "cipher":
                {
                    VPN.setCipherName(row[1].trim());
                }
                break;

                case "digest":
                {
                    VPN.setDigest(row[1].trim());
                }
                break;
            }
        }
    }
}
