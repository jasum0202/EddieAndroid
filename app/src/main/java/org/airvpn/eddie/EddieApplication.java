// <eddie_source_header>
// This file is part of Eddie/AirVPN software.
// Copyright (C)2014-2018 AirVPN (support@airvpn.org) / https://airvpn.org
//
// Eddie is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Eddie is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Eddie. If not, see <http://www.gnu.org/licenses/>.
// </eddie_source_header>
//
// 20 June 2018 - author: ProMIND - initial release. Based on revised code from com.eddie.android. (a tribute to the 1859 Perugia uprising occurred on 20 June 1859 and in memory of those brave inhabitants who fought for the liberty of Perugia)

package org.airvpn.eddie;

import android.app.Application;
import android.os.Build;

public class EddieApplication extends Application
{
    public static String LOG_TAG = "Eddie.Android";

    private static boolean initialized = false;

    public static boolean isInitialized()
    {
        return initialized;
    }

    @Override
    public void onCreate()
    {
        super.onCreate();

        if(!initialized)
        {
            EddieLogger eddieLogger = new EddieLogger();
            eddieLogger.init(this);

            EddieLogger.info("Eddie Version Code: %s", BuildConfig.VERSION_CODE);
            EddieLogger.info("Eddie Version Name: %s", BuildConfig.VERSION_NAME);
            EddieLogger.info("Initializing Eddie library");
            EddieLogger.info("%s - %s", EddieLibrary.qualifiedName(), EddieLibrary.releaseDate());
            EddieLogger.info("Eddie Library API level: %s", EddieLibrary.apiLevel());
            EddieLogger.info("Architecture: %s", EddieLibrary.architecture());
            EddieLogger.info("Platform: %s", EddieLibrary.platform());

            EddieLogger.info("Manufacturer: %s", Build.MANUFACTURER);
            EddieLogger.info("Model: %s", Build.MODEL);
            EddieLogger.info("Device: %s", Build.DEVICE);
            EddieLogger.info("Brand: %s", Build.BRAND);
            EddieLogger.info("Product: %s", Build.PRODUCT);
            EddieLogger.info("Display: %s", Build.DISPLAY);
            EddieLogger.info("Android API Level: %d", Build.VERSION.SDK_INT);
            EddieLogger.info("Android Version Release: %s", Build.VERSION.RELEASE);
            EddieLogger.info("Fingerprint: %s", Build.FINGERPRINT);
            EddieLogger.info("Tags: %s", Build.TAGS);
            EddieLogger.info("Rooted device: %s", SupportTools.isDeviceRooted());

            EddieLibraryResult result = EddieLibrary.initOpenVPN();

            if(result.code == EddieLibrary.SUCCESS)
            {
                initialized = true;

                EddieLogger.info("Eddie Library: OpenVPN initialization succeeded");
            }
            else
            {
                EddieLogger.error("Eddie Library: OpenVPN initialization failed. %s", result.description);
            }
        }
    }

    @Override
    public void onTerminate()
    {
        if(initialized)
        {
            initialized = false;

            EddieLibraryResult result = EddieLibrary.cleanUpOpenVPN();

            if(result.code != EddieLibrary.SUCCESS)
                EddieLogger.error("Eddie Library: OpenVPN cleanup failed. %s", result.description);

            result = EddieLibrary.disposeOpenVPNClient(null);

            if(result.code != EddieLibrary.SUCCESS)
                EddieLogger.error("Eddie Library: Failed to dispose OpenVPN client. '%s'", result.description);
        }

        super.onTerminate();
    }
}
