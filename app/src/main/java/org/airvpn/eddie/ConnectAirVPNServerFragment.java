// <eddie_source_header>
// This file is part of Eddie/AirVPN software.
// Copyright (C)2014-2018 AirVPN (support@airvpn.org) / https://airvpn.org
//
// Eddie is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Eddie is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Eddie. If not, see <http://www.gnu.org/licenses/>.
// </eddie_source_header>
//
// 3 October 2018 - author: ProMIND - initial release. (a tribute to the 1859 Perugia uprising occurred on 20 June 1859 and in memory of those brave inhabitants who fought for the liberty of Perugia)

package org.airvpn.eddie;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.BaseExpandableListAdapter;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Document;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import static android.app.Activity.RESULT_OK;

public class ConnectAirVPNServerFragment extends Fragment implements NetworkStatusListener, EddieEventListener
{
    private final int ACTIVITY_RESULT_SERVER_SETTINGS = 1002;
    private final int FRAGMENT_ID = 5002;

    public enum GroupType
    {
        HEADER,
        GROUP,
        COUNTRY
    }

    public enum ItemType
    {
        SERVER,
        COUNTRY
    };

    private AirVPNUser airVPNUser = null;
    private AirVPNManifest airVPNManifest = null;
    private EddieEvent eddieEvent = null;
    private static VPNManager vpnManager = null;
    private SupportTools supportTools = null;
    private NetworkStatusReceiver networkStatusReceiver = null;
    private SettingsManager settingsManager = null;
    private CountryContinent countryContinent = null;
    private OpenVPNProfileDatabase openVPNProfileDatabase = null;

    private Button btnInfo = null;
    private Button btnSettings = null;
    private Button btnSearch = null;
    private Button btnReloadManifest = null;
    private Button btnOk = null;
    private Button btnCancel = null;
    private Button btnReset = null;

    private LinearLayout llServerCipher = null;
    private TextView txtServerCipher = null;

    AlertDialog.Builder dialogBuilder = null;
    AlertDialog searchDialog = null;
    private Handler dialogHandler = null;
    private EditText edtKey = null;

    private ExpandableListView elvAirVPNServer = null;
    private AirVPNServerExpandableListAdapter serverListAdapter = null;

    private ArrayList<Group> groupList = null;
    private ArrayList<String> countryWhitelist = null;
    private ArrayList<String> countryBlacklist = null;

    private HashMap<String, String> profileInfo = null;

    private ListItem pendingServerConnection = null;

    private String searchKey = "";

    public void setVpnManager(VPNManager v)
    {
        vpnManager = v;
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        supportTools = new SupportTools(getActivity());
        settingsManager = new SettingsManager(getActivity());
        countryContinent = new CountryContinent(getContext());

        openVPNProfileDatabase = new OpenVPNProfileDatabase(getActivity());

        networkStatusReceiver = new NetworkStatusReceiver(getContext());
        networkStatusReceiver.addListener(this);

        eddieEvent = new EddieEvent();
        eddieEvent.addListener(this);

        airVPNUser = new AirVPNUser(getContext());

        airVPNManifest = new AirVPNManifest(getContext());
    }

    @Override
    public void onResume()
    {
        super.onResume();

        if(airVPNUser.isUserValid())
        {
            if(NetworkStatusReceiver.isNetworkConnected())
                supportTools.enableButton(btnReloadManifest, true);

            registerForContextMenu(elvAirVPNServer);
        }
        else
        {
            supportTools.enableButton(btnReloadManifest, false);

            unregisterForContextMenu(elvAirVPNServer);
        }

        if(searchKey.isEmpty())
            btnSearch.setBackgroundResource(R.drawable.search);
        else
            btnSearch.setBackgroundResource(R.drawable.search_on);

        refreshServerList();
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();

        networkStatusReceiver.removeListener(this);

        eddieEvent.removeListener(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View fragmentView = inflater.inflate(R.layout.fragment_airvpn_server_list_layout, container, false);

        elvAirVPNServer = (ExpandableListView)fragmentView.findViewById(R.id.airvpn_server_list);

        elvAirVPNServer.setOnChildClickListener(new ExpandableListView.OnChildClickListener()
        {
            @Override
            public boolean onChildClick(ExpandableListView expandablelistview, View clickedView, int groupPosition, int childPosition, long childId)
            {
                ListItem server = groupList.get(groupPosition).getItemList().get(childPosition);

                startServerConnection(server);

                return true;
            }
        });

        elvAirVPNServer.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener()
        {
            @Override
            public boolean onItemLongClick(AdapterView<?> arg0, View arg1, int pos, long id)
            {
                if(airVPNUser.isUserValid())
                    return false;
                else
                {
                    supportTools.infoDialog(R.string.airvpn_longpress_no_login, true);

                    return true;
                }
            }
        });

        createGroupList();

        serverListAdapter = new AirVPNServerExpandableListAdapter(getContext(), groupList);

        elvAirVPNServer.setAdapter(serverListAdapter);

        elvAirVPNServer.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener()
        {
            @Override
            public void onGroupExpand(int groupPosition)
            {
                if(groupPosition == 0 || groupPosition == 1)
                {
                    if(groupList.get(groupPosition).getItemList().size() == 0)
                        supportTools.infoDialog(R.string.airvpn_server_add_to_group, true);
                }
            }
        });

        if(airVPNUser.isUserValid())
            registerForContextMenu(elvAirVPNServer);
        else
            unregisterForContextMenu(elvAirVPNServer);

        btnInfo = (Button)fragmentView.findViewById(R.id.airvpn_server_info);
        btnSettings = (Button)fragmentView.findViewById(R.id.airvpn_server_settings);
        btnSearch = (Button)fragmentView.findViewById(R.id.airvpn_server_search);
        btnReloadManifest = (Button)fragmentView.findViewById(R.id.airvpn_server_reload_manifest);

        btnInfo.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View arg0)
            {
                showStatus();
            }
        });

        btnInfo.setOnFocusChangeListener(new View.OnFocusChangeListener()
        {
            @Override
            public void onFocusChange(View v, boolean hasFocus)
            {
                if(!btnInfo.isEnabled())
                    return;

                if(hasFocus)
                    btnInfo.setBackgroundResource(R.drawable.info_focus);
                else
                    btnInfo.setBackgroundResource(R.drawable.info);
            }
        });

        btnInfo.setAccessibilityDelegate(new View.AccessibilityDelegate()
        {
            @Override
            public void sendAccessibilityEvent(View host, int eventType)
            {
                super.sendAccessibilityEvent(host, eventType);

                btnInfo.setContentDescription(getString(R.string.accessibility_connection_info));
            }
        });

        btnSettings.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View arg0)
            {
                Intent settingsActivityIntent = new Intent(getContext(), AirVPNServerSettingsActivity.class);

                startActivityForResult(settingsActivityIntent, ACTIVITY_RESULT_SERVER_SETTINGS);
            }
        });

        btnSettings.setOnFocusChangeListener(new View.OnFocusChangeListener()
        {
            @Override
            public void onFocusChange(View v, boolean hasFocus)
            {
                if(!btnSettings.isEnabled())
                    return;

                if(hasFocus)
                    btnSettings.setBackgroundResource(R.drawable.settings_focus);
                else
                    btnSettings.setBackgroundResource(R.drawable.settings);
            }
        });

        btnSettings.setAccessibilityDelegate(new View.AccessibilityDelegate()
        {
            @Override
            public void sendAccessibilityEvent(View host, int eventType)
            {
                super.sendAccessibilityEvent(host, eventType);

                btnSettings.setContentDescription(getString(R.string.accessibility_server_settings));
            }
        });

        btnSearch.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View arg0)
            {
                final Runnable uiRunnable = new Runnable()
                {
                    @Override
                    public void run()
                    {
                        searchDialog();
                    }
                };

                supportTools.runOnUiActivity(getActivity(), uiRunnable);
            }
        });

        btnSearch.setOnFocusChangeListener(new View.OnFocusChangeListener()
        {
            @Override
            public void onFocusChange(View v, boolean hasFocus)
            {
                if(!btnSearch.isEnabled())
                    return;

                if(hasFocus)
                {
                    if(searchKey.isEmpty())
                        btnSearch.setBackgroundResource(R.drawable.search_focus);
                    else
                        btnSearch.setBackgroundResource(R.drawable.search_on_focus);
                }
                else
                {
                    if(searchKey.isEmpty())
                        btnSearch.setBackgroundResource(R.drawable.search);
                    else
                        btnSearch.setBackgroundResource(R.drawable.search_on);
                }
            }
        });

        btnSearch.setAccessibilityDelegate(new View.AccessibilityDelegate()
        {
            @Override
            public void sendAccessibilityEvent(View host, int eventType)
            {
                super.sendAccessibilityEvent(host, eventType);

                btnSearch.setContentDescription(getString(R.string.accessibility_search_server));
            }
        });

        btnReloadManifest.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View arg0)
            {
                if(airVPNManifest != null)
                {
                    supportTools.showProgressDialog(R.string.airvpn_server_refresh_manifest);

                    airVPNManifest.refreshManifestFromAirVPN();
                }
            }
        });

        btnReloadManifest.setOnFocusChangeListener(new View.OnFocusChangeListener()
        {
            @Override
            public void onFocusChange(View v, boolean hasFocus)
            {
                if(!btnReloadManifest.isEnabled())
                    return;

                if(hasFocus)
                    btnReloadManifest.setBackgroundResource(R.drawable.refresh_servers_focus);
                else
                    btnReloadManifest.setBackgroundResource(R.drawable.refresh_servers);
            }
        });

        btnReloadManifest.setAccessibilityDelegate(new View.AccessibilityDelegate()
        {
            @Override
            public void sendAccessibilityEvent(View host, int eventType)
            {
                super.sendAccessibilityEvent(host, eventType);

                btnReloadManifest.setContentDescription(getString(R.string.accessibility_reload_manifest));
            }
        });

        llServerCipher = (LinearLayout)fragmentView.findViewById(R.id.airvpn_server_cipher);
        txtServerCipher = (TextView)fragmentView.findViewById(R.id.txt_server_cipher);

        showServerCipher();

        if(airVPNUser.isUserValid() && NetworkStatusReceiver.isNetworkConnected())
            supportTools.enableButton(btnReloadManifest, true);
        else
            supportTools.enableButton(btnReloadManifest, false);

        return fragmentView;
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo)
    {
        int menuResouce = 0;

        super.onCreateContextMenu(menu, v, menuInfo);

        if(!airVPNUser.isUserValid())
            return;

        if(v.getId() == R.id.airvpn_server_list)
        {
            MenuInflater inflater = getActivity().getMenuInflater();

            ExpandableListView.ExpandableListContextMenuInfo info = (ExpandableListView.ExpandableListContextMenuInfo)menuInfo;

            int type = ExpandableListView.getPackedPositionType(info.packedPosition);
            int group = ExpandableListView.getPackedPositionGroup(info.packedPosition);
            int child = ExpandableListView.getPackedPositionChild(info.packedPosition);

            if(group == 0)
            {
                // Favorite

                if(child < 0)
                {
                    if(groupList.get(0).getItemList().size() > 0)
                        menuResouce = R.menu.empty_whitelist_server_menu;
                }
                else
                {
                    if(groupList.get(group).getItemList().get(child).getType() == ItemType.SERVER)
                        menuResouce = R.menu.whitelist_server_menu;
                    else
                        menuResouce = R.menu.whitelist_country_menu;
                }
            }
            else if(group == 1)
            {
                // Forbidden

                if(child < 0)
                {
                    if(groupList.get(1).getItemList().size() > 0)
                        menuResouce = R.menu.empty_blacklist_server_menu;
                }
                else
                {
                    if(groupList.get(group).getItemList().get(child).getType() == ItemType.SERVER)
                        menuResouce = R.menu.blacklist_server_menu;
                    else
                        menuResouce = R.menu.blacklist_country_menu;
                }
            }
            else if(group > 2)
            {
                // Countries and servers

                if(child >= 0)
                {
                    // Server item

                    ListItem item = groupList.get(group).getItemList().get(child);

                    if(item != null)
                    {
                        if(item.isFavorite())
                            menuResouce = R.menu.whitelist_server_menu;
                        else if(item.isForbidden())
                            menuResouce = R.menu.blacklist_server_menu;
                        else
                            menuResouce = R.menu.server_menu;
                    }
                }
                else
                {
                    // Country item

                    String countryCode = groupList.get(group).getCountryCode();

                    if(countryCode != null)
                    {
                        if(settingsManager.isAirVPNCountryWhitelisted(countryCode))
                            menuResouce = R.menu.whitelist_country_menu;
                        else if(settingsManager.isAirVPNCountryBlacklisted(countryCode))
                            menuResouce = R.menu.blacklist_country_menu;
                        else
                            menuResouce = R.menu.country_menu;
                    }
                }
            }

            if(menuResouce != 0)
            {
                inflater.inflate(menuResouce, menu);

                for(int i = 0; i < menu.size(); i++)
                {
                    MenuItem item = menu.getItem(i);

                    if(item != null)
                    {
                        Intent intent = new Intent();
                        intent.putExtra("fragment_id", FRAGMENT_ID);

                        item.setIntent(intent);
                    }
                }
            }
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem menuItem)
    {
        boolean processed = false;
        ListItem server = null;

        Intent intent = menuItem.getIntent();

        if(intent != null && intent.getIntExtra("fragment_id", -1) != FRAGMENT_ID)
            return false;

        ExpandableListView.ExpandableListContextMenuInfo info = (ExpandableListView.ExpandableListContextMenuInfo)menuItem.getMenuInfo();

        int type = ExpandableListView.getPackedPositionType(info.packedPosition);
        int group = ExpandableListView.getPackedPositionGroup(info.packedPosition);
        int child = ExpandableListView.getPackedPositionChild(info.packedPosition);

        if(child >= 0)
            server = groupList.get(group).getItemList().get(child);

        switch(menuItem.getItemId())
        {
            case R.id.context_menu_connect_server:
            {
                connectServer(server);

                processed = true;
            }
            break;

            case R.id.context_menu_add_favorite_server:
            {
                addServerToFavorites(server);

                processed = true;
            }
            break;

            case R.id.context_menu_remove_favorite_server:
            {
                removeServerFromFavorites(server);

                processed = true;
            }
            break;

            case R.id.context_menu_add_server_to_openvpn_profiles:
            {
                exportToOpenVPNProfiles(server, false, "");

                processed = true;
            }
            break;

            case R.id.context_menu_add_favorite_country:
            {
                addCountryToFavorites(groupList.get(group).getCountryCode());

                processed = true;
            }
            break;

            case R.id.context_menu_remove_favorite_country:
            {
                String countryCode = "";

                if(group == 0) // Favorite group
                    countryCode = groupList.get(group).getItemList().get(child).getCountryCode();
                else
                    countryCode = groupList.get(group).getCountryCode();

                removeCountryFromFavorites(countryCode);

                processed = true;
            }
            break;

            case R.id.context_menu_add_country_to_openvpn_profiles:
            {
                String countryCode = "";

                if(group == 0) // Favorite group
                    countryCode = groupList.get(group).getItemList().get(child).getCountryCode();
                else
                    countryCode = groupList.get(group).getCountryCode();

                server = groupList.get(group).getItemList().get(0);

                exportToOpenVPNProfiles(server, true, countryCode);

                processed = true;
            }
            break;

            case R.id.context_menu_empty_favorite_list:
            {
                emptyFavoriteList();

                processed = true;
            }
            break;

            case R.id.context_menu_add_forbidden_server:
            {
                addServerToForbidden(server);

                processed = true;
            }
            break;

            case R.id.context_menu_remove_forbidden_server:
            {
                removeServerFromForbidden(server);

                processed = true;
            }
            break;

            case R.id.context_menu_add_forbidden_country:
            {
                addCountryToForbidden(groupList.get(group).getCountryCode());

                processed = true;
            }
            break;

            case R.id.context_menu_remove_forbidden_country:
            {
                String countryCode = "";

                if(group == 1) // Forbidden group
                    countryCode = groupList.get(group).getItemList().get(child).getCountryCode();
                else
                    countryCode = groupList.get(group).getCountryCode();

                removeCountryFromForbidden(countryCode);

                processed = true;
            }
            break;

            case R.id.context_menu_empty_forbidden_list:
            {
                emptyForbiddenList();

                processed = true;
            }
            break;

            default:
            {
                processed = super.onContextItemSelected(menuItem);
            }
            break;
        }

        return processed;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        switch(requestCode)
        {
            case ACTIVITY_RESULT_SERVER_SETTINGS:
            {
                if(resultCode == RESULT_OK)
                    refreshServerList();
            }
            break;

            default:
            {
            }
            break;
        }
    }

    private void addServerToFavorites(ListItem server)
    {
        if(server == null)
            return;

        ArrayList<String> serverList = settingsManager.getAirVPNServerWhitelist();

        serverList.add(server.getName());

        settingsManager.setAirVPNServerWhitelist(serverList);

        refreshServerList();
    }

    private void removeServerFromFavorites(ListItem server)
    {
        if(server == null)
            return;

        ArrayList<String> serverList = settingsManager.getAirVPNServerWhitelist();

        serverList.remove(server.getName());

        settingsManager.setAirVPNServerWhitelist(serverList);

        refreshServerList();
    }

    private void exportToOpenVPNProfiles(ListItem server, boolean createCountryProfile, String countryCode)
    {
        String profileName, tlsMode, protocol, openVpnProfile, openVpnProfileName, newProfileName, countryServerName;
        HashMap<Integer, String> serverEntryIP = null;
        OpenVPNProfileDatabase.OpenVPNProfile dbOpenVPNProfile = null;
        OpenVPNProfileDatabase.ProtocolType proto = OpenVPNProfileDatabase.ProtocolType.UNKNOWN;
        int entry, port;
        boolean connectIPv6, profileNameExists;

        if(!supportTools.confirmationDialog(R.string.airvpn_export_openvpn_profile_warning))
            return;

        AirVPNServer airVPNServer = airVPNManifest.getServerByName(server.getName());

        if(airVPNServer == null)
            return;

        profileName = airVPNUser.getCurrentProfile();

        tlsMode = settingsManager.getAirVPNTLSMode();

        if(tlsMode.equals(SettingsManager.AIRVPN_TLS_MODE_AUTH))
            entry = 0;
        else
            entry = 2;

        switch(settingsManager.getAirVPNIPVersion())
        {
            case SettingsManager.AIRVPN_IP_VERSION_4:
            {
                serverEntryIP = airVPNServer.getEntryIPv4();

                connectIPv6 = false;
            }
            break;

            case SettingsManager.AIRVPN_IP_VERSION_6:
            {
                serverEntryIP = airVPNServer.getEntryIPv6();

                connectIPv6 = true;
            }
            break;

            case SettingsManager.AIRVPN_IP_VERSION_4_OVER_6:
            {
                serverEntryIP = airVPNServer.getEntryIPv4();

                connectIPv6 = true;
            }
            break;

            default:
            {
                serverEntryIP = airVPNServer.getEntryIPv4();

                connectIPv6 = false;
            }
            break;
        }

        protocol = settingsManager.getAirVPNProtocol();

        port = settingsManager.getAirVPNPort();

        openVpnProfile = airVPNUser.getOpenVPNProfile(profileName, serverEntryIP.get(entry), port, protocol, tlsMode, settingsManager.getAirVPNCipher(), connectIPv6, createCountryProfile, countryCode);

        if(createCountryProfile == false)
            openVpnProfileName = String.format("AirVPN_%s-%s_%s_%s-%d.ovpn", server.getCountryCode(), server.getLocation(), server.getName(), protocol, port);
        else
            openVpnProfileName = String.format("AirVPN_%s_%s-%d.ovpn", countryContinent.getCountryName(countryCode), protocol, port);

        profileNameExists = openVPNProfileDatabase.exists(openVpnProfileName);

        if(profileNameExists)
        {
            newProfileName = openVpnProfileName;

            while(profileNameExists)
            {
                newProfileName = supportTools.getTextOptionDialog(R.string.openvpn_profile_name_exists, openVpnProfileName, SupportTools.EditOption.DO_NOT_ALLOW_EMPTY_FIELD);

                if(!openVpnProfileName.equals(newProfileName))
                    profileNameExists = openVPNProfileDatabase.exists(newProfileName);
                else
                    return;
            }

            openVpnProfileName = newProfileName;
        }

        dbOpenVPNProfile = openVPNProfileDatabase.new OpenVPNProfile();

        dbOpenVPNProfile.setName(openVpnProfileName);

        if(createCountryProfile == false)
            dbOpenVPNProfile.setServer(serverEntryIP.get(entry));
        else
        {
            countryServerName = countryCode.toLowerCase();

            if(tlsMode.equals(SettingsManager.AIRVPN_TLS_MODE_CRYPT))
                countryServerName += "3";
            else
                countryServerName += "1";

            if(connectIPv6)
                countryServerName += ".ipv6";

            countryServerName += ".vpn.airdns.org";

            dbOpenVPNProfile.setServer(countryServerName);
        }

        dbOpenVPNProfile.setPort(port);

        switch(protocol)
        {
            case "UDP":
            {
                proto = OpenVPNProfileDatabase.ProtocolType.UDPv4;
            }
            break;

            case "TCP":
            {
                proto = OpenVPNProfileDatabase.ProtocolType.TCPv4;
            }
            break;

            case "UDP6":
            {
                proto = OpenVPNProfileDatabase.ProtocolType.UDPv6;
            }
            break;

            case "TCP6":
            {
                proto = OpenVPNProfileDatabase.ProtocolType.TCPv6;
            }
            break;
        }

        dbOpenVPNProfile.setProtocol(proto);

        dbOpenVPNProfile.setProfile(openVpnProfile);

        openVPNProfileDatabase.setProfile(openVpnProfileName, dbOpenVPNProfile);

        Toast.makeText(getActivity(), String.format(getResources().getString(R.string.airvpn_openvpn_profile_added), openVpnProfileName), Toast.LENGTH_LONG).show();
    }

    private void addCountryToFavorites(String countryCode)
    {
        if(countryCode == null || countryCode.isEmpty())
            return;

        ArrayList<String> countryList = settingsManager.getAirVPNCountryWhitelist();

        countryList.add(countryCode);

        settingsManager.setAirVPNCountryWhitelist(countryList);

        refreshServerList();
    }

    private void removeCountryFromFavorites(String countryCode)
    {
        if(countryCode == null || countryCode.isEmpty())
            return;

        ArrayList<String> countryList = settingsManager.getAirVPNCountryWhitelist();

        countryList.remove(countryCode);

        settingsManager.setAirVPNCountryWhitelist(countryList);

        refreshServerList();
    }

    private void addServerToForbidden(ListItem server)
    {
        if(server == null)
            return;

        ArrayList<String> serverList = settingsManager.getAirVPNServerBlacklist();

        serverList.add(server.getName());

        settingsManager.setAirVPNServerBlacklist(serverList);

        refreshServerList();
    }

    private void removeServerFromForbidden(ListItem server)
    {
        if(server == null)
            return;

        ArrayList<String> serverList = settingsManager.getAirVPNServerBlacklist();

        serverList.remove(server.getName());

        settingsManager.setAirVPNServerBlacklist(serverList);

        refreshServerList();
    }

    private void addCountryToForbidden(String countryCode)
    {
        if(countryCode == null || countryCode.isEmpty())
            return;

        ArrayList<String> countryList = settingsManager.getAirVPNCountryBlacklist();

        countryList.add(countryCode);

        settingsManager.setAirVPNCountryBlacklist(countryList);

        refreshServerList();
    }

    private void removeCountryFromForbidden(String countryCode)
    {
        if(countryCode == null || countryCode.isEmpty())
            return;

        ArrayList<String> countryList = settingsManager.getAirVPNCountryBlacklist();

        countryList.remove(countryCode);

        settingsManager.setAirVPNCountryBlacklist(countryList);

        refreshServerList();
    }

    private void emptyFavoriteList()
    {
        if(!supportTools.confirmationDialog(R.string.airvpn_confirm_empty_favorite_list))
            return;

        settingsManager.setAirVPNServerWhitelist(new ArrayList<String>());

        settingsManager.setAirVPNCountryWhitelist(new ArrayList<String>());

        refreshServerList();
    }

    private void emptyForbiddenList()
    {
        if(!supportTools.confirmationDialog(R.string.airvpn_confirm_empty_forbidden_list))
            return;

        settingsManager.setAirVPNServerBlacklist(new ArrayList<String>());

        settingsManager.setAirVPNCountryBlacklist(new ArrayList<String>());

        refreshServerList();
    }

    private void showStatus()
    {
        HashMap<String, String> currentProfile = VPN.getProfileInfo();
        String serverDescription = "";
        String networkStatus = "";
        String status = "";

        if(airVPNUser.isUserValid())
        {
            status += String.format("<font color='black'>%s</font><br><br>", String.format(getString(R.string.logged_in_as_user), airVPNUser.getUserName()));

            status += String.format("<font color='blue'>%s</font><br><br>", airVPNUser.getExpirationText());
        }

        if(currentProfile != null)
        {
            if(!currentProfile.get("description").equals(""))
                serverDescription = String.format("AirVPN %s (%s)", currentProfile.get("description"), currentProfile.get("server"));
            else
            {
                serverDescription = currentProfile.get("server");

                if(VPN.getConnectionMode() == VPN.ConnectionMode.OPENVPN_PROFILE)
                    serverDescription += " (" + getResources().getString(R.string.conn_type_openvpn_profile) + ")";
            }
        }

        if(NetworkStatusReceiver.getNetworkStatus() == NetworkStatusReceiver.Status.CONNECTED)
            networkStatus = String.format(Locale.getDefault(), getString(R.string.conn_status_connected), NetworkStatusReceiver.getNetworkDescription());
        else
            networkStatus = getString(R.string.conn_status_not_available);

        status += String.format("<font color='blue'>%s</font> <font color='black'>%s</font><br>", getString(R.string.conn_network_status_cap), networkStatus);

        status += String.format("<font color='blue'>%s</font> <font color='black'>%s</font><br>", getString(R.string.conn_vpn_status_cap), getString(VPN.descriptionResource(VPN.getConnectionStatus())));

        if(!serverDescription.isEmpty())
        {
            status += "<br>";

            status += String.format(Locale.getDefault(), getString(R.string.notification_text), serverDescription);
        }

        supportTools.infoDialogHtml(Html.fromHtml(status), true);
    }

    private synchronized void createGroupList()
    {
        Group group = null, airVPNGroup = null;
        ListItem itemList = null;
        ArrayList<String> serverWhitelist = null, serverBlacklist = null;
        ArrayList<Integer> cipherList = null;
        boolean includeServer = true, countryGroupAdded = false;
        int countedServers = 0, countedUsers = 0;
        long countedBandWidth = 0, countedMaxBandWidth = 0;

        if(airVPNManifest == null)
            return;

        if(!settingsManager.getAirVPNCipher().equals(SettingsManager.AIRVPN_CIPHER_SERVER))
        {
            CipherDatabase cipherDatabase = new CipherDatabase();

            if(cipherDatabase != null)
                cipherList = cipherDatabase.getMatchingCiphersArrayList(settingsManager.getAirVPNCipher(), CipherDatabase.CipherType.DATA);
        }

        countryWhitelist = settingsManager.getAirVPNCountryWhitelist();
        countryBlacklist = settingsManager.getAirVPNCountryBlacklist();

        groupList = new ArrayList<Group>();

        group = new Group();

        group.setType(GroupType.GROUP);
        group.setIcon(R.drawable.star_icon_on);
        group.setName(getString(R.string.airvpn_server_whitelist));
        group.setCountryCode("");
        group.setServers(0);
        group.setUsers(0);

        serverWhitelist = settingsManager.getAirVPNServerWhitelist();

        if(serverWhitelist != null && serverWhitelist.size() > 0)
        {
            for(String name : serverWhitelist)
            {
                AirVPNServer airVPNServer = airVPNManifest.getServerByName(name);

                if(airVPNServer != null)
                {
                    if(airVPNServer.isAvailable(cipherList))
                    {
                        itemList = new ListItem();

                        itemList.setType(ItemType.SERVER);
                        itemList.setName(airVPNServer.getName());
                        itemList.setCountryCode(airVPNServer.getCountryCode());
                        itemList.setIcon(getCountryFlagResource(airVPNServer.getCountryCode()));
                        itemList.setLocation(airVPNServer.getLocation());
                        itemList.setServers(0);
                        itemList.setUsers(airVPNServer.getUsers());
                        itemList.setBandWidth(airVPNServer.getBandWidth());
                        itemList.setMaxBandWidth(airVPNServer.getMaxBandWidth());
                        itemList.setFavorite(true);
                        itemList.setForbidden(false);
                        itemList.setWarningOpen(airVPNServer.getWarningOpen());
                        itemList.setWarningClosed(airVPNServer.getWarningClosed());

                        group.addItem(itemList);

                        group.setServers(group.getServers() + 1);
                        group.setUsers(group.getUsers() + airVPNServer.getUsers());
                        group.setBandWidth(group.getBandWidth() + airVPNServer.getBandWidth());
                        group.setMaxBandWidth(group.getMaxBandWidth() + airVPNServer.getMaxBandWidth());
                    }
                }
            }
        }

        if(countryWhitelist != null && countryWhitelist.size() > 0)
        {
            HashMap<String, AirVPNManifest.CountryStats> countryStats = (HashMap<String, AirVPNManifest.CountryStats>)airVPNManifest.getCountryStats().clone();

            if(countryStats != null)
            {
                for(String countryCode : countryWhitelist)
                {
                    AirVPNManifest.CountryStats country = countryStats.get(countryCode);

                    if(country != null)
                    {
                        itemList = new ListItem();

                        itemList.setType(ItemType.COUNTRY);
                        itemList.setName(countryContinent.getCountryName(country.getCountryISOCode()));
                        itemList.setCountryCode(country.getCountryISOCode());
                        itemList.setIcon(getCountryFlagResource(country.getCountryISOCode()));
                        itemList.setLocation("");
                        itemList.setServers(country.getServers());
                        itemList.setUsers(country.getUsers());
                        itemList.setBandWidth(country.getBandWidth());
                        itemList.setMaxBandWidth(country.getMaxBandWidth());
                        itemList.setFavorite(true);
                        itemList.setForbidden(false);
                        itemList.setWarningOpen("");
                        itemList.setWarningOpen("");

                        group.addItem(itemList);

                        group.setServers(group.getServers() + country.getServers());
                        group.setUsers(group.getUsers() + country.getUsers());
                        group.setBandWidth(group.getBandWidth() + country.getBandWidth());
                        group.setMaxBandWidth(group.getMaxBandWidth() + country.getMaxBandWidth());
                    }
                }
            }
        }

        group.sortItemList();

        groupList.add(group);

        group = new Group();

        group.setType(GroupType.GROUP);
        group.setIcon(R.drawable.blacklist_icon_on);
        group.setName(getString(R.string.airvpn_server_blacklist));
        group.setCountryCode("");
        group.setServers(0);
        group.setUsers(0);
        group.setBandWidth(0);
        group.setMaxBandWidth(0);

        serverBlacklist = settingsManager.getAirVPNServerBlacklist();

        if(serverBlacklist != null && serverBlacklist.size() > 0)
        {
            for(String name : serverBlacklist)
            {
                AirVPNServer airVPNServer = airVPNManifest.getServerByName(name);

                if(airVPNServer != null)
                {
                    itemList = new ListItem();

                    itemList.setType(ItemType.SERVER);
                    itemList.setName(airVPNServer.getName());
                    itemList.setCountryCode(airVPNServer.getCountryCode());
                    itemList.setIcon(getCountryFlagResource(airVPNServer.getCountryCode()));
                    itemList.setLocation(airVPNServer.getLocation());
                    itemList.setServers(0);
                    itemList.setUsers(airVPNServer.getUsers());
                    itemList.setBandWidth(airVPNServer.getBandWidth());
                    itemList.setMaxBandWidth(airVPNServer.getMaxBandWidth());
                    itemList.setFavorite(false);
                    itemList.setForbidden(true);
                    itemList.setWarningOpen(airVPNServer.getWarningOpen());
                    itemList.setWarningClosed(airVPNServer.getWarningClosed());

                    group.addItem(itemList);

                    group.setServers(group.getServers() + 1);
                    group.setUsers(group.getUsers() + airVPNServer.getUsers());
                    group.setBandWidth(group.getBandWidth() + airVPNServer.getBandWidth());
                    group.setMaxBandWidth(group.getMaxBandWidth() + airVPNServer.getMaxBandWidth());
                }
            }
        }

        if(countryBlacklist != null && countryBlacklist.size() > 0)
        {
            HashMap<String, AirVPNManifest.CountryStats> countryStats = (HashMap<String, AirVPNManifest.CountryStats>)airVPNManifest.getCountryStats().clone();

            if(countryStats != null)
            {
                for(String countryCode : countryBlacklist)
                {
                    AirVPNManifest.CountryStats country = countryStats.get(countryCode);

                    if(country != null)
                    {
                        itemList = new ListItem();

                        itemList.setType(ItemType.COUNTRY);
                        itemList.setName(countryContinent.getCountryName(country.getCountryISOCode()));
                        itemList.setCountryCode(country.getCountryISOCode());
                        itemList.setIcon(getCountryFlagResource(country.getCountryISOCode()));
                        itemList.setLocation("");
                        itemList.setServers(country.getServers());
                        itemList.setUsers(country.getUsers());
                        itemList.setBandWidth(country.getBandWidth());
                        itemList.setMaxBandWidth(country.getMaxBandWidth());
                        itemList.setFavorite(false);
                        itemList.setForbidden(true);
                        itemList.setWarningOpen("");
                        itemList.setWarningClosed("");

                        group.addItem(itemList);

                        group.setServers(group.getServers() + country.getServers());
                        group.setUsers(group.getUsers() + country.getUsers());
                        group.setBandWidth(group.getBandWidth() + country.getBandWidth());
                        group.setMaxBandWidth(group.getMaxBandWidth() + country.getMaxBandWidth());
                    }
                }
            }
        }

        group.sortItemList();

        groupList.add(group);

        airVPNGroup = new Group();

        airVPNGroup.setType(GroupType.HEADER);
        airVPNGroup.setIcon(R.drawable.icon);
        airVPNGroup.setName(getString(R.string.airvpn_server_countries));
        airVPNGroup.setCountryCode("");
        airVPNGroup.setServers(0);
        airVPNGroup.setUsers(0);
        airVPNGroup.setBandWidth(0);
        airVPNGroup.setMaxBandWidth(0);

        groupList.add(airVPNGroup);

        if(airVPNManifest.getCountryStats() != null)
        {
            ArrayList<String> countryNames = new ArrayList<String>();

            HashMap<String, AirVPNManifest.CountryStats> countryStats = (HashMap<String, AirVPNManifest.CountryStats>)airVPNManifest.getCountryStats().clone();

            if(countryStats != null)
            {
                for(Map.Entry<String, AirVPNManifest.CountryStats> pair : countryStats.entrySet())
                {
                    String name = countryContinent.getCountryName(pair.getKey());

                    if(name != null && !name.isEmpty())
                        countryNames.add(name);
                }

                Collections.sort(countryNames);

                for(String name : countryNames)
                {
                    AirVPNManifest.CountryStats country = countryStats.get(countryContinent.getCountryCode(name));

                    group = new Group();

                    group.setType(GroupType.COUNTRY);
                    group.setIcon(getCountryFlagResource(country.getCountryISOCode()));
                    group.setName(countryContinent.getCountryName(country.getCountryISOCode()));
                    group.setCountryCode(country.getCountryISOCode());
                    group.setServers(country.getServers());
                    group.setUsers(country.getUsers());
                    group.setBandWidth(country.getBandWidth());
                    group.setMaxBandWidth(country.getMaxBandWidth());

                    ArrayList<AirVPNServer> serverList = airVPNManifest.getServerListByCountry(country.getCountryISOCode());

                    if(serverList != null)
                    {
                        countedServers = 0;
                        countedUsers = 0;
                        countedBandWidth = 0;
                        countedMaxBandWidth = 0;

                        for(int i = 0; i < serverList.size(); i++)
                        {
                            AirVPNServer airVPNServer = serverList.get(i);

                            if(airVPNServer.isAvailable(cipherList))
                            {
                                if(searchKey.isEmpty())
                                    includeServer = true;
                                else
                                {
                                    includeServer = false;

                                    if(airVPNServer.getName().toLowerCase().contains(searchKey.toLowerCase()))
                                        includeServer = true;

                                    if(airVPNServer.getLocation().toLowerCase().contains(searchKey.toLowerCase()))
                                        includeServer = true;

                                    if(countryContinent.getCountryName(airVPNServer.getCountryCode()).toLowerCase().contains(searchKey.toLowerCase()))
                                        includeServer = true;
                                }

                                if(includeServer)
                                {
                                    itemList = new ListItem();

                                    itemList.setType(ItemType.SERVER);
                                    itemList.setName(airVPNServer.getName());
                                    itemList.setCountryCode(airVPNServer.getCountryCode());
                                    itemList.setIcon(getCountryFlagResource(airVPNServer.getCountryCode()));
                                    itemList.setLocation(airVPNServer.getLocation());
                                    itemList.setServers(0);
                                    itemList.setUsers(airVPNServer.getUsers());
                                    itemList.setBandWidth(airVPNServer.getBandWidth());
                                    itemList.setMaxBandWidth(airVPNServer.getMaxBandWidth());

                                    if(serverWhitelist.contains(itemList.getName()))
                                        itemList.setFavorite(true);
                                    else
                                        itemList.setFavorite(false);

                                    if(serverBlacklist.contains(itemList.getName()))
                                        itemList.setForbidden(true);
                                    else
                                        itemList.setForbidden(false);

                                    itemList.setWarningOpen(airVPNServer.getWarningOpen());
                                    itemList.setWarningClosed(airVPNServer.getWarningClosed());

                                    group.addItem(itemList);

                                    countryGroupAdded = true;

                                    if(cipherList != null)
                                    {
                                        countedServers++;
                                        countedUsers += airVPNServer.getUsers();
                                        countedBandWidth += airVPNServer.getBandWidth();
                                        countedMaxBandWidth += airVPNServer.getMaxBandWidth();
                                    }
                                }
                            }
                        }

                        if(cipherList != null)
                        {
                            group.setServers(countedServers);
                            group.setUsers(countedUsers);
                            group.setBandWidth(countedBandWidth);
                            group.setMaxBandWidth(countedMaxBandWidth);
                        }

                        group.sortItemList();
                    }

                    if(group.getItemList().size() > 0)
                    {
                        airVPNGroup.setServers(airVPNGroup.getServers() + group.getServers());
                        airVPNGroup.setUsers(airVPNGroup.getUsers() + group.getUsers());
                        airVPNGroup.setBandWidth(airVPNGroup.getBandWidth() + group.getBandWidth());
                        airVPNGroup.setMaxBandWidth(airVPNGroup.getMaxBandWidth() + group.getMaxBandWidth());

                        groupList.add(group);
                    }
                }
            }
        }

        if(!countryGroupAdded)
            supportTools.infoDialog(R.string.airvpn_server_not_found, true);
    }

    private void startServerConnection(final ListItem server)
    {
        if(server.getType() != ItemType.SERVER)
            return;

        final Runnable uiRunnable = new Runnable()
        {
            @Override
            public void run()
            {
                if(server.isForbidden())
                {
                    supportTools.infoDialog(R.string.airvpn_server_forbidden_connection, true);

                    return;
                }

                if(airVPNUser.checkUserLogin() == false)
                {
                    pendingServerConnection = server;

                    return;
                }

                connectServer(server);
            }
        };

        supportTools.runOnUiActivity(getActivity(), uiRunnable);
    }

    private void connectServer(ListItem server)
    {
        HashMap<Integer, String> serverEntryIP = null;
        String protocol = "", tlsMode = "";
        int port = 0;
        int entry = 0;
        boolean connectIPv6 = false;
        String profileName = "", progressMessage = "";

        pendingServerConnection = null;

        if(server.getType() != ItemType.SERVER || settingsManager == null || airVPNUser == null)
            return;

        VPN.Status currentConnectionStatus = VPN.getConnectionStatus();

        AirVPNServer airVPNServer = airVPNManifest.getServerByName(server.getName());

        if(airVPNServer == null)
        {
            EddieLogger.error("ConnectAirVPNServerFragment.connectServer(): airVPNServer is null");

            return;
        }

        profileName = airVPNUser.getCurrentProfile();

        tlsMode = settingsManager.getAirVPNTLSMode();

        if(tlsMode.equals(SettingsManager.AIRVPN_TLS_MODE_AUTH))
            entry = 0;
        else
            entry = 2;

        switch(settingsManager.getAirVPNIPVersion())
        {
            case SettingsManager.AIRVPN_IP_VERSION_4:
            {
                serverEntryIP = airVPNServer.getEntryIPv4();

                connectIPv6 = false;
            }
            break;

            case SettingsManager.AIRVPN_IP_VERSION_6:
            {
                serverEntryIP = airVPNServer.getEntryIPv6();

                connectIPv6 = true;
            }
            break;

            case SettingsManager.AIRVPN_IP_VERSION_4_OVER_6:
            {
                serverEntryIP = airVPNServer.getEntryIPv4();

                connectIPv6 = true;
            }
            break;

            default:
            {
                serverEntryIP = airVPNServer.getEntryIPv4();

                connectIPv6 = false;
            }
            break;
        }

        if(serverEntryIP == null)
        {
            EddieLogger.error("ConnectAirVPNServerFragment.connectServer(): serverEntryIP is null");

            return;
        }

        protocol = settingsManager.getAirVPNProtocol();

        port = settingsManager.getAirVPNPort();

        EddieLogger.info(String.format(Locale.getDefault(), "Trying to connect AirVPN server %s at %s, %s - Protocol %s, Port %d", airVPNServer.getName(), airVPNServer.getLocation(), countryContinent.getCountryName(airVPNServer.getCountryCode()), protocol, port));

        String openVpnProfile = airVPNUser.getOpenVPNProfile(profileName, serverEntryIP.get(entry), port, protocol, tlsMode, settingsManager.getAirVPNCipher(), connectIPv6, false, "");

        if(openVpnProfile.isEmpty())
        {
            EddieLogger.error("ConnectAirVPNServerFragment.connectServer(): openVpnProfile is empty");

            supportTools.infoDialog(R.string.connection_error, true);

            return;
        }

        profileInfo = new HashMap<String, String>();

        profileInfo.put("mode", String.format(Locale.getDefault(), "%d", VPN.connectionModeToInt(VPN.ConnectionMode.AIRVPN_SERVER)));
        profileInfo.put("name", "airvpn_server_connect");
        profileInfo.put("profile", "airvpn_server_connect");
        profileInfo.put("status", "ok");
        profileInfo.put("description", String.format("%s - %s, %s", airVPNServer.getName(), airVPNServer.getLocation(), countryContinent.getCountryName(airVPNServer.getCountryCode())));
        profileInfo.put("server", serverEntryIP.get(entry));
        profileInfo.put("port", String.format(Locale.getDefault(), "%d", port));
        profileInfo.put("protocol", protocol);

        progressMessage = String.format(getResources().getString(R.string.airvpn_server_try_connection), airVPNServer.getName(), airVPNServer.getLocation(), countryContinent.getCountryName(airVPNServer.getCountryCode()), protocol, port);

        if(currentConnectionStatus == VPN.Status.CONNECTING || currentConnectionStatus == VPN.Status.CONNECTED || currentConnectionStatus == VPN.Status.PAUSED_BY_USER || currentConnectionStatus == VPN.Status.PAUSED_BY_SYSTEM || currentConnectionStatus == VPN.Status.LOCKED)
        {
            HashMap<String, String> currentProfile = VPN.getProfileInfo();
            String message = "", serverDescription = "";

            if(currentProfile != null)
            {
                if(currentProfile.containsKey("description") && !currentProfile.get("description").isEmpty())
                    serverDescription = String.format("AirVPN %s (%s)", currentProfile.get("description"), currentProfile.get("server"));
                else
                    serverDescription = currentProfile.get("server");
            }

            message = String.format(Locale.getDefault(), getResources().getString(R.string.airvpn_server_connection_warning), serverDescription);
            message += " ";
            message += String.format(Locale.getDefault(), getResources().getString(R.string.airvpn_server_confirm_connection), server.getName(), server.getLocation(), countryContinent.getCountryName(server.getCountryCode()));

            if(supportTools.confirmationDialog(message))
            {
                try
                {
                    vpnManager.stop();

                    VPN.setPendingProfileInfo(profileInfo);
                    VPN.setPendingOpenVpnProfile(openVpnProfile);
                    VPN.setPendingProgressMessage(progressMessage);

                    return;
                }
                catch(Exception e)
                {
                    EddieLogger.error("ConnectAirVPNServerFragment.connectServer(): vpnManager.stop() exception: %s", e.getMessage());
                }
            }
            else
                return;
        }
        else
        {
            if(!supportTools.confirmationDialog(String.format(Locale.getDefault(), getResources().getString(R.string.airvpn_server_confirm_connection), server.getName(), server.getLocation(), countryContinent.getCountryName(server.getCountryCode()))))
                return;
        }

        supportTools.showConnectionProgressDialog(progressMessage);

        VPN.setProfileInfo(profileInfo);

        VPN.setConnectionMode(VPN.ConnectionMode.AIRVPN_SERVER);

        VPN.setConnectionModeDescription(getResources().getString(R.string.conn_type_airvpn_server));

        VPN.setUserProfileDescription(profileName);

        VPN.setUserName(airVPNUser.getUserName());

        VPN.setOpenVpnProfile(openVpnProfile);

        startConnection(openVpnProfile);
    }

    private void startConnection(String openVPNProfile)
    {
        if(vpnManager == null)
        {
            EddieLogger.error("ConnectAirVPNServerFragment.startConnection(): vpnManager is null");

            return;
        }

        if(openVPNProfile.equals(""))
            return;

        vpnManager.clearProfile();

        vpnManager.setProfile(openVPNProfile);

        String profileString = settingsManager.getOvpn3CustomDirectives().trim();

        if(profileString.length() > 0)
            vpnManager.addProfileString(profileString);

        vpnManager.start();
    }

    public class AirVPNServerExpandableListAdapter extends BaseExpandableListAdapter
    {
        private Context context;
        private ArrayList<Group> groupList;

        private class GroupListViewHolder
        {
            public ImageView imgIndicator;
            public ImageView imgIcon;
            public ImageView imgLoad;
            public TextView txtTitle;
            public TextView txtServers;
            public TextView txtLoad;
            public TextView txtUsers;
            public TextView txtBandWidth;
            public TextView txtMaxBandWidth;
        }

        private class CountryListViewHolder extends Object
        {
            public ImageView imgIndicator;
            public ImageView imgFlag;
            public ImageView imgLoad;
            public ImageView imgFavorite;
            public TextView txtName;
            public TextView txtServers;
            public TextView txtLoad;
            public TextView txtUsers;
            public TextView txtBandWidth;
            public TextView txtMaxBandWidth;
        }

        private class ItemListViewHolder extends Object
        {
            public ItemType type;
            public ImageView imgFlag;
            public ImageView imgLoad;
            public ImageView imgFavorite;
            public TextView txtName;
            public TextView txtLocation;
            public TextView txtLoad;
            public TextView txtBandWidth;
            public TextView txtMaxBandWidth;
            public TextView txtUsers;
            public TextView txtServers;
            public LinearLayout llServerWarning;
            public TextView txtServerWarning;
        }

        public AirVPNServerExpandableListAdapter(Context c, ArrayList<Group> g)
        {
            context = c;
            groupList = g;
        }

        public void dataSet(ArrayList<Group> g)
        {
            groupList = g;

            notifyDataSetChanged();
        }

        @Override
        public Object getGroup(int groupPosition)
        {
            return groupList.get(groupPosition);
        }

        @Override
        public int getGroupCount()
        {
            return groupList.size();
        }

        @Override
        public long getGroupId(int groupPosition)
        {
            return groupPosition;
        }

        @Override
        public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent)
        {
            Group group = (Group)getGroup(groupPosition);
            GroupListViewHolder groupListViewHolder = null;
            CountryListViewHolder countryListViewHolder = null;
            int drawable = R.drawable.icon;
            GroupType groupType = group.getType();

            switch(groupType)
            {
                case HEADER:
                case GROUP:
                {
                    if(convertView != null)
                    {
                        if(convertView.getTag() instanceof GroupListViewHolder)
                            groupListViewHolder = (GroupListViewHolder)convertView.getTag();
                        else
                            groupListViewHolder = null;
                    }

                    if(groupListViewHolder == null)
                    {
                        groupListViewHolder = new GroupListViewHolder();

                        LayoutInflater infalInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

                        convertView = infalInflater.inflate(R.layout.airvpn_server_listview_group_item, null);

                        groupListViewHolder.imgIcon = (ImageView)convertView.findViewById(R.id.img_group_icon);
                        groupListViewHolder.imgIndicator = (ImageView)convertView.findViewById(R.id.img_group_indicator);
                        groupListViewHolder.imgLoad = (ImageView)convertView.findViewById(R.id.img_group_load);
                        groupListViewHolder.txtTitle = (TextView)convertView.findViewById(R.id.txt_group_title);
                        groupListViewHolder.txtServers = (TextView)convertView.findViewById(R.id.txt_group_servers);
                        groupListViewHolder.txtLoad = (TextView)convertView.findViewById(R.id.txt_group_load);
                        groupListViewHolder.txtUsers = (TextView)convertView.findViewById(R.id.txt_group_users);
                        groupListViewHolder.txtBandWidth = (TextView)convertView.findViewById(R.id.txt_group_bandwidth);
                        groupListViewHolder.txtMaxBandWidth = (TextView)convertView.findViewById(R.id.txt_group_maxbandwidth);

                        convertView.setTag(groupListViewHolder);
                    }

                    groupListViewHolder.txtServers.setTypeface(null, Typeface.NORMAL);
                    groupListViewHolder.txtServers.setText(String.format(Locale.getDefault(), "%d", group.getServers()));

                    groupListViewHolder.imgLoad.setImageDrawable(context.getDrawable(getLoadIconResource(group.getLoad())));

                    groupListViewHolder.txtLoad.setTypeface(null, Typeface.NORMAL);
                    groupListViewHolder.txtLoad.setText(String.format(Locale.getDefault(), "%d%%", group.getLoad()));

                    groupListViewHolder.txtUsers.setTypeface(null, Typeface.NORMAL);
                    groupListViewHolder.txtUsers.setText(String.format(Locale.getDefault(), "%d", group.getUsers()));

                    groupListViewHolder.txtBandWidth.setTypeface(null, Typeface.NORMAL);
                    groupListViewHolder.txtBandWidth.setText(supportTools.formatTransferRate(group.getEffectiveBandWidth(), false));

                    groupListViewHolder.txtMaxBandWidth.setTypeface(null, Typeface.NORMAL);
                    groupListViewHolder.txtMaxBandWidth.setText(supportTools.formatTransferRate(group.getMaxBandWidth() * SupportTools.ONE_DECIMAL_MEGA, false));

                    if(groupType == GroupType.GROUP)
                    {
                        if(isExpanded)
                            drawable = R.drawable.arrow_down;
                        else
                            drawable = R.drawable.arrow_right;

                        groupListViewHolder.imgIndicator.setVisibility(View.VISIBLE);
                        groupListViewHolder.imgIndicator.setImageDrawable(context.getDrawable(drawable));
                    }
                    else
                    {
                        groupListViewHolder.imgIndicator.setVisibility(View.GONE);
                        groupListViewHolder.imgIndicator.setImageResource(android.R.color.transparent);
                    }

                    groupListViewHolder.imgIcon.setImageDrawable(context.getDrawable(group.getIcon()));

                    groupListViewHolder.txtTitle.setTypeface(null, Typeface.BOLD);
                    groupListViewHolder.txtTitle.setText(group.getName());
                }
                break;

                case COUNTRY:
                {
                    if(convertView != null)
                    {
                        if(convertView.getTag() instanceof CountryListViewHolder)
                            countryListViewHolder = (CountryListViewHolder)convertView.getTag();
                        else
                            countryListViewHolder = null;
                    }

                    if(countryListViewHolder == null)
                    {
                        countryListViewHolder = new CountryListViewHolder();

                        LayoutInflater infalInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

                        convertView = infalInflater.inflate(R.layout.airvpn_server_listview_country_group_item, null);

                        countryListViewHolder.imgIndicator = (ImageView)convertView.findViewById(R.id.img_country_indicator);
                        countryListViewHolder.imgFlag = (ImageView)convertView.findViewById(R.id.img_country_flag);
                        countryListViewHolder.imgLoad = (ImageView)convertView.findViewById(R.id.img_country_load);
                        countryListViewHolder.imgFavorite = (ImageView)convertView.findViewById(R.id.img_country_favorite);
                        countryListViewHolder.txtName = (TextView)convertView.findViewById(R.id.txt_country_name);
                        countryListViewHolder.txtServers = (TextView)convertView.findViewById(R.id.txt_country_servers);
                        countryListViewHolder.txtLoad = (TextView)convertView.findViewById(R.id.txt_country_load);
                        countryListViewHolder.txtUsers = (TextView)convertView.findViewById(R.id.txt_country_users);
                        countryListViewHolder.txtBandWidth = (TextView)convertView.findViewById(R.id.txt_country_bandwidth);
                        countryListViewHolder.txtMaxBandWidth = (TextView)convertView.findViewById(R.id.txt_country_maxbandwidth);

                        convertView.setTag(countryListViewHolder);
                    }

                    if(isExpanded)
                        drawable = R.drawable.arrow_down;
                    else
                        drawable = R.drawable.arrow_right;

                    countryListViewHolder.imgIndicator.setImageDrawable(context.getDrawable(drawable));

                    countryListViewHolder.imgFlag.setImageDrawable(context.getDrawable(group.getIcon()));

                    countryListViewHolder.txtName.setTypeface(null, Typeface.BOLD);
                    countryListViewHolder.txtName.setText(group.getName());

                    countryListViewHolder.txtServers.setTypeface(null, Typeface.NORMAL);
                    countryListViewHolder.txtServers.setText(String.format(Locale.getDefault(), "%d", group.getServers()));

                    countryListViewHolder.imgLoad.setImageDrawable(context.getDrawable(getLoadIconResource(group.getLoad())));

                    countryListViewHolder.txtLoad.setTypeface(null, Typeface.NORMAL);
                    countryListViewHolder.txtLoad.setText(String.format(Locale.getDefault(), "%d%%", group.getLoad()));

                    countryListViewHolder.txtUsers.setTypeface(null, Typeface.NORMAL);
                    countryListViewHolder.txtUsers.setText(String.format(Locale.getDefault(), "%d", group.getUsers()));

                    countryListViewHolder.txtBandWidth.setTypeface(null, Typeface.NORMAL);
                    countryListViewHolder.txtBandWidth.setText(supportTools.formatTransferRate(group.getEffectiveBandWidth(), false));

                    countryListViewHolder.txtMaxBandWidth.setTypeface(null, Typeface.NORMAL);
                    countryListViewHolder.txtMaxBandWidth.setText(supportTools.formatTransferRate(group.getMaxBandWidth() * SupportTools.ONE_DECIMAL_MEGA, false));

                    if(countryWhitelist.contains(group.getCountryCode()))
                    {
                        countryListViewHolder.imgFavorite.setVisibility(View.VISIBLE);

                        countryListViewHolder.imgFavorite.setImageDrawable(context.getDrawable(R.drawable.star_icon_on));
                    }
                    else if(countryBlacklist.contains(group.getCountryCode()))
                    {
                        countryListViewHolder.imgFavorite.setVisibility(View.VISIBLE);

                        countryListViewHolder.imgFavorite.setImageDrawable(context.getDrawable(R.drawable.blacklist_icon_on));
                    }
                    else
                    {
                        countryListViewHolder.imgFavorite.setVisibility(View.GONE);
                        countryListViewHolder.imgFavorite.setImageResource(android.R.color.transparent);
                    }
                }
                break;
            }

            return convertView;
        }

        @Override
        public Object getChild(int groupPosition, int childPosititon)
        {
            return groupList.get(groupPosition).getItemList().get(childPosititon);
        }

        @Override
        public long getChildId(int groupPosition, int childPosition)
        {
            return childPosition;
        }

        @Override
        public int getChildrenCount(int groupPosition)
        {
            return groupList.get(groupPosition).getItemList().size();
        }

        @Override
        public View getChildView(int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent)
        {
            ItemListViewHolder itemListViewHolder = null;
            ListItem itemList = (ListItem)getChild(groupPosition, childPosition);
            ItemType itemType = itemList.getType();

            if(convertView != null)
                itemListViewHolder = (ItemListViewHolder)convertView.getTag();

            switch(itemType)
            {
                case SERVER:
                {
                    if(itemListViewHolder == null || itemListViewHolder.type != ItemType.SERVER)
                    {
                        itemListViewHolder = new ItemListViewHolder();

                        LayoutInflater infalInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

                        convertView = infalInflater.inflate(R.layout.airvpn_server_listview_server_item, null);

                        itemListViewHolder.type = ItemType.SERVER;
                        itemListViewHolder.txtName = (TextView)convertView.findViewById(R.id.txt_server_name);
                        itemListViewHolder.imgFlag = (ImageView)convertView.findViewById(R.id.img_server_flag);
                        itemListViewHolder.imgLoad = (ImageView)convertView.findViewById(R.id.img_server_load);
                        itemListViewHolder.imgFavorite = (ImageView)convertView.findViewById(R.id.img_server_favorite);
                        itemListViewHolder.txtLocation = (TextView)convertView.findViewById(R.id.txt_server_location);
                        itemListViewHolder.txtLoad = (TextView)convertView.findViewById(R.id.txt_server_load);
                        itemListViewHolder.txtServers = null;
                        itemListViewHolder.txtUsers = (TextView)convertView.findViewById(R.id.txt_server_users);
                        itemListViewHolder.txtBandWidth = (TextView)convertView.findViewById(R.id.txt_server_bandwidth);
                        itemListViewHolder.txtMaxBandWidth = (TextView)convertView.findViewById(R.id.txt_server_maxbandwidth);
                        itemListViewHolder.llServerWarning = (LinearLayout)convertView.findViewById(R.id.server_warning_layout);
                        itemListViewHolder.txtServerWarning = (TextView)convertView.findViewById(R.id.txt_server_warning);

                        convertView.setTag(itemListViewHolder);
                    }
                }
                break;

                case COUNTRY:
                {
                    if(convertView != null)
                        itemListViewHolder = (ItemListViewHolder)convertView.getTag();

                    if(itemListViewHolder == null || itemListViewHolder.type != ItemType.COUNTRY)
                    {
                        itemListViewHolder = new ItemListViewHolder();

                        LayoutInflater infalInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

                        convertView = infalInflater.inflate(R.layout.airvpn_server_listview_country_group_item, null);

                        ImageView imgIndicator = (ImageView)convertView.findViewById(R.id.img_country_indicator);
                        imgIndicator.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.country_icon));
                        imgIndicator.setVisibility(View.VISIBLE);

                        itemListViewHolder.type = ItemType.COUNTRY;
                        itemListViewHolder.txtName = (TextView)convertView.findViewById(R.id.txt_country_name);
                        itemListViewHolder.imgFlag = (ImageView)convertView.findViewById(R.id.img_country_flag);
                        itemListViewHolder.imgLoad = (ImageView)convertView.findViewById(R.id.img_country_load);
                        itemListViewHolder.imgFavorite = (ImageView)convertView.findViewById(R.id.img_country_favorite);
                        itemListViewHolder.txtLocation = null;
                        itemListViewHolder.txtLoad = (TextView)convertView.findViewById(R.id.txt_country_load);
                        itemListViewHolder.txtServers = (TextView)convertView.findViewById(R.id.txt_country_servers);
                        itemListViewHolder.txtUsers = (TextView)convertView.findViewById(R.id.txt_country_users);
                        itemListViewHolder.txtBandWidth = (TextView)convertView.findViewById(R.id.txt_country_bandwidth);
                        itemListViewHolder.txtMaxBandWidth = (TextView)convertView.findViewById(R.id.txt_country_maxbandwidth);

                        convertView.setTag(itemListViewHolder);
                    }
                }
                break;
            }

            itemListViewHolder.txtName.setTypeface(null, Typeface.BOLD);
            itemListViewHolder.txtName.setText(itemList.getName());

            itemListViewHolder.imgFlag.setImageDrawable(context.getDrawable(itemList.getIcon()));

            if(itemType == ItemType.SERVER)
            {
                itemListViewHolder.txtLocation.setTypeface(null, Typeface.NORMAL);
                itemListViewHolder.txtLocation.setText(itemList.getLocation());
            }

            itemListViewHolder.imgLoad.setImageDrawable(context.getDrawable(getLoadIconResource(itemList.getLoad())));

            itemListViewHolder.txtLoad.setTypeface(null, Typeface.NORMAL);
            itemListViewHolder.txtLoad.setText(String.format(Locale.getDefault(), "%d%%", itemList.getLoad()));

            if(itemType == ItemType.COUNTRY)
            {
                itemListViewHolder.txtServers.setTypeface(null, Typeface.NORMAL);
                itemListViewHolder.txtServers.setText(String.format(Locale.getDefault(), "%d", itemList.getServers()));
            }

            itemListViewHolder.txtUsers.setTypeface(null, Typeface.NORMAL);
            itemListViewHolder.txtUsers.setText(String.format(Locale.getDefault(), "%d", itemList.getUsers()));

            itemListViewHolder.txtBandWidth.setTypeface(null, Typeface.NORMAL);
            itemListViewHolder.txtBandWidth.setText(supportTools.formatTransferRate(itemList.getEffectiveBandWidth(), false));

            itemListViewHolder.txtMaxBandWidth.setTypeface(null, Typeface.NORMAL);
            itemListViewHolder.txtMaxBandWidth.setText(supportTools.formatTransferRate(itemList.getMaxBandWidth() * SupportTools.ONE_DECIMAL_MEGA, false));

            if(itemList.isFavorite() || itemList.isForbidden())
            {
                itemListViewHolder.imgFavorite.setVisibility(View.VISIBLE);

                if(itemList.isFavorite())
                    itemListViewHolder.imgFavorite.setImageDrawable(context.getDrawable(R.drawable.star_icon_on));
                else
                    itemListViewHolder.imgFavorite.setImageDrawable(context.getDrawable(R.drawable.blacklist_icon_on));
            }
            else
            {
                itemListViewHolder.imgFavorite.setVisibility(View.GONE);
                itemListViewHolder.imgFavorite.setImageResource(android.R.color.transparent);
            }

            if(itemType == ItemType.SERVER)
            {
                if(itemList.getWarningOpen().isEmpty())
                {
                    itemListViewHolder.llServerWarning.setVisibility(View.GONE);
                    itemListViewHolder.txtServerWarning.setText("");

                }
                else
                {
                    itemListViewHolder.llServerWarning.setVisibility(View.VISIBLE);
                    itemListViewHolder.txtServerWarning.setText(itemList.getWarningOpen());
                }
            }

            return convertView;
        }
        @Override
        public boolean hasStableIds()
        {
            return false;
        }

        @Override
        public boolean isChildSelectable(int groupPosition, int childPosition)
        {
            return true;
        }
    }

    private int getCountryFlagResource(String id)
    {
        id = "flag_" + id.toLowerCase();

        int res = getContext().getResources().getIdentifier(id, "drawable", getContext().getPackageName());

        return res;
    }

    private int getLoadIconResource(int load)
    {
        if(load < 0)
            load = 0;

        if(load > 100)
            load = 100;

        int iconId = (load * 8) / 100;

        int res = getContext().getResources().getIdentifier(String.format(Locale.getDefault(),"load_icon_%d", iconId), "drawable", getContext().getPackageName());

        if(res == 0)
            res = R.drawable.load_icon_0;

        return res;
    }

    private void searchDialog()
    {
        TextView txtDialogTitle = null;

        btnOk = null;
        btnCancel = null;
        btnReset = null;

        dialogHandler = new Handler(new Handler.Callback()
        {
            @Override
            public boolean handleMessage(Message mesg)
            {
                throw new RuntimeException();
            }
        });

        dialogBuilder = new AlertDialog.Builder(getContext());

        View content = LayoutInflater.from(getContext()).inflate(R.layout.edit_option_dialog, null);

        txtDialogTitle = (TextView)content.findViewById(R.id.title);
        edtKey = (EditText)content.findViewById(R.id.key);

        edtKey.setText(searchKey);
        edtKey.setSelection(edtKey.getText().length());

        btnOk = (Button)content.findViewById(R.id.btn_ok);
        btnCancel = (Button)content.findViewById(R.id.btn_cancel);
        btnReset = (Button)content.findViewById(R.id.btn_service);

        btnOk.setText(R.string.search);

        btnReset.setText(R.string.airvpn_server_reset_button);
        btnReset.setVisibility(View.VISIBLE);

        supportTools.enableButton(btnOk, false);

        supportTools.enableButton(btnCancel, true);

        btnOk.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View arg0)
            {
                searchKey = edtKey.getText().toString();

                searchDialog.dismiss();

                dialogHandler.sendMessage(dialogHandler.obtainMessage());
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View arg0)
            {
                searchDialog.dismiss();

                dialogHandler.sendMessage(dialogHandler.obtainMessage());
            }
        });

        btnReset.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View arg0)
            {
                searchKey = "";

                searchDialog.dismiss();

                dialogHandler.sendMessage(dialogHandler.obtainMessage());
            }
        });

        edtKey.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void afterTextChanged(Editable s)
            {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                supportTools.enableButton(btnOk, !edtKey.getText().toString().isEmpty());
            }
        });

        txtDialogTitle.setText(R.string.airvpn_server_search_dialog);

        dialogBuilder.setTitle("");
        dialogBuilder.setView(content);

        searchDialog = dialogBuilder.create();
        searchDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        searchDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);

        if(getActivity().isFinishing())
            return;

        searchDialog.show();

        try
        {
            Looper.loop();
        }
        catch(RuntimeException e)
        {
        }

        refreshServerList();

        if(searchKey.isEmpty())
            btnSearch.setBackgroundResource(R.drawable.search);
        else
            btnSearch.setBackgroundResource(R.drawable.search_on);
    }

    private void refreshServerList()
    {
        createGroupList();

        serverListAdapter.dataSet(groupList);

        showServerCipher();
    }

    private void showServerCipher()
    {
        if(llServerCipher == null || txtServerCipher == null)
            return;

        if(!settingsManager.getAirVPNCipher().equals(SettingsManager.AIRVPN_CIPHER_SERVER))
        {
            llServerCipher.setVisibility(View.VISIBLE);
            txtServerCipher.setText(getResources().getString(R.string.conn_data_cipher_name_cap) + " " + settingsManager.getAirVPNCipher());
        }
        else
            llServerCipher.setVisibility(View.GONE);
    }

    private class Group
    {
        private GroupType type;
        private int icon;
        private String name;
        private String countryCode;
        private int servers;
        private int users;
        private long bandWidth;
        private long maxBandWidth;
        private ArrayList<ListItem> itemList;
        private String sortMode = "", sortBy = "";

        private Comparator<ListItem> comparatorServerName = new Comparator<ListItem>()
        {
            @Override
            public int compare(ListItem s1, ListItem s2)
            {
                int result;

                if(sortMode.equals(SettingsManager.AIRVPN_SERVER_SORT_MODE_ASCENDING))
                    result = s1.getName().compareToIgnoreCase(s2.getName());
                else
                    result = s2.getName().compareToIgnoreCase(s1.getName());

                return result;
            }
        };

        private Comparator<ListItem> comparatorServerLocation = new Comparator<ListItem>()
        {
            @Override
            public int compare(ListItem s1, ListItem s2)
            {
                int result;

                if(sortMode.equals(SettingsManager.AIRVPN_SERVER_SORT_MODE_ASCENDING))
                    result = s1.getLocation().compareToIgnoreCase(s2.getLocation());
                else
                    result = s2.getLocation().compareToIgnoreCase(s1.getLocation());

                return result;
            }
        };

        private Comparator<ListItem> comparatorServerLoad = new Comparator<ListItem>()
        {
            @Override
            public int compare(ListItem s1, ListItem s2)
            {
                int result;

                if(sortMode.equals(SettingsManager.AIRVPN_SERVER_SORT_MODE_ASCENDING))
                    result = s1.getLoad() - s2.getLoad();
                else
                    result = s2.getLoad() - s1.getLoad();

                return result;
            }
        };

        private Comparator<ListItem> comparatorServerUsers = new Comparator<ListItem>()
        {
            @Override
            public int compare(ListItem s1, ListItem s2)
            {
                int result;

                if(sortMode.equals(SettingsManager.AIRVPN_SERVER_SORT_MODE_ASCENDING))
                    result = s1.getUsers() - s2.getUsers();
                else
                    result = s2.getUsers() - s1.getUsers();

                return result;
            }
        };

        private Comparator<ListItem> comparatorServerBandwidth = new Comparator<ListItem>()
        {
            @Override
            public int compare(ListItem s1, ListItem s2)
            {
                int result;

                if(sortMode.equals(SettingsManager.AIRVPN_SERVER_SORT_MODE_ASCENDING))
                    result = (int)(s1.getBandWidth() - s2.getBandWidth());
                else
                    result = (int)(s2.getBandWidth() - s1.getBandWidth());

                return result;
            }
        };

        private Comparator<ListItem> comparatorServerMaxBandwidth = new Comparator<ListItem>()
        {
            @Override
            public int compare(ListItem s1, ListItem s2)
            {
                int result;

                if(sortMode.equals(SettingsManager.AIRVPN_SERVER_SORT_MODE_ASCENDING))
                    result = (int)(s1.getMaxBandWidth() - s2.getMaxBandWidth());
                else
                    result = (int)(s2.getMaxBandWidth() - s1.getMaxBandWidth());

                return result;
            }
        };

        Group()
        {
            type = GroupType.GROUP;
            name = "";
            countryCode = "";
            servers = 0;
            users = 0;
            bandWidth = 0;
            maxBandWidth = 0;
            itemList = new ArrayList<ListItem>();
        }

        public void setType(GroupType t)
        {
            type = t;
        }

        public GroupType getType()
        {
            return type;
        }

        public void setIcon(int i)
        {
            icon = i;
        }

        public int getIcon()
        {
            return icon;
        }

        public void setName(String n)
        {
            name = n;
        }

        public String getName()
        {
            return name;
        }

        public void setCountryCode(String c)
        {
            countryCode = c;
        }

        public String getCountryCode()
        {
            return countryCode;
        }

        public void setServers(int s)
        {
            servers = s;
        }

        public int getServers()
        {
            return servers;
        }

        public void setUsers(int u)
        {
            users = u;
        }

        public int getUsers()
        {
            return users;
        }

        public long getBandWidth()
        {
            return bandWidth;
        }

        public void setBandWidth(long b)
        {
            bandWidth = b;
        }

        public long getEffectiveBandWidth()
        {
            return (2 * (bandWidth * 8));
        }

        public long getMaxBandWidth()
        {
            return maxBandWidth;
        }

        public void setMaxBandWidth(long b)
        {
            maxBandWidth = b;
        }

        public void addItem(ListItem s)
        {
            itemList.add(s);
        }

        public ArrayList<ListItem> getItemList()
        {
            return itemList;
        }

        public int getLoad()
        {
            return supportTools.getLoad(bandWidth, maxBandWidth);
        }

        public void sortItemList()
        {
            sortBy = settingsManager.getAirVPNSortBy();
            sortMode = settingsManager.getAirVPNSortMode();

            switch(sortBy)
            {
                case SettingsManager.AIRVPN_SERVER_SORT_BY_NAME:
                {
                    Collections.sort(itemList, comparatorServerName);
                }
                break;

                case SettingsManager.AIRVPN_SERVER_SORT_BY_LOCATION:
                {
                    Collections.sort(itemList, comparatorServerLocation);
                }
                break;

                case SettingsManager.AIRVPN_SERVER_SORT_BY_LOAD:
                {
                    Collections.sort(itemList, comparatorServerLoad);
                }
                break;

                case SettingsManager.AIRVPN_SERVER_SORT_BY_USERS:
                {
                    Collections.sort(itemList, comparatorServerUsers);
                }
                break;

                case SettingsManager.AIRVPN_SERVER_SORT_BY_BANDWIDTH:
                {
                    Collections.sort(itemList, comparatorServerBandwidth);
                }
                break;

                case SettingsManager.AIRVPN_SERVER_SORT_BY_MAX_BANDWIDTH:
                {
                    Collections.sort(itemList, comparatorServerMaxBandwidth);
                }
                break;

                default:
                {
                    Collections.sort(itemList, comparatorServerName);
                }
                break;
            }
        }
    }

    private class ListItem
    {
        private ItemType type;
        private String name;
        private int icon;
        private String countryCode;
        private String location;
        private int servers;
        private int users;
        private long bandWidth;
        private long maxBandWidth;
        private boolean favorite;
        private boolean forbidden;
        private String warningOpen;
        private String warningClosed;

        ListItem()
        {
            type = ItemType.SERVER;
            name = "";
            icon = 0;
            countryCode = "";
            location = "";
            servers = 0;
            users = 0;
            bandWidth = 0;
            maxBandWidth = 0;
            favorite = false;
            forbidden = false;
            warningOpen = "";
            warningClosed = "";
        }

        public void setType(ItemType t)
        {
            type = t;
        }

        public ItemType getType()
        {
            return type;
        }

        public void setName(String n)
        {
            name = n;
        }

        public String getName()
        {
            return name;
        }

        public void setIcon(int i)
        {
            icon = i;
        }

        public int getIcon()
        {
            return icon;
        }

        public void setCountryCode(String c)
        {
            countryCode = c;
        }

        public String getCountryCode()
        {
            return countryCode;
        }

        public void setLocation(String l)
        {
            location = l;
        }

        public String getLocation()
        {
            return location;
        }

        public void setServers(int s)
        {
            servers = s;
        }

        public int getServers()
        {
            return servers;
        }

        public void setUsers(int u)
        {
            users = u;
        }

        public int getUsers()
        {
            return users;
        }

        public long getBandWidth()
        {
            return bandWidth;
        }

        public void setBandWidth(long b)
        {
            bandWidth = b;
        }

        public long getEffectiveBandWidth()
        {
            return (2 * (bandWidth * 8));
        }

        public long getMaxBandWidth()
        {
            return maxBandWidth;
        }

        public void setMaxBandWidth(long b)
        {
            maxBandWidth = b;
        }

        public void setFavorite(boolean f)
        {
            favorite = f;
        }

        public boolean isFavorite()
        {
            return favorite;
        }

        public void setForbidden(boolean f)
        {
            forbidden = f;
        }

        public boolean isForbidden()
        {
            return forbidden;
        }

        public void setWarningOpen(String s)
        {
            warningOpen = s;
        }

        public String getWarningOpen()
        {
            return warningOpen;
        }

        public void setWarningClosed(String s)
        {
            warningClosed = s;
        }

        public String getWarningClosed()
        {
            return warningClosed;
        }

        public int getLoad()
        {
            return supportTools.getLoad(bandWidth, maxBandWidth);
        }
    }

    // Eddie events

    public void onVpnConnectionDataChanged(final OpenVPNConnectionData connectionData)
    {
    }

    public void onVpnStatusChanged(VPN.Status status, String message)
    {
    }

    public void onVpnAuthFailed(final OpenVPNEvent oe)
    {
    }

    public void onVpnError(final OpenVPNEvent oe)
    {
    }

    public void onMasterPasswordChanged()
    {
    }

    public void onAirVPNLogin()
    {
        Runnable runnable = new Runnable()
        {
            @Override
            public void run()
            {
                if(NetworkStatusReceiver.isNetworkConnected())
                    supportTools.enableButton(btnReloadManifest, true);

                registerForContextMenu(elvAirVPNServer);

                if(pendingServerConnection != null)
                    connectServer(pendingServerConnection);
            }
        };

        supportTools.runOnUiActivity(getActivity(), runnable);
    }

    public void onAirVPNLoginFailed()
    {
        Runnable runnable = new Runnable()
        {
            @Override
            public void run()
            {
                supportTools.enableButton(btnReloadManifest, false);

                unregisterForContextMenu(elvAirVPNServer);
            }
        };

        supportTools.runOnUiActivity(getActivity(), runnable);
    }

    public void onAirVPNLogout()
    {
        Runnable runnable = new Runnable()
        {
            @Override
            public void run()
            {
                supportTools.enableButton(btnReloadManifest, false);

                unregisterForContextMenu(elvAirVPNServer);
            }
        };

        supportTools.runOnUiActivity(getActivity(), runnable);
    }

    public void onAirVPNUserDataChanged()
    {
    }

    public void onAirVPNUserProfileReceived(Document document)
    {
    }

    public void onAirVPNUserProfileDownloadError()
    {
    }

    public void onAirVPNUserProfileChanged()
    {
    }

    public void onAirVPNManifestReceived(Document document)
    {
    }

    public void onAirVPNManifestDownloadError()
    {
        supportTools.dismissProgressDialog();

        if(NetworkStatusReceiver.isNetworkConnected())
        {
            supportTools.infoDialog(String.format("%s%s", getResources().getString(R.string.manifest_download_error), getResources().getString(R.string.bootstrap_server_error)), true);

            EddieLogger.error("Error while retrieving AirVPN manifest from server");
        }
    }

    public void onAirVPNManifestChanged()
    {
        Runnable runnable = new Runnable()
        {
            @Override
            public void run()
            {
                createGroupList();

                if(elvAirVPNServer != null)
                {
                    serverListAdapter = new AirVPNServerExpandableListAdapter(getContext(), groupList);

                    elvAirVPNServer.setAdapter(serverListAdapter);

                    showServerCipher();
                }

                supportTools.dismissProgressDialog();
            }
        };

        supportTools.runOnUiActivity(getActivity(), runnable);
    }

    public void onAirVPNIgnoredDocumentRequest()
    {
    }

    public void onCancelConnection()
    {
    }

    // NetworkStatusReceiver

    public void onNetworkStatusNotAvailable()
    {
        if(btnReloadManifest != null)
            supportTools.enableButton(btnReloadManifest, false);
    }

    public void onNetworkStatusConnected()
    {
        if(btnReloadManifest != null && airVPNUser.isUserValid())
            supportTools.enableButton(btnReloadManifest, true);
    }

    public void onNetworkStatusIsConnecting()
    {
    }

    public void onNetworkStatusIsDisonnecting()
    {
    }

    public void onNetworkStatusSuspended()
    {
    }

    public void onNetworkStatusNotConnected()
    {
    }

    public void onNetworkTypeChanged()
    {
    }
}