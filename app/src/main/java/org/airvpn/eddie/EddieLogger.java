// <eddie_source_header>
// This file is part of Eddie/AirVPN software.
// Copyright (C)2014-2018 AirVPN (support@airvpn.org) / https://airvpn.org
//
// Eddie is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Eddie is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Eddie. If not, see <http://www.gnu.org/licenses/>.
// </eddie_source_header>
//
// 4 September 2018 - author: ProMIND - initial release.

package org.airvpn.eddie;

import android.content.Context;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;
import java.util.TimeZone;
import java.util.logging.FileHandler;
import java.util.logging.Formatter;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import java.util.logging.MemoryHandler;

public class EddieLogger
{
    private final static String EddieLogName = "EddieLogger";
    private final static String EddieLogFileName = "EddieLogger.log";
    private static Logger logger = null;
    private static File logFile = null;
    private static FileHandler logFileHandler = null;
    private static MemoryHandler logMemoryHandler = null;
    private final static int logMaxRecords = 500;
    private final static String lineFeedMarker = "**n*n**";

    class EddieLogFormatter extends Formatter
    {
        @Override
        public String format(LogRecord record)
        {
            return String.format(Locale.getDefault(), "%d|%d|%s\n", record.getMillis() / 1000, record.getLevel().intValue(), record.getMessage());
        }

        @Override
        public String getHead(Handler h)
        {
            return super.getHead(h);
        }

        @Override
        public String getTail(Handler h)
        {
            return super.getTail(h);
        }
    }

    public synchronized void init(Context context)
    {
        if(logger != null && logFile != null && logFileHandler != null && logMemoryHandler != null)
            return;

        if(logger == null)
            logger = Logger.getLogger(EddieLogName);

        try
        {
            logFile = new java.io.File(context.getFilesDir(), EddieLogFileName);
        }
        catch(Exception e)
        {
            logFile = null;
        }

        if(logFile != null)
        {
            try
            {
                if(logFile.exists())
                    emptyLogFile();
            }
            catch(Exception e)
            {
            }

            try
            {
                logFileHandler = new FileHandler(logFile.getPath(), false);
            }
            catch(Exception e)
            {
                logFileHandler = null;
            }

            if(logFileHandler != null)
            {
                try
                {
                    logMemoryHandler = new MemoryHandler(logFileHandler, logMaxRecords, Level.OFF);
                }
                catch(Exception e)
                {
                    logMemoryHandler = null;
                }

                if(logMemoryHandler != null)
                {
                    try
                    {
                        logMemoryHandler.setPushLevel(Level.OFF);

                        logger.addHandler(logMemoryHandler);

                        logFileHandler.setLevel(Level.ALL);
                        logFileHandler.setFormatter(new EddieLogFormatter());
                        logger.setLevel(Level.ALL);
                    }
                    catch(Exception e)
                    {
                        logFileHandler = null;
                        logMemoryHandler = null;
                        logger = null;
                    }
                }
            }
        }
    }

    private static synchronized void log(Level level, String message, Object... args)
    {
        if(logger == null || logFileHandler == null || logMemoryHandler == null)
            return;

        String logMessage = "";

        if(args != null && args.length > 0)
            logMessage = String.format(Locale.getDefault(), message, args);
        else
            logMessage = message;

        logger.log(level, logMessage.replace("\n", lineFeedMarker));
    }

    public static void debug(String message)
    {
        log(Level.FINE, message, "");
    }

    public static void debug(String message, Object... args)
    {
        log(Level.FINE, message, args);
    }

    public static void info(String message)
    {
        log(Level.INFO, message, "");
    }

    public static void info(String message, Object... args)
    {
        log(Level.INFO, message, args);
    }

    public static void warning(String message, Object... args)
    {
        log(Level.WARNING, message, args);
    }

    public static void error(String message, Object... args)
    {
        log(Level.SEVERE, message, args);
    }

    public static void error(String prefix, Exception e)
    {
        Object[] obj = {SupportTools.getExceptionDetails(e)};

        log(Level.SEVERE, prefix, obj);
    }

    public static void error(Exception e)
    {
        Object[] obj = {e.getMessage()};

        log(Level.SEVERE, "Exception: %s", obj);
    }

    public static void fatal(String message, Object... args)
    {
        log(Level.SEVERE, message, args);
    }

    public static void fatal(Exception e)
    {
        Object[] obj = {e.getMessage()};

        log(Level.SEVERE, "Fatal exception: %s", obj);
    }

    public static void fatal(String prefix, Exception e)
    {
        Object[] obj = {SupportTools.getExceptionDetails(e)};

        log(Level.SEVERE, prefix, obj);
    }

    public ArrayList<LogItem> getLog()
    {
        if(logger == null || logMemoryHandler == null ||logFileHandler == null || logFile == null)
            return null;

        logMemoryHandler.push();

        String[] item = null;

        ArrayList<LogItem> log = new ArrayList<LogItem>();

        InputStream inputStream = null;
        Reader reader = null;
        BufferedReader bufferedReader = null;

        try
        {
            String line;

            inputStream = new FileInputStream(logFile.getPath());

            reader = new InputStreamReader(inputStream, "UTF-8");

            bufferedReader = new BufferedReader(reader);

            while((line = bufferedReader.readLine()) != null)
            {
                LogItem logItem;

                item = line.split("\\|");

                if(item.length == 3)
                {
                    logItem = new LogItem();

                    try
                    {
                        logItem.utcUnixTimestamp = Long.parseLong(item[0]);
                    }
                    catch(NumberFormatException e)
                    {
                        logItem.utcUnixTimestamp = 0;
                    }

                    try
                    {
                        logItem.logLevel = Level.parse(item[1]);
                    }
                    catch(Exception e)
                    {
                        logItem.logLevel = Level.OFF;
                    }

                    logItem.message = item[2].replace(lineFeedMarker, "\n");

                    log.add(logItem);
                }
            }
        }
        catch(Exception e)
        {
            log = null;
        }
        finally
        {
            if(bufferedReader != null)
            {
                try
                {
                    bufferedReader.close();
                }
                catch(Throwable t)
                {
                }
            }

            if(reader != null)
            {
                try
                {
                    reader.close();
                }
                catch(Throwable t)
                {
                }
            }

            if(inputStream != null)
            {
                try
                {
                    inputStream.close();
                }
                catch(Throwable t)
                {
                }
            }
        }

        return log;
    }

    private void emptyLogFile()
    {
        PrintWriter printWriter = null;

        if(!logFile.exists())
            return;

        try
        {
            printWriter = new PrintWriter(logFile);
        }
        catch(FileNotFoundException e)
        {
        }
        finally
        {
            printWriter.print("");

            printWriter.close();
        }
    }

    public class LogItem
    {
        public long utcUnixTimestamp;
        public Level logLevel;
        public String message;
    }
}
