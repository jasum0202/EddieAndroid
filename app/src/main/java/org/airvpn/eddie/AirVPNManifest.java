// <eddie_source_header>
// This file is part of Eddie/AirVPN software.
// Copyright (C)2014-2018 AirVPN (support@airvpn.org) / https://airvpn.org
//
// Eddie is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Eddie is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Eddie. If not, see <http://www.gnu.org/licenses/>.
// </eddie_source_header>
//
// 12 October 2018 - author: ProMIND - initial release. (a tribute to the 1859 Perugia uprising occurred on 20 June 1859 and in memory of those brave inhabitants who fought for the liberty of Perugia)

package org.airvpn.eddie;

import android.content.Context;
import android.content.res.AssetManager;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.NodeList;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;

public class AirVPNManifest implements NetworkStatusListener, EddieEventListener
{
    public enum ManifestType
    {
        NOT_SET,
        PROCESSING,
        DEFAULT,
        STORED,
        FROM_SERVER
    }

    private final String AIRVPN_MANIFEST_FILE_NAME = "AirVPNManifest.dat";

    private static Document airVPNManifestDocument = null;
    private static AirVPNServerGroup airVPNServerGroup = null;
    private static CipherDatabase cipherDatabase = null;

    private static ManifestType manifestType = ManifestType.NOT_SET;

    private static Context appContext = null;
    private static SupportTools supportTools = null;
    private static EddieEvent eddieEvent = null;
    private static NetworkStatusReceiver networkStatusReceiver = null;

    private static long processTimeTS = 0;
    private static long manifestTimeTS = 0;
    private static long manifestNextUpdateTS = 0;
    private static int nextUpdateIntervalMinutes = 0;
    private static ArrayList<Message> manifestMessage = null;
    private static String dnsCheckHost = "";
    private static String dnsCheckRes1 = "";
    private static String dnsCheckRes2 = "";
    private static double speedFactor = 1.0;
    private static double loadFactor = 1.0;
    private static double userFactor = 1.0;
    private static int latencyFactor = 0;
    private static int penalityFactor = 0;
    private static int pingerDelay = 0;
    private static int pingerRetry = 0;
    private static String checkDomain = "";
    private static String checkDnsQuery = "";
    private static String openVpnDirectives = "";
    private static String modeProtocol = "";
    private static int modePort = 0;
    private static int modeAlt = 0;
    private static ArrayList<String> bootStrapServerUrl = null;
    private static ArrayList<AirVPNServer> airVpnServer = null;
    private static HashMap<String, CountryStats> countryStats = null;
    private static String rsaPublicKeyModulus = "";
    private static String rsaPublicKeyExponent = "";
    private static boolean refreshPending = false;

    public class Message
    {
        private String text = "";
        private String url = "";
        private String link = "";
        private String html = "";
        private long fromUTCTimeTS = 0;
        private long toUTCTimeTS = 0;
        private boolean shown = false;

        public void setText(String s)
        {
            text = s;
        }

        public String getText()
        {
            return text;
        }

        public void setUrl(String u)
        {
            url = u;
        }

        public String getUrl()
        {
            return url;
        }

        public void setLink(String l)
        {
            link = l;
        }

        public String getLink()
        {
            return link;
        }

        public void setHtml(String h)
        {
            html = h;
        }

        public String getHtml()
        {
            return html;
        }

        public void setFromUTCTimeTS(long ts)
        {
            fromUTCTimeTS = ts;
        }

        public long getFromUTCTimeTS()
        {
            return fromUTCTimeTS;
        }

        public void setToUTCTimeTS(long ts)
        {
            toUTCTimeTS = ts;
        }

        public long getToUTCTimeTS()
        {
            return toUTCTimeTS;
        }

        public void setShown(boolean s)
        {
            shown = s;
        }

        public boolean isShown()
        {
            return shown;
        }
    }

    public class CountryStats
    {
        private String countryISOCode = "";
        private int servers = 0;
        private int users = 0;
        private long bandWidth;
        private long maxBandWidth;

        public CountryStats()
        {
            countryISOCode = "";
            servers = 0;
            users = 0;
            bandWidth = 0;
            maxBandWidth = 0;
        }

        public void setCountryISOCode(String code)
        {
            countryISOCode = code;
        }

        public String getCountryISOCode()
        {
            return countryISOCode;
        }

        public void setServers(int s)
        {
            servers = s;
        }

        public int getServers()
        {
            return servers;
        }

        public void setUsers(int u)
        {
            users = u;
        }

        public int getUsers()
        {
            return users;
        }

        public long getBandWidth()
        {
            return bandWidth;
        }

        public void setBandWidth(long b)
        {
            bandWidth = b;
        }

        public long getMaxBandWidth()
        {
            return maxBandWidth;
        }

        public void setMaxBandWidth(long b)
        {
            maxBandWidth = b;
        }

        public int getLoad()
        {
            return supportTools.getLoad(bandWidth, maxBandWidth);
        }
    }

    public AirVPNManifest(Context c)
    {
        if(appContext == null)
            appContext = c;

        if(supportTools == null)
            supportTools = new SupportTools(appContext);

        if(airVPNServerGroup == null)
            airVPNServerGroup = new AirVPNServerGroup();

        if(cipherDatabase == null)
            cipherDatabase = new CipherDatabase();

        if(networkStatusReceiver == null)
        {
            networkStatusReceiver = new NetworkStatusReceiver(appContext);
            networkStatusReceiver.addListener(this);
        }

        if(eddieEvent == null)
        {
            eddieEvent = new EddieEvent();
            eddieEvent.addListener(this);
        }

        if(processTimeTS == 0)
        {
            processTimeTS = System.currentTimeMillis() / 1000;

            Document manifestDocument = supportTools.stringToXmlDocument(getDefaultManifest());

            manifestType = ManifestType.DEFAULT;

            processXmlManifest(manifestDocument);

            loadManifest();
        }

        refreshPending = false;
    }

    protected void finalize() throws Throwable
    {
        try
        {
            if(networkStatusReceiver != null)
                networkStatusReceiver.removeListener(this);

            if(eddieEvent != null)
                eddieEvent.removeListener(this);
        }
        catch(Exception e)
        {
            EddieLogger.error("AirVPNManifest.finalize() Exception: %s", e);
        }
        finally
        {
            try
            {
                super.finalize();
            }
            catch(Exception e)
            {
                EddieLogger.error("AirVPNManifest.finalize() Exception: %s", e);
            }
        }
    }

    public ArrayList<Message> getMessages()
    {
        return manifestMessage;
    }

    public void setMessageShown(int m)
    {
        if(m >= manifestMessage.size())
            return;

        manifestMessage.get(m).setShown(true);
    }

    public long getManifestTimeTS()
    {
        return manifestTimeTS;
    }

    public void getManifestTimeTS(long t)
    {
        manifestTimeTS = t;
    }

    public long getManifestNextUpdateTS()
    {
        return manifestNextUpdateTS;
    }

    public void getManifestNextUpdateTS(long t)
    {
        manifestNextUpdateTS = t;
    }

    public int getNextUpdateIntervalMinutes()
    {
        return nextUpdateIntervalMinutes;
    }

    public void setNextUpdateIntervalMinutes(int n)
    {
        nextUpdateIntervalMinutes = n;
    }

    public String getDnsCheckHost()
    {
        return dnsCheckHost;
    }

    public void setDnsCheckHost(String d)
    {
        dnsCheckHost = d;
    }

    public String getDnsCheckRes1()
    {
        return dnsCheckRes1;
    }

    public void setDnsCheckRes1(String d)
    {
        dnsCheckRes1 = d;
    }

    public String getDnsCheckRes2()
    {
        return dnsCheckRes2;
    }

    public void setDnsCheckRes2(String d)
    {
        dnsCheckRes2 = d;
    }

    public static double getSpeedFactor()
    {
        return speedFactor;
    }

    public void setSpeedFactor(double s)
    {
        speedFactor = s;
    }

    public static double getLoadFactor()
    {
        return loadFactor;
    }

    public void setLoadFactor(double l)
    {
        loadFactor = l;
    }

    public static double getUserFactor()
    {
        return userFactor;
    }

    public void setUserFactor(double u)
    {
        userFactor = u;
    }

    public int getLatencyFactor()
    {
        return latencyFactor;
    }

    public void setLatencyFactor(int l)
    {
        latencyFactor = l;
    }

    public int getPenalityFactor()
    {
        return penalityFactor;
    }

    public void setPenalityFactor(int p)
    {
        penalityFactor = p;
    }

    public int getPingerDelay()
    {
        return pingerDelay;
    }

    public void setPingerDelay(int p)
    {
        pingerDelay = p;
    }

    public int getPingerRetry()
    {
        return pingerRetry;
    }

    public void setPingerRetry(int p)
    {
        pingerRetry = p;
    }

    public String getCheckDomain()
    {
        return checkDomain;
    }

    public void setCheckDomain(String c)
    {
        checkDomain = c;
    }

    public static String getCheckDnsQuery()
    {
        return checkDnsQuery;
    }

    public void setCheckDnsQuery(String c)
    {
        checkDnsQuery = c;
    }

    public String getOpenVpnDirectives()
    {
        return openVpnDirectives;
    }

    public void setOpenVpnDirectives(String o)
    {
        openVpnDirectives = o;
    }

    public String getModeProtocol()
    {
        return modeProtocol;
    }

    public void setModeProtocol(String m)
    {
        modeProtocol = m;
    }

    public int getModePort()
    {
        return modePort;
    }

    public void setModePort(int m)
    {
        modePort = m;
    }

    public int getModeAlt()
    {
        return modeAlt;
    }

    public void setModeAlt(int m)
    {
        modeAlt = m;
    }

    public static ArrayList<String> getBootStrapServerUrlList()
    {
        return bootStrapServerUrl;
    }

    public void setBootStrapServerUrlList(ArrayList<String> b)
    {
        bootStrapServerUrl = b;
    }

    public void addBootStrapServerUrl(String url)
    {
        if(bootStrapServerUrl == null)
            bootStrapServerUrl = new ArrayList<String>();

        bootStrapServerUrl.add(url);
    }

    public ArrayList<AirVPNServer> getAirVpnServerList()
    {
        return airVpnServer;
    }

    public void setAirVpnServerList(ArrayList<AirVPNServer> a)
    {
        airVpnServer = a;
    }

    public synchronized void addAirVpnServer(AirVPNServer server)
    {
        if(airVpnServer == null)
            airVpnServer = new ArrayList<AirVPNServer>();

        if(server == null)
            return;

        airVpnServer.add(server);
    }

    public static HashMap<String, CountryStats> getCountryStats()
    {
        return countryStats;
    }

    public static ArrayList<AirVPNServer> getServerListByCountry(String code)
    {
        if(airVpnServer == null)
            return null;

        AirVPNServer server = null;
        ArrayList<AirVPNServer> serverList = new ArrayList<AirVPNServer>();

        for(int i = 0; i < airVpnServer.size(); i++)
        {
            server = airVpnServer.get(i);

            if(server != null && server.getCountryCode().equals(code))
                serverList.add(server);
        }

        return serverList;
    }

    public static AirVPNServer getServerByName(String name)
    {
        if(airVpnServer == null)
            return null;

        AirVPNServer server = null;

        for(int i = 0; i < airVpnServer.size() && server == null; i++)
        {
            if(airVpnServer.get(i).getName().equals(name))
                server = airVpnServer.get(i);
        }

        return server;
    }

    public static String getRsaPublicKeyModulus()
    {
        return rsaPublicKeyModulus;
    }

    public void setRsaPublicKeyModulus(String r)
    {
        rsaPublicKeyModulus = r;
    }

    public static String getRsaPublicKeyExponent()
    {
        return rsaPublicKeyExponent;
    }

    public void setRsaPublicKeyExponent(String r)
    {
        rsaPublicKeyExponent = r;
    }

    public static ManifestType getManifestType()
    {
        return manifestType;
    }

    private void initializeManifestData()
    {
        processTimeTS = 0;
        manifestTimeTS = 0;
        manifestNextUpdateTS = 0;
        nextUpdateIntervalMinutes = 0;
        dnsCheckHost = "";
        dnsCheckRes1 = "";
        dnsCheckRes2 = "";
        speedFactor = 1.0;
        loadFactor = 1.0;
        userFactor = 1.0;
        latencyFactor = 0;
        penalityFactor = 0;
        pingerDelay = 0;
        pingerRetry = 0;
        checkDomain = "";
        checkDnsQuery = "";
        openVpnDirectives = "";
        modeProtocol = "";
        modePort = 0;
        modeAlt = 0;
        bootStrapServerUrl = null;
        airVpnServer = null;
        rsaPublicKeyModulus = "";
        rsaPublicKeyExponent = "";
    }

    private void loadManifest()
    {
        Runnable runnable = new Runnable()
        {
            @Override
            public void run()
            {
                Document manifestDocument = null;
                File manifestFile = null;

                if(!rsaPublicKeyModulus.isEmpty() && !rsaPublicKeyExponent.isEmpty())
                {
                    HashMap<String, String> parameters = new HashMap<String, String>();

                    parameters.put("login", AirVPNUser.getUserName());
                    parameters.put("password", AirVPNUser.getUserPassword());
                    parameters.put("system", SupportTools.DEVICE_PLATFORM);
                    parameters.put("version", SupportTools.AIRVPN_SERVER_DOCUMENT_VERSION);
                    parameters.put("act", "manifest");

                    SupportTools.RequestAirVPNDocument manifestRequest = supportTools.new RequestAirVPNDocument();

                    manifestRequest.execute(parameters);
                }
                else if(!AirVPNUser.masterPassword().isEmpty())
                {
                    if(AirVPNUser.getUserName().isEmpty() || AirVPNUser.getUserPassword().isEmpty())
                    {
                        manifestFile = new File(appContext.getFilesDir(), AIRVPN_MANIFEST_FILE_NAME);

                        if(manifestFile.exists())
                        {
                            manifestDocument = supportTools.decryptFileToXmlDocument(AIRVPN_MANIFEST_FILE_NAME, AirVPNUser.masterPassword());

                            manifestType = ManifestType.STORED;
                        }
                        else
                        {
                            manifestDocument = supportTools.stringToXmlDocument(getDefaultManifest());

                            manifestType = ManifestType.DEFAULT;
                        }

                        processXmlManifest(manifestDocument);
                    }
                }
                else
                {
                    if(manifestType != ManifestType.DEFAULT && manifestType != ManifestType.PROCESSING)
                    {
                        manifestDocument = supportTools.stringToXmlDocument(getDefaultManifest());

                        manifestType = ManifestType.DEFAULT;

                        processXmlManifest(manifestDocument);
                    }
                }
            }
        };

        supportTools.startThread(runnable);
    }

    private String getDefaultManifest()
    {
        if(appContext == null)
            return "";

        AssetManager assetManager = appContext.getAssets();
        InputStream inputStream = null;
	    String manifest = "";

        try
        {
	        inputStream = assetManager.open("manifest.xml");

            int size = inputStream.available();
            byte[] buffer = new byte[size];
	        inputStream.read(buffer);
	        inputStream.close();

	        manifest = new String(buffer);
	    }
	    catch(Exception e)
        {
            EddieLogger.warning("AirVPNManifest.getDefaultManifest() Exception: %s", e);

            manifest = "";
	    }

	    return manifest;
    }

    public void refreshManifestFromAirVPN()
    {
        if(!AirVPNUser.isUserValid())
            return;

        if(supportTools.isNetworkConnectionActive() == false)
        {
            refreshPending = true;

            return;
        }

        if(!rsaPublicKeyModulus.isEmpty() && !rsaPublicKeyExponent.isEmpty())
        {
            EddieLogger.info("Refreshing AirVPN manifest");

            HashMap<String, String> parameters = new HashMap<String, String>();

            parameters.put("login", AirVPNUser.getUserName());
            parameters.put("password", AirVPNUser.getUserPassword());
            parameters.put("system", SupportTools.DEVICE_PLATFORM);
            parameters.put("version", SupportTools.AIRVPN_SERVER_DOCUMENT_VERSION);
            parameters.put("act", "manifest");

            SupportTools.RequestAirVPNDocument manifestRequest = supportTools.new RequestAirVPNDocument();

            manifestRequest.execute(parameters);

            refreshPending = false;
        }
    }

    private void processXmlManifest(Document manifestDocument)
    {
        NodeList nodeList = null;
        NamedNodeMap namedNodeMap = null;
        ManifestType newManifestType = manifestType;
        AirVPNServerGroup.ServerGroup serverGroup = null;
        AirVPNServer localAirVPNServer = null;
        CountryStats cStats = null;
        String value, row[], logMessage = "";
        int intVal = 0, i = 0, j = 0, group = -1;
        long longVal = 0;

        if(manifestDocument == null)
        {
            EddieLogger.error("AirVPNManifest.processXmlManifest(): manifestDocument is null.");

            return;
        }

        newManifestType = manifestType;

        manifestType = ManifestType.PROCESSING;

        switch(newManifestType)
        {
            case DEFAULT:
            {
                logMessage = "Setting manifest to default instance";
            }
            break;

            case STORED:
            {
                logMessage = "Setting manifest to the locally stored instance";
            }
            break;

            case FROM_SERVER:
            {
                logMessage = "Setting manifest to the instance downloaded from AirVPN server";
            }
            break;

            case PROCESSING:
            {
            }
            break;

            default:
            {
                logMessage = "Manifest type is not set. Using current manifest instance (if any).";
            }
            break;
        }

        EddieLogger.info(logMessage);

        if(newManifestType == ManifestType.NOT_SET)
        {
            eddieEvent.onAirVPNManifestChanged();

            manifestType = newManifestType;

            return;
        }

        initializeManifestData();

        processTimeTS = System.currentTimeMillis()/1000;

        countryStats = new HashMap<String, CountryStats>();

        // manifest attributes

        nodeList = manifestDocument.getElementsByTagName("manifest");

        if(nodeList != null)
        {
            namedNodeMap = nodeList.item(0).getAttributes();

            if(namedNodeMap == null)
            {
                EddieLogger.error("AirVPNManifest.processXmlManifest(): \"manifest\" node has no attributes");

                initializeManifestData();

                return;
            }

            try
            {
                value = supportTools.getXmlItemNodeValue(namedNodeMap, "time");

                manifestTimeTS = Long.parseLong(value);
            }
            catch(NumberFormatException e)
            {
                EddieLogger.warning("AirVPNManifest.processXmlManifest(): manifest.time not found");

                manifestTimeTS = 0;
            }

            try
            {
                value = supportTools.getXmlItemNodeValue(namedNodeMap, "next");

                manifestNextUpdateTS = Long.parseLong(value);
            }
            catch(NumberFormatException e)
            {
                EddieLogger.warning("AirVPNManifest.processXmlManifest(): manifest.next not found");

                manifestNextUpdateTS = 0;
            }

            try
            {
                value = supportTools.getXmlItemNodeValue(namedNodeMap, "next_update");

                nextUpdateIntervalMinutes = Integer.parseInt(value);
            }
            catch(NumberFormatException e)
            {
                EddieLogger.warning("AirVPNManifest.processXmlManifest(): manifest.next_update not found");

                manifestTimeTS = 0;
            }

            dnsCheckHost = supportTools.getXmlItemNodeValue(namedNodeMap, "dnscheck_host");
            dnsCheckRes1 = supportTools.getXmlItemNodeValue(namedNodeMap, "dnscheck_res1");
            dnsCheckRes2 = supportTools.getXmlItemNodeValue(namedNodeMap, "dnscheck_res2");

            try
            {
                value = supportTools.getXmlItemNodeValue(namedNodeMap, "speed_factor");

                speedFactor = Double.parseDouble(value);
            }
            catch(NumberFormatException e)
            {
                speedFactor = 1.0;
            }

            try
            {
                value = supportTools.getXmlItemNodeValue(namedNodeMap, "load_factor");

                loadFactor = Double.parseDouble(value);
            }
            catch(NumberFormatException e)
            {
                loadFactor = 1.0;
            }

            try
            {
                value = supportTools.getXmlItemNodeValue(namedNodeMap, "users_factor");

                userFactor = Double.parseDouble(value);
            }
            catch(NumberFormatException e)
            {
                userFactor = 1.0;
            }

            try
            {
                value = supportTools.getXmlItemNodeValue(namedNodeMap, "latency_factor");

                latencyFactor = Integer.parseInt(value);
            }
            catch(NumberFormatException e)
            {
                latencyFactor = 0;
            }

            try
            {
                value = supportTools.getXmlItemNodeValue(namedNodeMap, "penalty_factor");

                penalityFactor = Integer.parseInt(value);
            }
            catch(NumberFormatException e)
            {
                penalityFactor = 0;
            }

            try
            {
                value = supportTools.getXmlItemNodeValue(namedNodeMap, "pinger_delay");

                pingerDelay = Integer.parseInt(value);
            }
            catch(NumberFormatException e)
            {
                pingerDelay = 0;
            }

            try
            {
                value = supportTools.getXmlItemNodeValue(namedNodeMap, "pinger_retry");

                pingerRetry = Integer.parseInt(value);
            }
            catch(NumberFormatException e)
            {
                pingerRetry = 0;
            }

            checkDomain = supportTools.getXmlItemNodeValue(namedNodeMap, "check_domain");
            checkDnsQuery = supportTools.getXmlItemNodeValue(namedNodeMap, "check_dns_query");
            openVpnDirectives = supportTools.convertXmlEntities(supportTools.getXmlItemNodeValue(namedNodeMap, "openvpn_directives"));
            modeProtocol = supportTools.getXmlItemNodeValue(namedNodeMap, "mode_protocol");

            try
            {
                value = supportTools.getXmlItemNodeValue(namedNodeMap, "mode_port");

                modePort = Integer.parseInt(value);
            }
            catch(NumberFormatException e)
            {
                modePort = 0;
            }

            try
            {
                value = supportTools.getXmlItemNodeValue(namedNodeMap, "mode_alt");

                modeAlt = Integer.parseInt(value);
            }
            catch(NumberFormatException e)
            {
                modeAlt = 0;
            }

            // Manifest messages

            nodeList = manifestDocument.getElementsByTagName("message");

            if(nodeList != null && nodeList.getLength() > 0)
            {
                if(manifestMessage == null)
                    manifestMessage = new ArrayList<Message>();

                for(i = 0; i < nodeList.getLength(); i++)
                {
                    Message message = new Message();

                    namedNodeMap = nodeList.item(i).getAttributes();

                    if(namedNodeMap != null)
                    {
                        value = supportTools.getXmlItemNodeValue(namedNodeMap, "text");

                        if(!value.isEmpty())
                            message.setText(value);

                        value = supportTools.getXmlItemNodeValue(namedNodeMap, "url");

                        if(!value.isEmpty())
                            message.setUrl(value);

                        value = supportTools.getXmlItemNodeValue(namedNodeMap, "link");

                        if(!value.isEmpty())
                            message.setLink(value);

                        value = supportTools.getXmlItemNodeValue(namedNodeMap, "html");

                        if(!value.isEmpty())
                            message.setHtml(value);

                        try
                        {
                            longVal = Long.parseLong(supportTools.getXmlItemNodeValue(namedNodeMap, "from_time"));
                        }
                        catch(NumberFormatException e)
                        {
                            longVal = 0;
                        }

                        message.setFromUTCTimeTS(longVal);

                        try
                        {
                            longVal = Long.parseLong(supportTools.getXmlItemNodeValue(namedNodeMap, "to_time"));
                        }
                        catch(NumberFormatException e)
                        {
                            longVal = 0;
                        }

                        message.setToUTCTimeTS(longVal);

                        message.setShown(false);

                        boolean found = false;

                        for(Message m : manifestMessage)
                        {
                            if(m.getText().equals(message.getText()))
                                found = true;
                        }

                        if(found == false && !message.getText().isEmpty())
                            manifestMessage.add(message);
                    }
                }
            }

            // bootstrap urls

            nodeList = manifestDocument.getElementsByTagName("url");

            if(nodeList != null)
            {
                bootStrapServerUrl = null;

                for(i = 0; i < nodeList.getLength(); i++)
                {
                    namedNodeMap = nodeList.item(i).getAttributes();

                    if(namedNodeMap != null)
                    {
                        value = supportTools.getXmlItemNodeValue(namedNodeMap, "address");

                        if(!value.isEmpty())
                            addBootStrapServerUrl(value);
                        else
                            EddieLogger.warning("AirVPNManifest.processXmlManifest(): found empty \"url\" node in manifest");
                    }
                    else
                        EddieLogger.warning("AirVPNManifest.processXmlManifest(): found \"url\" node in manifest with no attributes");
                }
            }
            else
                EddieLogger.error("AirVPNManifest.processXmlManifest(): \"urls\" node not found in manifest");

            // RSA public key

            nodeList = manifestDocument.getElementsByTagName("Exponent");

            if(nodeList != null)
                rsaPublicKeyExponent = nodeList.item(0).getTextContent();
            else
                rsaPublicKeyExponent = "";

            nodeList = manifestDocument.getElementsByTagName("Modulus");

            if(nodeList != null)
                rsaPublicKeyModulus = nodeList.item(0).getTextContent();
            else
                rsaPublicKeyModulus = "";

            if(rsaPublicKeyExponent.isEmpty() || rsaPublicKeyModulus.isEmpty())
                EddieLogger.error("AirVPNManifest.processXmlManifest(): RSA public key is incomplete or empty in manifest");

            // AirVPN Server Groups (V 256)

            airVPNServerGroup.reset();
            cipherDatabase.reset();

            nodeList = manifestDocument.getElementsByTagName("servers_group");

            if(nodeList != null)
            {
                for(i = 0; i < nodeList.getLength(); i++)
                {
                    namedNodeMap = nodeList.item(i).getAttributes();

                    if(namedNodeMap != null)
                    {
                        serverGroup = new AirVPNServerGroup().new ServerGroup();
                        value = "";
                        intVal = -1;

                        if(serverGroup != null)
                        {
                            try
                            {
                                value = supportTools.getXmlItemNodeValue(namedNodeMap, "group");

                                intVal = Integer.parseInt(value);
                            }
                            catch (NumberFormatException e)
                            {
                                intVal = -1;
                            }

                            if(intVal != -1)
                                serverGroup.setGroup(intVal);
                            else
                                EddieLogger.error("AirVPNManifest.processXmlManifest(): server_group.group \"%s\" is invalid", value);

                            value = supportTools.getXmlItemNodeValue(namedNodeMap, "support_ipv4");

                            if(value.equals("true"))
                                serverGroup.setIPv4Support(true);
                            else
                                serverGroup.setIPv4Support(false);

                            value = supportTools.getXmlItemNodeValue(namedNodeMap, "support_ipv6");

                            if(value.equals("true"))
                                serverGroup.setIPv6Support(true);
                            else
                                serverGroup.setIPv6Support(false);

                            value = supportTools.getXmlItemNodeValue(namedNodeMap, "support_check");

                            if(value.equals("true"))
                                serverGroup.setCheckSupport(true);
                            else
                                serverGroup.setCheckSupport(false);

                            value = supportTools.getXmlItemNodeValue(namedNodeMap, "ciphers_tls");

                            serverGroup.setTlsCiphers(cipherDatabase.getCodeArrayList(value, CipherDatabase.CipherType.TLS));

                            value = supportTools.getXmlItemNodeValue(namedNodeMap, "ciphers_tlssuites");

                            serverGroup.setTlsSuiteCiphers(cipherDatabase.getCodeArrayList(value, CipherDatabase.CipherType.TLS_SUITE));

                            value = supportTools.getXmlItemNodeValue(namedNodeMap, "ciphers_data");

                            serverGroup.setDataCiphers(cipherDatabase.getCodeArrayList(value, CipherDatabase.CipherType.DATA));

                            airVPNServerGroup.add(serverGroup);
                        }
                        else
                            EddieLogger.error("AirVPNManifest.processXmlManifest(): Failed to instantiate a new ServerGroup");

                    }
                }
            }
            else
                EddieLogger.error("AirVPNManifest.processXmlManifest(): \"server_groups\" node not found in manifest");

            // AirVPN servers

            nodeList = manifestDocument.getElementsByTagName("server");

            if(nodeList != null)
            {
                for(int position = 0; position < nodeList.getLength(); position++)
                {
                    localAirVPNServer = new AirVPNServer(appContext);

                    namedNodeMap = nodeList.item(position).getAttributes();

                    if(namedNodeMap != null)
                    {
                        value = supportTools.getXmlItemNodeValue(namedNodeMap, "name");

                        if(!value.isEmpty())
                        {
                            localAirVPNServer.setName(value);
                            localAirVPNServer.setCountryCode(supportTools.getXmlItemNodeValue(namedNodeMap, "country_code").toUpperCase());
                            localAirVPNServer.setLocation(supportTools.getXmlItemNodeValue(namedNodeMap, "location"));

                            try
                            {
                                value = supportTools.getXmlItemNodeValue(namedNodeMap, "bw");

                                intVal = Integer.parseInt(value);
                            }
                            catch(NumberFormatException e)
                            {
                                intVal = 0;
                            }

                            localAirVPNServer.setBandWidth(intVal);

                            try
                            {
                                value = supportTools.getXmlItemNodeValue(namedNodeMap, "bw_max");

                                intVal = Integer.parseInt(value);
                            }
                            catch(NumberFormatException e)
                            {
                                intVal = 0;
                            }

                            localAirVPNServer.setMaxBandWidth(intVal);

                            try
                            {
                                value = supportTools.getXmlItemNodeValue(namedNodeMap, "users");

                                intVal = Integer.parseInt(value);
                            }
                            catch(NumberFormatException e)
                            {
                                intVal = 0;
                            }

                            localAirVPNServer.setUsers(intVal);

                            try
                            {
                                value = supportTools.getXmlItemNodeValue(namedNodeMap, "group");

                                if(!value.isEmpty())
                                    group = Integer.parseInt(value);
                                else
                                    group = -1;
                            }
                            catch(NumberFormatException e)
                            {
                                group = -1;
                            }

                            if(group != -1)
                            {
                                localAirVPNServer.setSupportIPv4(airVPNServerGroup.getGroupIPv4Support(group));
                                localAirVPNServer.setSupportIPv6(airVPNServerGroup.getGroupIPv6Support(group));
                                localAirVPNServer.setSupportCheck(airVPNServerGroup.getGroupSupportCheck(group));
                                localAirVPNServer.setTlsCiphers(airVPNServerGroup.getGroupTlsCiphers(group));
                                localAirVPNServer.setTlsSuiteCiphers(airVPNServerGroup.getGroupTlsSuiteCiphers(group));
                                localAirVPNServer.setDataCiphers(airVPNServerGroup.getGroupDataCiphers(group));
                            }
                            else
                                EddieLogger.error("AirVPNManifest.processXmlManifest(): Server \"%s\" has no group", localAirVPNServer.getName());

                            localAirVPNServer.setWarningOpen(supportTools.getXmlItemNodeValue(namedNodeMap, "warning_open"));
                            localAirVPNServer.setWarningClosed(supportTools.getXmlItemNodeValue(namedNodeMap, "warning_closed"));

                            try
                            {
                                value = supportTools.getXmlItemNodeValue(namedNodeMap, "scorebase");

                                intVal = Integer.parseInt(value);
                            }
                            catch(NumberFormatException e)
                            {
                                intVal = 0;
                            }

                            localAirVPNServer.setScoreBase(intVal);

                            value = supportTools.getXmlItemNodeValue(namedNodeMap, "ips_entry");

                            if(!value.isEmpty())
                            {
                                row = value.split(",");

                                if(localAirVPNServer.getSupportIPv6())
                                {
                                    j = row.length / 2;

                                    for(i = 0; i < j; i++)
                                    {
                                        localAirVPNServer.setEntryIPv4(i, row[i].trim());
                                        localAirVPNServer.setEntryIPv6(i, row[i + j].trim());
                                    }
                                }
                                else
                                {
                                    for(i = 0; i < row.length; i++)
                                        localAirVPNServer.setEntryIPv4(i, row[i]);
                                }
                            }

                            value = supportTools.getXmlItemNodeValue(namedNodeMap, "ips_exit");

                            if(!value.isEmpty())
                            {
                                if(localAirVPNServer.getSupportIPv6())
                                {
                                    row = value.split(",");

                                    localAirVPNServer.setExitIPv4(row[0].trim());
                                    localAirVPNServer.setExitIPv6(row[1].trim());
                                }
                                else
                                    localAirVPNServer.setExitIPv4(value.trim());
                            }

                            localAirVPNServer.computeServerScore();

                            addAirVpnServer(localAirVPNServer);

                            if(localAirVPNServer.isAvailable())
                            {
                                if(countryStats.containsKey(localAirVPNServer.getCountryCode()))
                                {
                                    cStats = countryStats.get(localAirVPNServer.getCountryCode());

                                    cStats.setServers(cStats.getServers() + 1);
                                    cStats.setUsers(cStats.getUsers() + localAirVPNServer.getUsers());
                                    cStats.setBandWidth(cStats.getBandWidth() + localAirVPNServer.getBandWidth());
                                    cStats.setMaxBandWidth(cStats.getMaxBandWidth() + localAirVPNServer.getMaxBandWidth());

                                    countryStats.put(localAirVPNServer.getCountryCode(), cStats);
                                }
                                else
                                {
                                    cStats = new CountryStats();

                                    cStats.setCountryISOCode(localAirVPNServer.getCountryCode());
                                    cStats.setServers(1);
                                    cStats.setUsers(localAirVPNServer.getUsers());
                                    cStats.setBandWidth(localAirVPNServer.getBandWidth());
                                    cStats.setMaxBandWidth(localAirVPNServer.getMaxBandWidth());

                                    countryStats.put(localAirVPNServer.getCountryCode(), cStats);
                                }
                            }
                        }
                        else
                            EddieLogger.warning("AirVPNManifest.processXmlManifest(): found unnamed \"server\" in manifest");
                    }
                    else
                        EddieLogger.warning("AirVPNManifest.processXmlManifest(): found \"server\" node in manifest with no attributes");
                }
            }
            else
                EddieLogger.error("AirVPNManifest.processXmlManifest(): \"urls\" node not found in manifest");
        }
        else
        {
            EddieLogger.error("AirVPNManifest.processXmlManifest(): \"manifest\" node not found in manifest");

            initializeManifestData();

            return;
        }

        manifestType = newManifestType;

        airVPNManifestDocument = manifestDocument;

        if(eddieEvent != null)
            eddieEvent.onAirVPNManifestChanged();

        if(!AirVPNUser.masterPassword().isEmpty())
        {
            if(!supportTools.encryptXmlDocumentToFile(airVPNManifestDocument, AirVPNUser.masterPassword(), AIRVPN_MANIFEST_FILE_NAME))
                EddieLogger.error("Error while saving AirVPN manifest to local storage");
        }
    }

    // Eddie events

    public void onVpnConnectionDataChanged(final OpenVPNConnectionData connectionData)
    {
    }

    public void onVpnStatusChanged(VPN.Status status, String message)
    {
        if(refreshPending == true && status == VPN.Status.CONNECTED)
            refreshManifestFromAirVPN();
    }

    public void onVpnAuthFailed(final OpenVPNEvent oe)
    {
    }

    public void onVpnError(final OpenVPNEvent oe)
    {
    }

    public void onMasterPasswordChanged()
    {
        if(manifestType == ManifestType.NOT_SET || manifestType == ManifestType.DEFAULT)
            loadManifest();
        else
        {
            if(!AirVPNUser.masterPassword().isEmpty() && airVPNManifestDocument != null)
            {
                if(!supportTools.encryptXmlDocumentToFile(airVPNManifestDocument, AirVPNUser.masterPassword(), AIRVPN_MANIFEST_FILE_NAME))
                    supportTools.infoDialog(R.string.manifest_save_error, true);
            }
        }
    }

    public void onAirVPNLogin()
    {
        loadManifest();
    }

    public void onAirVPNLoginFailed()
    {
    }

    public void onAirVPNLogout()
    {
    }

    public void onAirVPNUserDataChanged()
    {
    }

    public void onAirVPNUserProfileReceived(Document document)
    {
    }

    public void onAirVPNUserProfileDownloadError()
    {
    }

    public void onAirVPNUserProfileChanged()
    {
    }

    public void onAirVPNManifestReceived(final Document document)
    {
        Runnable runnable = new Runnable()
        {
            @Override
            public void run()
            {
                manifestType = ManifestType.FROM_SERVER;

                processXmlManifest(document);
            }
        };

        supportTools.startThread(runnable);
    }

    public void onAirVPNManifestDownloadError()
    {
        if(NetworkStatusReceiver.isNetworkConnected())
        {
            supportTools.infoDialog(String.format("%s%s", appContext.getResources().getString(R.string.manifest_download_error), appContext.getResources().getString(R.string.bootstrap_server_error)), true);

            EddieLogger.error("Error while retrieving AirVPN manifest from server");
        }
        else
            refreshPending = true;
    }

    public void onAirVPNManifestChanged()
    {
    }

    public void onAirVPNIgnoredDocumentRequest()
    {
    }

    public void onCancelConnection()
    {
    }

    // NetworkStatusReceiver

    public void onNetworkStatusNotAvailable()
    {
    }

    public void onNetworkStatusConnected()
    {
        if(refreshPending == true)
            refreshManifestFromAirVPN();
    }

    public void onNetworkStatusIsConnecting()
    {
    }

    public void onNetworkStatusIsDisonnecting()
    {
    }

    public void onNetworkStatusSuspended()
    {
    }

    public void onNetworkStatusNotConnected()
    {
    }

    public void onNetworkTypeChanged()
    {
    }
}
