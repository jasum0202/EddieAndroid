// <eddie_source_header>
// This file is part of Eddie/AirVPN software.
// Copyright (C)2014-2018 AirVPN (support@airvpn.org) / https://airvpn.org
//
// Eddie is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Eddie is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Eddie. If not, see <http://www.gnu.org/licenses/>.
// </eddie_source_header>
//
// 15 October 2018 - author: ProMIND - initial release. (a tribute to the 1859 Perugia uprising occurred on 20 June 1859 and in memory of those brave inhabitants who fought for the liberty of Perugia)

package org.airvpn.eddie;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.content.res.ResourcesCompat;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;

public class AirVPNServerSettingsActivity extends Activity
{
    private SupportTools supportTools = null;
    private SettingsManager settingsManager = null;

    private Typeface typeface = null;

    private TextView txtTitle = null;

    private LinearLayout llAirVPNProtocol = null;
    private LinearLayout llAirVPNPort = null;
    private LinearLayout llAirVPNIPVersion = null;
    private LinearLayout llAirVPNTLSMode = null;
    private LinearLayout llAirVPNEncryptionAlgorithm = null;
    private LinearLayout llUserProfile = null;
    private Spinner spnUserProfile = null;

    private RadioGroup rgSortBy = null;
    private RadioGroup rgSortMode = null;

    private AirVPNUser airVPNUser = null;

    private static String selectedUserProfile = "";

    private int settingsResult = RESULT_CANCELED;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        int radioButtonId = 0;

        super.onCreate(savedInstanceState);

        setContentView(R.layout.airvpn_server_settings_layout);

        supportTools = new SupportTools(this);
        settingsManager = new SettingsManager(this);

        airVPNUser = new AirVPNUser(this);

        selectedUserProfile = airVPNUser.getCurrentProfile();

        typeface = ResourcesCompat.getFont(this, R.font.default_font);

        txtTitle = (TextView)findViewById(R.id.title);

        txtTitle.setTypeface(typeface);

        llAirVPNProtocol = (LinearLayout)findViewById(R.id.settings_airvpn_default_protocol);
        llAirVPNPort = (LinearLayout)findViewById(R.id.settings_airvpn_default_port);
        llAirVPNIPVersion = (LinearLayout)findViewById(R.id.settings_airvpn_default_ip_version);
        llAirVPNTLSMode = (LinearLayout)findViewById(R.id.settings_airvpn_default_tls_mode);
        llAirVPNEncryptionAlgorithm = (LinearLayout)findViewById(R.id.settings_airvpn_default_encryption_algorithm);
        llUserProfile = (LinearLayout)findViewById(R.id.user_profile);
        spnUserProfile = (Spinner)findViewById(R.id.spn_user_profile);

        setupUserProfileSpinner(selectedUserProfile);

        rgSortBy = (RadioGroup)findViewById(R.id.settings_airvpn_sort_by_group);
        rgSortMode = (RadioGroup)findViewById(R.id.settings_airvpn_sort_mode_group);

        llAirVPNProtocol.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View arg0)
            {
                selectAirVPNProtocol();
            }
        });

        llAirVPNPort.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View arg0)
            {
                selectAirVPNPort();
            }
        });

        llAirVPNIPVersion.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View arg0)
            {
                selectAirVPNIPVersion();
            }
        });

        llAirVPNTLSMode.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View arg0)
            {
                selectAirVPNTLSMode();
            }
        });

        llAirVPNEncryptionAlgorithm.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View arg0)
            {
                selectAirVPNEncryptionAlgorithm();
            }
        });

        spnUserProfile.setAccessibilityDelegate(new View.AccessibilityDelegate()
        {
            @Override
            public void sendAccessibilityEvent(View host, int eventType)
            {
                super.sendAccessibilityEvent(host, eventType);

                spnUserProfile.setContentDescription(String.format("%s %s", getString(R.string.accessibility_selected_user_profile), spnUserProfile.getSelectedItem().toString()));
            }
        });

        switch(settingsManager.getAirVPNSortBy())
        {
            case SettingsManager.AIRVPN_SERVER_SORT_BY_NAME:
            {
                radioButtonId = R.id.settings_airvpn_sort_by_name;
            }
            break;

            case SettingsManager.AIRVPN_SERVER_SORT_BY_LOCATION:
            {
                radioButtonId = R.id.settings_airvpn_sort_by_location;
            }
            break;

            case SettingsManager.AIRVPN_SERVER_SORT_BY_LOAD:
            {
                radioButtonId = R.id.settings_airvpn_sort_by_load;
            }
            break;

            case SettingsManager.AIRVPN_SERVER_SORT_BY_USERS:
            {
                radioButtonId = R.id.settings_airvpn_sort_by_users;
            }
            break;

            case SettingsManager.AIRVPN_SERVER_SORT_BY_BANDWIDTH:
            {
                radioButtonId = R.id.settings_airvpn_sort_by_bandwidth;
            }
            break;

            case SettingsManager.AIRVPN_SERVER_SORT_BY_MAX_BANDWIDTH:
            {
                radioButtonId = R.id.settings_airvpn_sort_by_max_bandwidth;
            }
            break;

            default:
            {
                radioButtonId = R.id.settings_airvpn_sort_by_name;
            }
            break;
        }

        rgSortBy.check(radioButtonId);

        rgSortBy.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                String sortBy = "";

                switch(checkedId)
                {
                    case R.id.settings_airvpn_sort_by_name:
                    {
                        sortBy = SettingsManager.AIRVPN_SERVER_SORT_BY_NAME;
                    }
                    break;

                    case R.id.settings_airvpn_sort_by_location:
                    {
                        sortBy = SettingsManager.AIRVPN_SERVER_SORT_BY_LOCATION;
                    }
                    break;

                    case R.id.settings_airvpn_sort_by_load:
                    {
                        sortBy = SettingsManager.AIRVPN_SERVER_SORT_BY_LOAD;
                    }
                    break;

                    case R.id.settings_airvpn_sort_by_users:
                    {
                        sortBy = SettingsManager.AIRVPN_SERVER_SORT_BY_USERS;
                    }
                    break;

                    case R.id.settings_airvpn_sort_by_bandwidth:
                    {
                        sortBy = SettingsManager.AIRVPN_SERVER_SORT_BY_BANDWIDTH;
                    }
                    break;

                    case R.id.settings_airvpn_sort_by_max_bandwidth:
                    {
                        sortBy = SettingsManager.AIRVPN_SERVER_SORT_BY_MAX_BANDWIDTH;
                    }
                    break;

                    default:
                    {
                        sortBy = SettingsManager.AIRVPN_SERVER_SORT_BY_NAME;
                    }
                    break;
                }

                settingsManager.setAirVPNSortBy(sortBy);

                settingsResult = RESULT_OK;
            }
        });

        switch(settingsManager.getAirVPNSortMode())
        {
            case SettingsManager.AIRVPN_SERVER_SORT_MODE_ASCENDING:
            {
                radioButtonId = R.id.settings_airvpn_sort_servers_ascending;
            }
            break;

            case SettingsManager.AIRVPN_SERVER_SORT_MODE_DESCENDING:
            {
                radioButtonId = R.id.settings_airvpn_sort_servers_descending;
            }
            break;

            default:
            {
                radioButtonId = R.id.settings_airvpn_sort_servers_ascending;
            }
            break;
        }

        rgSortMode.check(radioButtonId);

        rgSortMode.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                String sortMode = "";

                switch(checkedId)
                {
                    case R.id.settings_airvpn_sort_servers_ascending:
                    {
                        sortMode = SettingsManager.AIRVPN_SERVER_SORT_MODE_ASCENDING;
                    }
                    break;

                    case R.id.settings_airvpn_sort_servers_descending:
                    {
                        sortMode = SettingsManager.AIRVPN_SERVER_SORT_MODE_DESCENDING;
                    }
                    break;

                    default:
                    {
                        sortMode = SettingsManager.AIRVPN_SERVER_SORT_MODE_ASCENDING;
                    }
                    break;
                }

                settingsManager.setAirVPNSortMode(sortMode);

                settingsResult = RESULT_OK;
            }
        });
    }

    @Override
    public void onResume()
    {
        super.onResume();

        selectedUserProfile = airVPNUser.getCurrentProfile();

        if(airVPNUser.isUserValid())
            setupUserProfileSpinner(selectedUserProfile);
        else
            llUserProfile.setVisibility(View.GONE);
    }

    @Override
    public void onBackPressed()
    {
        setResult(settingsResult, null);

        finish();
    }

    private void selectAirVPNProtocol()
    {
        final Runnable uiRunnable = new Runnable()
        {
            @Override
            public void run()
            {
                String value = "";

                value = supportTools.getOptionFromListDialog(R.string.airvpn_server_settings_protocol_title,
                                                             R.array.airvpn_protocol_labels,
                                                             R.array.airvpn_protocol_values,
                                                             settingsManager.getAirVPNProtocol());

                if(!value.equals(settingsManager.getAirVPNProtocol()))
                {
                    settingsManager.setAirVPNProtocol(value);

                    settingsResult = RESULT_OK;
                }
            }
        };

        executeOption(uiRunnable);
    }

    private void setupUserProfileSpinner(String selectedItem)
    {
        if(llUserProfile == null || spnUserProfile == null)
            return;

        ArrayList<String> keyNames = airVPNUser.getUserKeyNames();
        int selectedPosition = 0;

        Collections.sort(keyNames);

        if(keyNames != null && keyNames.size() > 0 && llUserProfile != null && spnUserProfile != null)
        {
            ArrayList<String> items = new ArrayList<String>();

            for(String profile : keyNames)
                items.add(profile);

            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, items);

            if(spnUserProfile != null)
            {
                spnUserProfile.setAdapter(adapter);

                for(int i = 0; i < items.size(); i++)
                {
                    if(spnUserProfile.getItemAtPosition(i).toString().equals(selectedItem))
                        selectedPosition = i;
                }

                spnUserProfile.setSelection(selectedPosition);

                spnUserProfile.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
                {
                    @Override
                    public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id)
                    {
                        selectedUserProfile = spnUserProfile.getItemAtPosition(position).toString();

                        airVPNUser.setCurrentProfile(selectedUserProfile);
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parentView)
                    {
                    }
                });
            }

            if(llUserProfile != null)
                llUserProfile.setVisibility(View.VISIBLE);
        }
        else
        {
            if(llUserProfile != null)
                llUserProfile.setVisibility(View.GONE);
        }
    }

    private void selectAirVPNPort()
    {
        final Runnable uiRunnable = new Runnable()
        {
            @Override
            public void run()
            {
                String value = "";
                int intVal = 0;

                value = supportTools.getOptionFromListDialog(R.string.airvpn_server_settings_port_title,
                                                             R.array.airvpn_port_labels,
                                                             R.array.airvpn_port_values,
                                                             String.format("%d", settingsManager.getAirVPNPort()));

                try
                {
                    intVal = Integer.parseInt(value);
                }
                catch(NumberFormatException e)
                {
                    intVal = settingsManager.AIRVPN_PORT_DEFAULT;
                }

                if(intVal != settingsManager.getAirVPNPort())
                {
                    settingsManager.setAirVPNPort(intVal);

                    settingsResult = RESULT_OK;
                }
            }
        };

        executeOption(uiRunnable);
    }

    private void selectAirVPNIPVersion()
    {
        final Runnable uiRunnable = new Runnable()
        {
            @Override
            public void run()
            {
                String value = "";

                value = supportTools.getOptionFromListDialog(R.string.airvpn_server_settings_ip_version_title,
                                                             R.array.airvpn_ip_version_labels,
                                                             R.array.airvpn_ip_version_values,
                                                             settingsManager.getAirVPNIPVersion());

                if(!value.equals(settingsManager.getAirVPNIPVersion()))
                {
                    settingsManager.setAirVPNIPVersion(value);

                    settingsResult = RESULT_OK;
                }
            }
        };

        executeOption(uiRunnable);
    }

    private void selectAirVPNTLSMode()
    {
        final Runnable uiRunnable = new Runnable()
        {
            @Override
            public void run()
            {
                String value = "";

                value = supportTools.getOptionFromListDialog(R.string.airvpn_server_settings_tls_mode_title,
                                                             R.array.airvpn_tls_mode_labels,
                                                             R.array.airvpn_tls_mode_values,
                                                             settingsManager.getAirVPNTLSMode());

                if(!value.equals(settingsManager.getAirVPNTLSMode()))
                {
                    settingsManager.setAirVPNTLSMode(value);

                    settingsResult = RESULT_OK;
                }
            }
        };

        executeOption(uiRunnable);
    }

    private void selectAirVPNEncryptionAlgorithm()
    {
        final Runnable uiRunnable = new Runnable()
        {
            @Override
            public void run()
            {
                String value = "";

                value = supportTools.getOptionFromListDialog(R.string.settings_airvpn_encryption_algorithm_title,
                                                             R.array.airvpn_encryption_algorithm_labels,
                                                             R.array.airvpn_encryption_algorithm_values,
                                                             settingsManager.getAirVPNCipher());

                if(!value.equals(settingsManager.getAirVPNCipher()))
                {
                    settingsManager.setAirVPNCipher(value);

                    if(value.equals(SettingsManager.AIRVPN_CIPHER_AES_256_CBC))
                        settingsManager.setOvpn3ForceAESCBCCiphersuites(true);
                    else
                        settingsManager.setOvpn3ForceAESCBCCiphersuites(false);

                    settingsResult = RESULT_OK;
                }
            }
        };

        executeOption(uiRunnable);
    }

    private void executeOption(final Runnable uiRunnable)
    {
        if(uiRunnable == null)
            return;

        supportTools.runOnUiActivity(this, uiRunnable);
    }
}
