// <eddie_source_header>
// This file is part of Eddie/AirVPN software.
// Copyright (C)2014-2018 AirVPN (support@airvpn.org) / https://airvpn.org
//
// Eddie is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Eddie is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Eddie. If not, see <http://www.gnu.org/licenses/>.
// </eddie_source_header>
//
// 20 June 2018 - author: ProMIND - initial release. (a tribute to the 1859 Perugia uprising occurred on 20 June 1859 and in memory of those brave inhabitants who fought for the liberty of Perugia)

package org.airvpn.eddie;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.LinkAddress;
import android.net.LinkProperties;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.provider.OpenableColumns;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.InputType;
import android.text.Spanned;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.accessibility.AccessibilityManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.math.BigInteger;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.Inet4Address;
import java.net.Inet6Address;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.security.KeyFactory;
import java.security.SecureRandom;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.RSAPublicKeySpec;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import static android.content.Context.ACCESSIBILITY_SERVICE;

public class SupportTools
{
    public enum EditOptionType
    {
        TEXT,
        PASSWORD,
        NUMERIC,
        IP_PORT,
        IP_ADDRESS_LIST
    }

    public enum EditOption
    {
        ALLOW_EMPTY_FIELD,
        ALLOW_ZERO_VALUE,
        DO_NOT_ALLOW_EMPTY_FIELD,
        DO_NOT_ALLOW_ZERO_VALUE
    }

    private Context appContext = null;
    private static Handler dialogHandler = null;
    private static ProgressDialog progressDialog = null;
    private static ProgressDialog connectionProgressDialog = null;
    private boolean dialogConfirm = false;
    private EddieEvent eddieEvent = null;
    private SettingsManager settingsManager = null;
    private String dialogReturnStringValue = "";

    private AlertDialog.Builder dialogBuilder = null;
    private AlertDialog settingDialog = null;
    private String[] dialogLabels;
    private String[] dialogValues;
    private Button btnOk = null;
    private Button btnCancel = null;
    private EditText edtKey = null;

    private static boolean appIsVisible = false;

    public static final int HTTP_CONNECTION_TIMEOUT = 15000;    // 15 seconds
    public static final int HTTP_READ_TIMEOUT = 30000;          // 30 seconds

    public static final String DEVICE_PLATFORM = "Android";
    public static final String AIRVPN_SERVER_DOCUMENT_VERSION = "256";

    public static final int AIRVPN_REQUEST_DOCUMENT_SUCCESS = 1;
    public static final int AIRVPN_REQUEST_DOCUMENT_FAIL = 2;
    public static final int AIRVPN_REQUEST_DOCUMENT_IGNORE = 3;

    public static final long ONE_DECIMAL_KILO = 1000;
    public static final long ONE_DECIMAL_MEGA = 1000000;
    public static final long ONE_DECIMAL_GIGA = 1000000000;
    public static final long ONE_KILOBYTE = 1024; // Real KB 2^10
    public static final long ONE_MEGABYTE = 1048576; // Real MB 2^20
    public static final long ONE_GIGABYTE = 1073741824; // Real GB 2^30

    public SupportTools(Context context)
    {
        appContext = context;

        eddieEvent = new EddieEvent();

        settingsManager = new SettingsManager(appContext);
    }

    public static void setAppIsVisible(boolean value)
    {
        appIsVisible = value;
    }

    public static boolean isAppIsVisible()
    {
        return appIsVisible;
    }

    public HashMap<String, String> getOpenVPNProfile(Uri profileUri)
    {
        HashMap<String, String> result = new HashMap<String, String>();
        InputStream inputStream = null;
        Reader reader = null;
        BufferedReader bufferedReader = null;
        ContentResolver contentResolver = null;
        Cursor uriCursor = null;
        String line = "", profile = "", fileName = "";
        String[] item = null;
        int MAX_FILE_SIZE = 200000;
        int validItems = 0;
        long fileSize = 0;

        result.put("description", "");

        if(profileUri == null)
        {
            result.put("status", "invalid");

            return result;
        }

        contentResolver = appContext.getContentResolver();

        try
        {
            uriCursor = contentResolver.query(profileUri, null, null, null, null);
        }
        catch(RuntimeException e)
        {
            uriCursor = null;
        }

        if(uriCursor != null)
        {
            try
            {
                if(uriCursor.moveToFirst())
                {
                    fileSize = uriCursor.getLong(uriCursor.getColumnIndex(OpenableColumns.SIZE));

                    fileName = uriCursor.getString(uriCursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                }
                else
                    fileSize = 0;
            }
            catch(Exception e)
            {
                fileSize = 0;
            }
            finally
            {
                if(uriCursor != null)
                    uriCursor.close();
            }

            if(fileSize == 0 || fileSize > MAX_FILE_SIZE)
            {
                result.put("status", "invalid");

                return result;
            }
        }
        else
            fileName = profileUri.getLastPathSegment();

        try
        {
            inputStream = contentResolver.openInputStream(profileUri);
        }
        catch(FileNotFoundException e)
        {
            result.put("status", "not_found");

            return result;
        }
        catch(SecurityException e)
        {
            result.put("status", "no_permission");

            return result;
        }

        try
        {
            reader = new InputStreamReader(inputStream, "UTF-8");

            bufferedReader = new BufferedReader(reader);

            validItems = 0;

            while((line = bufferedReader.readLine()) != null)
            {
                profile += line + "\n";

                item = line.split(" ");

                if(item.length > 0)
                {
                    if(item[0].equals("remote"))
                    {
                        if(result.containsKey("server") == false && item.length > 1)
                            result.put("server", item[1]);
                        else
                            result.put("server", "???");

                        if(result.containsKey("port") == false && item.length > 2)
                            result.put("port", item[2]);
                        else
                            result.put("port", "???");

                        validItems++;
                    }
                    else if(item[0].equals("proto"))
                    {
                        if(result.containsKey("protocol") == false && item.length > 1)
                            result.put("protocol", item[1]);
                        else
                            result.put("protocol", "???");

                        validItems++;
                    }
                }
            }

            if(bufferedReader != null)
            {
                try
                {
                    bufferedReader.close();
                }
                catch(Throwable t)
                {
                }
            }

            if(reader != null)
            {
                try
                {
                    reader.close();
                }
                catch(Throwable t)
                {
                }
            }

            if(inputStream != null)
            {
                try
                {
                    inputStream.close();
                }
                catch(Throwable t)
                {
                }
            }
        }
        catch(IOException e)
        {
            validItems = 0;
        }

        if(validItems == 2)
        {
            result.put("name", fileName);
            result.put("profile", profile);
            result.put("status", "ok");
        }
        else
            result.put("status", "invalid");

        return result;
    }

    public void setLocale(String localeCode)
    {
        String language[] = localeCode.split("_");

        Resources resources = appContext.getResources();

        DisplayMetrics displayMetrics = resources.getDisplayMetrics();

        android.content.res.Configuration configuration = resources.getConfiguration();

        if(language.length == 2)
            configuration.setLocale(new Locale(language[0], language[1]));
        else
            configuration.setLocale(new Locale(language[0]));

        resources.updateConfiguration(configuration, displayMetrics);
    }

    public static boolean isDeviceRooted()
    {
        boolean isTestOS = false, isSuPresent = false;

        isTestOS = android.os.Build.TAGS != null && android.os.Build.TAGS.contains("test-keys");

        String[] suPath = {"/sbin/su",
                           "/system/bin/su",
                           "/system/xbin/su",
                           "/data/local/xbin/su",
                           "/data/local/bin/su",
                           "/system/sd/xbin/su",
                           "/system/bin/failsafe/su",
                           "/data/local/su",
                           "/su/bin/su"};

        for(String path : suPath)
        {
            if(new File(path).exists())
                isSuPresent = true;
        }

        return isTestOS | isSuPresent;
    }

    public void showProgressDialog(int resource)
    {
        showProgressDialog(appContext.getResources().getString(resource));
    }

    public void showProgressDialog(String message)
    {
        if(progressDialog != null && progressDialog.isShowing())
        {
            setProgressDialogMessage(message);

            return;
        }

        progressDialog = new ProgressDialog(appContext);
        progressDialog.setMessage(message);
        progressDialog.setCancelable(false);

        if(appContext instanceof Activity)
        {
            if(((Activity)appContext).isFinishing())
                return;
        }

        progressDialog.show();
    }

    public void setProgressDialogMessage(int resource)
    {
        setProgressDialogMessage(appContext.getResources().getString(resource));
    }

    public void setProgressDialogMessage(String message)
    {
        if(progressDialog == null || !progressDialog.isShowing())
        {
            showProgressDialog(message);

            return;
        }

        progressDialog.setMessage(message);
    }

    public void dismissProgressDialog()
    {
        if(progressDialog != null && progressDialog.isShowing())
            progressDialog.dismiss();

        progressDialog = null;
    }

    public void showConnectionProgressDialog(String message)
    {
        if(connectionProgressDialog != null && connectionProgressDialog.isShowing())
        {
            setConnectionProgressDialogMessage(message);

            return;
        }

        connectionProgressDialog = new ProgressDialog(appContext);

        if(connectionProgressDialog == null)
        {
            EddieLogger.error("SupportTools.showConnectionProgressDialog(): cannot create dialog");

            return;
        }

        connectionProgressDialog.setMessage(message);
        connectionProgressDialog.setCancelable(false);

        connectionProgressDialog.setButton(DialogInterface.BUTTON_NEGATIVE, appContext.getString(R.string.cancel), new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                eddieEvent.onCancelConnection();

                dismissConnectionProgressDialog();
            }
        });

        connectionProgressDialog.show();
    }

    public void setConnectionProgressDialogMessage(int resource)
    {
        setConnectionProgressDialogMessage(appContext.getResources().getString(resource));
    }

    public void setConnectionProgressDialogMessage(String message)
    {
        if(connectionProgressDialog == null || !connectionProgressDialog.isShowing())
        {
            showConnectionProgressDialog(message);

            return;
        }

        connectionProgressDialog.setMessage(message);
    }

    public void dismissConnectionProgressDialog()
    {
        if(connectionProgressDialog != null && connectionProgressDialog.isShowing())
            connectionProgressDialog.dismiss();

        connectionProgressDialog = null;
    }


    public void infoDialog(int resource, boolean HighPriority)
    {
        infoDialog(appContext.getResources().getString(resource), HighPriority);
    }

    public void infoDialog(String message, boolean HighPriority)
    {
        if(!appIsVisible || (!HighPriority && !settingsManager.areMessageDialogsEnabled()))
            return;

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(appContext);

        if(dialogBuilder == null)
        {
            EddieLogger.error("SupportTools.infoDialog(): cannot create dialog");

            return;
        }

        final AlertDialog infoDialog = dialogBuilder.create();

        infoDialog.setTitle(appContext.getResources().getString(R.string.eddie));

        infoDialog.setIcon(android.R.drawable.ic_dialog_info);

        infoDialog.setMessage(message);

        infoDialog.setButton(AlertDialog.BUTTON_POSITIVE, appContext.getResources().getString(R.string.ok), new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                infoDialog.dismiss();
            }
        });

        if(appContext instanceof Activity)
        {
            if(((Activity)appContext).isFinishing())
                return;
        }

        infoDialog.show();
    }

    public void infoDialogHtml(Spanned message, boolean HighPriority)
    {
        if(!appIsVisible || (!HighPriority && !settingsManager.areMessageDialogsEnabled()))
            return;

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(appContext);

        if(dialogBuilder == null)
        {
            EddieLogger.error("SupportTools.infoDialog(): cannot create dialog");

            return;
        }

        final AlertDialog infoDialog = dialogBuilder.create();

        infoDialog.setTitle(appContext.getResources().getString(R.string.eddie));

        infoDialog.setIcon(android.R.drawable.ic_dialog_info);

        infoDialog.setMessage(message);

        infoDialog.setButton(AlertDialog.BUTTON_POSITIVE, appContext.getResources().getString(R.string.ok), new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                infoDialog.dismiss();
            }
        });

        if(appContext instanceof Activity)
        {
            if(((Activity)appContext).isFinishing())
                return;
        }

        infoDialog.show();
    }

    public void waitInfoDialog(int resource)
    {
        waitInfoDialog(appContext.getResources().getString(resource));
    }

    public void waitInfoDialog(String message)
    {
        if(!appIsVisible)
            return;

        dialogConfirm = false;

        dialogHandler = new Handler(new Handler.Callback()
        {
            @Override
            public boolean handleMessage(Message mesg)
            {
                throw new RuntimeException();
            }
        });

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(appContext);

        if(dialogBuilder == null)
        {
            EddieLogger.error("SupportTools.infoDialog(): cannot create dialog");

            return;
        }

        final AlertDialog waitInfoDialog = dialogBuilder.create();

        waitInfoDialog.setTitle(appContext.getResources().getString(R.string.eddie));

        waitInfoDialog.setIcon(android.R.drawable.ic_dialog_alert);

        waitInfoDialog.setMessage(message);

        waitInfoDialog.setButton(AlertDialog.BUTTON_POSITIVE, appContext.getResources().getString(R.string.ok), new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                dialogConfirm = true;

                dialogHandler.sendMessage(dialogHandler.obtainMessage());

                waitInfoDialog.dismiss();
            }
        });

        if(appContext instanceof Activity)
        {
            if(((Activity)appContext).isFinishing())
                return;
        }

        waitInfoDialog.show();

        try
        {
            Looper.loop();
        }
        catch(RuntimeException e)
        {
        }
    }

    public boolean confirmationDialog(int resource)
    {
        return confirmationDialog(appContext.getResources().getString(resource));
    }

    public boolean confirmationDialog(String message)
    {
        dialogConfirm = false;

        dialogHandler = new Handler(new Handler.Callback()
        {
            @Override
            public boolean handleMessage(Message mesg)
            {
                throw new RuntimeException();
            }
        });

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(appContext);

        if(dialogBuilder == null)
        {
            EddieLogger.error("SupportTools.infoDialog(): cannot create dialog");

            return false;
        }

        final AlertDialog confirmationDialog = dialogBuilder.create();

        confirmationDialog.setTitle(appContext.getResources().getString(R.string.eddie));

        confirmationDialog.setIcon(android.R.drawable.ic_dialog_alert);

        confirmationDialog.setMessage(message);

        confirmationDialog.setButton(AlertDialog.BUTTON_POSITIVE, appContext.getResources().getString(R.string.yes), new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                dialogConfirm = true;

                dialogHandler.sendMessage(dialogHandler.obtainMessage());

                confirmationDialog.dismiss();
            }
        });

        confirmationDialog.setButton(AlertDialog.BUTTON_NEGATIVE, appContext.getResources().getString(R.string.no), new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                dialogConfirm = false;

                dialogHandler.sendMessage(dialogHandler.obtainMessage());

                confirmationDialog.dismiss();
            }
        });

        if(appContext instanceof Activity)
        {
            if(((Activity)appContext).isFinishing())
                return false;
        }

        confirmationDialog.show();

        try
        {
            Looper.loop();
        }
        catch(RuntimeException e)
        {
        }

        return dialogConfirm;
    }

    public static String getExceptionDetails(Exception e)
    {
        Writer writer = new StringWriter();

        e.printStackTrace(new PrintWriter(writer));

        String s = writer.toString();

        return s;
    }

    public static Thread startThread(Runnable runnable)
    {
        if(runnable == null)
            return null;

        Thread thread = new Thread(runnable);

        if(thread != null)
        {
            try
            {
                thread.start();
            }
            catch(IllegalThreadStateException e)
            {
                thread = null;
            }
        }

        return thread;
    }

    public static Thread runOnUiActivity(final Activity activity, final Runnable runnable)
    {
        Runnable uiRunnable = new Runnable()
        {
            @Override
            public void run()
            {
                if(activity != null && runnable != null)
                    activity.runOnUiThread(runnable);
            }
        };

        return startThread(uiRunnable);
    }

    public static void enableButton(Button btn, boolean enabled)
    {
        if(btn == null)
            return;

        btn.setEnabled(enabled);

        if(enabled)
            btn.setAlpha(1.0f);
        else
            btn.setAlpha(0.4f);
    }

    public static void enableImageButton(ImageButton btn, boolean enabled)
    {
        if(btn == null)
            return;

        btn.setEnabled(enabled);

        if(enabled)
            btn.setAlpha(1.0f);
        else
            btn.setAlpha(0.4f);
    }

    public byte[] hashMapToUtf8Bytes(HashMap<String, byte[]> h)
    {
        String output = "";

        for(Map.Entry<String, byte[]> pair : h.entrySet())
        {
            output += Base64.encodeToString(pair.getKey().getBytes(StandardCharsets.UTF_8), Base64.NO_WRAP);

            output += ":";

            output += Base64.encodeToString(pair.getValue(), Base64.NO_WRAP);

            output += "\n";
        }

        return output.getBytes();
    }

    public String xmlDocumentToString(Document doc)
    {
        String xml = "";
        try
        {
            StringWriter stringWriter = new StringWriter();
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();

            transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
            transformer.setOutputProperty(OutputKeys.METHOD, "xml");
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");

            transformer.transform(new DOMSource(doc), new StreamResult(stringWriter));

            xml = stringWriter.toString();
        }
        catch (Exception e)
        {
            xml = "";

            EddieLogger.error("SupportTool.xmlDocumentToString() Exception: %s", e);
        }

        return xml;
    }

    public Document stringToXmlDocument(String data)
    {
        Document document = null;

        try
        {
            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();

            document = documentBuilder.parse(new InputSource(new StringReader(data)));
        }
        catch(Exception e)
        {
            document = null;

            EddieLogger.error("SupportTool.stringToXmlDocument() Exception: %s", e);
        }

        return document;
    }

    public boolean encryptStringToFile(String data, String key, String fileName)
    {
        byte[] encryptedData = null;
        SecretKeySpec secretKey = null;

        if(data.isEmpty() || key.isEmpty() || fileName.isEmpty())
            return false;

        try
        {
            secretKey = create256BitEncryptionKey(key);

            Cipher aesCipher = Cipher.getInstance("AES");

            aesCipher.init(Cipher.ENCRYPT_MODE, secretKey);

            encryptedData = aesCipher.doFinal(data.getBytes());
        }
        catch(Exception e)
        {
            EddieLogger.error("SupportTool.encryptStringToFile() Exception: %s", e);

            return false;
        }

        if(encryptedData != null)
        {
            try
            {
                File dataFile = new File(appContext.getFilesDir(), fileName);

                if(!dataFile.exists())
                    dataFile.createNewFile();

                FileOutputStream fileOutputStream = new FileOutputStream(dataFile);

                fileOutputStream.write(encryptedData);
                fileOutputStream.flush();
                fileOutputStream.close();
            }
            catch (IOException e)
            {
                EddieLogger.error("SupportTool.encryptStringToFile() Exception: %s", e);

                return false;
            }
        }

        return true;
    }

    public byte[] decryptFileToBytes(String fileName, String key)
    {
        byte[] encryptedData = null, decryptedData = null;
        byte[] buffer = null;

        SecretKeySpec secretKey = null;

        if(key.isEmpty() || fileName.isEmpty())
            return null;

        try
        {
            File dataFile = new File(appContext.getFilesDir(), fileName);

            if(!dataFile.exists())
            {
                EddieLogger.warning("SupportTool.decryptFileToBytes(): file %s not found", fileName);

                return null;
            }

            buffer = new byte[0xFFFF];

            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            BufferedInputStream buf = new BufferedInputStream(new FileInputStream(dataFile));

            for(int len = buf.read(buffer, 0, buffer.length); len != -1; len = buf.read(buffer))
                outputStream.write(buffer, 0, len);

            buf.close();

            encryptedData = outputStream.toByteArray();
        }
        catch(Exception e)
        {
            EddieLogger.error("SupportTool.decryptFileToBytes() Exception: %s", e);

            return null;
        }

        try
        {
            secretKey = create256BitEncryptionKey(key);

            Cipher aesDecipher = Cipher.getInstance("AES");

            aesDecipher.init(Cipher.DECRYPT_MODE, secretKey);

            decryptedData = aesDecipher.doFinal(encryptedData);
        }
        catch(Exception e)
        {
            EddieLogger.error("SupportTool.decryptFileToBytes() Exception: %s", e);

            return null;
        }

        return decryptedData;
    }

    public boolean saveXmlDocumentToFile(Document document, String fileName)
    {
        if(document == null || fileName.isEmpty())
            return false;

        String data = xmlDocumentToString(document);

        try
        {
            File dataFile = new File(appContext.getFilesDir(), fileName);

            if(!dataFile.exists())
                dataFile.createNewFile();

            FileOutputStream fileOutputStream = new FileOutputStream(dataFile);

            fileOutputStream.write(data.getBytes());
            fileOutputStream.flush();
            fileOutputStream.close();
        }
        catch (IOException e)
        {
            EddieLogger.error("SupportTool.saveXmlDocumentToFile() Exception: %s", e);

            return false;
        }

        return true;
    }

    public Document loadXmlFileToDocument(String fileName)
    {
        byte[] fileData = null, buffer = null;

        if(fileName.isEmpty())
            return null;

        File dataFile = new File(appContext.getFilesDir(), fileName);

        if(!dataFile.exists())
        {
            EddieLogger.warning("SupportTool.decryptFileToBytes(): file %s not found", fileName);

            return null;
        }

        try
        {
            buffer = new byte[0xFFFF];

            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            BufferedInputStream buf = new BufferedInputStream(new FileInputStream(dataFile));

            for(int len = buf.read(buffer, 0, buffer.length); len != -1; len = buf.read(buffer))
                outputStream.write(buffer, 0, len);

            buf.close();

            fileData = outputStream.toByteArray();
        }
        catch(Exception e)
        {
            EddieLogger.error("SupportTool.decryptFileToBytes() Exception: %s", e);

            return null;
        }

        return stringToXmlDocument(new String(fileData));
    }

    public boolean encryptXmlDocumentToFile(Document document, String key, String fileName)
    {
        if(document == null || key.isEmpty() || fileName.isEmpty())
            return false;

        String data = xmlDocumentToString(document);

        return encryptStringToFile(data, key, fileName);
    }

    public Document decryptFileToXmlDocument(String fileName, String key)
    {
        Document xmlDocument = null;

        if(fileName.isEmpty() || key.isEmpty())
            return null;

        byte[] decryptedData = decryptFileToBytes(fileName, key);

        if(decryptedData != null)
            xmlDocument = stringToXmlDocument(new String(decryptedData));

        return xmlDocument;
    }

    private SecretKeySpec create256BitEncryptionKey(String key)
    {
        String encryptionKey = "";
        SecretKeySpec secretKey = null;
        int keyLen = 32; // 32 bytes, 256 bit

        while(encryptionKey.length() < keyLen)  // 256 bit key
            encryptionKey += key;

        secretKey = new SecretKeySpec(encryptionKey.getBytes(), 0, keyLen, "AES");

        return secretKey;
    }

    public String getXmlItemValue(Document document, String item)
    {
        String value = "";

        try
        {
            NodeList nodeList = document.getElementsByTagName(item);

            if(nodeList != null)
            {
                Element element = (Element) nodeList.item(0);

                if(element != null)
                    value = element.getTextContent();
                else
                    value = "";
            }
        }
        catch(Exception e)
        {
            value = "";
        }

        return convertXmlEntities(value);
    }

    public String getXmlItemNodeValue(NamedNodeMap namedNodeMap, String item)
    {
        String value = "";

        try
        {
            Node node = namedNodeMap.getNamedItem(item);

            if(node != null)
                value = node.getNodeValue();
            else
                value = "";
        }
        catch(Exception e)
        {
            value = "";
        }

        return convertXmlEntities(value);
    }

    public String convertXmlEntities(String xml)
    {
        String value = xml;
        String[] xmlEntity = { "&#10;", "&#13;", "&#38;", "&gt;", "&lt;", "&amp;", "&quot;", "&apos;", "\\'", "\\\"" };
        String[] character = { "\n", "\r", "&", ">", "<", "&", "\"", "'", "'", "\""};

        for(int i = 0; i < xmlEntity.length; i++)
            xml = xml.replaceAll(xmlEntity[i], character[i]);

        return value;
    }

    public boolean isNetworkConnectionActive()
    {
        boolean active = false;

        NetworkStatusReceiver.Status networkStatus = NetworkStatusReceiver.getNetworkStatus();
        VPN.Status vpnStatus = VPN.getConnectionStatus();

        if(networkStatus != NetworkStatusReceiver.Status.CONNECTED)
            return false;

        if(vpnStatus == VPN.Status.LOCKED || vpnStatus == VPN.Status.PAUSED_BY_SYSTEM || vpnStatus == VPN.Status.PAUSED_BY_USER)
            active = false;
        else
            active = true;

        return active;
    }

    public ArrayList<String> getLocalIPs(boolean ipv6)
    {
        LinkProperties linkProperties = null;
        NetworkCapabilities networkCapabilities = null;
        ArrayList<String> localIPs = new ArrayList<String>();

        ConnectivityManager connectivityManager = (ConnectivityManager) appContext.getSystemService(Context.CONNECTIVITY_SERVICE);

        Network[] deviceNetworks = connectivityManager.getAllNetworks();

        for(Network network : deviceNetworks)
        {
            linkProperties = connectivityManager.getLinkProperties(network);

            networkCapabilities = connectivityManager.getNetworkCapabilities(network);

            if (networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_VPN))
                continue;

            if (networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR))
                continue;

            for(LinkAddress linkAddress : linkProperties.getLinkAddresses())
            {
                if((linkAddress.getAddress() instanceof Inet4Address && !ipv6) || (linkAddress.getAddress() instanceof Inet6Address && ipv6))
                    localIPs.add(linkAddress.toString());
            }
        }

        return localIPs;
    }

    public static long IPToLong(String ip)
    {
        String[] octect = ip.split("\\.");

        long longIP = 0;

        for(int i = 0; i < octect.length; i++)
        {
            int power = 3 - i;

            try
            {
                longIP += ((Integer.parseInt(octect[i]) % 256 * Math.pow(256, power)));
            }
            catch(NumberFormatException e)
            {
            }
        }

        return longIP;
    }

    public static String longToIP(long ip)
    {
        return String.format("%d.%d.%d.%d", ((ip >> 24 ) & 0xff), ((ip >> 16 ) & 0xff), ((ip >>  8 ) & 0xff), ip & 0xff);
    }

    public String formatTransferRate(long rate)
    {
        return formatTransferRate(rate, true);
    }

    public String formatTransferRate(long rate, boolean decimal)
    {
        String txt = "", floatFormat = "";
        float r = (float)rate;

        if(decimal)
            floatFormat = "%.2f %s";
        else
            floatFormat = "%.0f %s";

        if(rate >= ONE_DECIMAL_GIGA)
            txt = String.format(Locale.getDefault(), floatFormat, r / ONE_DECIMAL_GIGA, "Gbit/s");
        else if(rate >= ONE_DECIMAL_MEGA)
            txt = String.format(Locale.getDefault(), floatFormat, r / ONE_DECIMAL_MEGA, "Mbit/s");
        else if(rate > ONE_DECIMAL_KILO)
            txt = String.format(Locale.getDefault(), floatFormat, r / ONE_DECIMAL_KILO, "Kbit/s");
        else
            txt = String.format(Locale.getDefault(),"%d bit/s", (int)r);

        return txt;
    }

    public String formatDataVolume(long bytes)
    {
        return formatDataVolume(bytes, true);
    }

    public String formatDataVolume(long bytes, boolean decimal)
    {
        String txt = "", floatFormat = "";
        float fBytes = (float)bytes;

        if(decimal)
            floatFormat = "%.2f %s";
        else
            floatFormat = "%.0f %s";

        if(bytes >= ONE_GIGABYTE)
            txt = String.format(Locale.getDefault(), floatFormat, fBytes / ONE_GIGABYTE, "GB");
        else if(bytes >= ONE_MEGABYTE)
            txt = String.format(Locale.getDefault(), floatFormat, fBytes / ONE_MEGABYTE, "MB");
        else if(bytes >= ONE_KILOBYTE)
            txt = String.format(Locale.getDefault(), floatFormat, fBytes / ONE_KILOBYTE, "KB");
        else
            txt = String.format(Locale.getDefault(),"%d bytes", bytes);

        return txt;
    }

    public int getLoad(long byteBandWidth, long maxMBitBandWidth)
    {
        long bwCur = 2 * (byteBandWidth * 8) / ONE_DECIMAL_MEGA; // to Mbit/s
        int load = 0;

        if(maxMBitBandWidth > 0)
            load = (int)((bwCur * 100) / maxMBitBandWidth);
        else
            load = 0;

        return load;
    }

    public String getOptionFromListDialog(int resTitle, int resLabel, int resValue, final String selectedValue)
    {
        dialogLabels = appContext.getResources().getStringArray(resLabel);
        dialogValues = appContext.getResources().getStringArray(resValue);

        dialogReturnStringValue = selectedValue;

        dialogHandler = new Handler(new Handler.Callback()
        {
            @Override
            public boolean handleMessage(Message mesg)
            {
                throw new RuntimeException();
            }
        });

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(appContext);

        if(dialogBuilder == null)
        {
            EddieLogger.error("SupportTools.infoDialog(): cannot create dialog");

            return selectedValue;
        }

        dialogBuilder.setTitle(appContext.getResources().getString(resTitle));

        int checkedItem = Arrays.asList(dialogValues).indexOf(selectedValue);

        dialogBuilder.setSingleChoiceItems(resLabel, checkedItem,  new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialogInterface, int i)
            {
                dialogReturnStringValue = dialogValues[i];
            }
        });

        dialogBuilder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface dialog, int id)
            {
                dialogHandler.sendMessage(dialogHandler.obtainMessage());
            }
        });

        dialogBuilder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface dialog, int id)
            {
                dialogReturnStringValue = selectedValue;

                dialogHandler.sendMessage(dialogHandler.obtainMessage());
            }
        });

        settingDialog = dialogBuilder.create();

        if(appContext instanceof Activity)
        {
            if(((Activity)appContext).isFinishing())
                return "";
        }

        settingDialog.show();

        try
        {
            Looper.loop();
        }
        catch(RuntimeException e)
        {
        }

        return dialogReturnStringValue;
    }

    public String getTextOptionDialog(int resTitle, String selectedValue)
    {
        return editOptionDialog(resTitle, selectedValue, EditOptionType.TEXT, EditOption.DO_NOT_ALLOW_EMPTY_FIELD);
    }

    public String getTextOptionDialog(int resTitle, String selectedValue, EditOption editOption)
    {
        return editOptionDialog(resTitle, selectedValue, EditOptionType.TEXT, editOption);
    }

    public String getPasswordOptionDialog(int resTitle, String selectedValue)
    {
        return editOptionDialog(resTitle, selectedValue, EditOptionType.PASSWORD, EditOption.DO_NOT_ALLOW_EMPTY_FIELD);
    }

    public String getPasswordOptionDialog(int resTitle, String selectedValue, EditOption editOption)
    {
        return editOptionDialog(resTitle, selectedValue, EditOptionType.PASSWORD, editOption);
    }

    public long getNumericOptionDialog(int resTitle, long selectedValue)
    {
        return getNumericOptionDialog(resTitle, selectedValue, EditOption.DO_NOT_ALLOW_ZERO_VALUE);
    }

    public long getNumericOptionDialog(int resTitle, long selectedValue, EditOption editOption)
    {
        String value = "";
        long retVal = 0;

        value = editOptionDialog(resTitle, String.format(Locale.getDefault(), "%d", selectedValue), EditOptionType.NUMERIC, editOption);

        try
        {
            retVal = Long.parseLong(value);
        }
        catch(NumberFormatException e)
        {
            retVal = 0;
        }

        return retVal;
    }

    public String getIPv4AddressOptionDialog(int resTitle, String selectedValue)
    {
        return editOptionDialog(resTitle, selectedValue, EditOptionType.IP_ADDRESS_LIST, EditOption.DO_NOT_ALLOW_EMPTY_FIELD);
    }

    public String getIPv4AddressOptionDialog(int resTitle, String selectedValue, EditOption editOption)
    {
        return editOptionDialog(resTitle, selectedValue, EditOptionType.IP_ADDRESS_LIST, editOption);
    }

    public long getIpPortOptionDialog(int resTitle, long selectedValue)
    {
        return getIpPortOptionDialog(resTitle, selectedValue, EditOption.DO_NOT_ALLOW_ZERO_VALUE);
    }

    public long getIpPortOptionDialog(int resTitle, long selectedValue, EditOption editOption)
    {
        String value = "";
        long retVal = 0;

        value = editOptionDialog(resTitle, String.format(Locale.getDefault(), "%d", selectedValue), EditOptionType.IP_PORT, editOption);

        try
        {
            retVal = Long.parseLong(value);
        }
        catch(NumberFormatException e)
        {
            retVal = 0;
        }

        return retVal;
    }

    public String editOptionDialog(int resTitle, String selectedValue)
    {
        return editOptionDialog(resTitle, selectedValue, EditOptionType.TEXT, EditOption.DO_NOT_ALLOW_EMPTY_FIELD);
    }

    public String editOptionDialog(int resTitle, final String selectedValue, final EditOptionType editType, final EditOption editOption)
    {
        TextView txtDialogTitle = null;

        btnOk = null;
        btnCancel = null;

        dialogHandler = new Handler(new Handler.Callback()
        {
            @Override
            public boolean handleMessage(Message mesg)
            {
                throw new RuntimeException();
            }
        });

        dialogBuilder = new AlertDialog.Builder(appContext);

        if(dialogBuilder == null)
        {
            EddieLogger.error("SupportTools.infoDialog(): cannot create dialog");

            return selectedValue;
        }

        View content = LayoutInflater.from(appContext).inflate(R.layout.edit_option_dialog, null);

        txtDialogTitle = (TextView)content.findViewById(R.id.title);
        edtKey = (EditText)content.findViewById(R.id.key);

        edtKey.setText(selectedValue);

        if(editType == EditOptionType.NUMERIC || editType == EditOptionType.IP_PORT)
        {
            edtKey.setRawInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_SIGNED);

            edtKey.setGravity(Gravity.RIGHT);

            edtKey.setSelection(edtKey.getText().length());
        }

        if(editType == EditOptionType.PASSWORD)
        {
            edtKey.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD | InputType.TYPE_CLASS_TEXT);
        }

        edtKey.setSelection(edtKey.getText().length());

        btnOk = (Button)content.findViewById(R.id.btn_ok);
        btnCancel = (Button)content.findViewById(R.id.btn_cancel);

        enableButton(btnOk, editFieldOptionIsValid(editType, selectedValue, editOption));

        enableButton(btnCancel, true);

        btnOk.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View arg0)
            {
                int errMsgResource = 0;

                if(editFieldIsValid(editType, edtKey.getText().toString(), editOption) == false)
                {
                    switch(editType)
                    {
                        case IP_ADDRESS_LIST:
                        {
                            errMsgResource = R.string.settings_ip_address_warning;
                        }
                        break;

                        case IP_PORT:
                        {
                            errMsgResource = R.string.settings_ip_port_warning;
                        }
                        break;

                        default:
                        {
                            errMsgResource = R.string.settings_value_warning;
                        }
                        break;
                    }

                    infoDialog(errMsgResource, true);

                    return;
                }

                if(edtKey.getText().length() > 0 || editOption == EditOption.ALLOW_EMPTY_FIELD || editOption == EditOption.ALLOW_ZERO_VALUE)
                    dialogReturnStringValue = edtKey.getText().toString();

                settingDialog.dismiss();

                dialogHandler.sendMessage(dialogHandler.obtainMessage());
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View arg0)
            {
                dialogReturnStringValue = selectedValue;

                settingDialog.dismiss();

                dialogHandler.sendMessage(dialogHandler.obtainMessage());
            }
        });

        edtKey.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void afterTextChanged(Editable s)
            {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                enableButton(btnOk, editFieldOptionIsValid(editType, edtKey.getText().toString(), editOption));
            }
        });

        txtDialogTitle.setText(appContext.getResources().getString(resTitle));

        dialogBuilder.setTitle("");
        dialogBuilder.setView(content);

        settingDialog = dialogBuilder.create();
        settingDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        settingDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);

        if(appContext instanceof Activity)
        {
            if(((Activity)appContext).isFinishing())
                return "";
        }

        settingDialog.show();

        try
        {
            Looper.loop();
        }
        catch(RuntimeException e)
        {
        }

        return dialogReturnStringValue.trim();
    }

    private boolean editFieldOptionIsValid(EditOptionType editType, String value, EditOption option)
    {
        boolean isValid = false;

        switch(editType)
        {
            case TEXT:
            case PASSWORD:
            case IP_ADDRESS_LIST:
            {
                if(value.length() > 0 || option == EditOption.ALLOW_EMPTY_FIELD)
                    isValid = true;
                else
                    isValid = false;
            }
            break;

            case NUMERIC:
            case IP_PORT:
            {
                long tVal = 0;

                try
                {
                    tVal = Long.parseLong(value);
                }
                catch(NumberFormatException e)
                {
                    tVal = 0;
                }

                if(tVal > 0 || option == EditOption.ALLOW_ZERO_VALUE)
                    isValid = true;
                else
                    isValid = false;
            }
            break;
        }

        return isValid;
    }

    private boolean editFieldIsValid(EditOptionType editType, String value)
    {
        return editFieldIsValid(editType, value, EditOption.DO_NOT_ALLOW_EMPTY_FIELD);
    }

    private boolean editFieldIsValid(EditOptionType editType, String value, EditOption editOption)
    {
        boolean isValid = false;
        long tVal = 0;

        switch(editType)
        {
            case TEXT:
            {
                isValid = true;
            }
            break;

            case PASSWORD:
            {
                isValid = true;
            }
            break;

            case NUMERIC:
            {
                isValid = true;
            }
            break;

            case IP_ADDRESS_LIST:
            {
                String[] octect = null;
                isValid = true;

                String[] ipAdrressArray = value.split(SettingsManager.DEFAULT_SPLIT_SEPARATOR);

                for(String ipAddress : ipAdrressArray)
                {
                    octect = ipAddress.split("\\.");

                    if(octect.length != 4)
                        isValid = false;

                    for(String val : octect)
                    {
                        try
                        {
                            tVal = Long.parseLong(val);
                        }
                        catch(NumberFormatException e)
                        {
                            tVal = 0;

                            isValid = false;
                        }

                        if(tVal < 0 || tVal > 255)
                            isValid = false;
                    }
                }

                if(value.length() == 0 && editOption == EditOption.ALLOW_EMPTY_FIELD)
                    isValid = true;
            }
            break;

            case IP_PORT:
            {
                try
                {
                    tVal = Long.parseLong(value);
                }
                catch(NumberFormatException e)
                {
                    tVal = 0;

                    isValid = false;
                }

                if(tVal >= 1 && tVal <= 65535)
                    isValid = true;
                else
                    isValid = false;

                if(tVal == 0 && editOption == EditOption.ALLOW_ZERO_VALUE)
                    isValid = true;
            }
            break;

            default:
            {
                isValid = false;
            }
            break;
        }

        return isValid;
    }

    public boolean isAccessibilityEnabled()
    {
        AccessibilityManager am = (AccessibilityManager)appContext.getSystemService(ACCESSIBILITY_SERVICE);

        return (am.isEnabled() || am.isTouchExplorationEnabled());
    }

    public class RequestAirVPNDocument extends AsyncTask<HashMap<String, String>, Void, Integer>
    {
        private Document airVpnDocument = null;
        private HashMap<String, byte[]> parameters = null;

        @Override
        protected void onPreExecute()
        {
        }

        @Override
        protected Integer doInBackground(HashMap<String, String>... params)
        {
            byte[] iv = null, bytesParamS = null, aesDataIn = null, bytesParamD = null;
            HashMap<String, byte[]> assocParamS = null;
            Cipher rsaCipher = null, aesCipher = null, aesDecipher = null;
            KeyFactory keyFactory = null;
            KeyGenerator keyGenerator = null;
            SecureRandom secureRandom = null;
            RSAPublicKeySpec publicKeySpec = null;
            RSAPublicKey publicKey = null;
            HttpURLConnection httpURLConnection = null;
            DocumentBuilderFactory documentBuilderFactory = null;
            DocumentBuilder documentBuilder = null;
            InputSource inputSource = null;
            ArrayList<String> bootstrapServer = null;
            String rsaModulus = "", rsaExponent = "", urlParameters = "", xmlDocument = "", logTryNext = "";
            AirVPNManifest.ManifestType manifestType = AirVPNManifest.ManifestType.NOT_SET;
            boolean documentRetrieved = false;
            Integer operationResult = AIRVPN_REQUEST_DOCUMENT_FAIL;

            if(params[0] == null)
            {
                EddieLogger.error("RequestAirVPNDocument(): parameters is null");

                return AIRVPN_REQUEST_DOCUMENT_FAIL;
            }

            parameters = new HashMap<String, byte[]>();

            for(Map.Entry<String, String> pair : params[0].entrySet())
                parameters.put(pair.getKey(), (pair.getValue()).getBytes());

            if(isNetworkConnectionActive() == false)
            {
                if(parameters.containsKey("act"))
                    EddieLogger.error("Cannot download %s document from AirVPN. Network is not available.", new String(parameters.get("act")));
                else
                    EddieLogger.error("Cannot download document from AirVPN. Network is not available.");

                return AIRVPN_REQUEST_DOCUMENT_FAIL;
            }

            manifestType = AirVPNManifest.getManifestType();

            if(parameters.containsKey("act"))
            {
                String act = new String(parameters.get("act"));

                switch(act)
                {
                    case "manifest":
                    {
                        if(manifestType == AirVPNManifest.ManifestType.NOT_SET || manifestType == AirVPNManifest.ManifestType.PROCESSING)
                            return AIRVPN_REQUEST_DOCUMENT_IGNORE;
                    }
                    break;

                    case "user":
                    {
                        if(AirVPNUser.getUserProfileType() == AirVPNUser.UserProfileType.PROCESSING)
                            return AIRVPN_REQUEST_DOCUMENT_IGNORE;
                    }
                    break;

                    default:
                    {
                        EddieLogger.error("Unknown AirVPN document %s", act);

                        return AIRVPN_REQUEST_DOCUMENT_IGNORE;
                    }
                }

                EddieLogger.info("Requesting %s document to AirVPN", act);
            }
            else
            {
                EddieLogger.error("RequestAirVPNDocument(): Missing document type");

                return AIRVPN_REQUEST_DOCUMENT_FAIL;
            }

            assocParamS = new HashMap<String, byte[]>();

            try
            {
                rsaCipher = Cipher.getInstance("RSA/None/PKCS1Padding");
                aesCipher = Cipher.getInstance("AES/CBC/PKCS7Padding");
                aesDecipher = Cipher.getInstance("AES/CBC/PKCS7Padding");
                keyFactory = KeyFactory.getInstance("RSA");

                keyGenerator = KeyGenerator.getInstance("AES");
                secureRandom = SecureRandom.getInstance("SHA1PRNG");
            }
            catch(Exception e)
            {
                EddieLogger.error("RequestAirVPNDocument() Exception: %s", e);

                return AIRVPN_REQUEST_DOCUMENT_FAIL;
            }

            keyGenerator.init(256, secureRandom);

            iv = new byte[aesCipher.getBlockSize()];

            secureRandom.nextBytes(iv);
            SecretKey skey = keyGenerator.generateKey();
            IvParameterSpec ivspec = new IvParameterSpec(iv);

            manifestType = AirVPNManifest.getManifestType();

            if(manifestType == AirVPNManifest.ManifestType.NOT_SET || manifestType == AirVPNManifest.ManifestType.PROCESSING)
                return AIRVPN_REQUEST_DOCUMENT_IGNORE;

            rsaModulus = AirVPNManifest.getRsaPublicKeyModulus();
            rsaExponent = AirVPNManifest.getRsaPublicKeyExponent();

            if(rsaModulus.isEmpty() || rsaExponent.isEmpty())
                return AIRVPN_REQUEST_DOCUMENT_IGNORE;

            publicKeySpec = new RSAPublicKeySpec(new BigInteger(1, Base64.decode(rsaModulus.getBytes(), Base64.NO_WRAP)), new BigInteger(1, Base64.decode(rsaExponent.getBytes(), Base64.NO_WRAP)));

            try
            {
                publicKey = (RSAPublicKey) keyFactory.generatePublic(publicKeySpec);
            }
            catch(Exception e)
            {
                EddieLogger.error("RequestAirVPNDocument() Exception: %s", e);

                return AIRVPN_REQUEST_DOCUMENT_FAIL;
            }

            assocParamS.put("key", skey.getEncoded());
            assocParamS.put("iv", ivspec.getIV());

            try
            {
                rsaCipher.init(Cipher.ENCRYPT_MODE, publicKey);
            }
            catch(Exception e)
            {
                EddieLogger.error("RequestAirVPNDocument() Exception: %s", e);

                return AIRVPN_REQUEST_DOCUMENT_FAIL;
            }

            try
            {
                bytesParamS = rsaCipher.doFinal(hashMapToUtf8Bytes(assocParamS));
            }
            catch(Exception e)
            {
                EddieLogger.error("RequestAirVPNDocument()", e);

                return AIRVPN_REQUEST_DOCUMENT_FAIL;
            }

            aesDataIn = hashMapToUtf8Bytes(parameters);

            try
            {
                aesCipher.init(Cipher.ENCRYPT_MODE, skey, ivspec);
            }
            catch(Exception e)
            {
                EddieLogger.error("RequestAirVPNDocument() Exception: %s", e);

                return AIRVPN_REQUEST_DOCUMENT_FAIL;
            }

            try
            {
                bytesParamD = aesCipher.doFinal(aesDataIn);
            }
            catch(Exception e)
            {
                EddieLogger.error("RequestAirVPNDocument() Exception: %s", e);

                return AIRVPN_REQUEST_DOCUMENT_FAIL;
            }

            try
            {
                urlParameters = "s=";
                urlParameters += URLEncoder.encode(Base64.encodeToString(bytesParamS, Base64.NO_WRAP), "UTF-8");
                urlParameters += "&d=";
                urlParameters += URLEncoder.encode(Base64.encodeToString(bytesParamD, Base64.NO_WRAP), "UTF-8");
            }
            catch(Exception e)
            {
                EddieLogger.error("RequestAirVPNDocument() Exception: %s", e);

                return AIRVPN_REQUEST_DOCUMENT_FAIL;
            }

            if(!settingsManager.getAirVPNCustomBootstrap().isEmpty())
            {
                EddieLogger.info("RequestAirVPNDocument(): Connection will use custom bootstrap servers");

                bootstrapServer = new ArrayList<String>();

                for(String bootserver : settingsManager.getAirVPNCustomBootstrapList())
                {
                    if(!bootserver.substring(0, 6).toLowerCase().equals("http://"))
                        bootserver = "http://" + bootserver;

                    bootstrapServer.add(bootserver);
                }

                bootstrapServer.addAll(AirVPNManifest.getBootStrapServerUrlList());
            }
            else
                bootstrapServer = AirVPNManifest.getBootStrapServerUrlList();

            if(bootstrapServer == null)
                return AIRVPN_REQUEST_DOCUMENT_IGNORE;

            documentRetrieved = false;

            for(int i = 0; i < bootstrapServer.size() && documentRetrieved == false; i++)
            {
                if(i < bootstrapServer.size() - 1)
                    logTryNext = ". Attempting to connect to next server.";
                else
                    logTryNext = "";

                try
                {
                    URL url = new URL(bootstrapServer.get(i));

                    HttpURLConnection.setFollowRedirects(false);

                    httpURLConnection = (HttpURLConnection) url.openConnection();
                    httpURLConnection.setConnectTimeout(HTTP_CONNECTION_TIMEOUT);
                    httpURLConnection.setReadTimeout(HTTP_READ_TIMEOUT);
                    httpURLConnection.setRequestMethod("POST");
                    httpURLConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                    httpURLConnection.setRequestProperty("Content-Length", String.valueOf(urlParameters.getBytes().length));
                    httpURLConnection.setDoOutput(true);
                    httpURLConnection.getOutputStream().write(urlParameters.getBytes());

                    int res = httpURLConnection.getResponseCode();

                    if(res == HttpURLConnection.HTTP_OK)
                    {
                        Reader in = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream(), "UTF-8"));

                        InputStream inputStream = httpURLConnection.getInputStream();

                        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                        byte[] buffer = new byte[0xFFFF];

                        for(int len = inputStream.read(buffer, 0, buffer.length); len != -1; len = inputStream.read(buffer))
                            byteArrayOutputStream.write(buffer, 0, len);

                        aesDecipher.init(Cipher.DECRYPT_MODE, skey, ivspec);

                        xmlDocument = new String(aesDecipher.doFinal(byteArrayOutputStream.toByteArray()));

                        documentBuilderFactory = DocumentBuilderFactory.newInstance();

                        documentBuilder = documentBuilderFactory.newDocumentBuilder();

                        inputSource = new InputSource(new StringReader(xmlDocument));

                        airVpnDocument = documentBuilder.parse(inputSource);

                        documentRetrieved = true;
                    }
                }
                catch(SocketTimeoutException e)
                {
                    EddieLogger.error("RequestAirVPNDocument(): AirVPN server connection timeout%s", logTryNext);
                }
                catch(ConnectException e)
                {
                    EddieLogger.error("RequestAirVPNDocument(): Cannot connect to AirVPN server");
                }
                catch(MalformedURLException e)
                {
                    EddieLogger.error("RequestAirVPNDocument(): Malformed URL at host entry %d%s", i, logTryNext);
                }
                catch(IOException e)
                {
                    EddieLogger.error("RequestAirVPNDocument(): I/O Error while contacting server%s - Exception: %s", logTryNext, e);
                }
                catch(Exception e)
                {
                    EddieLogger.error("RequestAirVPNDocument(): Connection error%s - Exception: %s", logTryNext, e);
                }
            }

            if(documentRetrieved)
                operationResult = AIRVPN_REQUEST_DOCUMENT_SUCCESS;
            else
                operationResult = AIRVPN_REQUEST_DOCUMENT_FAIL;

            return operationResult;
        }

        @Override
        protected void onPostExecute(Integer result)
        {
            switch(result)
            {
                case AIRVPN_REQUEST_DOCUMENT_SUCCESS:
                {
                    if(parameters != null && airVpnDocument != null && parameters.containsKey("act"))
                    {
                        String act = new String(parameters.get("act"));

                        EddieLogger.info("Successfully received %s document from AirVPN", act);

                        if(act.equals("manifest"))
                            eddieEvent.onAirVPNManifestReceived(airVpnDocument);
                        else if(act.equals("user"))
                            eddieEvent.onAirVPNUserProfileReceived(airVpnDocument);
                    }
                }
                break;

                case AIRVPN_REQUEST_DOCUMENT_FAIL:
                {
                    if(parameters != null && parameters.containsKey("act"))
                    {
                        String act = new String(parameters.get("act"));

                        EddieLogger.error("RequestAirVPNDocument(): Cannot retrieve document %s from server", act);

                        if(act.equals("manifest"))
                            eddieEvent.onAirVPNManifestDownloadError();
                        else if(act.equals("user"))
                            eddieEvent.onAirVPNUserProfileDownloadError();
                    }
                    else
                        EddieLogger.error("RequestAirVPNDocument(): Cannot retrieve document from server");
                }
                break;

                case AIRVPN_REQUEST_DOCUMENT_IGNORE:
                {
                    eddieEvent.onAirVPNIgnoredDocumentRequest();
                }
                break;

                default:
                {
                    eddieEvent.onAirVPNIgnoredDocumentRequest();
                }
                break;
            }
        }
    }
}
