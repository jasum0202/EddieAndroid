// <eddie_source_header>
// This file is part of Eddie/AirVPN software.
// Copyright (C)2014-2018 AirVPN (support@airvpn.org) / https://airvpn.org
//
// Eddie is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Eddie is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Eddie. If not, see <http://www.gnu.org/licenses/>.
// </eddie_source_header>
//
// 20 June 2018 - author: ProMIND - initial release. Based on revised code from com.eddie.android. (a tribute to the 1859 Perugia uprising occurred on 20 June 1859 and in memory of those brave inhabitants who fought for the liberty of Perugia)

package org.airvpn.eddie;

public class OpenVPNEvent
{
    public static final int UDP_PARTIAL_SEND_ERROR = -5;
    public static final int FATAL_ERROR = -4;
    public static final int ERROR = -3;
    public static final int FORMAL_WARNING = -2;
    public static final int MESSAGE = -1;
    public static final int DISCONNECTED = 0;
    public static final int CONNECTED = 1;
    public static final int RECONNECTING = 2;
    public static final int AUTH_PENDING = 3;
    public static final int RESOLVE = 4;
    public static final int WAIT = 5;
    public static final int WAIT_PROXY = 6;
    public static final int CONNECTING = 7;
    public static final int GET_CONFIG = 8;
    public static final int ASSIGN_IP = 9;
    public static final int ADD_ROUTES = 10;
    public static final int ECHO_OPT = 11;
    public static final int INFO = 12;
    public static final int WARN = 13;
    public static final int PAUSE = 14;
    public static final int RESUME = 15;
    public static final int RELAY = 16;
    public static final int UNSUPPORTED_FEATURE = 17;
    public static final int TRANSPORT_ERROR = 18;
    public static final int TUN_ERROR = 19;
    public static final int CLIENT_RESTART = 20;
    public static final int AUTH_FAILED = 21;
    public static final int CERT_VERIFY_FAIL = 22;
    public static final int TLS_VERSION_MIN = 23;
    public static final int CLIENT_HALT = 24;
    public static final int CLIENT_SETUP = 25;
    public static final int CONNECTION_TIMEOUT = 26;
    public static final int INACTIVE_TIMEOUT = 27;
    public static final int DYNAMIC_CHALLENGE = 28;
    public static final int PROXY_NEED_CREDS = 29;
    public static final int PROXY_ERROR = 30;
    public static final int TUN_SETUP_FAILED = 31;
    public static final int TUN_IFACE_CREATE = 32;
    public static final int TUN_IFACE_DISABLED = 33;
    public static final int EPKI_ERROR = 34;
    public static final int EPKI_INVALID_ALIAS = 35;
    public static final int RELAY_ERROR = 36;
    public static final int N_TYPES = 37;

    public int type;
    public String name;
    public String info;
    public Object data;
}
