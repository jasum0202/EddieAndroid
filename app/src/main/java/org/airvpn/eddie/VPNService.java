// <eddie_source_header>
// This file is part of Eddie/AirVPN software.
// Copyright (C)2014-2018 AirVPN (support@airvpn.org) / https://airvpn.org
//
// Eddie is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Eddie is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Eddie. If not, see <http://www.gnu.org/licenses/>.
// </eddie_source_header>
//
// 20 June 2018 - author: ProMIND - initial release. Based on revised code from com.eddie.android. (a tribute to the 1859 Perugia uprising occurred on 20 June 1859 and in memory of those brave inhabitants who fought for the liberty of Perugia)
// <eddie_source_header>
// This file is part of Eddie/AirVPN software.
// Copyright (C)2014-2018 AirVPN (support@airvpn.org) / https://airvpn.org
//
// Eddie is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Eddie is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Eddie. If not, see <http://www.gnu.org/licenses/>.
// </eddie_source_header>
//
// 20 June 2018 - author: ProMIND - initial release. Based on revised code from com.eddie.android. (a tribute to the 1859 Perugia uprising occurred on 20 June 1859 and in memory of those brave inhabitants who fought for the liberty of Perugia)

package org.airvpn.eddie;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.net.VpnService;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.provider.Settings;
import android.support.v4.app.NotificationCompat;

import java.util.HashMap;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

public class VPNService extends VpnService implements MessageHandlerListener, NetworkStatusListener
{
    public static final int SERVICE_RUNNING_NOTIFICATION_ID = 10000;
    public static final String PARAM_START = "START";
    public static final String PARAM_STOP = "STOP";
    public static final String PARAM_PROFILE = "PROFILE";

    public static final String EXTRA_RUN_ARGS = "RUN_ARGS";
    public static final String MESSAGE_TEXT = "MESSAGE";

    public static final int MSG_BIND = 50000;
    public static final int MSG_START = 50001;
    public static final int MSG_STOP = 50002;
    public static final int MSG_PAUSE = 50003;
    public static final int MSG_RESUME = 50004;
    public static final int MSG_STATUS = 50005;
    public static final int MSG_REVOKE = 50006;

    public static final int MSG_BIND_ARG_REMOVE = 60000;
    public static final int MSG_BIND_ARG_ADD = 60001;

    public static final int THREAD_MAX_JOIN_TIME = 15000;  // 15 seconds

    private VPN.Status vpnStatus = VPN.Status.UNKNOWN;
    private VPN.Status targetVpnDisconnectionStatus = VPN.Status.UNKNOWN;
    private String currentNotificationText = "";

    private NotificationCompat.Builder notification = null;
    private int alertNotificationId = 2000;

    private Messenger serviceMessenger = null;
    private Thread vpnThread = null;
    private OpenVPNTunnel vpnTunnel = null;

    private Messenger clientMessenger = null;

    private ScreenReceiver screenReceiver = null;
    private SettingsManager settingsManager = null;
    private NetworkStatusReceiver networkStatusReceiver = null;
    private EddieLogger eddieLogger = null;
    private SupportTools supportTools = null;

    public static final int STATS_UPDATE_INTERVAL_SECONDS = 2; // Two seconds
    private Timer vpnStatTimer = null;
    private long statVpnPrevBytesIn = 0, statVpnPrevBytesOut = 0;
    private long vpnInDiff = 0, vpnOutDiff = 0;
    private long vpnInRate = 0, vpnOutRate = 0;
    private OpenVPNTransportStats vpnStats = null;

    private class ScreenReceiver extends BroadcastReceiver
    {
        private VPNService vpnService = null;
        private EddieLogger eddieLogger = null;

        public ScreenReceiver(VPNService service)
        {
            vpnService = service;

            eddieLogger = new EddieLogger();

            eddieLogger.init(service);
        }

        @Override
        public void onReceive(Context context, Intent intent)
        {
            String action = intent.getAction();

            EddieLogger.debug(String.format(Locale.getDefault(), "ScreenReceiver.onReceive() action: '%s'", action));

            if(action == Intent.ACTION_SCREEN_ON)
                vpnService.onScreenChanged(true);
            else if(action == Intent.ACTION_SCREEN_OFF)
                vpnService.onScreenChanged(false);
            else
                EddieLogger.error(String.format(Locale.getDefault(), "Unhandled action '%s' received in ScreenReceiver", action));
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId)
    {
        int result = START_STICKY;

        vpnStatus = VPN.Status.UNKNOWN;

        if(intent != null)
        {
            if(intent.getBooleanExtra(PARAM_START, false))
            {
                EddieLogger.debug("VPNService.onStartCommand(): Received start command");

                vpnStatus = VPN.Status.CONNECTING;
            }
            else if(intent.getBooleanExtra(PARAM_STOP, false))
            {
                EddieLogger.debug("VPNService.onStartCommand(): Received stop command");

                vpnStatus = VPN.Status.DISCONNECTING;
            }
            else
                return result;

            updateService(vpnStatus, intent.getBundleExtra(EXTRA_RUN_ARGS));
        }
        else
            EddieLogger.debug("VPNService.onStartCommand(): No intent");

        return result;
    }

    @Override
    public IBinder onBind(Intent intent)
    {
        String action = null;
        IBinder binder = null;

        if(intent != null)
            action = intent.getAction();

        if(action != null && action.equals(VpnService.SERVICE_INTERFACE))
            binder = super.onBind(intent);
        else
            binder = serviceMessenger != null ? serviceMessenger.getBinder() : null;

        return binder;
    }

    private void init()
    {
        serviceMessenger = new Messenger(new MessageHandler(this));
    }

    private void cleanup()
    {
        if(serviceMessenger != null)
            serviceMessenger = null;
    }

    @Override
    public void onCreate()
    {
        super.onCreate();

        settingsManager = new SettingsManager(this);
        supportTools = new SupportTools(this);

        eddieLogger = new EddieLogger();
        eddieLogger.init(this);

        init();

        networkStatusReceiver = new NetworkStatusReceiver();
        networkStatusReceiver.addListener(this);
        this.registerReceiver(networkStatusReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
    }

    @Override
    public void onDestroy()
    {
        cleanup();

        super.onDestroy();

        networkStatusReceiver.removeListener(this);

        this.unregisterReceiver(networkStatusReceiver);
    }

    @Override
    public void onRevoke()
    {
        alertNotification(getResources().getString(R.string.connection_revoked));

        EddieLogger.warning("VPNService.onRevoke(): System revoked VPN connection, probably because another app requested VPN connection. Eddie has been disconnected.");

        targetVpnDisconnectionStatus = VPN.Status.CONNECTION_REVOKED_BY_SYSTEM;

        if(clientMessenger != null)
        {
            Message message = Message.obtain(null, VPNService.MSG_REVOKE, 0, 0);

            sendMessage(clientMessenger, message);
        }

        updateService(VPN.Status.DISCONNECTING, null);
    }

    public synchronized void onMessage(Message msg)
    {
        if(msg == null)
            return;

        switch(msg.what)
        {
            case MSG_BIND:
            {
                if(msg.arg1 == MSG_BIND_ARG_ADD)
                {
                    clientMessenger = msg.replyTo;

                    sendMessage(clientMessenger, createStatusMessage(vpnStatus, getResources().getString(VPN.descriptionResource(vpnStatus))));
                }
                else
                    clientMessenger = null;
            }
            break;

            case MSG_START:
            {
                vpnStatus = VPN.Status.CONNECTING;

                updateService(vpnStatus, msg.getData());
            }
            break;

            case MSG_STOP:
            {
                vpnStatus = VPN.Status.DISCONNECTING;

                if(targetVpnDisconnectionStatus != VPN.Status.CONNECTION_ERROR)
                    targetVpnDisconnectionStatus = VPN.Status.NOT_CONNECTED;

                updateService(vpnStatus, null);
            }
            break;

            case MSG_PAUSE:
            {
                pauseService();
            }
            break;

            case MSG_RESUME:
            {
                resumeService();
            }
            break;
        }
    }

    private synchronized boolean updateService(VPN.Status status, Bundle data)
    {
        boolean running = (vpnStatus == VPN.Status.CONNECTED) || (vpnStatus == VPN.Status.CONNECTING) || (vpnStatus == VPN.Status.PAUSED_BY_USER) || (vpnStatus == VPN.Status.PAUSED_BY_SYSTEM) || (vpnStatus == VPN.Status.LOCKED);

        if(running && status == VPN.Status.CONNECTED)
            return true;

        switch(status)
        {
            case CONNECTING:
            {
                doStart(data);
            }
            break;

            case DISCONNECTING:
            {
                doStop(status);
            }
            break;

            default:
            {
                return false;
            }
        }

        return true;
    }

    private void onServiceStarted()
    {
        ensureReceivers();
    }

    private void onServiceStopped()
    {
        cleanupReceivers();
    }

    private void onScreenChanged(boolean active)
    {
        EddieLogger.debug(String.format(Locale.getDefault(), "VPNService.onScreenChanged(): active is %s", Boolean.toString(active)));

        try
        {
            if(vpnTunnel != null)
            {
                VPN.Status status;

                status = vpnTunnel.handleScreenChanged(active);

                doChangeStatus(status);
            }
        }
        catch(Exception e)
        {
            EddieLogger.error("VPNService.onScreenChanged() Exception: %s", e);
        }
    }

    private void networkStatusChanged(OpenVPNTunnel.VPNAction action)
    {
        EddieLogger.debug(String.format(Locale.getDefault(), "VPNService.networkStatusChanged() action: '%s'", action.toString()));

        try
        {
            if(vpnTunnel != null)
                vpnTunnel.networkStatusChanged(action);
        }
        catch(Exception e)
        {
            EddieLogger.error("VPNService.networkStatusChanged() Exception: %s", e);
        }
    }

    private void ensureReceivers()
    {
        if(screenReceiver != null)
            return;

        IntentFilter intentFilter = new IntentFilter();

        intentFilter.addAction(Intent.ACTION_SCREEN_ON);
        intentFilter.addAction(Intent.ACTION_SCREEN_OFF);

        screenReceiver = new ScreenReceiver(this);

        try
        {
            registerReceiver(screenReceiver, intentFilter);
        }
        catch(Exception e)
        {
            EddieLogger.error("VPNService.ensureReceivers(): Error while registering screenReceiver. Exception: %s", e);
        }
    }

    private void cleanupReceivers()
    {
        if(screenReceiver != null)
        {
            try
            {
                unregisterReceiver(screenReceiver);
            }
            catch(Exception e)
            {
            }

            screenReceiver = null;
        }
    }

    public void handleThreadStarted()
    {
        doStartForeground();

        onServiceStarted();

        doChangeStatus(VPN.Status.CONNECTED);
    }

    public void handleConnectionError()
    {
        doStop(VPN.Status.CONNECTION_ERROR);
    }

    public void handleThreadException(Exception e)
    {
        EddieLogger.error("VPNService.handleThreadException() Exception: %s", e);

        doStop(VPN.Status.NOT_CONNECTED);
    }

    private void doStopService(VPN.Status status)
    {
        cleanupTunnel();

        doStopForeground();

        onServiceStopped();

        doChangeStatus(status);

        stopSelf();
    }

    private void pauseService()
    {
        try
        {
            if(vpnTunnel != null)
            {
                vpnTunnel.networkStatusChanged(OpenVPNTunnel.VPNAction.USER_PAUSE);

                doChangeStatus(VPN.Status.PAUSED_BY_USER);
            }
        }
        catch(Exception e)
        {
            EddieLogger.error("VPNService.pauseService() Exception: %s", e);
        }
    }

    private void resumeService()
    {
        try
        {
            if(vpnTunnel != null)
            {
                vpnTunnel.networkStatusChanged(OpenVPNTunnel.VPNAction.USER_RESUME);

                doChangeStatus(VPN.Status.CONNECTED);
            }
        }
        catch(Exception e)
        {
            EddieLogger.error("VPNService.resumeService() Exception: %s", e);
        }
    }

    private void cleanupTunnel()
    {
        try
        {
            if(vpnTunnel != null)
                vpnTunnel.cleanup();
        }
        catch(Exception e)
        {
            EddieLogger.error("VPNService.cleanupTunnel() exception: %s", e.getMessage());
        }
        finally
        {
            vpnTunnel = null;
        }
    }

    public synchronized void doChangeStatus(VPN.Status status)
    {
        if(vpnStatus == status)
            return;

        VPN.setConnectionStatus(status);

        vpnStatus = status;

        if(clientMessenger != null)
            sendMessage(clientMessenger, createStatusMessage(status, getResources().getString(VPN.descriptionResource(status))));
        else
        {
            EddieEvent eddieEvent = new EddieEvent();

            eddieEvent.onVpnStatusChanged(status, getResources().getString(VPN.descriptionResource(status)));
        }

        switch(status)
        {
            case CONNECTED:
            {
                startVPNStats();
            }
            break;

            case PAUSED_BY_USER:
            case PAUSED_BY_SYSTEM:
            case LOCKED:
            case NOT_CONNECTED:
            case CONNECTION_REVOKED_BY_SYSTEM:
            case CONNECTION_ERROR:
            {
                stopVPNStats();
            }
            break;

            default:
            {
            }
            break;
        }
    }

    private Message createStatusMessage(VPN.Status status, String messageText)
    {
        Message message = Message.obtain(null, VPNService.MSG_STATUS, VPN.statusToInt(status), 0);

        message.getData().putString(MESSAGE_TEXT, messageText);

        return message;
    }

    private void sendMessage(Messenger client, Message message)
    {
        try
        {
            client.send(message);
        }
        catch(RemoteException e)
        {
            EddieLogger.error("VPNService.sendMessage(): error in sending message. Exception: %s", e);
        }
    }

    private void doStart(Bundle data)
    {
        if(EddieApplication.isInitialized())
        {
            try
            {
                tunnelSetup(data);
            }
            catch(Exception e)
            {
                EddieLogger.error("VPNService.doStart() exception: %s", e.getMessage());

                doStopService(VPN.Status.NOT_CONNECTED);

                return;
            }

            Thread newVpnTask = SupportTools.startThread(new Runnable()
            {
                @Override
                public void run()
                {
                    EddieLogger.info("Starting VPN thread");

                    vpnTunnel.run();
                }
            });

            if(newVpnTask != null)
                vpnThread = newVpnTask;
        }
        else
        {
            EddieLogger.error("VPNService.doStart() initialization failed");

            doStopService(VPN.Status.NOT_CONNECTED);
        }
    }

    private void doStop(final VPN.Status status)
    {
        doChangeStatus(VPN.Status.DISCONNECTING);

        SupportTools.startThread(new Runnable()
        {
            @Override
            public void run()
            {
                doStopService(targetVpnDisconnectionStatus);

                currentNotificationText = "";

                waitForVpnThreadToFinish();
            }
        });
    }

    public PendingIntent createConfigIntent()
    {
        Intent configIntent = new Intent(this, SettingsActivity.class);

        configIntent.addFlags( Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);

        return PendingIntent.getActivity(this, 0, configIntent, 0);
    }

    private void tunnelSetup(Bundle data) throws Exception
    {
        if(vpnTunnel != null)
            throw new Exception("VPNService.tunnelSetup(): vpnTunnel already initialized");

        if(data == null)
            throw new Exception("VPNService.tunnelSetup(): data bundle is null)");

        vpnTunnel = new OpenVPNTunnel(this);

        try
        {
            vpnTunnel.init();
        }
        catch(Exception e)
        {
            throw e;
        }

        String profile = data.getString(PARAM_PROFILE, "");

        if(profile.length() == 0)
            throw new Exception("VPNService.tunnelSetup(): profile is empty");

        vpnTunnel.loadProfileString(profile);

        vpnTunnel.bindOptions();
    }

    private void waitForVpnThreadToFinish()
    {
        if(vpnThread == null)
            return;

        try
        {
            vpnThread.join(THREAD_MAX_JOIN_TIME);
        }
        catch(InterruptedException e)
        {
            EddieLogger.error("VPNService.waitVpnThreadToFinish(): VPN thread has been interrupted");
        }
        finally
        {
            if(vpnThread.isAlive())
                EddieLogger.error("VPNService.waitVpnThreadToFinish(): VPN thread did not end");
            else
                EddieLogger.info("VPN thread execution has completed");
        }
    }

    private void doStartForeground()
    {
        if(settingsManager.isSystemPersistentNotification() && (notification == null))
        {
            String text, server = "";
            HashMap<String, String> profileInfo = VPN.getProfileInfo();

            String channelId = getResources().getString(R.string.notification_channel_id);
            String channelName = getResources().getString(R.string.notification_channel_name);

            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
            {
                NotificationManager notificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);

                NotificationChannel notificationChannel = new NotificationChannel(channelId, channelName, NotificationManager.IMPORTANCE_HIGH);

                notificationManager.createNotificationChannel(notificationChannel);
            }

            if(profileInfo != null)
            {
                if(VPN.getConnectionMode() == VPN.ConnectionMode.AIRVPN_SERVER || VPN.getConnectionMode() == VPN.ConnectionMode.QUICK_CONNECT)
                    server = String.format("AirVPN %s (%s)", profileInfo.get("description"), profileInfo.get("server"));
                else
                {
                    if(!profileInfo.get("description").isEmpty())
                        server = profileInfo.get("description");
                    else
                        server = profileInfo.get("server");
                }
            }

            text = String.format(Locale.getDefault(), getResources().getString(R.string.notification_text), server);

            if(!NetworkStatusReceiver.getNetworkDescription().equals(""))
                text += " " + String.format(Locale.getDefault(), getResources().getString(R.string.notification_network), NetworkStatusReceiver.getNetworkDescription());

            notification = new NotificationCompat.Builder(this, channelId);

            if(notification != null)
            {
                notification.setContentTitle(getResources().getString(R.string.notification_title))
                        .setStyle(new NotificationCompat.BigTextStyle().bigText(text))
                        .setContentText(text)
                        .setSmallIcon(R.drawable.notification_icon)
                        .setColor(getResources().getColor(R.color.notificationColor))
                        .setContentIntent(buildMainActivityIntent())
                        .setChannelId(channelId)
                        .setPriority(NotificationCompat.PRIORITY_HIGH)
                        .setOngoing(true);

                try
                {
                    if(Build.VERSION.SDK_INT < Build.VERSION_CODES.O)
                    {
                        if(settingsManager.isSystemNotificationSound())
                            notification.setSound(Settings.System.DEFAULT_NOTIFICATION_URI);
                        else
                            notification.setSound(Uri.parse("android.resource://" + getApplicationContext().getPackageName() + "/" + R.raw.silence));
                    }

                    startForeground(SERVICE_RUNNING_NOTIFICATION_ID, notification.build());

                    currentNotificationText = text;
                }
                catch(Exception e)
                {
                }
            }
        }
    }

    public void updateNotification(String text)
    {
        if(settingsManager.isSystemPersistentNotification() && notification != null && !text.equals("") && !text.equals(currentNotificationText))
        {
            String channelId = getResources().getString(R.string.notification_channel_id);
            String channelName = getResources().getString(R.string.notification_channel_name);

            NotificationManager notificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);

            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
            {
                NotificationChannel notificationChannel = new NotificationChannel(channelId, channelName, NotificationManager.IMPORTANCE_HIGH);

                notificationManager.createNotificationChannel(notificationChannel);
            }

            notification = new NotificationCompat.Builder(this, channelId);

            if(notification != null)
            {
                notification.setContentTitle(getResources().getString(R.string.notification_title))
                        .setStyle(new NotificationCompat.BigTextStyle().bigText(text))
                        .setContentText(text)
                        .setSmallIcon(R.drawable.notification_icon)
                        .setColor(getResources().getColor(R.color.notificationColor))
                        .setContentIntent(buildMainActivityIntent())
                        .setChannelId(channelId)
                        .setPriority(NotificationCompat.PRIORITY_HIGH)
                        .setOngoing(true);

                try
                {
                    if(Build.VERSION.SDK_INT < Build.VERSION_CODES.O)
                    {
                        if(settingsManager.isSystemNotificationSound())
                            notification.setSound(Settings.System.DEFAULT_NOTIFICATION_URI);
                        else
                            notification.setSound(Uri.parse("android.resource://" + getApplicationContext().getPackageName() + "/" + R.raw.silence));
                    }

                    notificationManager.notify(SERVICE_RUNNING_NOTIFICATION_ID, notification.build());

                    currentNotificationText = text;
                }
                catch(Exception e)
                {
                }
            }
        }
    }

    public void alertNotification(String message)
    {
        if(message.equals("") && !message.equals(currentNotificationText))
            return;

        supportTools.dismissConnectionProgressDialog();
        supportTools.dismissProgressDialog();

        String channelId = getResources().getString(R.string.notification_channel_id);
        String channelName = getResources().getString(R.string.notification_channel_name);

        NotificationManager notificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
        {
            NotificationChannel notificationChannel = new NotificationChannel(channelId, channelName, NotificationManager.IMPORTANCE_HIGH);

            notificationManager.createNotificationChannel(notificationChannel);
        }

        NotificationCompat.Builder alertNotification = new NotificationCompat.Builder(this, channelId);

        if(alertNotification != null)
        {
            alertNotification.setContentTitle(getResources().getString(R.string.notification_title))
                    .setSmallIcon(R.drawable.notification_icon)
                    .setColor(getResources().getColor(R.color.notificationColor))
                    .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                    .setContentText(message)
                    .setContentIntent(buildMainActivityIntent())
                    .setChannelId(channelId)
                    .setPriority(NotificationCompat.PRIORITY_HIGH)
                    .setAutoCancel(true);

            if(Build.VERSION.SDK_INT < Build.VERSION_CODES.O && notification != null)
            {
                if(settingsManager.isSystemNotificationSound())
                    notification.setSound(Settings.System.DEFAULT_NOTIFICATION_URI);
                else
                    notification.setSound(Uri.parse("android.resource://" + getApplicationContext().getPackageName() + "/" + R.raw.silence));
            }

            try
            {
                notificationManager.notify(alertNotificationId, alertNotification.build());
            }
            catch (NullPointerException e)
            {
            }

            alertNotificationId++;

            currentNotificationText = message;
        }
    }

    private PendingIntent buildMainActivityIntent()
    {
        Intent intent = new Intent(this, MainActivity.class);

        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);

        return PendingIntent.getActivity(this, 0, intent, 0);
    }

    private void doStopForeground()
    {
        if(notification != null)
        {
            stopForeground(true);

            notification = null;
        }
    }

    private void startVPNStats()
    {
        int updateInterval = STATS_UPDATE_INTERVAL_SECONDS * 1000;

        statVpnPrevBytesIn = 0;
        statVpnPrevBytesOut = 0;

        if(vpnStatTimer != null)
            vpnStatTimer.cancel();

        vpnStatTimer = new Timer();

        vpnStatTimer.schedule(new TimerTask()
        {
            @Override
            public void run()
            {
                vpnStats = EddieLibrary.getOpenVPNClientTransportStats();

                if(vpnStats.resultCode == EddieLibrary.SUCCESS)
                {
                    vpnInDiff = vpnStats.bytesIn - statVpnPrevBytesIn;
                    vpnOutDiff = vpnStats.bytesOut - statVpnPrevBytesOut;

                    if(statVpnPrevBytesIn > 0)
                    {
                        vpnInRate = (vpnInDiff * 8) / STATS_UPDATE_INTERVAL_SECONDS;

                        VPN.setInRate(vpnInRate);

                        if(vpnInRate > VPN.getMaxInRate())
                            VPN.setMaxInRate(vpnInRate);
                    }

                    if(statVpnPrevBytesOut > 0)
                    {
                        vpnOutRate = (vpnOutDiff * 8) / STATS_UPDATE_INTERVAL_SECONDS;

                        VPN.setOutRate(vpnOutRate);

                        if(vpnOutRate > VPN.getMaxOutRate())
                            VPN.setMaxOutRate(vpnOutRate);
                    }

                    VPN.addSecondsConnectionTime(STATS_UPDATE_INTERVAL_SECONDS);

                    VPN.setVpnTransportStats(vpnStats);

                    statVpnPrevBytesIn = vpnStats.bytesIn;
                    statVpnPrevBytesOut = vpnStats.bytesOut;
                }
                else
                    EddieLogger.warning("VPNService: getOpenVPNClientTransportStats() error: %s", vpnStats.resultDescription);
            }
        }, updateInterval, updateInterval);
    }

    public void stopVPNStats()
    {
        if(vpnStatTimer != null)
            vpnStatTimer.cancel();

        vpnStatTimer = null;

        VPN.resetSessionTime();
        VPN.setInRate(0);
        VPN.setOutRate(0);
    }

    // NetworkStatusReceiver

    public void onNetworkStatusNotAvailable()
    {
        EddieLogger.info("Network is not available");

        networkStatusChanged(OpenVPNTunnel.VPNAction.SYSTEM_PAUSE);
    }

    public void onNetworkStatusConnected()
    {
        String text, server = "";
        HashMap<String, String> profileInfo = VPN.getProfileInfo();

        EddieLogger.info("Network is connected to %s", NetworkStatusReceiver.getNetworkDescription());

        networkStatusChanged(OpenVPNTunnel.VPNAction.SYSTEM_RESUME);

        if(profileInfo != null)
        {
            if(VPN.getConnectionMode() == VPN.ConnectionMode.AIRVPN_SERVER || VPN.getConnectionMode() == VPN.ConnectionMode.QUICK_CONNECT)
                server = String.format("AirVPN %s (%s)", profileInfo.get("description"), profileInfo.get("server"));
            else
            {
                if(!profileInfo.get("description").isEmpty())
                    server = profileInfo.get("description");
                else
                    server = profileInfo.get("server");
            }
        }

        text = String.format(Locale.getDefault(), getResources().getString(R.string.notification_text), server);

        if(!NetworkStatusReceiver.getNetworkDescription().equals(""))
            text += " " + String.format(Locale.getDefault(), getResources().getString(R.string.notification_network), NetworkStatusReceiver.getNetworkDescription());

        updateNotification(text);
    }

    public void onNetworkStatusIsConnecting()
    {
        EddieLogger.info("Network is connecting");

        networkStatusChanged(OpenVPNTunnel.VPNAction.SYSTEM_PAUSE);
    }

    public void onNetworkStatusIsDisonnecting()
    {
        EddieLogger.info("Network is disconnecting");

        networkStatusChanged(OpenVPNTunnel.VPNAction.SYSTEM_PAUSE);
    }

    public void onNetworkStatusSuspended()
    {
        EddieLogger.info("Network is suspended");

        networkStatusChanged(OpenVPNTunnel.VPNAction.SYSTEM_PAUSE);
    }

    public void onNetworkStatusNotConnected()
    {
        EddieLogger.info("Network is not connected");

        networkStatusChanged(OpenVPNTunnel.VPNAction.SYSTEM_PAUSE);
    }

    public void onNetworkTypeChanged()
    {
        EddieLogger.info("Network type has changed to %s", NetworkStatusReceiver.getNetworkDescription());

        networkStatusChanged(OpenVPNTunnel.VPNAction.NETWORK_TYPE_CHANGED);
    }
}
