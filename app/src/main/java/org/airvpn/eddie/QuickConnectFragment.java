// <eddie_source_header>
// This file is part of Eddie/AirVPN software.
// Copyright (C)2014-2018 AirVPN (support@airvpn.org) / https://airvpn.org
//
// Eddie is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Eddie is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Eddie. If not, see <http://www.gnu.org/licenses/>.
// </eddie_source_header>
//
// 3 October 2018 - author: ProMIND - initial release. (a tribute to the 1859 Perugia uprising occurred on 20 June 1859 and in memory of those brave inhabitants who fought for the liberty of Perugia)

package org.airvpn.eddie;

import android.content.res.AssetManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import org.w3c.dom.Document;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Locale;

public class QuickConnectFragment extends Fragment implements NetworkStatusListener, EddieEventListener
{
    public enum Status
    {
        IDLE,
        CONNECTION_IN_PROGRESS,
        CONNECTED
    }

    private final int FRAGMENT_ID = 5001;
    private final String AIRVPN_CONNECTION_SEQUENCE_FILE_NAME = "connection_sequence.csv";
    private final int CONNECTION_WAIT_TIME = 5000;

    private class ConnectionScheme
    {
        private String protocol;
        private int port;
        private int entry;

        void setProtocol(String p)
        {
            protocol = p;
        }

        String getProtocol()
        {
            return protocol;
        }

        void setPort(int p)
        {
            port = p;
        }

        int getPort()
        {
            return port;
        }

        void setEntry(int e)
        {
            entry = e;
        }

        int getEntry()
        {
            return entry;
        }
    }

    private SupportTools supportTools = null;
    private EddieEvent eddieEvent = null;
    private static VPNManager vpnManager = null;
    private SettingsManager settingsManager = null;
    private NetworkStatusReceiver networkStatusReceiver = null;
    private CountryContinent countryContinent = null;

    private Button btnQuickConnect = null;
    private TextView txtConnectionStatus = null, txtAirVPNSubscriptionStatus = null;
    private TextView txtVpnStatus = null, txtNetworkStatus = null, txtServerCipher = null;
    private LinearLayout llUserProfile = null, llServerCipher = null;
    private Spinner spnUserProfile = null;

    private AirVPNUser airVPNUser = null;
    private AirVPNServerProdiver airVPNServerProdiver = null;

    private ArrayList<AirVPNServer> airVPNServerList = null;

    private Status currentStatus = Status.IDLE;

    private ArrayList<ConnectionScheme> connectionSchemeList = null;

    private int currentServerIndex = 0, currentConnectionSchemeIndex = 0;
    private static String selectedUserProfile = "";

    private VPN.Status lastVpnStatus = VPN.Status.NOT_CONNECTED;
    private boolean preparingNextServerConnection = false;

    private String currentVpnStatusDescription = "", connectionStatus = "";
    private HashMap<String, String> profileInfo = null;

    public void setVpnManager(VPNManager v)
    {
        vpnManager = v;
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        supportTools = new SupportTools(getContext());
        settingsManager = new SettingsManager(getActivity());
        countryContinent = new CountryContinent(getContext());

        networkStatusReceiver = new NetworkStatusReceiver(getContext());
        networkStatusReceiver.addListener(this);

        eddieEvent = new EddieEvent();
        eddieEvent.addListener(this);

        airVPNUser = new AirVPNUser(getContext());

        airVPNServerProdiver = new AirVPNServerProdiver(getContext());

        loadConnectionSchemes();
    }

    @Override
    public void onResume()
    {
        super.onResume();

        updateConnectionStatus(VPN.getConnectionStatus());

        setConnectButton();

        if(txtAirVPNSubscriptionStatus != null)
        {
            if(airVPNUser.isUserValid())
            {
                txtAirVPNSubscriptionStatus.setText(airVPNUser.getExpirationText());

                txtAirVPNSubscriptionStatus.setVisibility(View.VISIBLE);
            }
            else
                txtAirVPNSubscriptionStatus.setVisibility(View.GONE);
        }

        if(txtNetworkStatus != null)
        {
            if(NetworkStatusReceiver.getNetworkStatus() == NetworkStatusReceiver.Status.CONNECTED)
                txtNetworkStatus.setText(String.format(Locale.getDefault(), getResources().getString(R.string.conn_status_connected), NetworkStatusReceiver.getNetworkDescription()));
            else
                txtNetworkStatus.setText(getResources().getString(R.string.conn_status_not_available));
        }

        if(txtVpnStatus != null)
            txtVpnStatus.setText(currentVpnStatusDescription);

        if(txtConnectionStatus != null)
            txtConnectionStatus.setText(connectionStatus);

        selectedUserProfile = airVPNUser.getCurrentProfile();

        if(airVPNUser.isUserValid())
            setupUserProfileSpinner(selectedUserProfile);
        else
            llUserProfile.setVisibility(View.GONE);

        if(!settingsManager.getAirVPNCipher().equals(SettingsManager.AIRVPN_CIPHER_SERVER))
        {
            llServerCipher.setVisibility(View.VISIBLE);
            txtServerCipher.setText(settingsManager.getAirVPNCipher());
        }
        else
            llServerCipher.setVisibility(View.GONE);
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();

        networkStatusReceiver.removeListener(this);

        eddieEvent.removeListener(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View fragmentView = inflater.inflate(R.layout.fragment_quick_connect_layout, container, false);

        btnQuickConnect = (Button)fragmentView.findViewById(R.id.quick_connect_button);
        txtConnectionStatus = (TextView)fragmentView.findViewById(R.id.connection_status);
        llUserProfile = (LinearLayout)fragmentView.findViewById(R.id.user_profile);
        spnUserProfile = (Spinner)fragmentView.findViewById(R.id.spn_user_profile);
        llServerCipher = (LinearLayout)fragmentView.findViewById(R.id.server_cipher);
        txtServerCipher = (TextView)fragmentView.findViewById(R.id.txt_server_cipher);

        btnQuickConnect.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View arg0)
            {
                VPN.Status vpnStatus = VPN.getConnectionStatus();

                if(vpnStatus == VPN.Status.NOT_CONNECTED || vpnStatus == VPN.Status.CONNECTION_REVOKED_BY_SYSTEM || vpnStatus == VPN.Status.UNKNOWN)
                    quickConnectToAirVPN();
                else
                    disconnectCurrentProfile();
            }
        });

        btnQuickConnect.setOnFocusChangeListener(new View.OnFocusChangeListener()
        {
            @Override
            public void onFocusChange(View v, boolean hasFocus)
            {
                 if(btnQuickConnect.isEnabled() == false)
                     return;

                if(hasFocus)
                {
                    if(VPN.getConnectionStatus() == VPN.Status.CONNECTED)
                        btnQuickConnect.setBackgroundResource(R.drawable.quick_connect_off);
                    else
                        btnQuickConnect.setBackgroundResource(R.drawable.quick_connect_on);
                }
                else
                {
                    if(VPN.getConnectionStatus() == VPN.Status.CONNECTED)
                        btnQuickConnect.setBackgroundResource(R.drawable.quick_connect_on);
                    else
                        btnQuickConnect.setBackgroundResource(R.drawable.quick_connect_off);
                }
            }
        });

        btnQuickConnect.setAccessibilityDelegate(new View.AccessibilityDelegate()
        {
            @Override
            public void sendAccessibilityEvent(View host, int eventType)
            {
                super.sendAccessibilityEvent(host, eventType);

                btnQuickConnect.setContentDescription(connectionStatus);
            }
        });

        spnUserProfile.setAccessibilityDelegate(new View.AccessibilityDelegate()
        {
            @Override
            public void sendAccessibilityEvent(View host, int eventType)
            {
                super.sendAccessibilityEvent(host, eventType);

                spnUserProfile.setContentDescription(String.format("%s %s", getString(R.string.accessibility_selected_user_profile), spnUserProfile.getSelectedItem().toString()));
            }
        });

        setupUserProfileSpinner(selectedUserProfile);

        llUserProfile.setVisibility(View.GONE);

        if(currentVpnStatusDescription.isEmpty())
            currentVpnStatusDescription = getResources().getString(VPN.descriptionResource(VPN.getConnectionStatus()));

        txtAirVPNSubscriptionStatus = (TextView)fragmentView.findViewById(R.id.airvpn_subscription_status);

        txtVpnStatus = (TextView)fragmentView.findViewById(R.id.vpn_connection_status);
        txtVpnStatus.setText(currentVpnStatusDescription);

        txtNetworkStatus = (TextView)fragmentView.findViewById(R.id.network_connection_status);

        if(NetworkStatusReceiver.getNetworkStatus() == NetworkStatusReceiver.Status.CONNECTED)
            txtNetworkStatus.setText(String.format(Locale.getDefault(), getResources().getString(R.string.conn_status_connected), NetworkStatusReceiver.getNetworkDescription()));
        else
            txtNetworkStatus.setText(getResources().getString(R.string.conn_status_not_available));

        if(!settingsManager.getAirVPNCipher().equals(SettingsManager.AIRVPN_CIPHER_SERVER))
        {
            llServerCipher.setVisibility(View.VISIBLE);
            txtServerCipher.setText(settingsManager.getAirVPNCipher());
        }
        else
            llServerCipher.setVisibility(View.GONE);

        return fragmentView;
    }

    public Status getCurrentStatus()
    {
        return currentStatus;
    }

    private void quickConnectToAirVPN()
    {
        Runnable runnable = new Runnable()
        {
            @Override
            public void run()
            {
                if(airVPNUser.checkUserLogin())
                {
                    btnQuickConnect.setBackgroundResource(R.drawable.quick_connect_on);

                    airVPNServerProdiver.setUserIP(airVPNUser.getUserIP());
                    airVPNServerProdiver.setUserCountry(airVPNUser.getUserCountry());

                    if(settingsManager.getAirVPNQuickConnectMode().equals(SettingsManager.AIRVPN_QUICK_CONNECT_MODE_AUTO))
                    {
                        airVPNServerProdiver.setTlsMode(AirVPNServerProdiver.TLSMode.TLS_CRYPT);

                        airVPNServerProdiver.setSupportIPv4(true);
                        airVPNServerProdiver.setSupportIPv6(false);
                    }
                    else
                    {
                        if(settingsManager.getAirVPNDefaultTLSMode().equals(SettingsManager.AIRVPN_TLS_MODE_CRYPT))
                            airVPNServerProdiver.setTlsMode(AirVPNServerProdiver.TLSMode.TLS_CRYPT);
                        else
                            airVPNServerProdiver.setTlsMode(AirVPNServerProdiver.TLSMode.TLS_AUTH);

                        switch(settingsManager.getAirVPNDefaultIPVersion())
                        {
                            case SettingsManager.AIRVPN_IP_VERSION_4:
                            {
                                airVPNServerProdiver.setSupportIPv4(true);
                                airVPNServerProdiver.setSupportIPv6(false);
                            }
                            break;

                            case SettingsManager.AIRVPN_IP_VERSION_6:
                            case SettingsManager.AIRVPN_IP_VERSION_4_OVER_6:
                            {
                                airVPNServerProdiver.setSupportIPv4(true);
                                airVPNServerProdiver.setSupportIPv6(true);
                            }
                            break;

                            default:
                            {
                                airVPNServerProdiver.setSupportIPv4(true);
                                airVPNServerProdiver.setSupportIPv6(false);
                            }
                            break;
                        }
                    }

                    airVPNServerList = airVPNServerProdiver.getFilteredServerList();

                    currentServerIndex = 0;
                    currentConnectionSchemeIndex = 0;

                    currentStatus = Status.CONNECTION_IN_PROGRESS;

                    preparingNextServerConnection = true;

                    connectToNextServer();
                }
            }
        };

        supportTools.runOnUiActivity(getActivity(), runnable);
    }

    private void disconnectCurrentProfile()
    {
        VPN.Status vpnStatus = VPN.getConnectionStatus();

        if(vpnStatus != VPN.Status.CONNECTED && vpnStatus != VPN.Status.PAUSED_BY_USER && vpnStatus != VPN.Status.PAUSED_BY_SYSTEM && vpnStatus != VPN.Status.LOCKED)
            return;

        final Runnable runnable = new Runnable()
        {
            @Override
            public void run()
            {
                if(!supportTools.confirmationDialog(R.string.conn_confirm_disconnection))
                    return;

                try
                {
                    vpnManager.stop();
                }
                catch(Exception e)
                {
                    connectionStatus = e.getMessage();

                    txtConnectionStatus.setText(connectionStatus);

                    EddieLogger.error(connectionStatus);
                }
            }
        };

        supportTools.runOnUiActivity(getActivity(), runnable);
    }

    private void connectToNextServer()
    {
        AirVPNServer server = null;
        HashMap<Integer, String> serverEntryIP = null;
        ConnectionScheme connectionScheme = null;
        String protocol = "", tlsMode = "", cipher = "";
        int port = 0;
        int entry = 0;
        boolean connectIPv6 = false;
        String openVpnProfile = "", profileName = "";

        if(currentStatus != Status.CONNECTION_IN_PROGRESS)
            return;

        if(airVPNServerList == null)
        {
            EddieLogger.error("QuickConnectFragment.connectToNextServer(): airVPNServerList is null");

            final Runnable uiRunnable = new Runnable()
            {
                @Override
                public void run()
                {
                    supportTools.infoDialog(R.string.quick_connect_end_of_server_list, true);

                    currentStatus = Status.IDLE;

                    setConnectButton();
                }
            };

            supportTools.runOnUiActivity(getActivity(), uiRunnable);

            return;
        }

        VPN.setConnectionStatus(VPN.Status.CONNECTING);

        if(currentServerIndex > airVPNServerList.size() - 1)
        {
            EddieLogger.error("QuickConnectFragment.startConnection(): End of server list reached. No server available for connection.");

            connectionStatus = getResources().getString(R.string.quick_connect_end_of_server_list);

            final Runnable uiRunnable = new Runnable()
            {
                @Override
                public void run()
                {
                    supportTools.dismissConnectionProgressDialog();

                    if(txtConnectionStatus != null)
                        txtConnectionStatus.setText(connectionStatus);

                    supportTools.infoDialog(connectionStatus, true);

                    currentStatus = Status.IDLE;

                    setConnectButton();
                }
            };

            supportTools.runOnUiActivity(getActivity(), uiRunnable);

            vpnManager.stop();

            return;
        }

        if(spnUserProfile != null && spnUserProfile.getCount() > 0)
        {
            profileName = spnUserProfile.getSelectedItem().toString();
        }
        else
        {
            ArrayList<String> keyNames = airVPNUser.getUserKeyNames();

            if(keyNames != null && keyNames.size() > 0)
                profileName = keyNames.get(0);
            else
            {
                connectionStatus = getResources().getString(R.string.quick_connect_user_has_no_profiles);

                txtConnectionStatus.setText(connectionStatus);

                EddieLogger.error("QuickConnectFragment.connectToNextServer(): No user profile defined. Cannot connect to AirVPN.");

                currentStatus = Status.IDLE;

                return;
            }
        }

        server = airVPNServerList.get(currentServerIndex);

        if(airVPNServerProdiver.getTlsMode().equals(AirVPNServerProdiver.TLSMode.TLS_AUTH))
        {
            tlsMode = SettingsManager.AIRVPN_TLS_MODE_AUTH;

            entry = 0;
        }
        else
        {
            tlsMode = SettingsManager.AIRVPN_TLS_MODE_CRYPT;

            entry = 2;
        }

        if(settingsManager.getAirVPNQuickConnectMode().equals(SettingsManager.AIRVPN_QUICK_CONNECT_MODE_AUTO))
        {
            if(connectionSchemeList == null)
            {
                EddieLogger.error("QuickConnectFragment.startConnection(): connectionSchemeList is null");

                return;
            }

            connectionScheme = connectionSchemeList.get(currentConnectionSchemeIndex);

            protocol = connectionScheme.getProtocol();

            port = connectionScheme.getPort();

            entry += connectionScheme.getEntry();

            serverEntryIP = server.getEntryIPv4();

            connectIPv6 = false;

            currentConnectionSchemeIndex++;

            if(currentConnectionSchemeIndex > connectionSchemeList.size() - 1)
            {
                currentConnectionSchemeIndex = 0;

                currentServerIndex++;
            }
        }
        else
        {
            protocol = settingsManager.getAirVPNDefaultProtocol();

            port = settingsManager.getAirVPNDefaultPort();

            switch(settingsManager.getAirVPNDefaultIPVersion())
            {
                case SettingsManager.AIRVPN_IP_VERSION_4:
                {
                    serverEntryIP = server.getEntryIPv4();

                    connectIPv6 = false;
                }
                break;

                case SettingsManager.AIRVPN_IP_VERSION_6:
                {
                    serverEntryIP = server.getEntryIPv6();

                    protocol += "6";

                    connectIPv6 = true;
                }
                break;

                case SettingsManager.AIRVPN_IP_VERSION_4_OVER_6:
                {
                    serverEntryIP = server.getEntryIPv4();

                    connectIPv6 = true;
                }
                break;

                default:
                {
                    serverEntryIP = server.getEntryIPv4();

                    connectIPv6 = false;
                }
                break;
            }

            currentServerIndex++;
        }

        if(settingsManager.isAirVPNServerWhitelisted(server.getName()))
        {
            EddieLogger.info(String.format(Locale.getDefault(), "Trying to quick connect favorite AirVPN server %s at %s, %s - Protocol %s, Port %d", server.getName(), server.getLocation(), countryContinent.getCountryName(server.getCountryCode()), protocol, port));

            connectionStatus = String.format(getResources().getString(R.string.quick_connect_try_favorite_connection), server.getName(), server.getLocation(), countryContinent.getCountryName(server.getCountryCode()), protocol, port);
        }
        else
        {
            EddieLogger.info(String.format(Locale.getDefault(), "Trying to quick connect AirVPN server %s at %s, %s - Protocol %s, Port %d", server.getName(), server.getLocation(), countryContinent.getCountryName(server.getCountryCode()), protocol, port));

            connectionStatus = String.format(getResources().getString(R.string.quick_connect_try_connection), server.getName(), server.getLocation(), countryContinent.getCountryName(server.getCountryCode()), protocol, port);
        }

        try
        {
            openVpnProfile = airVPNUser.getOpenVPNProfile(profileName, serverEntryIP.get(entry), port, protocol, tlsMode, settingsManager.getAirVPNCipher(), connectIPv6, false, "");
        }
        catch(Exception e)
        {
            openVpnProfile = "";
        }

        if(openVpnProfile.isEmpty())
        {
            EddieLogger.error("QuickConnectFragment.connectToNextServer(): openVpnProfile is empty");

            final Runnable uiRunnable = new Runnable()
            {
                @Override
                public void run()
                {
                    supportTools.infoDialog(R.string.quick_connect_empty_profile, true);

                    currentStatus = Status.IDLE;

                    setConnectButton();
                }
            };

            supportTools.runOnUiActivity(getActivity(), uiRunnable);

            return;
        }

        final Runnable runnable = new Runnable()
        {
            @Override
            public void run()
            {
                supportTools.setConnectionProgressDialogMessage(connectionStatus);

                if(txtConnectionStatus != null)
                    txtConnectionStatus.setText(connectionStatus);
            }
        };

        supportTools.runOnUiActivity(getActivity(), runnable);

        profileInfo = new HashMap<String, String>();

        profileInfo.put("mode", String.format(Locale.getDefault(), "%d", VPN.connectionModeToInt(VPN.ConnectionMode.QUICK_CONNECT)));
        profileInfo.put("name", "quick_connect");
        profileInfo.put("profile", "quick_connect");
        profileInfo.put("status", "ok");
        profileInfo.put("description", String.format("%s - %s, %s", server.getName(), server.getLocation(), countryContinent.getCountryName(server.getCountryCode())));
        profileInfo.put("server", serverEntryIP.get(entry));
        profileInfo.put("port", String.format(Locale.getDefault(), "%d", port));
        profileInfo.put("protocol", protocol);

        VPN.setProfileInfo(profileInfo);

        VPN.setConnectionMode(VPN.ConnectionMode.QUICK_CONNECT);

        VPN.setConnectionModeDescription(getResources().getString(R.string.conn_type_quick_connect));

        VPN.setUserProfileDescription(profileName);

        VPN.setUserName(airVPNUser.getUserName());

        startConnection(openVpnProfile);

        preparingNextServerConnection = false;
    }

    private void startConnection(String openVPNProfile)
    {
        if(vpnManager == null)
        {
            EddieLogger.error("QuickConnectFragment.startConnection(): vpnManager is null");

            return;
        }

        if(openVPNProfile.isEmpty())
        {
            connectionStatus = getResources().getString(R.string.quick_connect_empty_profile);

            txtConnectionStatus.setText(connectionStatus);

            return;
        }

        vpnManager.clearProfile();

        vpnManager.setProfile(openVPNProfile);

        String profileString = settingsManager.getOvpn3CustomDirectives().trim();

        if(profileString.length() > 0)
            vpnManager.addProfileString(profileString);

        vpnManager.start();
    }

    private void tryNextServerConnection()
    {
        if(currentStatus != Status.CONNECTION_IN_PROGRESS)
            return;

        preparingNextServerConnection = true;

        Runnable runnable = new Runnable()
        {
            @Override
            public void run()
            {
                connectionStatus = getResources().getString(R.string.quick_connect_failed_attempt);

                txtConnectionStatus.setText(connectionStatus);

                supportTools.setConnectionProgressDialogMessage(connectionStatus);
            }
        };

        supportTools.runOnUiActivity(getActivity(), runnable);

        EddieLogger.info("Failed to connect server. Terminating connection for current AirVPN server attempt");

        Runnable connectRunnable = new Runnable()
        {
            @Override
            public void run()
            {
                try
                {
                    vpnManager.stop();

                    Thread.sleep(CONNECTION_WAIT_TIME);
                }
                catch (Exception e)
                {
                }

                if(currentStatus == Status.CONNECTION_IN_PROGRESS)
                    connectToNextServer();
            }
        };

        supportTools.startThread(connectRunnable);
    }

    private void updateConnectionStatus(VPN.Status vpnStatus)
    {
        if(!isAdded())
            return;

        if(txtConnectionStatus != null)
        {
            switch(vpnStatus)
            {
                case UNKNOWN:
                case NOT_CONNECTED:
                case CONNECTION_REVOKED_BY_SYSTEM:
                {
                    if(airVPNUser.masterPassword().isEmpty() || !airVPNUser.isUserValid())
                    {
                        connectionStatus = getResources().getString(R.string.quick_connect_login);

                        if(llUserProfile != null)
                            llUserProfile.setVisibility(View.GONE);
                    }
                    else
                    {
                        if(currentStatus != Status.CONNECTION_IN_PROGRESS)
                            connectionStatus = String.format(getResources().getString(R.string.quick_connect_start_connection), airVPNUser.getUserName());

                        if(llUserProfile != null)
                            llUserProfile.setVisibility(View.VISIBLE);
                    }

                    if(currentStatus != Status.CONNECTION_IN_PROGRESS)
                        currentStatus = Status.IDLE;
                }
                break;

                case CONNECTED:
                {
                    HashMap<String, String> currentProfile = VPN.getProfileInfo();

                    if(currentProfile != null)
                    {
                        if(!currentProfile.get("description").isEmpty())
                            connectionStatus = String.format("AirVPN %s (%s)", currentProfile.get("description"), currentProfile.get("server"));
                        else
                        {
                            connectionStatus = currentProfile.get("server");

                            if(VPN.getConnectionMode() == VPN.ConnectionMode.OPENVPN_PROFILE)
                                connectionStatus += " (" + getResources().getString(R.string.conn_type_openvpn_profile) + ")";
                        }
                    }
                    else
                        connectionStatus = getResources().getString(R.string.vpn_status_connected);

                    connectionStatus = String.format(Locale.getDefault(), getResources().getString(R.string.notification_text), connectionStatus);

                    connectionStatus += " " + getResources().getString(R.string.quick_connect_tap_button_to_disconnect);

                    currentStatus = Status.CONNECTED;
                }
            }

            lastVpnStatus = vpnStatus;
        }

        if((airVPNUser.masterPassword().isEmpty() || !airVPNUser.isUserValid()) && (vpnStatus == VPN.Status.NOT_CONNECTED || vpnStatus == VPN.Status.CONNECTION_REVOKED_BY_SYSTEM))
        {
            connectionStatus = getResources().getString(R.string.quick_connect_login);

            if(llUserProfile != null)
                llUserProfile.setVisibility(View.GONE);
        }

        if(!connectionStatus.isEmpty())
            txtConnectionStatus.setText(connectionStatus);
    }

    public void updateVpnStatus(String vpnStatus)
    {
        if(txtVpnStatus != null)
            txtVpnStatus.setText(vpnStatus);

        updateConnectionStatus(VPN.getConnectionStatus());

        currentVpnStatusDescription = vpnStatus;
    }

    public void setConnectButton()
    {
        boolean enabled = false;

        if(btnQuickConnect == null)
            return;

        switch(VPN.getConnectionStatus())
        {
            case CONNECTING:
            case CONNECTED:
            case PAUSED_BY_USER:
            case PAUSED_BY_SYSTEM:
            case LOCKED:
            {
                enabled = true;

                if(btnQuickConnect != null)
                    btnQuickConnect.setBackgroundResource(R.drawable.quick_connect_on);

                if(spnUserProfile != null)
                    spnUserProfile.setEnabled(false);
            }
            break;

            case DISCONNECTING:
            case NOT_CONNECTED:
            case CONNECTION_REVOKED_BY_SYSTEM:
            case UNKNOWN:
            {
                enabled = true;

                if(btnQuickConnect != null)
                    btnQuickConnect.setBackgroundResource(R.drawable.quick_connect_off);

                if(spnUserProfile != null)
                    spnUserProfile.setEnabled(true);
            }
            break;

            default:
            {
                enabled = true;
            }
            break;
        }

        if(!NetworkStatusReceiver.isNetworkConnected())
        {
            enabled = false;

            if(spnUserProfile != null)
                spnUserProfile.setEnabled(false);
        }

        supportTools.enableButton(btnQuickConnect, enabled);
    }

    private void setupUserProfileSpinner(String selectedItem)
    {
        if(llUserProfile == null || spnUserProfile == null)
            return;

        ArrayList<String> keyNames = airVPNUser.getUserKeyNames();
        int selectedPosition = 0;

        Collections.sort(keyNames);

        if(keyNames != null && keyNames.size() > 0 && llUserProfile != null && spnUserProfile != null)
        {
            ArrayList<String> items = new ArrayList<String>();

            for(String profile : keyNames)
                items.add(profile);

            ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_dropdown_item, items);

            if(spnUserProfile != null)
            {
                spnUserProfile.setAdapter(adapter);

                for(int i = 0; i < items.size(); i++)
                {
                    if(spnUserProfile.getItemAtPosition(i).toString().equals(selectedItem))
                        selectedPosition = i;
                }

                spnUserProfile.setSelection(selectedPosition);

                spnUserProfile.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
                {
                    @Override
                    public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id)
                    {
                        selectedUserProfile = spnUserProfile.getItemAtPosition(position).toString();

                        airVPNUser.setCurrentProfile(selectedUserProfile);
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parentView)
                    {
                    }
                });
            }

            if(llUserProfile != null)
                llUserProfile.setVisibility(View.VISIBLE);
        }
        else
        {
            if(llUserProfile != null)
                llUserProfile.setVisibility(View.GONE);
        }
    }

    void loadConnectionSchemes()
    {
        AssetManager assetManager = getContext().getAssets();
        InputStream inputStream = null;
        ConnectionScheme connectionScheme = null;
        int intVal = 0;

        try
        {
            inputStream = assetManager.open(AIRVPN_CONNECTION_SEQUENCE_FILE_NAME);

            if(inputStream != null)
            {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

                String line = "";
                String row[] = null;

                connectionSchemeList = new ArrayList<ConnectionScheme>();

                while((line = bufferedReader.readLine()) != null)
                {
                    connectionScheme = new ConnectionScheme();

                    row = line.split(",");

                    if(row != null && row.length == 3)
                    {
                        connectionScheme.setProtocol(row[0]);

                        try
                        {
                            intVal = Integer.parseInt(row[1]);
                        }
                        catch(NumberFormatException e)
                        {
                            intVal = 0;
                        }

                        connectionScheme.setPort(intVal);

                        try
                        {
                            intVal = Integer.parseInt(row[2]);
                        }
                        catch(NumberFormatException e)
                        {
                            intVal = 0;
                        }

                        connectionScheme.setEntry(intVal);

                        connectionSchemeList.add(connectionScheme);
                    }
                }
            }
        }
        catch(Exception e)
        {
            EddieLogger.warning("QuickConnectFragment.loadConnectionSchemes(): %s not found.", AIRVPN_CONNECTION_SEQUENCE_FILE_NAME);
        }
        finally
        {
            try
            {
                inputStream.close();
            }
            catch (Exception e)
            {
            }
        }
    }

    // Eddie events

    public void onVpnConnectionDataChanged(final OpenVPNConnectionData connectionData)
    {
    }

    public void onVpnStatusChanged(final VPN.Status vpnStatus, final String message)
    {
        if((vpnStatus == VPN.Status.NOT_CONNECTED || vpnStatus == VPN.Status.CONNECTION_REVOKED_BY_SYSTEM) && currentStatus != Status.CONNECTION_IN_PROGRESS)
            currentStatus = Status.IDLE;

        Runnable runnable = new Runnable()
        {
            @Override
            public void run()
            {
                updateVpnStatus(message);

                setConnectButton();
            }
        };

        supportTools.runOnUiActivity(getActivity(), runnable);
    }

    public void onVpnAuthFailed(final OpenVPNEvent oe)
    {
        synchronized(this)
        {
            if(preparingNextServerConnection == false && currentStatus == Status.CONNECTION_IN_PROGRESS)
            {
                preparingNextServerConnection = true;

                tryNextServerConnection();
            }
        }
    }

    public void onVpnError(final OpenVPNEvent oe)
    {
        synchronized(this)
        {
            if(preparingNextServerConnection == false && currentStatus == Status.CONNECTION_IN_PROGRESS)
            {
                preparingNextServerConnection = true;

                tryNextServerConnection();
            }
        }
    }

    public void onMasterPasswordChanged()
    {
    }

    public void onAirVPNLogin()
    {
        Runnable runnable = new Runnable()
        {
            @Override
            public void run()
            {
                connectionStatus = String.format(getResources().getString(R.string.quick_connect_start_connection), airVPNUser.getUserName());

                if(txtConnectionStatus != null)
                    txtConnectionStatus.setText(connectionStatus);

                if(airVPNUser != null)
                {
                    selectedUserProfile = airVPNUser.getCurrentProfile();

                    setupUserProfileSpinner(selectedUserProfile);
                }

                if(txtAirVPNSubscriptionStatus != null)
                {
                    txtAirVPNSubscriptionStatus.setVisibility(View.VISIBLE);

                    txtAirVPNSubscriptionStatus.setText(airVPNUser.getExpirationText());
                }

                currentStatus = Status.IDLE;
            }
        };

        supportTools.runOnUiActivity(getActivity(), runnable);
    }

    public void onAirVPNLoginFailed()
    {
        Runnable runnable = new Runnable()
        {
            @Override
            public void run()
            {
                connectionStatus = getResources().getString(R.string.quick_connect_login_error);

                txtConnectionStatus.setText(connectionStatus);

                txtAirVPNSubscriptionStatus.setVisibility(View.GONE);

                currentStatus = Status.IDLE;
            }
        };

        supportTools.runOnUiActivity(getActivity(), runnable);
    }

    public void onAirVPNLogout()
    {
        Runnable runnable = new Runnable()
        {
            @Override
            public void run()
            {
                if(llUserProfile != null)
                    llUserProfile.setVisibility(View.GONE);

                connectionStatus = getResources().getString(R.string.quick_connect_login);

                if(txtConnectionStatus != null)
                    txtConnectionStatus.setText(connectionStatus);

                if(txtAirVPNSubscriptionStatus != null)
                    txtAirVPNSubscriptionStatus.setVisibility(View.GONE);

                currentStatus = Status.IDLE;
            }
        };

        supportTools.runOnUiActivity(getActivity(), runnable);
    }

    public void onAirVPNUserDataChanged()
    {
    }

    public void onAirVPNUserProfileReceived(Document document)
    {
    }

    public void onAirVPNUserProfileDownloadError()
    {
        supportTools.dismissProgressDialog();
    }

    public void onAirVPNUserProfileChanged()
    {
    }

    public void onAirVPNManifestReceived(Document document)
    {
    }

    public void onAirVPNManifestDownloadError()
    {
    }

    public void onAirVPNManifestChanged()
    {
    }

    public void onAirVPNIgnoredDocumentRequest()
    {
    }

    public void onCancelConnection()
    {
        currentStatus = Status.IDLE;

        preparingNextServerConnection = false;

        setConnectButton();
    }

    // NetworkStatusReceiver

    public void onNetworkStatusNotAvailable()
    {
        if(txtNetworkStatus != null)
            txtNetworkStatus.setText(getResources().getString(R.string.conn_status_not_available));

        if(btnQuickConnect != null)
            supportTools.enableButton(btnQuickConnect, false);

        if(spnUserProfile != null)
            spnUserProfile.setEnabled(false);

        if(txtConnectionStatus != null)
            txtConnectionStatus.setText(R.string.login_network_not_available);
    }

    public void onNetworkStatusConnected()
    {
        if(txtNetworkStatus != null)
            txtNetworkStatus.setText(String.format(Locale.getDefault(), getResources().getString(R.string.conn_status_connected), NetworkStatusReceiver.getNetworkDescription()));

        if(btnQuickConnect != null)
            supportTools.enableButton(btnQuickConnect, true);

        if(spnUserProfile != null)
        {
            if(VPN.getConnectionStatus() == VPN.Status.CONNECTED)
                spnUserProfile.setEnabled(false);
            else
                spnUserProfile.setEnabled(true);
        }

        if(txtConnectionStatus != null)
            txtConnectionStatus.setText(connectionStatus);
    }

    public void onNetworkStatusIsConnecting()
    {
        if(txtNetworkStatus != null)
            txtNetworkStatus.setText(getResources().getString(R.string.conn_status_not_available));
    }

    public void onNetworkStatusIsDisonnecting()
    {
        if(txtNetworkStatus != null)
            txtNetworkStatus.setText(getResources().getString(R.string.conn_status_not_available));
    }

    public void onNetworkStatusSuspended()
    {
        if(txtNetworkStatus != null)
            txtNetworkStatus.setText(getResources().getString(R.string.conn_status_not_available));
    }

    public void onNetworkStatusNotConnected()
    {
        if(txtNetworkStatus != null)
            txtNetworkStatus.setText(getResources().getString(R.string.conn_status_disconnected));
    }

    public void onNetworkTypeChanged()
    {
        if(txtNetworkStatus != null)
            txtNetworkStatus.setText(getResources().getString(R.string.conn_status_disconnected));
    }
}