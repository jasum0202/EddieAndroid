// <eddie_source_header>
// This file is part of Eddie/AirVPN software.
// Copyright (C)2014-2018 AirVPN (support@airvpn.org) / https://airvpn.org
//
// Eddie is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Eddie is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Eddie. If not, see <http://www.gnu.org/licenses/>.
// </eddie_source_header>
//
// 20 June 2018 - author: ProMIND - initial release. Based on revised code from com.eddie.android. (a tribute to the 1859 Perugia uprising occurred on 20 June 1859 and in memory of those brave inhabitants who fought for the liberty of Perugia)

package org.airvpn.eddie;

public class OpenVPNClient
{
    private static EddieLibraryResult libResult;

    private OpenVPNClient()
    {
    }

    public static OpenVPNClient create(Object callbackObject)
    {
        libResult = EddieLibrary.createOpenVPNClient(callbackObject);

        if(libResult.code == EddieLibraryResult.SUCCESS)
            return new OpenVPNClient();
        else
        {
            EddieLogger.error("OpenVPNClient.create(): Failed to create a new OpenVPN Client. %s", libResult.description);

            return null;
        }
    }

    public OpenVPNTransportStats getTransportStats()
    {
        return EddieLibrary.getOpenVPNClientTransportStats();
    }

    public EddieLibraryResult loadProfileFile(Object callbackObject, String filename)
    {
        return EddieLibrary.loadProfileToOpenVPNClient(callbackObject, filename);
    }

    public EddieLibraryResult loadProfileString(Object callbackObject, String str)
    {
        return EddieLibrary.loadStringProfileToOpenVPNClient(callbackObject, str);
    }

    public EddieLibraryResult setOption(Object callbackObject, String option, String value)
    {
        return EddieLibrary.setOpenVPNClientOption(callbackObject, option, value);
    }

    public EddieLibraryResult start(Object callbackObject)
    {
        return EddieLibrary.startOpenVPNClient(callbackObject);
    }

    public EddieLibraryResult stop(Object callbackObject)
    {
        return EddieLibrary.stopOpenVPNClient(callbackObject);
    }

    public EddieLibraryResult pause(Object callbackObject, String reason)
    {
        return EddieLibrary.pauseOpenVPNClient(callbackObject, reason);
    }

    public EddieLibraryResult resume(Object callbackObject)
    {
        return EddieLibrary.resumeOpenVPNClient(callbackObject);
    }

    public EddieLibraryResult destroy(Object callbackObject)
    {
        return EddieLibrary.disposeOpenVPNClient(callbackObject);
    }
}
