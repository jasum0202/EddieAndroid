// <eddie_source_header>
// This file is part of Eddie/AirVPN software.
// Copyright (C)2014-2018 AirVPN (support@airvpn.org) / https://airvpn.org
//
// Eddie is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Eddie is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Eddie. If not, see <http://www.gnu.org/licenses/>.
// </eddie_source_header>
//
// 15 October 2018 - author: ProMIND - initial release. (a tribute to the 1859 Perugia uprising occurred on 20 June 1859 and in memory of those brave inhabitants who fought for the liberty of Perugia)

package org.airvpn.eddie;

import android.content.Context;
import android.content.res.AssetManager;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

public class AirVPNServerProdiver
{
    public enum TLSMode
    {
        NOT_SET,
        TLS_AUTH,
        TLS_CRYPT
    }

    private final String AIRVPN_CONNECTION_PRIORITY_FILE_NAME = "connection_priority.txt";

    private static AirVPNManifest airVPNManifest = null;
    private static SettingsManager settingsManager = null;

    private Context appContext = null;

    private String userIP = "";
    private String userCountry = "";
    private String userContinent = "";
    private TLSMode tlsMode = TLSMode.NOT_SET;
    private boolean supportIPv4 = false;
    private boolean supportIPv6 = false;

    private CountryContinent countryContinent = null;

    private static HashMap<String, String> connectionPriority = null;

    private static Comparator<AirVPNServer> compareServerScore = null;

    public AirVPNServerProdiver(Context c)
    {
        appContext = c;

        if(airVPNManifest == null)
            airVPNManifest = new AirVPNManifest(appContext);

        if(settingsManager == null)
            settingsManager = new SettingsManager(appContext);

        if(connectionPriority == null)
            loadConnectionPriorities();

        userIP = "";
        userCountry = "";
        tlsMode = TLSMode.NOT_SET;
        supportIPv4 = true;
        supportIPv6 = true;

        countryContinent = new CountryContinent(c);

        if(compareServerScore == null)
        {
            compareServerScore = new Comparator<AirVPNServer>()
            {
                public int compare(AirVPNServer s1, AirVPNServer s2)
                {
                    return (s1.getScore() - s2.getScore());
                }
            };
        }
    }

    private void loadConnectionPriorities()
    {
        if(appContext == null)
        {
            EddieLogger.warning("AirVPNServerProdiver.loadConnectionPriorities(): Context is null.");

            connectionPriority = null;

            return;
        }

        AssetManager assetManager = appContext.getAssets();
        InputStream inputStream = null;

        try
        {
            inputStream = assetManager.open(AIRVPN_CONNECTION_PRIORITY_FILE_NAME);

            if(inputStream != null)
            {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

                String line = "";
                String row[] = null;

                connectionPriority = new HashMap<String, String>();

                while((line = bufferedReader.readLine()) != null)
                {
                    row = line.split("->");

                    if(row != null && row.length == 2)
                        connectionPriority.put(row[0].trim(), row[1].trim());
                }
            }
        }
        catch (Exception e)
        {
            EddieLogger.warning("AirVPNServerProdiver.loadConnectionPriorities(): %s not found.", AIRVPN_CONNECTION_PRIORITY_FILE_NAME);
        }
        finally
        {
            try
            {
                inputStream.close();
            }
            catch (Exception e)
            {
            }
        }
    }

    public String getUserIP()
    {
        return userIP;
    }

    public void setUserIP(String u)
    {
        userIP = u;
    }

    public String getUserCountry()
    {
        return userCountry;
    }

    public void setUserCountry(String c)
    {
        userCountry = c;

        userContinent = countryContinent.getCountryContinent(userCountry);
    }

    public TLSMode getTlsMode()
    {
        return tlsMode;
    }

    public void setTlsMode(TLSMode m)
    {
        tlsMode = m;
    }

    public boolean getSupportIPv4()
    {
        return supportIPv4;
    }

    public void setSupportIPv4(boolean s)
    {
        supportIPv4 = s;
    }

    public boolean getSupportIPv6()
    {
        return supportIPv6;
    }

    public void setSupportIPv6(boolean s)
    {
        supportIPv6 = s;
    }

    public synchronized ArrayList<AirVPNServer> getFilteredServerList()
    {
        boolean include = false, ipEntryFound = false;
        boolean forbidLocalServerConnection = settingsManager.isAirVPNForbidQuickConnectionToUserCountry();
        ArrayList<AirVPNServer> serverList = null, priorityServerList = null, filteredServerList = null;
        ArrayList<String> userConnectionPriority = null, serverWhitelist = null, serverBlacklist = null;
        ArrayList<String> countryWhitelist = null, countryBlacklist = null;
        ArrayList<Integer> cipherList = null;
        HashMap<Integer, String> ipEntry = null;
        AirVPNServer airVPNServer = null;
        int validItems = 0, validItemsForInclusion = 1;

        if(airVPNManifest == null)
        {
            EddieLogger.error("AirVPNServerProdiver.getFilteredServerList(): airVPNManifest is null");

            return null;
        }

        if(!settingsManager.getAirVPNCipher().equals(SettingsManager.AIRVPN_CIPHER_SERVER))
        {
            CipherDatabase cipherDatabase = new CipherDatabase();

            if(cipherDatabase != null)
                cipherList = cipherDatabase.getMatchingCiphersArrayList(settingsManager.getAirVPNCipher(), CipherDatabase.CipherType.DATA);
            else
                cipherList = null;
        }

        serverWhitelist = settingsManager.getAirVPNServerWhitelist();
        serverBlacklist = settingsManager.getAirVPNServerBlacklist();

        countryWhitelist = settingsManager.getAirVPNCountryWhitelist();
        countryBlacklist = settingsManager.getAirVPNCountryBlacklist();

        if(serverWhitelist.size() == 0 && countryWhitelist.size() == 0)
            serverList = airVPNManifest.getAirVpnServerList();
        else
        {
            serverList = new ArrayList<AirVPNServer>();

            for(String name : serverWhitelist)
            {
                airVPNServer = airVPNManifest.getServerByName(name);

                if(airVPNServer != null && airVPNServer.isAvailable(cipherList))
                    serverList.add(airVPNServer);
            }

            if(countryWhitelist.size() > 0)
            {
                for(AirVPNServer server : airVPNManifest.getAirVpnServerList())
                {
                    if(countryWhitelist.contains(server.getCountryCode()) && server.isAvailable(cipherList) && !serverWhitelist.contains(server.getName()))
                        serverList.add(server);
                }
            }

            if(serverList.size() == 0)
                serverList = airVPNManifest.getAirVpnServerList();
        }

        userConnectionPriority = getUserConnectionPriority();

        if(forbidLocalServerConnection == false)
            userConnectionPriority.add(0, userCountry);

        if(userConnectionPriority != null && userConnectionPriority.size() > 0 && serverList != null)
        {
            priorityServerList = new ArrayList<AirVPNServer>();

            for(int srv = 0; srv < serverList.size(); srv++)
            {
                String area = "";
                int penalty = 0;

                include = false;

                airVPNServer = serverList.get(srv);

                for(int i = 0; i < userConnectionPriority.size() && include == false; i++)
                {
                    area = userConnectionPriority.get(i);

                    if(area.length() == 2)
                    {
                        if(area.equals(airVPNServer.getCountryCode()))
                        {
                            include = true;

                            penalty = i;
                        }
                    }
                    else if(area.length() == 3)
                    {
                        if(area.equals(airVPNServer.getContinent()))
                        {
                            include = true;

                            penalty = i;
                        }
                    }
                    else
                    {
                        for(String place : airVPNServer.getLocation().split(","))
                        {
                            if(area.equals(place))
                            {
                                include = true;

                                penalty = i;
                            }
                        }
                    }
                }

                if(userCountry.equals(airVPNServer.getCountryCode()) && forbidLocalServerConnection)
                    include = false;

                if(!airVPNServer.isAvailable(cipherList))
                    include = false;

                if(serverBlacklist.contains(airVPNServer.getName()))
                    include = false;

                if(countryBlacklist.contains(airVPNServer.getCountryCode()))
                    include = false;

                if(include)
                {
                    airVPNServer.setScore(airVPNServer.getScore() + (10000 * penalty));

                    priorityServerList.add(airVPNServer);
                }
            }
        }
        else
            priorityServerList = serverList;

        if(priorityServerList == null)
            return null;

        filteredServerList = new ArrayList<AirVPNServer>();

        if(supportIPv4)
            validItemsForInclusion++;

        if(supportIPv6)
            validItemsForInclusion++;

        for(AirVPNServer server : priorityServerList)
        {
            validItems = 0;

            if(supportIPv4 && server.getSupportIPv4() == true)
                validItems++;

            if(supportIPv6 && server.getSupportIPv6() == true)
                validItems++;

            ipEntry = server.getEntryIPv4();
            ipEntryFound = false;

            if(ipEntry != null && airVPNManifest.getManifestType() != AirVPNManifest.ManifestType.DEFAULT)
            {
                switch(tlsMode)
                {
                    case TLS_AUTH:
                    {
                        if(ipEntry.containsKey(1))
                        {
                            if(!ipEntry.get((Integer)1).isEmpty())
                                ipEntryFound = true;
                        }

                        if(ipEntry.containsKey((Integer)2))
                        {
                            if(!ipEntry.get((Integer)2).toString().isEmpty())
                                ipEntryFound = true;
                        }

                        if(ipEntryFound)
                            validItems++;
                    }
                    break;

                    case TLS_CRYPT:
                    {
                        if(ipEntry.containsKey((Integer)3))
                        {
                            if(!ipEntry.get((Integer)3).toString().isEmpty())
                                ipEntryFound = true;
                        }

                        if(ipEntry.containsKey((Integer)4))
                        {
                            if(!ipEntry.get((Integer)4).toString().isEmpty())
                                ipEntryFound = true;
                        }

                        if (ipEntryFound)
                            validItems++;
                    }
                    break;
                }
            }
            else
                validItems++;

            if(validItems == validItemsForInclusion)
                filteredServerList.add(server);
        }

        Collections.sort(filteredServerList, compareServerScore);

        return filteredServerList;
    }

    private ArrayList<String> getUserConnectionPriority()
    {
        ArrayList<String> areaList = new ArrayList<String>();
        String areaPriority = null;
        int i;

        if(userCountry != "RU")
            areaPriority = connectionPriority.get(userCountry);
        else
        {
            String ruArea = "";

            if(AirVPNUser.getUserLongitude() < 103.851959)
                ruArea = "R1";
            else
                ruArea = "R2";

            areaPriority = connectionPriority.get(ruArea);
        }

        if(areaPriority != null && !areaPriority.isEmpty())
        {
            String[] priority = areaPriority.split(",");

            if(priority.length > 0)
            {
                for(i = 0; i < priority.length; i++)
                    areaList.add(priority[i]);
            }
        }

        areaPriority = connectionPriority.get(userContinent);

        if(areaPriority != null && !areaPriority.isEmpty())
        {
            String[] priority = areaPriority.split(",");

            if(priority.length > 0)
            {
                for(i = 0; i < priority.length; i++)
                    areaList.add(priority[i]);
            }
        }

        return areaList;
    }
}
