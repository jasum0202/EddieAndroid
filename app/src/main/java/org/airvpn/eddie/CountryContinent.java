// <eddie_source_header>
// This file is part of Eddie/AirVPN software.
// Copyright (C)2014-2018 AirVPN (support@airvpn.org) / https://airvpn.org
//
// Eddie is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Eddie is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Eddie. If not, see <http://www.gnu.org/licenses/>.
// </eddie_source_header>
//
// 13 October 2018 - author: ProMIND - initial release. (a tribute to the 1859 Perugia uprising occurred on 20 June 1859 and in memory of those brave inhabitants who fought for the liberty of Perugia)

package org.airvpn.eddie;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.AssetManager;
import android.os.Build;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class CountryContinent
{
    private final String COUNTRY_CONTINENT_FILE_NAME = "country_continent.csv";
    private final String COUNTRY_NAMES_FILE_NAME = "country_names";

    private static HashMap<String, String> countryContinent = null;
    private static HashMap<String, String> countryNames = null;
    private Context appContext = null;

    CountryContinent(Context c)
    {
        String countryFileName = "";
        appContext = c;

        if(appContext != null && countryContinent == null)
        {
            if(appContext == null)
            {
                EddieLogger.warning("CountryContinent(): Context is null.");

                countryContinent = null;

                return;
            }

            AssetManager assetManager = appContext.getAssets();
            InputStream inputStream = null;

            try
            {
                inputStream = assetManager.open(COUNTRY_CONTINENT_FILE_NAME);

                if(inputStream != null)
                {
                    InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                    BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

                    String line = "";
                    String row[] = null;

                    countryContinent = new HashMap<String, String>();

                    while((line = bufferedReader.readLine()) != null)
                    {
                        row = line.split(",");

                        if(row != null && row.length == 2)
                            countryContinent.put(row[0], row[1]);
                    }
                }
            }
            catch(Exception e)
            {
                EddieLogger.warning("CountryContinent(): %s not found.", COUNTRY_CONTINENT_FILE_NAME);
            }
            finally
            {
                try
                {
                    inputStream.close();
                }
                catch(Exception e)
                {
                }
            }

            try
            {
                String locale = "en_US";

                if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
                    locale = appContext.getResources().getConfiguration().getLocales().get(0).toString();
                else
                    locale = appContext.getResources().getConfiguration().locale.toString();

                if(!locale.equals("zh_RCN") && !locale.equals("zh_RTW"))
                {
                    locale = locale.substring(0, 2);

                    if(locale.equals("zh"))
                        locale = "zh_RCN";
                }

                countryFileName = String.format("%s.%s", COUNTRY_NAMES_FILE_NAME, locale);

                if(Arrays.asList(appContext.getResources().getAssets().list("")).contains(countryFileName) == false)
                    countryFileName = COUNTRY_NAMES_FILE_NAME + ".en";

                inputStream = assetManager.open(countryFileName);

                if(inputStream != null)
                {
                    InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                    BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

                    String line = "";
                    String row[] = null;

                    countryNames = new HashMap<String, String>();

                    while((line = bufferedReader.readLine()) != null)
                    {
                        row = line.split(",");

                        if(row != null && row.length == 2)
                            countryNames.put(row[0], row[1]);
                    }
                }
            }
            catch(Exception e)
            {
                EddieLogger.warning("CountryContinent(): %s not found.", countryFileName);
            }
            finally
            {
                try
                {
                    inputStream.close();
                }
                catch(Exception e)
                {
                }
            }
        }
    }

    public String getCountryContinent(String countryCode)
    {
        if(countryContinent == null)
            return "";

        String continent = "";

        if(countryContinent.containsKey(countryCode))
            continent = countryContinent.get(countryCode);

        return continent;
    }

    public String getCountryName(String countryCode)
    {
        if(countryNames == null)
            return "";

        String name = "";

        if(countryNames.containsKey(countryCode))
            name = countryNames.get(countryCode);

        return name;
    }

    public String getCountryCode(String countryName)
    {
        if(countryNames == null)
            return "";

        String code = "";

        for(Map.Entry<String, String> entry : countryNames.entrySet())
        {
            if(entry.getValue().equals(countryName))
                code = entry.getKey();
        }

        return code;
    }
}
