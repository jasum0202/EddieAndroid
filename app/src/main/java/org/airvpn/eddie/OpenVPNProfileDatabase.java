// <eddie_source_header>
// This file is part of Eddie/AirVPN software.
// Copyright (C)2014-2019 AirVPN (support@airvpn.org) / https://airvpn.org
//
// Eddie is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Eddie is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Eddie. If not, see <http://www.gnu.org/licenses/>.
// </eddie_source_header>
//
// 7 January 2019 - author: ProMIND - initial release. (a tribute to the 1859 Perugia uprising occurred on 20 June 1859 and in memory of those brave inhabitants who fought for the liberty of Perugia)

package org.airvpn.eddie;

import android.content.Context;
import android.util.Base64;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.NodeList;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

public class OpenVPNProfileDatabase
{
    public enum ProtocolType
    {
        UNKNOWN,
        UDPv4,
        TCPv4,
        UDPv6,
        TCPv6;

        public static ProtocolType fromInteger(int t)
        {
            ProtocolType result = UNKNOWN;

            switch(t)
            {
                case 0:
                {
                    result = UNKNOWN;
                }
                break;

                case 1:
                {
                    result = UDPv4;
                }
                break;

                case 2:
                {
                    result = TCPv4;
                }
                break;

                case 3:
                {
                    result = UDPv6;
                }
                break;

                case 4:
                {
                    result = TCPv6;
                }
                break;

                default:
                {
                    result = UNKNOWN;
                }
                break;
            }

            return result;
        }

        public static int toInteger(ProtocolType t)
        {
            int result = 0;

            switch(t)
            {
                case UNKNOWN:
                {
                    result = 0;
                }
                break;

                case UDPv4:
                {
                    result = 1;
                }
                break;

                case TCPv4:
                {
                    result = 2;
                }
                break;

                case UDPv6:
                {
                    result = 3;
                }
                break;

                case TCPv6:
                {
                    result = 4;
                }
                break;

                default:
                {
                    result = 0;
                }
                break;
            }

            return result;
        }

        public static String toString(ProtocolType t)
        {
            String result = "";

            switch(t)
            {
                case UNKNOWN:
                {
                    result = "UNKNOWN";
                }
                break;

                case UDPv4:
                {
                    result = "UDPv4";
                }
                break;

                case TCPv4:
                {
                    result = "TCPv4";
                }
                break;

                case UDPv6:
                {
                    result = "UDPv6";
                }
                break;

                case TCPv6:
                {
                    result = "TCPv6";
                }
                break;

                default:
                {
                    result = "UNKNOWN";
                }
                break;
            }

            return result;
        }
    }

    public enum SortMode
    {
        NO_SORT,
        SORT_NAMES
    }

    private final String OPENVPN_PROFILE_DATABASE_FILE_NAME = "OpenVPNDatabase.xml";
    private final String OPENVPN_PROFILE_GROUP = "openvpn_group";
    private final String OPENVPN_PROFILE_ITEM = "openvpn_profile";
    private final String OPENVPN_PROFILE_NAME = "name";
    private final String OPENVPN_PROFILE_SERVER = "server";
    private final String OPENVPN_PROFILE_PORT = "port";
    private final String OPENVPN_PROFILE_PROTOCOL = "protocol";
    private final String OPENVPN_PROFILE_PROFILE = "profile";

    private static HashMap<String, OpenVPNProfile> profileDatabase = null;

    private Context appContext = null;

    private SupportTools supportTools = null;

    public class OpenVPNProfile
    {
        private String name = "";
        private String server = "";
        private int port = 0;
        private ProtocolType protocol = ProtocolType.UNKNOWN;
        private String profile = "";

        public OpenVPNProfile()
        {
            name = "";
            server = "";
            port = 0;
            protocol = ProtocolType.UNKNOWN;
            profile = "";
        }

        public String getName()
        {
            return name;
        }

        public void setName(String n)
        {
            name = n;
        }

        public String getServer()
        {
            return server;
        }

        public void setServer(String s)
        {
            server = s;
        }

        public int getPort()
        {
            return port;
        }

        public void setPort(int p)
        {
            port = p;
        }

        public ProtocolType getProtocol()
        {
            return protocol;
        }

        public void setProtocol(ProtocolType p)
        {
            protocol = p;
        }

        public String getProfile()
        {
            return profile;
        }

        public void setProfile(String p)
        {
            profile = p;
        }
    }

    public OpenVPNProfileDatabase(Context c)
    {
        appContext = c;

        supportTools = new SupportTools(appContext);

        if(profileDatabase == null)
        {
            profileDatabase = new HashMap<String, OpenVPNProfile>();

            loadOpenVPNProfileDatabase();
        }
    }

    public boolean exists(String n)
    {
        return profileDatabase.containsKey(n);
    }

    public OpenVPNProfile getProfile(String n)
    {
        OpenVPNProfile profile = null;

        if(profileDatabase.containsKey(n))
            profile = profileDatabase.get(n);

        return profile;
    }

    public void setProfile(String n, OpenVPNProfile p)
    {
        setProfile(n, p, false);
    }

    public void setProfile(String n, OpenVPNProfile p, boolean replace)
    {
        if(n.isEmpty() || p == null)
            return;

        if(replace == false && profileDatabase.containsKey(n))
            return;

        profileDatabase.put(n, p);

        saveOpenVPNProfileDatabase();
    }

    public boolean renameProfile(String oldName, String newName)
    {
        OpenVPNProfile profile;
        boolean result = false;

        if(profileDatabase.containsKey(oldName))
        {
            profile = getProfile(oldName);

            profile.setName(newName);

            profileDatabase.remove(oldName);

            profileDatabase.put(newName, profile);

            saveOpenVPNProfileDatabase();

            result = true;
        }

        return result;
    }

    public boolean deleteProfile(String n)
    {
        boolean result = false;

        if(profileDatabase.containsKey(n))
        {
            profileDatabase.remove(n);

            saveOpenVPNProfileDatabase();

            result = true;
        }

        return result;
    }

    public ArrayList<String> getProfileNameList(SortMode sortMode)
    {
        ArrayList<String> profileList = new ArrayList<String>();

        for(Map.Entry<String, OpenVPNProfile> entry : profileDatabase.entrySet())
            profileList.add(entry.getKey());

        if(sortMode == SortMode.SORT_NAMES)
            Collections.sort(profileList);

        return profileList;
    }

    public int size()
    {
        return profileDatabase.size();
    }

    private void loadOpenVPNProfileDatabase()
    {
        File openVPNProfileFile = null;
        Document openVPNProfileDocument = null;
        NodeList nodeList = null;
        NamedNodeMap namedNodeMap = null;
        byte[] b64 = null;
        String value;
        int val;

        if(appContext == null)
            return;

        openVPNProfileFile = new File(appContext.getFilesDir(), OPENVPN_PROFILE_DATABASE_FILE_NAME);

        if(!openVPNProfileFile.exists())
            return;

        openVPNProfileDocument = supportTools.loadXmlFileToDocument(OPENVPN_PROFILE_DATABASE_FILE_NAME);

        if(openVPNProfileDocument == null)
            return;

        nodeList = openVPNProfileDocument.getElementsByTagName(OPENVPN_PROFILE_ITEM);

        if(nodeList != null && nodeList.getLength() > 0)
        {
            for(int i = 0; i < nodeList.getLength(); i++)
            {
                OpenVPNProfile openVPNProfile = new OpenVPNProfile();

                namedNodeMap = nodeList.item(i).getAttributes();

                if(namedNodeMap != null)
                {
                    openVPNProfile.setName(supportTools.getXmlItemNodeValue(namedNodeMap, OPENVPN_PROFILE_NAME));
                    openVPNProfile.setServer(supportTools.getXmlItemNodeValue(namedNodeMap, OPENVPN_PROFILE_SERVER));

                    try
                    {
                        val = Integer.parseInt(supportTools.getXmlItemNodeValue(namedNodeMap, OPENVPN_PROFILE_PORT));
                    }
                    catch(NumberFormatException e)
                    {
                        val = 0;
                    }

                    openVPNProfile.setPort(val);

                    try
                    {
                        val = Integer.parseInt(supportTools.getXmlItemNodeValue(namedNodeMap, OPENVPN_PROFILE_PROTOCOL));

                        openVPNProfile.setProtocol(ProtocolType.fromInteger(val));
                    }
                    catch(NumberFormatException e)
                    {
                        openVPNProfile.setProtocol(ProtocolType.UNKNOWN);
                    }

                    value = supportTools.getXmlItemNodeValue(namedNodeMap, OPENVPN_PROFILE_PROFILE);

                    if(!value.equals(""))
                    {
                        b64 = Base64.decode(value, Base64.NO_WRAP);

                        openVPNProfile.setProfile(new String(b64, StandardCharsets.UTF_8));
                    }
                    else
                        openVPNProfile.setProfile("");

                    profileDatabase.put(openVPNProfile.getName(), openVPNProfile);
                }
            }
        }
    }

    private boolean saveOpenVPNProfileDatabase()
    {
        DocumentBuilderFactory documentBuilderFactory = null;
        DocumentBuilder documentBuilder = null;
        Document profileDocument = null;

        try
        {
            documentBuilderFactory = DocumentBuilderFactory.newInstance();

            documentBuilder = documentBuilderFactory.newDocumentBuilder();
        }
        catch(ParserConfigurationException e)
        {
            return false;
        }

        profileDocument = documentBuilder.newDocument();

        if(profileDocument == null)
            return false;

        Element rootElement = profileDocument.createElement(OPENVPN_PROFILE_GROUP);
        profileDocument.appendChild(rootElement);

        for(Map.Entry<String, OpenVPNProfile> entry : profileDatabase.entrySet())
        {
            OpenVPNProfile profile = entry.getValue();

            Element profileElement = profileDocument.createElement(OPENVPN_PROFILE_ITEM);

            profileElement.setAttribute(OPENVPN_PROFILE_NAME, profile.getName());
            profileElement.setAttribute(OPENVPN_PROFILE_SERVER, profile.getServer());
            profileElement.setAttribute(OPENVPN_PROFILE_PORT, String.format("%d", profile.getPort()));
            profileElement.setAttribute(OPENVPN_PROFILE_PROTOCOL, String.format("%d", ProtocolType.toInteger(profile.getProtocol())));
            profileElement.setAttribute(OPENVPN_PROFILE_PROFILE, Base64.encodeToString(profile.getProfile().getBytes(StandardCharsets.UTF_8), Base64.NO_WRAP));

            rootElement.appendChild(profileElement);
        }

        supportTools.saveXmlDocumentToFile(profileDocument, OPENVPN_PROFILE_DATABASE_FILE_NAME);

        return true;
    }
}
