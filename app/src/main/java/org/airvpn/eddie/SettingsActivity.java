// <eddie_source_header>
// This file is part of Eddie/AirVPN software.
// Copyright (C)2014-2018 AirVPN (support@airvpn.org) / https://airvpn.org
//
// Eddie is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Eddie is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Eddie. If not, see <http://www.gnu.org/licenses/>.
// </eddie_source_header>
//
// 20 June 2018 - author: ProMIND - initial release. (a tribute to the 1859 Perugia uprising occurred on 20 June 1859 and in memory of those brave inhabitants who fought for the liberty of Perugia)

package org.airvpn.eddie;

import android.app.Activity;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.content.res.ResourcesCompat;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

public class SettingsActivity extends Activity
{
    private final int ACTIVITY_RESULT_PACKAGE_CHOOSER = 1000;

    private SupportTools supportTools = null;
    private SettingsManager settingsManager = null;
    private AirVPNUser airVPNUser = null;

    private Typeface typeface = null;

    private TextView txtTitle = null;

    private LinearLayout llAirVPNCategory = null;
    private LinearLayout llAirVPNGroup = null;
    private LinearLayout llAirVPNMasterPassword = null;
    private LinearLayout llAirVPNRememberMe = null;
    private LinearLayout llAirVPNUserName = null;
    private LinearLayout llAirVPNPassword = null;
    private LinearLayout llAirVPNProtocol = null;
    private LinearLayout llAirVPNPort = null;
    private LinearLayout llAirVPNIPVersion = null;
    private LinearLayout llAirVPNTLSMode = null;
    private LinearLayout llAirVPNQuickConnectMode = null;
    private LinearLayout llAirVPNEncryptionAlgorithm = null;
    private LinearLayout llAirVPNForbidQuickConnectToUserCountry = null;
    private LinearLayout llAirVPNCustomBootstrap = null;

    private LinearLayout llVpnCategory = null;
    private LinearLayout llVpnGroup = null;
    private LinearLayout llVpnMinimumTLSVersion = null;
    private LinearLayout llVpnTransportProtocol = null;
    private LinearLayout llVpnIPV6 = null;
    private LinearLayout llVpnTimeout = null;
    private LinearLayout llVpnTunPersist = null;
    private LinearLayout llVpnCompressionMode = null;
    private LinearLayout llVpnLock = null;
    private LinearLayout llVpnReconnect = null;
    private LinearLayout llVpnReconnectionRetries = null;

    private LinearLayout llDnsCategory = null;
    private LinearLayout llDnsGroup = null;
    private LinearLayout llDnsOverride = null;
    private LinearLayout llDnsOverrideSettings = null;
    private LinearLayout llCustomDns = null;

    private LinearLayout llAuthenticationCategory = null;
    private LinearLayout llAuthenticationGroup = null;
    private LinearLayout llVpnUsername = null;
    private LinearLayout llVpnPassword = null;

    private LinearLayout llSystemCategory = null;
    private LinearLayout llSystemGroup = null;
    private LinearLayout llExcludeLocalNetworks = null;
    private LinearLayout llPauseVpnWhenScreenIsOff = null;
    private LinearLayout llPersistentNotification = null;
    private LinearLayout llNotificationSound = null;
    private LinearLayout llNotificationChannel = null;
    private LinearLayout llShowMessageDialogs = null;
    private LinearLayout llRestoreLastProfile = null;
    private LinearLayout llApplicationFilterType = null;
    private LinearLayout llApplicationFilter = null;
    private LinearLayout llApplicationLanguage = null;

    private LinearLayout llProxyCategory = null;
    private LinearLayout llProxyGroup = null;
    private LinearLayout llProxyEnable = null;
    private LinearLayout llProxySettings = null;
    private LinearLayout llProxyHost = null;
    private LinearLayout llProxyPort = null;
    private LinearLayout llProxyUsername = null;
    private LinearLayout llProxyPassword = null;
    private LinearLayout llProxyAllowClearTextAuth = null;

    private LinearLayout llAdvancedCategory = null;
    private LinearLayout llAdvancedGroup = null;
    private LinearLayout llSynchronousDnsLookup = null;
    private LinearLayout llCustomMtu = null;
    private LinearLayout llAutologinSessions = null;
    private LinearLayout llDisableClientCert = null;
    private LinearLayout llSslDebugLevel = null;
    private LinearLayout llPrivateKeyPassword = null;
    private LinearLayout llDefaultKeyDirection = null;
    private LinearLayout llForceAesCbcCiphers = null;
    private LinearLayout llTlsCertProfile = null;
    private LinearLayout llCustomDirectives = null;

    private ImageView imgAirVPNIndicator = null;
    private ImageView imgVpnIndicator = null;
    private ImageView imgDnsIndicator = null;
    private ImageView imgAuthenticationIndicator = null;
    private ImageView imgSystemIndicator = null;
    private ImageView imgProxyIndicator = null;
    private ImageView imgAdvancedIndicator = null;

    private Switch swAirVPNRememberMe = null;
    private Switch swAirVPNForbidQuickConnectToUserCountry = null;
    private Switch swVpnTunPersist = null;
    private Switch swVpnLock = null;
    private Switch swVpnReconnect = null;
    private Switch swDnsOverride = null;
    private Switch swExcludeLocalNetworks = null;
    private Switch swPauseVpnWhenScreeIsOff = null;
    private Switch swPersistentNotification = null;
    private Switch swNotificationSound = null;
    private Switch swShowMessageDialogs = null;
    private Switch swRestoreLastProfile = null;
    private Switch swProxyEnable = null;
    private Switch swProxyAllowClearTextAuth = null;
    private Switch swSynchronousDnsLookup = null;
    private Switch swAutologinSessions = null;
    private Switch swDisableClientCert = null;
    private Switch swForceAesCbcCiphers = null;

    private Button btnResetOptions = null;

    private TextView txtApplicationFilterTitle = null;

    private int settingsResult = RESULT_CANCELED;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        supportTools = new SupportTools(this);
        settingsManager = new SettingsManager(this);
        airVPNUser = new AirVPNUser(this);

        setContentView(R.layout.settings_activity_layout);

        typeface = ResourcesCompat.getFont(this, R.font.default_font);

        txtTitle = (TextView)findViewById(R.id.title);

        txtTitle.setTypeface(typeface);

        llAirVPNCategory = (LinearLayout)findViewById(R.id.setting_airvpn_category);
        llAirVPNGroup = (LinearLayout)findViewById(R.id.setting_airvpn_group);
        llAirVPNMasterPassword = (LinearLayout)findViewById(R.id.setting_airvpn_master_password);
        llAirVPNRememberMe = (LinearLayout)findViewById(R.id.setting_airvpn_remember_me);
        llAirVPNUserName = (LinearLayout)findViewById(R.id.setting_airvpn_username);
        llAirVPNPassword = (LinearLayout)findViewById(R.id.setting_airvpn_password);
        llAirVPNProtocol = (LinearLayout)findViewById(R.id.settings_airvpn_default_protocol);
        llAirVPNPort = (LinearLayout)findViewById(R.id.settings_airvpn_default_port);
        llAirVPNIPVersion = (LinearLayout)findViewById(R.id.settings_airvpn_default_ip_version);
        llAirVPNTLSMode = (LinearLayout)findViewById(R.id.settings_airvpn_default_tls_mode);
        llAirVPNQuickConnectMode = (LinearLayout)findViewById(R.id.settings_airvpn_quick_connect_mode);
        llAirVPNEncryptionAlgorithm = (LinearLayout)findViewById(R.id.settings_airvpn_encryption_algorithm);
        llAirVPNForbidQuickConnectToUserCountry = (LinearLayout)findViewById(R.id.setting_airvpn_forbid_quick_connect_to_user_country);
        llAirVPNCustomBootstrap = (LinearLayout)findViewById(R.id.setting_airvpn_custom_bootstrap);

        llVpnCategory = (LinearLayout)findViewById(R.id.setting_vpn_category);
        llVpnGroup = (LinearLayout)findViewById(R.id.setting_vpn_group);
        llVpnMinimumTLSVersion = (LinearLayout)findViewById(R.id.setting_ovpn3_tls_min_version);
        llVpnTransportProtocol = (LinearLayout)findViewById(R.id.setting_ovpn3_protocol);
        llVpnIPV6 = (LinearLayout)findViewById(R.id.setting_ovpn3_ipv6);
        llVpnTimeout = (LinearLayout)findViewById(R.id.setting_ovpn3_timeout);
        llVpnTunPersist = (LinearLayout)findViewById(R.id.setting_ovpn3_tun_persist);
        llVpnCompressionMode = (LinearLayout)findViewById(R.id.setting_ovpn3_compression_mode);
        llVpnLock = (LinearLayout)findViewById(R.id.setting_vpn_lock);
        llVpnReconnect = (LinearLayout)findViewById(R.id.setting_vpn_reconnect);
        llVpnReconnectionRetries = (LinearLayout)findViewById(R.id.setting_vpn_reconnect_retries);

        llDnsCategory = (LinearLayout)findViewById(R.id.setting_dns_category);
        llDnsGroup = (LinearLayout)findViewById(R.id.setting_dns_group);
        llDnsOverride = (LinearLayout)findViewById(R.id.setting_dns_override);
        llDnsOverrideSettings = (LinearLayout)findViewById(R.id.dns_override_layout);
        llCustomDns = (LinearLayout)findViewById(R.id.setting_custom_dns);

        llAuthenticationCategory = (LinearLayout)findViewById(R.id.setting_authentication_category);
        llAuthenticationGroup = (LinearLayout)findViewById(R.id.setting_authentication_group);
        llVpnUsername = (LinearLayout)findViewById(R.id.setting_ovpn3_username);
        llVpnPassword = (LinearLayout)findViewById(R.id.setting_ovpn3_password);

        llSystemCategory = (LinearLayout)findViewById(R.id.setting_system_category);
        llSystemGroup = (LinearLayout)findViewById(R.id.setting_system_group);
        llExcludeLocalNetworks = (LinearLayout)findViewById(R.id.setting_exclude_local_networks);
        llPauseVpnWhenScreenIsOff = (LinearLayout)findViewById(R.id.setting_pause_vpn_when_screen_is_off);
        llPersistentNotification = (LinearLayout)findViewById(R.id.setting_persistent_notification);
        llNotificationSound = (LinearLayout)findViewById(R.id.setting_notification_sound);
        llNotificationChannel = (LinearLayout)findViewById(R.id.setting_notification_channel);
        llShowMessageDialogs = (LinearLayout)findViewById(R.id.setting_show_message_dialogs);
        llRestoreLastProfile = (LinearLayout)findViewById(R.id.setting_restore_last_profile);
        llApplicationFilterType = (LinearLayout)findViewById(R.id.setting_application_filter_type);
        llApplicationFilter = (LinearLayout)findViewById(R.id.setting_application_filter);
        llApplicationLanguage = (LinearLayout)findViewById(R.id.setting_application_language);

        llProxyCategory = (LinearLayout)findViewById(R.id.setting_proxy_category);
        llProxyGroup = (LinearLayout)findViewById(R.id.setting_proxy_group);
        llProxyEnable = (LinearLayout)findViewById(R.id.setting_proxy_enable);
        llProxySettings = (LinearLayout)findViewById(R.id.proxy_settings_layout);
        llProxyHost = (LinearLayout)findViewById(R.id.setting_proxy_host);
        llProxyPort = (LinearLayout)findViewById(R.id.setting_proxy_port);
        llProxyUsername = (LinearLayout)findViewById(R.id.setting_proxy_username);
        llProxyPassword = (LinearLayout)findViewById(R.id.setting_proxy_password);
        llProxyAllowClearTextAuth = (LinearLayout)findViewById(R.id.setting_proxy_allow_cleartext_auth);

        llAdvancedCategory = (LinearLayout)findViewById(R.id.setting_advanced_category);
        llAdvancedGroup = (LinearLayout)findViewById(R.id.setting_advanced_group);
        llSynchronousDnsLookup = (LinearLayout)findViewById(R.id.setting_synchronous_dns_lookup);
        llCustomMtu = (LinearLayout)findViewById(R.id.setting_custom_mtu);
        llAutologinSessions = (LinearLayout)findViewById(R.id.setting_autologin_sessions);
        llDisableClientCert = (LinearLayout)findViewById(R.id.setting_disable_client_cert);
        llSslDebugLevel = (LinearLayout)findViewById(R.id.setting_ssl_debug_level);
        llPrivateKeyPassword = (LinearLayout)findViewById(R.id.setting_private_key_password);
        llDefaultKeyDirection = (LinearLayout)findViewById(R.id.setting_default_key_direction);
        llForceAesCbcCiphers = (LinearLayout)findViewById(R.id.setting_force_aes_cbc_ciphers);
        llTlsCertProfile = (LinearLayout)findViewById(R.id.setting_tls_cert_profile);
        llCustomDirectives = (LinearLayout)findViewById(R.id.setting_custom_directives);

        imgAirVPNIndicator = (ImageView)findViewById(R.id.img_airvpn_category_indicator);
        imgVpnIndicator = (ImageView)findViewById(R.id.img_vpn_category_indicator);
        imgDnsIndicator = (ImageView)findViewById(R.id.img_dns_category_indicator);
        imgAuthenticationIndicator = (ImageView)findViewById(R.id.img_authentication_indicator);
        imgSystemIndicator = (ImageView)findViewById(R.id.img_system_indicator);
        imgProxyIndicator = (ImageView)findViewById(R.id.img_proxy_indicator);
        imgAdvancedIndicator = (ImageView)findViewById(R.id.img_advanced_indicator);

        swAirVPNRememberMe = (Switch)findViewById(R.id.switch_airvpn_remember_me);
        swAirVPNForbidQuickConnectToUserCountry = (Switch)findViewById(R.id.switch_airvpn_forbid_quick_connect_to_user_country_summary);
        swVpnTunPersist = (Switch)findViewById(R.id.switch_ovpn3_tun_persist);
        swVpnLock = (Switch)findViewById(R.id.switch_vpn_lock);
        swVpnReconnect = (Switch)findViewById(R.id.switch_vpn_reconnect);
        swDnsOverride = (Switch)findViewById(R.id.switch_dns_override);
        swExcludeLocalNetworks = (Switch)findViewById(R.id.switch_exclude_local_networks);
        swPauseVpnWhenScreeIsOff = (Switch)findViewById(R.id.switch_pause_vpn_when_screen_is_off);
        swPersistentNotification = (Switch)findViewById(R.id.switch_persistent_notification);
        swNotificationSound = (Switch)findViewById(R.id.switch_notification_sound);
        swShowMessageDialogs = (Switch)findViewById(R.id.switch_show_message_dialogs);
        swRestoreLastProfile = (Switch)findViewById(R.id.switch_restore_last_profile);
        swProxyEnable = (Switch)findViewById(R.id.switch_proxy_enable);
        swProxyAllowClearTextAuth = (Switch)findViewById(R.id.switch_proxy_allow_cleartext_auth);
        swSynchronousDnsLookup = (Switch)findViewById(R.id.switch_synchronous_dns_lookup);
        swAutologinSessions = (Switch)findViewById(R.id.switch_autologin_sessions);
        swDisableClientCert = (Switch)findViewById(R.id.switch_disable_client_cert);
        swForceAesCbcCiphers = (Switch)findViewById(R.id.switch_force_aes_cbc_ciphers);

        btnResetOptions = (Button)findViewById(R.id.btn_reset_settings);

        txtApplicationFilterTitle = (TextView)findViewById(R.id.settings_application_filter_title);

        if(Build.VERSION.SDK_INT < Build.VERSION_CODES.O)
        {
            llNotificationSound.setVisibility(View.VISIBLE);

            llNotificationChannel.setVisibility(View.GONE);
        }
        else
        {
            String channelId = getResources().getString(R.string.notification_channel_id);
            NotificationManager notificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
            NotificationChannel notificationChannel = notificationManager.getNotificationChannel(channelId);

            llNotificationSound.setVisibility(View.GONE);

            if(notificationChannel != null)
                llNotificationChannel.setVisibility(View.VISIBLE);
            else
                llNotificationChannel.setVisibility(View.GONE);
        }

        llAirVPNCategory.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View arg0)
            {
                toggleSettingsCategory(llAirVPNGroup, imgAirVPNIndicator);
            }
        });

        llAirVPNMasterPassword.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View arg0)
            {
                selectAirVPNMasterPassword();
            }
        });

        llAirVPNUserName.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View arg0)
            {
                selectAirVPNUserName();
            }
        });

        llAirVPNPassword.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View arg0)
            {
                selectAirVPNPassword();
            }
        });

        llAirVPNRememberMe.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View arg0)
            {
                selectAirVPNRememberMe();
            }
        });

        llAirVPNProtocol.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View arg0)
            {
                selectAirVPNDefaultProtocol();
            }
        });

        llAirVPNPort.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View arg0)
            {
                selectAirVPNDefaultPort();
            }
        });

        llAirVPNIPVersion.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View arg0)
            {
                selectAirVPNDefaultIPVersion();
            }
        });

        llAirVPNTLSMode.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View arg0)
            {
                selectAirVPNDefaultTLSMode();
            }
        });

        llAirVPNQuickConnectMode.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View arg0)
            {
                selectAirVPNQuickConnectMode();
            }
        });

        llAirVPNEncryptionAlgorithm.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View arg0)
            {
                selectAirVPNEncryptionAlgorithm();
            }
        });

        llAirVPNForbidQuickConnectToUserCountry.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View arg0)
            {
                selectAirVPNForbidQuickConnectToUserCountry();
            }
        });

        llAirVPNCustomBootstrap.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View arg0)
            {
                selectAirVPNCustomBootstrap();
            }
        });

        llVpnCategory.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View arg0)
            {
                toggleSettingsCategory(llVpnGroup, imgVpnIndicator);
            }
        });

        llVpnMinimumTLSVersion.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View arg0)
            {
                selectVpnMinimumTLSVersion();
            }
        });

        llVpnTransportProtocol.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View arg0)
            {
                selectVpnTransportProtocol();
            }
        });

        llVpnIPV6.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View arg0)
            {
                selectVpnIPV6();
            }
        });

        llVpnTimeout.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View arg0)
            {
                selectVpnTimeout();
            }
        });

        llVpnTunPersist.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View arg0)
            {
                selectVpnTunPersist();
            }
        });

        llVpnCompressionMode.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View arg0)
            {
                selectVpnCompressionMode();
            }
        });

        llVpnLock.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View arg0)
            {
                selectVpnLock();
            }
        });

        llVpnReconnect.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View arg0)
            {
                selectVpnReconnect();
            }
        });

        llVpnReconnectionRetries.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View arg0)
            {
                selectVpnReconnectionRetries();
            }
        });

        llDnsCategory.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View arg0)
            {
                toggleSettingsCategory(llDnsGroup, imgDnsIndicator);
            }
        });

        llDnsOverride.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View arg0)
            {
                selectDnsOverrideSettings();
            }
        });

        llCustomDns.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View arg0)
            {
                selectCustomDns();
            }
        });

        llAuthenticationCategory.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View arg0)
            {
                toggleSettingsCategory(llAuthenticationGroup, imgAuthenticationIndicator);
            }
        });

        llVpnUsername.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View arg0)
            {
                selectVpnUsername();
            }
        });

        llVpnPassword.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View arg0)
            {
                selectVpnPassword();
            }
        });

        llSystemCategory.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View arg0)
            {
                toggleSettingsCategory(llSystemGroup, imgSystemIndicator);
            }
        });

        llExcludeLocalNetworks.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View arg0)
            {
                selectExcludeLocalNetworks();
            }
        });

        llPauseVpnWhenScreenIsOff.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View arg0)
            {
                selectPauseVpnWhenScreenIsOff();
            }
        });

        llPersistentNotification.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View arg0)
            {
                selectPersistentNotification();
            }
        });

        llNotificationSound.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View arg0)
            {
                selectNotificationSound();
            }
        });

        llNotificationChannel.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View arg0)
            {
                selectNotificationChannel();
            }
        });

        llShowMessageDialogs.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View arg0)
            {
                selectShowMessageDialogs();
            }
        });

        llRestoreLastProfile.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View arg0)
            {
                selectRestoreLastProfile();
            }
        });

        llApplicationFilterType.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View arg0)
            {
                selectApplicationFilterType();
            }
        });

        llApplicationFilter.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View arg0)
            {
                selectApplicationFilter();
            }
        });

        llApplicationLanguage.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View arg0)
            {
                selectApplicationLanguage();
            }
        });

        llProxyCategory.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View arg0)
            {
                toggleSettingsCategory(llProxyGroup, imgProxyIndicator);
            }
        });

        llProxyEnable.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View arg0)
            {
                selectProxyEnable();
            }
        });

        llProxyHost.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View arg0)
            {
                selectProxyHost();
            }
        });

        llProxyPort.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View arg0)
            {
                selectProxyPort();
            }
        });

        llProxyUsername.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View arg0)
            {
                selectProxyUsername();
            }
        });

        llProxyPassword.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View arg0)
            {
                selectProxyPassword();
            }
        });

        llProxyAllowClearTextAuth.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View arg0)
            {
                selectProxyAllowClearTextAuth();
            }
        });

        llAdvancedCategory.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View arg0)
            {
                toggleSettingsCategory(llAdvancedGroup, imgAdvancedIndicator);
            }
        });

        llSynchronousDnsLookup.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View arg0)
            {
                selectSynchronousDnsLookup();
            }
        });

        llCustomMtu.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View arg0)
            {
                selectCustomMtu();
            }
        });

        llAutologinSessions.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View arg0)
            {
                selectAutologinSessions();
            }
        });

        llDisableClientCert.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View arg0)
            {
                selectDisableClientCert();
            }
        });

        llSslDebugLevel.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View arg0)
            {
                selectSslDebugLevel();
            }
        });

        llPrivateKeyPassword.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View arg0)
            {
                selectPrivateKeyPassword();
            }
        });

        llDefaultKeyDirection.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View arg0)
            {
                selectDefaultKeyDirection();
            }
        });

        llForceAesCbcCiphers.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View arg0)
            {
                selectForceAesCbcCiphers();
            }
        });

        llTlsCertProfile.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View arg0)
            {
                selectTlsCertProfile();
            }
        });

        llCustomDirectives.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View arg0)
            {
                selectCustomDirectives();
            }
        });

        btnResetOptions.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View arg0)
            {
                resetToDefaultOptions();
            }
        });

        setupSettingControls();
    }

    @Override
    public void onBackPressed()
    {
        if(settingsAreValid())
        {
            setResult(settingsResult, null);

            finish();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        switch(requestCode)
        {
            case ACTIVITY_RESULT_PACKAGE_CHOOSER:
            {
                if(data != null)
                {
                    settingsManager.setSystemApplicationFilter(data.getStringExtra(PackageChooserActivity.PARAM_PACKAGES));

                    settingsResult = RESULT_OK;
                }
            }
            break;
        }
    }

    private void setupSettingControls()
    {
        swAirVPNRememberMe.setChecked(settingsManager.isAirVPNRememberMe());
        swAirVPNForbidQuickConnectToUserCountry.setChecked(settingsManager.isAirVPNForbidQuickConnectionToUserCountry());
        swVpnTunPersist.setChecked(settingsManager.isOvpn3TunPersist());
        swVpnLock.setChecked(settingsManager.isVPNLockEnabled());
        swVpnReconnect.setChecked(settingsManager.isVPNReconnectEnabled());
        swDnsOverride.setChecked(settingsManager.isSystemDNSOverrideEnable());
        swExcludeLocalNetworks.setChecked(settingsManager.areLocalNetworksExcluded());
        swPauseVpnWhenScreeIsOff.setChecked(settingsManager.isSystemPauseVpnWhenScreenIsOff());
        swPersistentNotification.setChecked(settingsManager.isSystemPersistentNotification());
        swNotificationSound.setChecked(settingsManager.isSystemNotificationSound());
        swShowMessageDialogs.setChecked(settingsManager.areMessageDialogsEnabled());
        swRestoreLastProfile.setChecked(settingsManager.isSystemRestoreLastProfile());
        swProxyEnable.setChecked(settingsManager.isSystemProxyEnabled());
        swProxyAllowClearTextAuth.setChecked(settingsManager.isOvpn3ProxyAllowCleartextAuth());
        swSynchronousDnsLookup.setChecked(settingsManager.isOvpn3SynchronousDNSLookup());
        swAutologinSessions.setChecked(settingsManager.isOvpn3AutologinSessions());
        swDisableClientCert.setChecked(settingsManager.isOvpn3DisableClientCert());
        swForceAesCbcCiphers.setChecked(settingsManager.isOvpn3AESCBCCiphersuitesForced());

        if(settingsManager.isAirVPNRememberMe())
        {
            llAirVPNUserName.setVisibility(View.VISIBLE);
            llAirVPNPassword.setVisibility(View.VISIBLE);
        }
        else
        {
            llAirVPNUserName.setVisibility(View.GONE);
            llAirVPNPassword.setVisibility(View.GONE);
        }

        if(settingsManager.isVPNLockEnabled())
        {
            llVpnReconnect.setVisibility(View.GONE);
            llVpnReconnectionRetries.setVisibility(View.GONE);
        }
        else
        {
            llVpnReconnect.setVisibility(View.VISIBLE);

            if(settingsManager.isVPNReconnectEnabled())
                llVpnReconnectionRetries.setVisibility(View.VISIBLE);
            else
                llVpnReconnectionRetries.setVisibility(View.GONE);
        }

        if(settingsManager.isSystemDNSOverrideEnable())
            llDnsOverrideSettings.setVisibility(View.VISIBLE);
        else
            llDnsOverrideSettings.setVisibility(View.GONE);

        if(settingsManager.isSystemProxyEnabled())
            llProxySettings.setVisibility(View.VISIBLE);
        else
            llProxySettings.setVisibility(View.GONE);

        if(settingsManager.getSystemApplicationFilterType().equals(SettingsManager.SYSTEM_OPTION_APPLICATION_FILTER_TYPE_NONE))
            llApplicationFilter.setVisibility(View.GONE);
        else
            llApplicationFilter.setVisibility(View.VISIBLE);

        if(settingsManager.getSystemApplicationFilterType().equals(SettingsManager.SYSTEM_OPTION_APPLICATION_FILTER_TYPE_WHITELIST))
            txtApplicationFilterTitle.setText(getResources().getString(R.string.settings_system_application_whitelist_title));
        else
            txtApplicationFilterTitle.setText(getResources().getString(R.string.settings_system_application_blacklist_title));
    }

    private void selectAirVPNMasterPassword()
    {
        final Runnable uiRunnable = new Runnable()
        {
            @Override
            public void run()
            {
                if(settingsManager.getAirVPNMasterPasswordHashCode() == settingsManager.AIRVPN_MASTER_PASSWORD_HASHCODE_DEFAULT)
                    airVPNUser.setMasterPassword();
                else
                {
                    if(!airVPNUser.checkMasterPassword(AirVPNUser.CheckPasswordMode.ASK_PASSWORD))
                        return;

                    airVPNUser.setMasterPassword();
                }
            }
        };

        executeOption(uiRunnable);
    }

    private void selectAirVPNUserName()
    {
        final Runnable uiRunnable = new Runnable()
        {
            @Override
            public void run()
            {
                String value = "";

                if(!airVPNUser.checkMasterPassword(AirVPNUser.CheckPasswordMode.USE_CURRENT_PASSWORD))
                    return;

                value = supportTools.getTextOptionDialog(R.string.settings_airvpn_username_summary,
                                                         airVPNUser.getUserName(),
                                                         SupportTools.EditOption.ALLOW_EMPTY_FIELD);

                if(!value.equals(airVPNUser.getUserName()))
                {
                    airVPNUser.logout();

                    airVPNUser.setUserName(value);

                    settingsResult = RESULT_OK;
                }
            }
        };

        executeOption(uiRunnable);
    }

    private void selectAirVPNPassword()
    {
        final Runnable uiRunnable = new Runnable()
        {
            @Override
            public void run()
            {
                String value = "";

                if(!airVPNUser.checkMasterPassword(AirVPNUser.CheckPasswordMode.USE_CURRENT_PASSWORD))
                    return;

                value = supportTools.getPasswordOptionDialog(R.string.settings_airvpn_password_summary,
                                                             airVPNUser.getUserPassword(),
                                                             SupportTools.EditOption.ALLOW_EMPTY_FIELD);

                if(!value.equals(airVPNUser.getUserPassword()))
                {
                    airVPNUser.logout();

                    airVPNUser.setUserPassword(value);

                    settingsResult = RESULT_OK;
                }
            }
        };

        executeOption(uiRunnable);
    }

    private void selectAirVPNRememberMe()
    {
        final Runnable uiRunnable = new Runnable()
        {
            @Override
            public void run()
            {
                if(!airVPNUser.checkMasterPassword(AirVPNUser.CheckPasswordMode.USE_CURRENT_PASSWORD))
                    return;

                if(settingsManager.isAirVPNRememberMe())
                {
                    if(supportTools.confirmationDialog(R.string.settings_airvpn_remember_me_warning))
                    {
                        settingsManager.setAirVPNRememberMe(false);

                        llAirVPNUserName.setVisibility(View.GONE);
                        llAirVPNPassword.setVisibility(View.GONE);

                        airVPNUser.forgetAirVPNCredentials();

                        settingsResult = RESULT_OK;
                    }
                }
                else
                {
                    settingsManager.setAirVPNRememberMe(true);

                    airVPNUser.saveUserCredentials();

                    llAirVPNUserName.setVisibility(View.VISIBLE);
                    llAirVPNPassword.setVisibility(View.VISIBLE);
                }

                swAirVPNRememberMe.setChecked(settingsManager.isAirVPNRememberMe());
            }
        };

        executeOption(uiRunnable);
    }

    private void selectAirVPNDefaultProtocol()
    {
        final Runnable uiRunnable = new Runnable()
        {
            @Override
            public void run()
            {
                String value = "";

                value = supportTools.getOptionFromListDialog(R.string.settings_airvpn_default_protocol_title,
                                                             R.array.airvpn_protocol_labels,
                                                             R.array.airvpn_protocol_values,
                                                             settingsManager.getAirVPNDefaultProtocol());

                if(!value.equals(settingsManager.getAirVPNDefaultProtocol()))
                {
                    settingsManager.setAirVPNDefaultProtocol(value);

                    settingsResult = RESULT_OK;
                }
            }
        };

        executeOption(uiRunnable);
    }

    private void selectAirVPNDefaultPort()
    {
        final Runnable uiRunnable = new Runnable()
        {
            @Override
            public void run()
            {
                String value = "";
                int intVal = 0;

                value = supportTools.getOptionFromListDialog(R.string.settings_airvpn_default_port_title,
                                                             R.array.airvpn_port_labels,
                                                             R.array.airvpn_port_values,
                                                             String.format("%d", settingsManager.getAirVPNDefaultPort()));

                try
                {
                    intVal = Integer.parseInt(value);
                }
                catch(NumberFormatException e)
                {
                    intVal = settingsManager.AIRVPN_PORT_DEFAULT;
                }

                if(intVal != settingsManager.getAirVPNDefaultPort())
                {
                    settingsManager.setAirVPNDefaultPort(intVal);

                    settingsResult = RESULT_OK;
                }
            }
        };

        executeOption(uiRunnable);
    }

    private void selectAirVPNDefaultIPVersion()
    {
        final Runnable uiRunnable = new Runnable()
        {
            @Override
            public void run()
            {
                String value = "";

                value = supportTools.getOptionFromListDialog(R.string.settings_airvpn_default_ip_version_title,
                                                             R.array.airvpn_ip_version_labels,
                                                             R.array.airvpn_ip_version_values,
                                                             settingsManager.getAirVPNDefaultIPVersion());

                if(!value.equals(settingsManager.getAirVPNDefaultIPVersion()))
                {
                    settingsManager.setAirVPNDefaultIPVersion(value);

                    settingsResult = RESULT_OK;
                }
            }
        };

        executeOption(uiRunnable);
    }

    private void selectAirVPNDefaultTLSMode()
    {
        final Runnable uiRunnable = new Runnable()
        {
            @Override
            public void run()
            {
                String value = "";

                value = supportTools.getOptionFromListDialog(R.string.settings_airvpn_default_tls_mode_title,
                                                             R.array.airvpn_tls_mode_labels,
                                                             R.array.airvpn_tls_mode_values,
                                                             settingsManager.getAirVPNDefaultTLSMode());

                if(!value.equals(settingsManager.getAirVPNDefaultTLSMode()))
                {
                    settingsManager.setAirVPNDefaultTLSMode(value);

                    settingsResult = RESULT_OK;
                }
            }
        };

        executeOption(uiRunnable);
    }

    private void selectAirVPNQuickConnectMode()
    {
        final Runnable uiRunnable = new Runnable()
        {
            @Override
            public void run()
            {
                String value = "";

                value = supportTools.getOptionFromListDialog(R.string.settings_airvpn_quick_connect_mode_title,
                                                             R.array.airvpn_quick_connect_mode_labels,
                                                             R.array.airvpn_quick_connect_mode_values,
                                                             settingsManager.getAirVPNQuickConnectMode());

                if(!value.equals(settingsManager.getAirVPNQuickConnectMode()))
                {
                    settingsManager.setAirVPNQuickConnectMode(value);

                    settingsResult = RESULT_OK;
                }
            }
        };

        executeOption(uiRunnable);
    }

    private void selectAirVPNEncryptionAlgorithm()
    {
        final Runnable uiRunnable = new Runnable()
        {
            @Override
            public void run()
            {
                String value = "";

                value = supportTools.getOptionFromListDialog(R.string.settings_airvpn_encryption_algorithm_title,
                                                             R.array.airvpn_encryption_algorithm_labels,
                                                             R.array.airvpn_encryption_algorithm_values,
                                                             settingsManager.getAirVPNCipher());

                if(!value.equals(settingsManager.getAirVPNCipher()))
                {
                    settingsManager.setAirVPNCipher(value);

                    if(value.equals(SettingsManager.AIRVPN_CIPHER_AES_256_CBC))
                        settingsManager.setOvpn3ForceAESCBCCiphersuites(true);
                    else
                        settingsManager.setOvpn3ForceAESCBCCiphersuites(false);

                    swForceAesCbcCiphers.setChecked(settingsManager.isOvpn3AESCBCCiphersuitesForced());

                    settingsResult = RESULT_OK;
                }
            }
        };

        executeOption(uiRunnable);
    }

    private void selectAirVPNForbidQuickConnectToUserCountry()
    {
        final Runnable uiRunnable = new Runnable()
        {
            @Override
            public void run()
            {
                if(settingsManager.isAirVPNForbidQuickConnectionToUserCountry())
                {
                    if(supportTools.confirmationDialog(R.string.settings_airvpn_forbid_quick_connect_to_user_country_warning))
                    {
                        settingsManager.setAirVPNForbidQuickConnectionToUserCountry(false);

                        settingsResult = RESULT_OK;
                    }
                }
                else
                    settingsManager.setAirVPNForbidQuickConnectionToUserCountry(true);

                swAirVPNForbidQuickConnectToUserCountry.setChecked(settingsManager.isAirVPNForbidQuickConnectionToUserCountry());
            }
        };

        executeOption(uiRunnable);
    }

    private void selectAirVPNCustomBootstrap()
    {
        final Runnable uiRunnable = new Runnable()
        {
            @Override
            public void run()
            {
                String value = "";

                value = supportTools.getTextOptionDialog(R.string.setting_airvpn_custom_bootstrap_title,
                                                         settingsManager.getAirVPNCustomBootstrap(),
                                                         SupportTools.EditOption.ALLOW_EMPTY_FIELD);

                if(!value.equals(settingsManager.getAirVPNCustomBootstrap()))
                {
                    settingsManager.setAirVPNCustomBootstrap(value);

                    settingsResult = RESULT_OK;
                }
            }
        };

        executeOption(uiRunnable);
    }

    private void selectVpnMinimumTLSVersion()
    {
        final Runnable uiRunnable = new Runnable()
        {
            @Override
            public void run()
            {
                String value = "";

                value = supportTools.getOptionFromListDialog(R.string.settings_ovpn3_tls_min_version_title,
                                                             R.array.settings_ovpn3_tls_min_version_labels,
                                                             R.array.settings_ovpn3_tls_min_version_values,
                                                             settingsManager.getOvpn3TLSMinVersion());

                if(!value.equals(settingsManager.getOvpn3TLSMinVersion()))
                {
                    settingsManager.setOvpn3TLSMinVersion(value);

                    settingsResult = RESULT_OK;
                }
            }
        };

        executeOption(uiRunnable);
    }

    private void selectVpnTransportProtocol()
    {
        final Runnable uiRunnable = new Runnable()
        {
            @Override
            public void run()
            {
                String value = "";

                value = supportTools.getOptionFromListDialog(R.string.settings_ovpn3_protocol_title,
                                                             R.array.settings_ovpn3_protocol_labels,
                                                             R.array.settings_ovpn3_protocol_values,
                                                             settingsManager.getOvpn3Protocol());

                if(!value.equals(settingsManager.getOvpn3Protocol()))
                {
                    settingsManager.setOvpn3Protocol(value);

                    settingsResult = RESULT_OK;
                }
            }
        };

        executeOption(uiRunnable);
    }

    private void selectVpnIPV6()
    {
        final Runnable uiRunnable = new Runnable()
        {
            @Override
            public void run()
            {
                String value = "";

                value = supportTools.getOptionFromListDialog(R.string.settings_ovpn3_ipv6_title,
                                                             R.array.settings_ovpn3_ipv6_labels,
                                                             R.array.settings_ovpn3_ipv6_values,
                                                             settingsManager.getOvpn3IPV6());

                if(!value.equals(settingsManager.getOvpn3IPV6()))
                {
                    settingsManager.setOvpn3IPV6(value);

                    settingsResult = RESULT_OK;
                }
            }
        };

        executeOption(uiRunnable);
    }

    private void selectVpnTimeout()
    {
        final Runnable uiRunnable = new Runnable()
        {
            @Override
            public void run()
            {
                String value = "";

                value = supportTools.getOptionFromListDialog(R.string.settings_ovpn3_timeout_title,
                                                             R.array.settings_ovpn3_timeout_labels,
                                                             R.array.settings_ovpn3_timeout_values,
                                                             settingsManager.getOvpn3Timeout());

                if(!value.equals(settingsManager.getOvpn3Timeout()))
                {
                    settingsManager.setOvpn3Timeout(value);

                    settingsResult = RESULT_OK;
                }
            }
        };

        executeOption(uiRunnable);
    }

    private void selectVpnTunPersist()
    {
        settingsManager.setOvpn3TunPersist(!settingsManager.isOvpn3TunPersist());

        swVpnTunPersist.setChecked(settingsManager.isOvpn3TunPersist());

        if(!settingsManager.isOvpn3TunPersist())
            supportTools.infoDialog(R.string.settings_tun_persist_warning, true);

        settingsResult = RESULT_OK;
    }

    private void selectVpnCompressionMode()
    {
        final Runnable uiRunnable = new Runnable()
        {
            @Override
            public void run()
            {
                String value = "";

                value = supportTools.getOptionFromListDialog(R.string.settings_ovpn3_compression_mode_title,
                                                             R.array.settings_ovpn3_compression_mode_labels,
                                                             R.array.settings_ovpn3_compression_mode_values,
                                                             settingsManager.getOvpn3CompressionMode());

                if(!value.equals(settingsManager.getOvpn3CompressionMode()))
                {
                    settingsManager.setOvpn3CompressionMode(value);

                    settingsResult = RESULT_OK;
                }
            }
        };

        executeOption(uiRunnable);
    }

    private void selectVpnLock()
    {
        if(settingsManager.isVPNLockEnabled())
        {
            if(supportTools.confirmationDialog(R.string.settings_vpn_lock_warning) == false)
                return;
        }

        settingsManager.setVPNLock(!settingsManager.isVPNLockEnabled());

        swVpnLock.setChecked(settingsManager.isVPNLockEnabled());

        settingsManager.setVPNReconnectionRetries(SettingsManager.SYSTEM_OPTION_VPN_RECONNECTION_RETRIES_DEFAULT);

        if(settingsManager.isVPNLockEnabled())
        {
            llVpnReconnect.setVisibility(View.GONE);
            llVpnReconnectionRetries.setVisibility(View.GONE);

            settingsManager.setVPNReconnect(false);
            swVpnReconnect.setChecked(false);
            llVpnReconnectionRetries.setVisibility(View.GONE);
        }
        else
        {
            llVpnReconnect.setVisibility(View.VISIBLE);

            settingsManager.setVPNReconnect(true);
            swVpnReconnect.setChecked(true);
            llVpnReconnectionRetries.setVisibility(View.VISIBLE);
        }

        settingsResult = RESULT_OK;
    }

    private void selectVpnReconnect()
    {
        settingsManager.setVPNReconnect(!settingsManager.isVPNReconnectEnabled());

        swVpnReconnect.setChecked(settingsManager.isVPNReconnectEnabled());

        if(settingsManager.isVPNReconnectEnabled())
            llVpnReconnectionRetries.setVisibility(View.VISIBLE);
        else
            llVpnReconnectionRetries.setVisibility(View.GONE);

        settingsResult = RESULT_OK;
    }

    private void selectVpnReconnectionRetries()
    {
        final Runnable uiRunnable = new Runnable()
        {
            @Override
            public void run()
            {
                String value = "";

                value = supportTools.getOptionFromListDialog(R.string.settings_vpn_reconnect_retries_title,
                                                             R.array.settings_vpn_reconnect_retries_labels,
                                                             R.array.settings_vpn_reconnect_retries_values,
                                                             settingsManager.getVPNReconnectionRetries());

                if(!value.equals(settingsManager.getVPNReconnectionRetries()))
                {
                    settingsManager.setVPNReconnectionRetries(value);

                    settingsResult = RESULT_OK;
                }
            }
        };

        executeOption(uiRunnable);
    }

    private void selectDnsOverrideSettings()
    {
        settingsManager.setSystemDNSOverrideEnable(!settingsManager.isSystemDNSOverrideEnable());

        swDnsOverride.setChecked(settingsManager.isSystemDNSOverrideEnable());

        if(settingsManager.isSystemDNSOverrideEnable())
            llDnsOverrideSettings.setVisibility(View.VISIBLE);
        else
            llDnsOverrideSettings.setVisibility(View.GONE);

        settingsResult = RESULT_OK;
    }

    private void selectCustomDns()
    {
        final Runnable uiRunnable = new Runnable()
        {
            @Override
            public void run()
            {
                String value = "";

                value = supportTools.getTextOptionDialog(R.string.settings_system_dns_custom_title,
                                                         settingsManager.getSystemCustomDNS(),
                                                         SupportTools.EditOption.ALLOW_EMPTY_FIELD);

                if(!value.equals(settingsManager.getSystemCustomDNS()))
                {
                    settingsManager.setSystemCustomDNS(value);

                    settingsResult = RESULT_OK;
                }
            }
        };

        executeOption(uiRunnable);
    }

    private void selectVpnUsername()
    {
        final Runnable uiRunnable = new Runnable()
        {
            @Override
            public void run()
            {
                String value = "";

                value = supportTools.getTextOptionDialog(R.string.settings_ovpn3_username_title,
                                                         settingsManager.getOvpn3Username(),
                                                         SupportTools.EditOption.ALLOW_EMPTY_FIELD);

                if(!value.equals(settingsManager.getOvpn3Username()))
                {
                    settingsManager.setOvpn3Username(value);

                    settingsResult = RESULT_OK;
                }
            }
        };

        executeOption(uiRunnable);
    }

    private void selectVpnPassword()
    {
        final Runnable uiRunnable = new Runnable()
        {
            @Override
            public void run()
            {
                String value = "";

                value = supportTools.getPasswordOptionDialog(R.string.settings_ovpn3_password_title,
                                                             settingsManager.getOvpn3Password(),
                                                             SupportTools.EditOption.ALLOW_EMPTY_FIELD);

                if(!value.equals(settingsManager.getOvpn3Password()))
                {
                    settingsManager.setOvpn3Password(value);

                    settingsResult = RESULT_OK;
                }
            }
        };

        executeOption(uiRunnable);
    }

    private void selectExcludeLocalNetworks()
    {
        settingsManager.setExcludeLocalNetworks(!settingsManager.areLocalNetworksExcluded());

        swExcludeLocalNetworks.setChecked(settingsManager.areLocalNetworksExcluded());

        settingsResult = RESULT_OK;
    }

    private void selectPauseVpnWhenScreenIsOff()
    {
        settingsManager.setSystemPauseVpnWhenScreenIsOff(!settingsManager.isSystemPauseVpnWhenScreenIsOff());

        swPauseVpnWhenScreeIsOff.setChecked(settingsManager.isSystemPauseVpnWhenScreenIsOff());

        if(settingsManager.isSystemPauseVpnWhenScreenIsOff())
            supportTools.infoDialog(R.string.settings_pause_vpn_when_screen_is_off_warning, true);

        settingsResult = RESULT_OK;
    }

    private void selectPersistentNotification()
    {
        settingsManager.setSystemPersistentNotification(!settingsManager.isSystemPersistentNotification());

        swPersistentNotification.setChecked(settingsManager.isSystemPersistentNotification());

        if(!settingsManager.isSystemPersistentNotification())
            supportTools.infoDialog(R.string.settings_persistent_notification_warning, true);

        settingsResult = RESULT_OK;
    }

    private void selectNotificationSound()
    {
        settingsManager.setSystemNotificationSound(!settingsManager.isSystemNotificationSound());

        swNotificationSound.setChecked(settingsManager.isSystemNotificationSound());
    }

    private void selectNotificationChannel()
    {
        String channelId = getResources().getString(R.string.notification_channel_id);

        Intent channelSettingsIntent = new Intent(Settings.ACTION_CHANNEL_NOTIFICATION_SETTINGS);

        if(channelSettingsIntent != null)
        {
            channelSettingsIntent.putExtra(Settings.EXTRA_APP_PACKAGE, getPackageName());
            channelSettingsIntent.putExtra(Settings.EXTRA_CHANNEL_ID, channelId);
            channelSettingsIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);

            startActivity(channelSettingsIntent);
        }
    }

    private void selectShowMessageDialogs()
    {
        settingsManager.setMessageDialogsEnabled(!settingsManager.areMessageDialogsEnabled());

        swShowMessageDialogs.setChecked(settingsManager.areMessageDialogsEnabled());
    }

    private void selectRestoreLastProfile()
    {
        settingsManager.setSystemRestoreLastProfile(!settingsManager.isSystemRestoreLastProfile());

        swRestoreLastProfile.setChecked(settingsManager.isSystemRestoreLastProfile());

        settingsResult = RESULT_OK;
    }

    private void selectApplicationFilterType()
    {
        final Runnable uiRunnable = new Runnable()
        {
            @Override
            public void run()
            {
                String value = "";

                value = supportTools.getOptionFromListDialog(R.string.settings_system_application_filter_type_title,
                                                             R.array.settings_system_application_filter_type_labels,
                                                             R.array.settings_system_application_filter_type_values,
                                                             settingsManager.getSystemApplicationFilterType());

                if(!value.equals(settingsManager.getSystemApplicationFilterType()))
                {
                    if(settingsManager.getSystemApplicationFilter() != "")
                    {
                        if(supportTools.confirmationDialog(R.string.settings_application_filter_type_change_warning) == false)
                            return;
                    }

                    settingsManager.setSystemApplicationFilterType(value);

                    if(settingsManager.getSystemApplicationFilterType().equals(SettingsManager.SYSTEM_OPTION_APPLICATION_FILTER_TYPE_NONE))
                        llApplicationFilter.setVisibility(View.GONE);
                    else
                    {
                        llApplicationFilter.setVisibility(View.VISIBLE);

                        if(settingsManager.getSystemApplicationFilterType().equals(SettingsManager.SYSTEM_OPTION_APPLICATION_FILTER_TYPE_WHITELIST))
                            txtApplicationFilterTitle.setText(getResources().getString(R.string.settings_system_application_whitelist_title));
                        else
                            txtApplicationFilterTitle.setText(getResources().getString(R.string.settings_system_application_blacklist_title));
                    }

                    settingsManager.setSystemApplicationFilter("");

                    settingsResult = RESULT_OK;
                }
            }
        };

        executeOption(uiRunnable);
    }

    private void selectApplicationFilter()
    {
        String title = "";

        Intent packageChooserIntent = new Intent(this, PackageChooserActivity.class);

        packageChooserIntent.putExtra(PackageChooserActivity.PARAM_PACKAGES, settingsManager.getSystemApplicationFilter());

        if(settingsManager.getSystemApplicationFilterType().equals(SettingsManager.SYSTEM_OPTION_APPLICATION_FILTER_TYPE_WHITELIST))
            title = getResources().getString(R.string.settings_application_whitelist_title);
        else
            title = getResources().getString(R.string.settings_application_blacklist_title);

        packageChooserIntent.putExtra(PackageChooserActivity.CHOOSER_TITLE, title);

        startActivityForResult(packageChooserIntent, ACTIVITY_RESULT_PACKAGE_CHOOSER);
    }

    private void selectApplicationLanguage()
    {
        final Runnable uiRunnable = new Runnable()
        {
            @Override
            public void run()
            {
                String value = "";

                value = supportTools.getOptionFromListDialog(R.string.settings_system_application_language_title,
                                                             R.array.settings_system_application_language_labels,
                                                             R.array.settings_system_application_language_values,
                                                             settingsManager.getSystemApplicationLanguage());

                if(!value.equals(settingsManager.getSystemApplicationLanguage()))
                {
                    settingsManager.setSystemApplicationLanguage(value);
                }
            }
        };

        executeOption(uiRunnable);
    }

    private void selectProxyEnable()
    {
        settingsManager.setSystemProxyEnabled(!settingsManager.isSystemProxyEnabled());

        swProxyEnable.setChecked(settingsManager.isSystemProxyEnabled());

        if(settingsManager.isSystemProxyEnabled())
            llProxySettings.setVisibility(View.VISIBLE);
        else
            llProxySettings.setVisibility(View.GONE);

        settingsResult = RESULT_OK;
    }

    private void selectProxyHost()
    {
        final Runnable uiRunnable = new Runnable()
        {
            @Override
            public void run()
            {
                String value = "";

                value = supportTools.getTextOptionDialog(R.string.settings_ovpn3_proxy_host_title,
                                                         settingsManager.getOvpn3ProxyHost(),
                                                         SupportTools.EditOption.ALLOW_EMPTY_FIELD);

                if(!value.equals(settingsManager.getOvpn3ProxyHost()))
                {
                    settingsManager.setOvpn3ProxyHost(value);

                    settingsResult = RESULT_OK;
                }
            }
        };

        executeOption(uiRunnable);
    }

    private void selectProxyPort()
    {
        final Runnable uiRunnable = new Runnable()
        {
            @Override
            public void run()
            {
                long value = 0;

                value = supportTools.getIpPortOptionDialog(R.string.settings_ovpn3_proxy_port_title,
                                                           settingsManager.getOvpn3ProxyPortValue(),
                                                           SupportTools.EditOption.ALLOW_ZERO_VALUE);

                if(value != settingsManager.getOvpn3ProxyPortValue())
                {
                    settingsManager.setOvpn3ProxyPortValue(value);

                    settingsResult = RESULT_OK;
                }
            }
        };

        executeOption(uiRunnable);
    }

    private void selectProxyUsername()
    {
        final Runnable uiRunnable = new Runnable()
        {
            @Override
            public void run()
            {
                String value = "";

                value = supportTools.getTextOptionDialog(R.string.settings_ovpn3_proxy_username_title,
                                                         settingsManager.getOvpn3ProxyUsername(),
                                                         SupportTools.EditOption.ALLOW_EMPTY_FIELD);

                if(!value.equals(settingsManager.getOvpn3ProxyUsername()))
                {
                    settingsManager.setOvpn3ProxyUsername(value);

                    settingsResult = RESULT_OK;
                }
            }
        };

        executeOption(uiRunnable);
    }

    private void selectProxyPassword()
    {
        final Runnable uiRunnable = new Runnable()
        {
            @Override
            public void run()
            {
                String value = "";

                value = supportTools.getPasswordOptionDialog(R.string.settings_ovpn3_proxy_password_title,
                                                             settingsManager.getOvpn3ProxyPassword(),
                                                             SupportTools.EditOption.ALLOW_EMPTY_FIELD);

                if(!value.equals(settingsManager.getOvpn3ProxyPassword()))
                {
                    settingsManager.setOvpn3ProxyPassword(value);

                    settingsResult = RESULT_OK;
                }
            }
        };

        executeOption(uiRunnable);
    }

    private void selectProxyAllowClearTextAuth()
    {
        settingsManager.setOvpn3ProxyAllowCleartextAuth(!settingsManager.isOvpn3ProxyAllowCleartextAuth());

        swProxyAllowClearTextAuth.setChecked(settingsManager.isOvpn3ProxyAllowCleartextAuth());

        settingsResult = RESULT_OK;
    }

    private void selectSynchronousDnsLookup()
    {
        settingsManager.setOvpn3SynchronousDNSLookup(!settingsManager.isOvpn3SynchronousDNSLookup());

        swSynchronousDnsLookup.setChecked(settingsManager.isOvpn3SynchronousDNSLookup());

        settingsResult = RESULT_OK;
    }

    private void selectCustomMtu()
    {
        final Runnable uiRunnable = new Runnable()
        {
            @Override
            public void run()
            {
                long value = 0;

                value = supportTools.getNumericOptionDialog(R.string.settings_system_custom_mtu_title,
                                                            settingsManager.getSystemCustomMTUValue(),
                                                            SupportTools.EditOption.ALLOW_ZERO_VALUE);

                if(value != settingsManager.getSystemCustomMTUValue())
                {
                    settingsManager.setSystemCustomMTUValue(value);

                    settingsResult = RESULT_OK;
                }
            }
        };

        executeOption(uiRunnable);
    }

    private void selectAutologinSessions()
    {
        settingsManager.setOvpn3AutologinSessions(!settingsManager.isOvpn3AutologinSessions());

        swAutologinSessions.setChecked(settingsManager.isOvpn3AutologinSessions());

        settingsResult = RESULT_OK;
    }

    private void selectDisableClientCert()
    {
        settingsManager.setOvpn3DisableClientCert(!settingsManager.isOvpn3DisableClientCert());

        swDisableClientCert.setChecked(settingsManager.isOvpn3DisableClientCert());

        settingsResult = RESULT_OK;
    }

    private void selectSslDebugLevel()
    {
        final Runnable uiRunnable = new Runnable()
        {
            @Override
            public void run()
            {
                long value = 0;

                value = supportTools.getNumericOptionDialog(R.string.settings_ovpn3_ssl_debug_level_title,
                                                            settingsManager.getOvpn3SSLDebugLevelValue(),
                                                            SupportTools.EditOption.ALLOW_ZERO_VALUE);

                if(value != settingsManager.getOvpn3SSLDebugLevelValue())
                {
                    settingsManager.setOvpn3SSLDebugLevelValue(value);

                    settingsResult = RESULT_OK;
                }
            }
        };

        executeOption(uiRunnable);
    }

    private void selectPrivateKeyPassword()
    {
        final Runnable uiRunnable = new Runnable()
        {
            @Override
            public void run()
            {
                String value = "";

                value = supportTools.getPasswordOptionDialog(R.string.settings_ovpn3_private_key_password_title,
                                                             settingsManager.getOvpn3PrivateKeyPassword(),
                                                             SupportTools.EditOption.ALLOW_EMPTY_FIELD);

                if(!value.equals(settingsManager.getOvpn3PrivateKeyPassword()))
                {
                    settingsManager.setOvpn3PrivateKeyPassword(value);

                    settingsResult = RESULT_OK;
                }
            }
        };

        executeOption(uiRunnable);
    }

    private void selectDefaultKeyDirection()
    {
        final Runnable uiRunnable = new Runnable()
        {
            @Override
            public void run()
            {
                String value = "";

                value = supportTools.getOptionFromListDialog(R.string.settings_ovpn3_default_key_direction_title,
                                                             R.array.settings_ovpn3_default_key_direction_labels,
                                                             R.array.settings_ovpn3_default_key_direction_values,
                                                             settingsManager.getOvpn3DefaultKeyDirection());

                if(!value.equals(settingsManager.getOvpn3DefaultKeyDirection()))
                {
                    settingsManager.setOvpn3DefaultKeyDirection(value);

                    settingsResult = RESULT_OK;
                }
            }
        };

        executeOption(uiRunnable);
    }

    private void selectForceAesCbcCiphers()
    {
        settingsManager.setOvpn3ForceAESCBCCiphersuites(!settingsManager.isOvpn3AESCBCCiphersuitesForced());

        swForceAesCbcCiphers.setChecked(settingsManager.isOvpn3AESCBCCiphersuitesForced());

        settingsResult = RESULT_OK;
    }

    private void selectTlsCertProfile()
    {
        final Runnable uiRunnable = new Runnable()
        {
            @Override
            public void run()
            {
                String value = "";

                value = supportTools.getTextOptionDialog(R.string.settings_ovpn3_tls_cert_profile_title,
                                                         settingsManager.getOvpn3TLSCertProfile(),
                                                         SupportTools.EditOption.ALLOW_EMPTY_FIELD);

                if(!value.equals(settingsManager.getOvpn3TLSCertProfile()))
                {
                    settingsManager.setOvpn3TLSCertProfile(value);

                    settingsResult = RESULT_OK;
                }
            }
        };

        executeOption(uiRunnable);
    }

    private void selectCustomDirectives()
    {
        final Runnable uiRunnable = new Runnable()
        {
            @Override
            public void run()
            {
                String value = "";

                value = supportTools.getTextOptionDialog(R.string.settings_ovpn3_custom_directives_title,
                                                         settingsManager.getOvpn3CustomDirectives(),
                                                         SupportTools.EditOption.ALLOW_EMPTY_FIELD);

                if(!value.equals(settingsManager.getOvpn3CustomDirectives()))
                {
                    settingsManager.setOvpn3CustomDirectives(value);

                    settingsResult = RESULT_OK;
                }
            }
        };

        executeOption(uiRunnable);
    }

    private void executeOption(Runnable runnable)
    {
        if(runnable == null)
            return;

        supportTools.runOnUiActivity(this, runnable);
    }

    private void resetToDefaultOptions()
    {
        if(supportTools.confirmationDialog(R.string.settings_reset_to_default))
        {
            settingsManager.setAirVPNRememberMe(SettingsManager.SYSTEM_AIRVPN_REMEMBER_ME_DEFAULT);
            settingsManager.setAirVPNDefaultProtocol(SettingsManager.AIRVPN_PROTOCOL_DEFAULT);
            settingsManager.setAirVPNDefaultPort(SettingsManager.AIRVPN_PORT_DEFAULT);
            settingsManager.setAirVPNDefaultIPVersion(SettingsManager.AIRVPN_IP_VERSION_DEFAULT);
            settingsManager.setAirVPNDefaultTLSMode(SettingsManager.AIRVPN_TLS_MODE_DEFAULT);
            settingsManager.setAirVPNQuickConnectMode(SettingsManager.AIRVPN_QUICK_CONNECT_MODE_DEFAULT);
            settingsManager.setAirVPNForbidQuickConnectionToUserCountry(SettingsManager.AIRVPN_FORBID_QUICK_CONNECTION_TO_USER_COUNTRY_DEFAULT);
            settingsManager.setAirVPNCustomBootstrap(SettingsManager.AIRVPN_CUSTOM_BOOTSTRAP_SERVERS_DEFAULT);
            settingsManager.setAirVPNCipher(SettingsManager.AIRVPN_CIPHER_DEFAULT);
            settingsManager.setOvpn3TLSMinVersion(SettingsManager.OVPN3_OPTION_TLS_MIN_VERSION_DEFAULT);
            settingsManager.setOvpn3Protocol(SettingsManager.OVPN3_OPTION_PROTOCOL_DEFAULT);
            settingsManager.setOvpn3IPV6(SettingsManager.OVPN3_OPTION_IPV6_DEFAULT);
            settingsManager.setOvpn3Timeout(SettingsManager.OVPN3_OPTION_TIMEOUT_DEFAULT);
            settingsManager.setOvpn3TunPersist(SettingsManager.OVPN3_OPTION_TUN_PERSIST_DEFAULT);
            settingsManager.setOvpn3CompressionMode(SettingsManager.OVPN3_OPTION_COMPRESSION_MODE_DEFAULT);
            settingsManager.setOvpn3Username(SettingsManager.OVPN3_OPTION_USERNAME_DEFAULT);
            settingsManager.setOvpn3Password(SettingsManager.OVPN3_OPTION_PASSWORD_DEFAULT);
            settingsManager.setOvpn3SynchronousDNSLookup(SettingsManager.OVPN3_OPTION_SYNCHRONOUS_DNS_LOOKUP_DEFAULT);
            settingsManager.setOvpn3AutologinSessions(SettingsManager.OVPN3_OPTION_AUTOLOGIN_SESSIONS_DEFAULT);
            settingsManager.setOvpn3DisableClientCert(SettingsManager.OVPN3_OPTION_DISABLE_CLIENT_CERT_DEFAULT);
            settingsManager.setOvpn3SSLDebugLevel(SettingsManager.OVPN3_OPTION_SSL_DEBUG_LEVEL_DEFAULT);
            settingsManager.setOvpn3PrivateKeyPassword(SettingsManager.OVPN3_OPTION_PRIVATE_KEY_PASSWORD_DEFAULT);
            settingsManager.setOvpn3DefaultKeyDirection(SettingsManager.OVPN3_OPTION_DEFAULT_KEY_DIRECTION_DEFAULT);
            settingsManager.setOvpn3ForceAESCBCCiphersuites(SettingsManager.OVPN3_OPTION_FORCE_AES_CBC_CIPHERSUITES_DEFAULT);
            settingsManager.setOvpn3TLSCertProfile(SettingsManager.OVPN3_OPTION_TLS_CERT_PROFILE_DEFAULT);
            settingsManager.setOvpn3ProxyHost(SettingsManager.OVPN3_OPTION_PROXY_HOST_DEFAULT);
            settingsManager.setOvpn3ProxyPort(SettingsManager.OVPN3_OPTION_PROXY_PORT_DEFAULT);
            settingsManager.setOvpn3ProxyUsername(SettingsManager.OVPN3_OPTION_PROXY_USERNAME_DEFAULT);
            settingsManager.setOvpn3ProxyPassword(SettingsManager.OVPN3_OPTION_PROXY_PASSWORD_DEFAULT);
            settingsManager.setOvpn3ProxyAllowCleartextAuth(SettingsManager.OVPN3_OPTION_PROXY_ALLOW_CLEARTEXT_AUTH_DEFAULT);
            settingsManager.setOvpn3CustomDirectives(SettingsManager.OVPN3_OPTION_CUSTOM_DIRECTIVES_DEFAULT);
            settingsManager.setVPNLock(SettingsManager.SYSTEM_OPTION_VPN_LOCK_DEFAULT);
            settingsManager.setVPNReconnect(SettingsManager.SYSTEM_OPTION_VPN_RECONNECT_DEFAULT);
            settingsManager.setVPNReconnectionRetries(SettingsManager.SYSTEM_OPTION_VPN_RECONNECTION_RETRIES_DEFAULT);
            settingsManager.setSystemDNSOverrideEnable(SettingsManager.SYSTEM_OPTION_DNS_OVERRIDE_ENABLE_DEFAULT);
            settingsManager.setSystemCustomDNS(SettingsManager.SYSTEM_OPTION_DNS_CUSTOM_DEFAULT);
            settingsManager.setSystemProxyEnabled(SettingsManager.SYSTEM_OPTION_PROXY_ENABLE_DEFAULT);
            settingsManager.setSystemPersistentNotification(SettingsManager.SYSTEM_OPTION_PERSISTENT_NOTIFICATION_DEFAULT);
            settingsManager.setSystemNotificationSound(SettingsManager.SYSTEM_OPTION_NOTIFICATION_SOUND_DEFAULT);
            settingsManager.setMessageDialogsEnabled(SettingsManager.SYSTEM_OPTION_SHOW_MESSAGE_DIALOGS_DEFAULT);
            settingsManager.setSystemCustomMTU(SettingsManager.SYSTEM_CUSTOM_MTU_DEFAULT);
            settingsManager.setSystemApplicationFilterType(SettingsManager.SYSTEM_OPTION_APPLICATION_FILTER_TYPE_DEFAULT);
            settingsManager.setSystemApplicationFilter(SettingsManager.SYSTEM_OPTION_APPLICATION_FILTER_DEFAULT);
            settingsManager.setSystemApplicationLanguage(SettingsManager.SYSTEM_OPTION_APPLICATION_LANGUAGE_DEFAULT);
            settingsManager.setSystemRestoreLastProfile(SettingsManager.SYSTEM_OPTION_RESTORE_LAST_PROFILE_DEFAULT);
            settingsManager.setExcludeLocalNetworks(SettingsManager.SYSTEM_OPTION_EXCLUDE_LOCAL_NETWORKS_DEFAULT);
            settingsManager.setSystemPauseVpnWhenScreenIsOff(SettingsManager.SYSTEM_OPTION_PAUSE_VPN_WHEN_SCREEN_IS_OFF_DEFAULT);

            setupSettingControls();

            supportTools.infoDialog(R.string.settings_reset_to_default_done, true);

            settingsResult = RESULT_OK;
        }
    }

    private boolean settingsAreValid()
    {
        boolean valid = true;

        if(settingsManager.isSystemDNSOverrideEnable() && settingsManager.getSystemCustomDNS().isEmpty())
        {
            supportTools.infoDialog(R.string.settings_error_no_custom_dns, true);

            valid = false;
        }

        return valid;
    }

    private void toggleSettingsCategory(LinearLayout llCategory, ImageView imgIndicator)
    {
        if(llCategory.isShown())
        {
            llCategory.setVisibility(View.GONE);

            imgIndicator.setImageDrawable(getDrawable(R.drawable.arrow_right));
        }
        else
        {
            llCategory.setVisibility(View.VISIBLE);

            imgIndicator.setImageDrawable(getDrawable(R.drawable.arrow_down));
        }
    }
}
