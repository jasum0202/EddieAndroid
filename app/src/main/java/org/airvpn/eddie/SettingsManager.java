// <eddie_source_header>
// This file is part of Eddie/AirVPN software.
// Copyright (C)2014-2018 AirVPN (support@airvpn.org) / https://airvpn.org
//
// Eddie is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Eddie is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Eddie. If not, see <http://www.gnu.org/licenses/>.
// </eddie_source_header>
//
// 20 June 2018 - author: ProMIND - initial release. Based on revised code from com.eddie.android. (a tribute to the 1859 Perugia uprising occurred on 20 June 1859 and in memory of those brave inhabitants who fought for the liberty of Perugia)

package org.airvpn.eddie;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Base64;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class SettingsManager
{
    public static final String OVPN3_OPTION_TLS_MIN_VERSION = "ovpn3_tls_version_min";
    public static final String OVPN3_OPTION_TLS_MIN_VERSION_NATIVE = "tls_version_min";
    public static final String OVPN3_OPTION_TLS_MIN_VERSION_DEFAULT = "tls_1_0";

    public static final String OVPN3_OPTION_PROTOCOL = "ovpn3_protocol";
    public static final String OVPN3_OPTION_PROTOCOL_NATIVE = "protocol";
    public static final String OVPN3_OPTION_PROTOCOL_DEFAULT = "";

    public static final String OVPN3_OPTION_IPV6 = "ovpn3_ipv6";
    public static final String OVPN3_OPTION_IPV6_NATIVE = "ipv6";
    public static final String OVPN3_OPTION_IPV6_DEFAULT = "";

    public static final String OVPN3_OPTION_TIMEOUT = "ovpn3_timeout";
    public static final String OVPN3_OPTION_TIMEOUT_NATIVE = "timeout";
    public static final String OVPN3_OPTION_TIMEOUT_DEFAULT = "60";

    public static final String OVPN3_OPTION_TUN_PERSIST = "ovpn3_tun_persist";
    public static final String OVPN3_OPTION_TUN_PERSIST_NATIVE = "tun_persist";
    public static final boolean OVPN3_OPTION_TUN_PERSIST_DEFAULT = true;

    public static final String OVPN3_OPTION_COMPRESSION_MODE = "ovpn3_compression_mode";
    public static final String OVPN3_OPTION_COMPRESSION_MODE_NATIVE = "compression_mode";
    public static final String OVPN3_OPTION_COMPRESSION_MODE_DEFAULT = "yes";

    public static final String SYSTEM_OPTION_VPN_LOCK = "system_vpn_lock";
    public static final boolean SYSTEM_OPTION_VPN_LOCK_DEFAULT = true;

    public static final String SYSTEM_OPTION_VPN_RECONNECT = "system_vpn_reconnect";
    public static final boolean SYSTEM_OPTION_VPN_RECONNECT_DEFAULT = false;

    public static final String SYSTEM_OPTION_VPN_RECONNECTION_RETRIES = "system_vpn_reconnect_retries";
    public static final String SYSTEM_OPTION_VPN_RECONNECTION_RETRIES_DEFAULT = "5";

    public static final String OVPN3_OPTION_USERNAME = "ovpn3_username";
    public static final String OVPN3_OPTION_USERNAME_NATIVE = "username";
    public static final String OVPN3_OPTION_USERNAME_DEFAULT = "";

    public static final String OVPN3_OPTION_PASSWORD = "ovpn3_password";
    public static final String OVPN3_OPTION_PASSWORD_NATIVE = "password";
    public static final String OVPN3_OPTION_PASSWORD_DEFAULT = "";

    public static final String OVPN3_OPTION_SYNCHRONOUS_DNS_LOOKUP = "ovpn3_synchronous_dns_lookup";
    public static final String OVPN3_OPTION_SYNCHRONOUS_DNS_LOOKUP_NATIVE = "synchronous_dns_lookup";
    public static final boolean OVPN3_OPTION_SYNCHRONOUS_DNS_LOOKUP_DEFAULT = false;

    public static final String OVPN3_OPTION_AUTOLOGIN_SESSIONS = "ovpn3_autologin_sessions";
    public static final String OVPN3_OPTION_AUTOLOGIN_SESSIONS_NATIVE = "autologin_sessions";
    public static final boolean OVPN3_OPTION_AUTOLOGIN_SESSIONS_DEFAULT = true;

    public static final String OVPN3_OPTION_DISABLE_CLIENT_CERT = "ovpn3_disable_client_cert";
    public static final String OVPN3_OPTION_DISABLE_CLIENT_CERT_NATIVE = "disable_client_cert";
    public static final boolean OVPN3_OPTION_DISABLE_CLIENT_CERT_DEFAULT = false;

    public static final String OVPN3_OPTION_SSL_DEBUG_LEVEL = "ovpn3_ssl_debug_level";
    public static final String OVPN3_OPTION_SSL_DEBUG_LEVEL_NATIVE = "ssl_debug_level";
    public static final String OVPN3_OPTION_SSL_DEBUG_LEVEL_DEFAULT = "0";

    public static final String OVPN3_OPTION_PRIVATE_KEY_PASSWORD = "ovpn3_private_key_password";
    public static final String OVPN3_OPTION_PRIVATE_KEY_PASSWORD_NATIVE = "private_key_password";
    public static final String OVPN3_OPTION_PRIVATE_KEY_PASSWORD_DEFAULT = "";

    public static final String OVPN3_OPTION_DEFAULT_KEY_DIRECTION = "ovpn3_default_key_direction";
    public static final String OVPN3_OPTION_DEFAULT_KEY_DIRECTION_NATIVE = "default_key_direction";
    public static final String OVPN3_OPTION_DEFAULT_KEY_DIRECTION_DEFAULT = "-1";

    public static final String OVPN3_OPTION_FORCE_AES_CBC_CIPHERSUITES = "ovpn3_force_aes_cbc_ciphersuites";
    public static final String OVPN3_OPTION_FORCE_AES_CBC_CIPHERSUITES_NATIVE = "force_aes_cbc_ciphersuites";
    public static final boolean OVPN3_OPTION_FORCE_AES_CBC_CIPHERSUITES_DEFAULT = false;

    public static final String OVPN3_OPTION_TLS_CERT_PROFILE = "ovpn3_tls_cert_profile";
    public static final String OVPN3_OPTION_TLS_CERT_PROFILE_NATIVE = "tls_cert_profile";
    public static final String OVPN3_OPTION_TLS_CERT_PROFILE_DEFAULT = "";

    public static final String OVPN3_OPTION_PROXY_HOST = "ovpn3_proxy_host";
    public static final String OVPN3_OPTION_PROXY_HOST_NATIVE = "proxy_host";
    public static final String OVPN3_OPTION_PROXY_HOST_DEFAULT = "";

    public static final String OVPN3_OPTION_PROXY_PORT = "ovpn3_proxy_port";
    public static final String OVPN3_OPTION_PROXY_PORT_NATIVE = "proxy_port";
    public static final String OVPN3_OPTION_PROXY_PORT_DEFAULT = "";

    public static final String OVPN3_OPTION_PROXY_USERNAME = "ovpn3_proxy_username";
    public static final String OVPN3_OPTION_PROXY_USERNAME_NATIVE = "proxy_username";
    public static final String OVPN3_OPTION_PROXY_USERNAME_DEFAULT = "";

    public static final String OVPN3_OPTION_PROXY_PASSWORD = "ovpn3_proxy_password";
    public static final String OVPN3_OPTION_PROXY_PASSWORD_NATIVE = "proxy_password";
    public static final String OVPN3_OPTION_PROXY_PASSWORD_DEFAULT = "";

    public static final String OVPN3_OPTION_PROXY_ALLOW_CLEARTEXT_AUTH = "ovpn3_proxy_allow_cleartext_auth";
    public static final String OVPN3_OPTION_PROXY_ALLOW_CLEARTEXT_AUTH_NATIVE = "proxy_allow_cleartext_auth";
    public static final boolean OVPN3_OPTION_PROXY_ALLOW_CLEARTEXT_AUTH_DEFAULT = false;

    public static final String OVPN3_OPTION_CUSTOM_DIRECTIVES = "ovpn3_custom_directives";
    public static final String OVPN3_OPTION_CUSTOM_DIRECTIVES_DEFAULT = "";

    public static final String SYSTEM_OPTION_DNS_OVERRIDE_ENABLE = "system_dns_override_enable";
    public static final boolean SYSTEM_OPTION_DNS_OVERRIDE_ENABLE_DEFAULT = false;

    public static final String SYSTEM_OPTION_DNS_CUSTOM = "system_dns_custom";
    public static final String SYSTEM_OPTION_DNS_CUSTOM_DEFAULT = "";

    public static final String SYSTEM_OPTION_PROXY_ENABLE = "system_proxy_enable";
    public static final boolean SYSTEM_OPTION_PROXY_ENABLE_DEFAULT = false;

    public static final String SYSTEM_OPTION_PERSISTENT_NOTIFICATION = "system_persistent_notification";
    public static final boolean SYSTEM_OPTION_PERSISTENT_NOTIFICATION_DEFAULT = true;

    public static final String SYSTEM_OPTION_NOTIFICATION_SOUND = "system_notification_sound";
    public static final boolean SYSTEM_OPTION_NOTIFICATION_SOUND_DEFAULT = true;

    public static final String SYSTEM_OPTION_SHOW_MESSAGE_DIALOGS = "system_show_message_dialogs";
    public static final boolean SYSTEM_OPTION_SHOW_MESSAGE_DIALOGS_DEFAULT = true;

    public static final String SYSTEM_CUSTOM_MTU = "system_forced_mtu";
    public static final String SYSTEM_CUSTOM_MTU_DEFAULT = "";

    public static final String SYSTEM_OPTION_APPLICATION_FILTER_TYPE = "system_application_filter_type";
    public static final String SYSTEM_OPTION_APPLICATION_FILTER_TYPE_NONE = "0";
    public static final String SYSTEM_OPTION_APPLICATION_FILTER_TYPE_WHITELIST = "1";
    public static final String SYSTEM_OPTION_APPLICATION_FILTER_TYPE_BLACKLIST = "2";
    public static final String SYSTEM_OPTION_APPLICATION_FILTER_TYPE_DEFAULT = SYSTEM_OPTION_APPLICATION_FILTER_TYPE_NONE;

    public static final String SYSTEM_OPTION_APPLICATION_FILTER = "system_application_filter";
    public static final String SYSTEM_OPTION_APPLICATION_FILTER_DEFAULT = "";

    public static final String SYSTEM_OPTION_APPLICATION_LANGUAGE = "system_application_language";
    public static final String SYSTEM_OPTION_APPLICATION_LANGUAGE_DEFAULT = "";

    public static final String SYSTEM_OPTION_FIRST_RUN = "system_first_run";
    public static final boolean SYSTEM_OPTION_FIRST_RUN_DEFAULT = true;

    public static final String SYSTEM_OPTION_RESTORE_LAST_PROFILE = "system_restore_last_profile";
    public static final boolean SYSTEM_OPTION_RESTORE_LAST_PROFILE_DEFAULT = true;

    public static final String SYSTEM_OPTION_LAST_PROFILE_IS_CONNECTED = "system_last_profile_is_connected";
    public static final boolean SYSTEM_OPTION_LAST_PROFILE_IS_CONNECTED_DEFAULT = false;

    public static final String SYSTEM_OPTION_LAST_PROFILE = "system_last_profile";

    public static final String SYSTEM_OPTION_LAST_PROFILE_INFO = "system_last_profile_info";

    public static final String SYSTEM_OPTION_EXCLUDE_LOCAL_NETWORKS = "system_exclude_local_networks";
    public static final boolean SYSTEM_OPTION_EXCLUDE_LOCAL_NETWORKS_DEFAULT = false;

    public static final String SYSTEM_OPTION_PAUSE_VPN_WHEN_SCREEN_IS_OFF = "system_pause_vpn_when_screen_is_off";
    public static final boolean SYSTEM_OPTION_PAUSE_VPN_WHEN_SCREEN_IS_OFF_DEFAULT = true;

    public static final String SYSTEM_AIRVPN_REMEMBER_ME = "system_airvpn_remember_me";
    public static final boolean SYSTEM_AIRVPN_REMEMBER_ME_DEFAULT = false;

    public static final String AIRVPN_MASTER_PASSWORD_HASHCODE = "system_mp";
    public static final int AIRVPN_MASTER_PASSWORD_HASHCODE_DEFAULT = -1;

    public static final String AIRVPN_PROTOCOL_UDP = "UDP";
    public static final String AIRVPN_PROTOCOL_TCP = "TCP";

    public static final String AIRVPN_DEFAULT_PROTOCOL = "airvpn_default_protocol";
    public static final String AIRVPN_PROTOCOL = "airvpn_protocol";
    public static final String AIRVPN_PROTOCOL_DEFAULT = AIRVPN_PROTOCOL_UDP;

    public static final int AIRVPN_PORT_DEFAULT = 443;

    public static final String AIRVPN_DEFAULT_PORT = "airvpn_default_port";
    public static final String AIRVPN_PORT = "airvpn_port";

    public static final String AIRVPN_IP_VERSION_4 = "IPv4";
    public static final String AIRVPN_IP_VERSION_6 = "IPv6";
    public static final String AIRVPN_IP_VERSION_4_OVER_6 = "IPv6overIPv4";

    public static final String AIRVPN_DEFAULT_IP_VERSION = "airvpn_default_ip_version";
    public static final String AIRVPN_IP_VERSION = "airvpn_ip_version";
    public static final String AIRVPN_IP_VERSION_DEFAULT = AIRVPN_IP_VERSION_4;

    public static final String AIRVPN_TLS_MODE_AUTH = "tls-auth";
    public static final String AIRVPN_TLS_MODE_CRYPT = "tls-crypt";

    public static final String AIRVPN_DEFAULT_TLS_MODE = "airvpn_default_tls_mode";
    public static final String AIRVPN_TLS_MODE = "airvpn_tls_mode";
    public static final String AIRVPN_TLS_MODE_DEFAULT = AIRVPN_TLS_MODE_CRYPT;

    public static final String AIRVPN_CIPHER = "airvpn_cipher";
    public static final String AIRVPN_CIPHER_SERVER = "SERVER";
    public static final String AIRVPN_CIPHER_CHACHA20_POLY1305 = "CHACHA20-POLY1305";
    public static final String AIRVPN_CIPHER_AES_256_GCM = "AES-256-GCM";
    public static final String AIRVPN_CIPHER_AES_256_CBC = "AES-256-CBC";
    public static final String AIRVPN_CIPHER_DEFAULT = AIRVPN_CIPHER_SERVER;

    public static final String AIRVPN_QUICK_CONNECT_MODE = "airvpn_quick_connect_mode";
    public static final String AIRVPN_QUICK_CONNECT_MODE_DEFAULT_OPTIONS = "default";
    public static final String AIRVPN_QUICK_CONNECT_MODE_AUTO = "auto";
    public static final String AIRVPN_QUICK_CONNECT_MODE_DEFAULT = AIRVPN_QUICK_CONNECT_MODE_AUTO;

    public static final String AIRVPN_FORBID_QUICK_CONNECTION_TO_USER_COUNTRY = "airvpn_forbid_quick_connection_to_user_country";
    public static final boolean AIRVPN_FORBID_QUICK_CONNECTION_TO_USER_COUNTRY_DEFAULT = true;

    public static final String AIRVPN_CUSTOM_BOOTSTRAP_SERVERS = "airvpn_custom_bootstrap_servers";
    public static final String AIRVPN_CUSTOM_BOOTSTRAP_SERVERS_DEFAULT = "";

    public static final String AIRVPN_SERVER_WHITELIST = "airvpn_server_whitelist";
    public static final String AIRVPN_SERVER_WHITELIST_DEFAULT = "";
    public static final String AIRVPN_SERVER_BLACKLIST = "airvpn_server_blacklist";
    public static final String AIRVPN_SERVER_BLACKLIST_DEFAULT = "";

    public static final String AIRVPN_COUNTRY_WHITELIST = "airvpn_country_whitelist";
    public static final String AIRVPN_COUNTRY_WHITELIST_DEFAULT = "";
    public static final String AIRVPN_COUNTRY_BLACKLIST = "airvpn_country_blacklist";
    public static final String AIRVPN_COUNTRY_BLACKLIST_DEFAULT = "";

    public static final String AIRVPN_SERVER_SORT_BY_NAME = "sort_name";
    public static final String AIRVPN_SERVER_SORT_BY_LOCATION = "sort_location";
    public static final String AIRVPN_SERVER_SORT_BY_LOAD = "sort_load";
    public static final String AIRVPN_SERVER_SORT_BY_USERS = "sort_users";
    public static final String AIRVPN_SERVER_SORT_BY_BANDWIDTH = "sort_bandwidth";
    public static final String AIRVPN_SERVER_SORT_BY_MAX_BANDWIDTH = "sort_max_bandwidth";

    public static final String AIRVPN_SERVER_SORT_BY = "airvpn_server_sort_by";
    public static final String AIRVPN_SERVER_SORT_BY_DEFAULT = AIRVPN_SERVER_SORT_BY_NAME;

    public static final String AIRVPN_SERVER_SORT_MODE_ASCENDING = "sort_ascending";
    public static final String AIRVPN_SERVER_SORT_MODE_DESCENDING = "sort_descending";

    public static final String AIRVPN_SERVER_SORT_MODE = "airvpn_server_sort_mode";
    public static final String AIRVPN_SERVER_SORT_MODE_DEFAULT = AIRVPN_SERVER_SORT_MODE_ASCENDING;

    public static final String DEFAULT_SPLIT_SEPARATOR = ",";

    private SharedPreferences appPrefs = null;
    private SharedPreferences.Editor prefsEditor = null;

    public SettingsManager(Context context)
    {
        appPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        
        prefsEditor = appPrefs.edit();
    }

    public String getOvpn3TLSMinVersion()
    {
        return getString(OVPN3_OPTION_TLS_MIN_VERSION, OVPN3_OPTION_TLS_MIN_VERSION_DEFAULT);
    }

    public void setOvpn3TLSMinVersion(String value)
    {
        saveString(OVPN3_OPTION_TLS_MIN_VERSION, value);
    }

    public String getOvpn3Protocol()
    {
        return getString(OVPN3_OPTION_PROTOCOL, OVPN3_OPTION_PROTOCOL_DEFAULT);
    }

    public void setOvpn3Protocol(String value)
    {
        saveString(OVPN3_OPTION_PROTOCOL, value);
    }

    public String getOvpn3IPV6()
    {
        return getString(OVPN3_OPTION_IPV6, OVPN3_OPTION_IPV6_DEFAULT);
    }

    public void setOvpn3IPV6(String value)
    {
        saveString(OVPN3_OPTION_IPV6, value);
    }

    public String getOvpn3Timeout()
    {
        return getString(OVPN3_OPTION_TIMEOUT, OVPN3_OPTION_TIMEOUT_DEFAULT);
    }

    public void setOvpn3Timeout(String value)
    {
        saveString(OVPN3_OPTION_TIMEOUT, value);
    }


    public boolean isOvpn3TunPersist()
    {
        return getBoolean(OVPN3_OPTION_TUN_PERSIST, OVPN3_OPTION_TUN_PERSIST_DEFAULT);
    }

    public void setOvpn3TunPersist(boolean value)
    {
        saveBoolean(OVPN3_OPTION_TUN_PERSIST, value);
    }

    public String getOvpn3CompressionMode()
    {
        return getString(OVPN3_OPTION_COMPRESSION_MODE, OVPN3_OPTION_COMPRESSION_MODE_DEFAULT);
    }

    public void setOvpn3CompressionMode(String value)
    {
        saveString(OVPN3_OPTION_COMPRESSION_MODE, value);
    }

    public boolean isVPNLockEnabled()
    {
        return getBoolean(SYSTEM_OPTION_VPN_LOCK, SYSTEM_OPTION_VPN_LOCK_DEFAULT);
    }

    public void setVPNLock(boolean value)
    {
        saveBoolean(SYSTEM_OPTION_VPN_LOCK, value);
    }

    public boolean isVPNReconnectEnabled()
    {
        return getBoolean(SYSTEM_OPTION_VPN_RECONNECT, SYSTEM_OPTION_VPN_RECONNECT_DEFAULT);
    }

    public void setVPNReconnect(boolean value)
    {
        saveBoolean(SYSTEM_OPTION_VPN_RECONNECT, value);
    }

    public String getVPNReconnectionRetries()
    {
        return getString(SYSTEM_OPTION_VPN_RECONNECTION_RETRIES, SYSTEM_OPTION_VPN_RECONNECTION_RETRIES_DEFAULT);
    }

    public void setVPNReconnectionRetries(String value)
    {
        saveString(SYSTEM_OPTION_VPN_RECONNECTION_RETRIES, value);
    }

    public String getOvpn3Username()
    {
        return getString(OVPN3_OPTION_USERNAME, OVPN3_OPTION_USERNAME_DEFAULT);
    }

    public void setOvpn3Username(String value)
    {
        saveString(OVPN3_OPTION_USERNAME, value);
    }

    public String getOvpn3Password()
    {
        return getString(OVPN3_OPTION_PASSWORD, OVPN3_OPTION_PASSWORD_DEFAULT);
    }

    public void setOvpn3Password(String value)
    {
        saveString(OVPN3_OPTION_PASSWORD, value);
    }

    public boolean isOvpn3SynchronousDNSLookup()
    {
        return getBoolean(OVPN3_OPTION_SYNCHRONOUS_DNS_LOOKUP, OVPN3_OPTION_SYNCHRONOUS_DNS_LOOKUP_DEFAULT);
    }

    public void setOvpn3SynchronousDNSLookup(boolean value)
    {
        saveBoolean(OVPN3_OPTION_SYNCHRONOUS_DNS_LOOKUP, value);
    }

    public boolean isOvpn3AutologinSessions()
    {
        return getBoolean(OVPN3_OPTION_AUTOLOGIN_SESSIONS, OVPN3_OPTION_AUTOLOGIN_SESSIONS_DEFAULT);
    }

    public void setOvpn3AutologinSessions(boolean value)
    {
        saveBoolean(OVPN3_OPTION_AUTOLOGIN_SESSIONS, value);
    }

    public boolean isOvpn3DisableClientCert()
    {
        return getBoolean(OVPN3_OPTION_DISABLE_CLIENT_CERT, OVPN3_OPTION_DISABLE_CLIENT_CERT_DEFAULT);
    }

    public void setOvpn3DisableClientCert(boolean value)
    {
        saveBoolean(OVPN3_OPTION_DISABLE_CLIENT_CERT, value);
    }

    public String getOvpn3SSLDebugLevel()
    {
        return getString(OVPN3_OPTION_SSL_DEBUG_LEVEL, OVPN3_OPTION_SSL_DEBUG_LEVEL_DEFAULT);
    }

    public void setOvpn3SSLDebugLevel(String value)
    {
        saveString(OVPN3_OPTION_SSL_DEBUG_LEVEL, value);
    }

    public long getOvpn3SSLDebugLevelValue()
    {
        long defVal = 0;

        try
        {
            defVal = Long.parseLong(OVPN3_OPTION_SSL_DEBUG_LEVEL_DEFAULT);
        }
        catch(NumberFormatException e)
        {
            defVal = 0;
        }

        return getLong(OVPN3_OPTION_SSL_DEBUG_LEVEL, defVal);
    }

    public void setOvpn3SSLDebugLevelValue(long value)
    {
        String prefVal = "";

        if(value <= 0)
            prefVal = OVPN3_OPTION_SSL_DEBUG_LEVEL_DEFAULT;
        else
            prefVal = String.format(Locale.getDefault(), "%d", value);

        saveString(OVPN3_OPTION_SSL_DEBUG_LEVEL, prefVal);
    }

    public String getOvpn3PrivateKeyPassword()
    {
        return getString(OVPN3_OPTION_PRIVATE_KEY_PASSWORD, OVPN3_OPTION_PRIVATE_KEY_PASSWORD_DEFAULT);
    }

    public void setOvpn3PrivateKeyPassword(String value)
    {
        saveString(OVPN3_OPTION_PRIVATE_KEY_PASSWORD, value);
    }

    public String getOvpn3DefaultKeyDirection()
    {
        return getString(OVPN3_OPTION_DEFAULT_KEY_DIRECTION, OVPN3_OPTION_DEFAULT_KEY_DIRECTION_DEFAULT);
    }

    public void setOvpn3DefaultKeyDirection(String value)
    {
        saveString(OVPN3_OPTION_DEFAULT_KEY_DIRECTION, value);
    }

    public boolean isOvpn3AESCBCCiphersuitesForced()
    {
        return getBoolean(OVPN3_OPTION_FORCE_AES_CBC_CIPHERSUITES, OVPN3_OPTION_FORCE_AES_CBC_CIPHERSUITES_DEFAULT);
    }

    public void setOvpn3ForceAESCBCCiphersuites(boolean value)
    {
        saveBoolean(OVPN3_OPTION_FORCE_AES_CBC_CIPHERSUITES, value);
    }

    public String getOvpn3TLSCertProfile()
    {
        return getString(OVPN3_OPTION_TLS_CERT_PROFILE, OVPN3_OPTION_TLS_CERT_PROFILE_DEFAULT);
    }

    public void setOvpn3TLSCertProfile(String value)
    {
        saveString(OVPN3_OPTION_TLS_CERT_PROFILE, value);
    }

    public String getOvpn3ProxyHost()
    {
        return getString(OVPN3_OPTION_PROXY_HOST, OVPN3_OPTION_PROXY_HOST_DEFAULT);
    }
    
    public void setOvpn3ProxyHost(String value)
    {
        saveString(OVPN3_OPTION_PROXY_HOST, value);
    }

    public String getOvpn3ProxyPort()
    {
        return getString(OVPN3_OPTION_PROXY_PORT, OVPN3_OPTION_PROXY_PORT_DEFAULT);
    }

    public void setOvpn3ProxyPort(String value)
    {
        saveString(OVPN3_OPTION_PROXY_PORT, value);
    }

    public long getOvpn3ProxyPortValue()
    {
        long defVal = 0;

        try
        {
            defVal = Long.parseLong(OVPN3_OPTION_PROXY_PORT_DEFAULT);
        }
        catch(NumberFormatException e)
        {
            defVal = 0;
        }

        return getLong(OVPN3_OPTION_PROXY_PORT, defVal);
    }

    public void setOvpn3ProxyPortValue(long value)
    {
        String prefVal = "";

        if(value <= 0)
            prefVal = OVPN3_OPTION_PROXY_PORT_DEFAULT;
        else
            prefVal = String.format(Locale.getDefault(), "%d", value);

        saveString(OVPN3_OPTION_PROXY_PORT, prefVal);
    }

    public String getOvpn3ProxyUsername()
    {
        return getString(OVPN3_OPTION_PROXY_USERNAME, OVPN3_OPTION_PROXY_USERNAME_DEFAULT);
    }

    public void setOvpn3ProxyUsername(String value)
    {
        saveString(OVPN3_OPTION_PROXY_USERNAME, value);
    }

    public String getOvpn3ProxyPassword()
    {
        return getString(OVPN3_OPTION_PROXY_PASSWORD, OVPN3_OPTION_PROXY_PASSWORD_DEFAULT);
    }

    public void setOvpn3ProxyPassword(String value)
    {
        saveString(OVPN3_OPTION_PROXY_PASSWORD, value);
    }

    public boolean isOvpn3ProxyAllowCleartextAuth()
    {
        return getBoolean(OVPN3_OPTION_PROXY_ALLOW_CLEARTEXT_AUTH, OVPN3_OPTION_PROXY_ALLOW_CLEARTEXT_AUTH_DEFAULT);
    }

    public void setOvpn3ProxyAllowCleartextAuth(boolean value)
    {
        saveBoolean(OVPN3_OPTION_PROXY_ALLOW_CLEARTEXT_AUTH, value);
    }

    public String getOvpn3CustomDirectives()
    {
        return getString(OVPN3_OPTION_CUSTOM_DIRECTIVES, OVPN3_OPTION_CUSTOM_DIRECTIVES_DEFAULT);
    }

    public void setOvpn3CustomDirectives(String value)
    {
        saveString(OVPN3_OPTION_CUSTOM_DIRECTIVES, value);
    }

    public boolean isSystemDNSOverrideEnable()
    {
        return getBoolean(SYSTEM_OPTION_DNS_OVERRIDE_ENABLE, SYSTEM_OPTION_DNS_OVERRIDE_ENABLE_DEFAULT);
    }

    public void setSystemDNSOverrideEnable(boolean value)
    {
        saveBoolean(SYSTEM_OPTION_DNS_OVERRIDE_ENABLE, value);
    }

    public String getSystemCustomDNS()
    {
        return getString(SYSTEM_OPTION_DNS_CUSTOM, SYSTEM_OPTION_DNS_CUSTOM_DEFAULT);
    }

    public void setSystemCustomDNS(String value)
    {
        saveString(SYSTEM_OPTION_DNS_CUSTOM, value);
    }

    public ArrayList<String> getSystemDNSCustomList()
    {
        ArrayList<String> list = new ArrayList<String>();
        String[] valArray = null;
        String prefVal = "";

        prefVal = getString(SYSTEM_OPTION_DNS_CUSTOM, SYSTEM_OPTION_DNS_CUSTOM_DEFAULT);

        valArray = prefVal.split(DEFAULT_SPLIT_SEPARATOR);

        for(String item : valArray)
            list.add(item);

        return list;
    }
        
    public void setSystemDNSCustomList(ArrayList<String> value)
    {
        String prefVal = "";

        for(String item : value)
        {
            if(!prefVal.equals(""))
                prefVal += DEFAULT_SPLIT_SEPARATOR;

            prefVal += item;
        }

        if(prefVal != null)
            saveString(SYSTEM_OPTION_DNS_CUSTOM, prefVal);
    }

    public boolean isSystemProxyEnabled()
    {
        return getBoolean(SYSTEM_OPTION_PROXY_ENABLE, SYSTEM_OPTION_PROXY_ENABLE_DEFAULT);
    }

    public void setSystemProxyEnabled(boolean value)
    {
        saveBoolean(SYSTEM_OPTION_PROXY_ENABLE, value);
    }

    public boolean isSystemPersistentNotification()
    {
        return getBoolean(SYSTEM_OPTION_PERSISTENT_NOTIFICATION, SYSTEM_OPTION_PERSISTENT_NOTIFICATION_DEFAULT);
    }

    public void setSystemPersistentNotification(boolean value)
    {
        saveBoolean(SYSTEM_OPTION_PERSISTENT_NOTIFICATION, value);
    }

    public boolean isSystemNotificationSound()
    {
        return getBoolean(SYSTEM_OPTION_NOTIFICATION_SOUND, SYSTEM_OPTION_NOTIFICATION_SOUND_DEFAULT);
    }

    public void setSystemNotificationSound(boolean value)
    {
        saveBoolean(SYSTEM_OPTION_NOTIFICATION_SOUND, value);
    }

    public boolean areMessageDialogsEnabled()
    {
        return getBoolean(SYSTEM_OPTION_SHOW_MESSAGE_DIALOGS, SYSTEM_OPTION_SHOW_MESSAGE_DIALOGS_DEFAULT);
    }

    public void setMessageDialogsEnabled(boolean value)
    {
        saveBoolean(SYSTEM_OPTION_SHOW_MESSAGE_DIALOGS, value);
    }

    public String getSystemCustomMTU()
    {
        return getString(SYSTEM_CUSTOM_MTU, SYSTEM_CUSTOM_MTU_DEFAULT);
    }

    public void setSystemCustomMTU(String value)
    {
        saveString(SYSTEM_CUSTOM_MTU, value);
    }

    public long getSystemCustomMTUValue()
    {
        long defVal = 0;

        try
        {
            defVal = Long.parseLong(SYSTEM_CUSTOM_MTU_DEFAULT);
        }
        catch(NumberFormatException e)
        {
            defVal = 0;
        }

        return getLong(SYSTEM_CUSTOM_MTU, defVal);
    }

    public void setSystemCustomMTUValue(long value)
    {
        String prefVal = "";

        if(value <= 0)
            prefVal = SYSTEM_CUSTOM_MTU_DEFAULT;
        else
            prefVal = String.format(Locale.getDefault(), "%d", value);

        saveString(SYSTEM_CUSTOM_MTU, prefVal);
    }

    public String getSystemApplicationFilterType()
    {
        return getString(SYSTEM_OPTION_APPLICATION_FILTER_TYPE, SYSTEM_OPTION_APPLICATION_FILTER_TYPE_DEFAULT);
    }

    public void setSystemApplicationFilterType(String value)
    {
        saveString(SYSTEM_OPTION_APPLICATION_FILTER_TYPE, value);
    }

    public String getSystemApplicationLanguage()
    {
        return getString(SYSTEM_OPTION_APPLICATION_LANGUAGE, SYSTEM_OPTION_APPLICATION_LANGUAGE_DEFAULT);
    }

    public void setSystemApplicationLanguage(String value)
    {
        saveString(SYSTEM_OPTION_APPLICATION_LANGUAGE, value);
    }

    public String getSystemApplicationFilter()
    {
        return getString(SYSTEM_OPTION_APPLICATION_FILTER, SYSTEM_OPTION_APPLICATION_FILTER_DEFAULT);
    }

    public void setSystemApplicationFilter(String value)
    {
        saveString(SYSTEM_OPTION_APPLICATION_FILTER, value);
    }

    public ArrayList<String> getSystemApplicationFilterList()
    {
        ArrayList<String> list = new ArrayList<String>();
        String[] valArray = null;
        String prefVal = "";

        prefVal = getString(SYSTEM_OPTION_APPLICATION_FILTER, SYSTEM_OPTION_APPLICATION_FILTER_DEFAULT);

        if(!prefVal.equals(""))
        {
            valArray = prefVal.split(DEFAULT_SPLIT_SEPARATOR);

            for(String item : valArray)
                list.add(item);
        }

        return list;
    }
        
    public void setSystemApplicationFilterList(ArrayList<String> value)
    {
        String prefVal = "";

        for(String item : value)
        {
            if(!prefVal.equals(""))
                prefVal += DEFAULT_SPLIT_SEPARATOR;

            prefVal += item;
        }

        if(prefVal != null)
            saveString(SYSTEM_OPTION_APPLICATION_FILTER, prefVal);
    }

    public boolean isSystemFirstRun()
    {
        return getBoolean(SYSTEM_OPTION_FIRST_RUN, SYSTEM_OPTION_FIRST_RUN_DEFAULT);
    }

    public void setSystemFirstRun(boolean value)
    {
        saveBoolean(SYSTEM_OPTION_FIRST_RUN, value);
    }

    public boolean isSystemRestoreLastProfile()
    {
        return getBoolean(SYSTEM_OPTION_RESTORE_LAST_PROFILE, SYSTEM_OPTION_RESTORE_LAST_PROFILE_DEFAULT);
    }

    public void setSystemRestoreLastProfile(boolean value)
    {
        saveBoolean(SYSTEM_OPTION_RESTORE_LAST_PROFILE, value);
    }

    public boolean isSystemLastProfileIsConnected()
    {
        return getBoolean(SYSTEM_OPTION_LAST_PROFILE_IS_CONNECTED, SYSTEM_OPTION_LAST_PROFILE_IS_CONNECTED_DEFAULT);
    }

    public void setSystemLastProfileIsConnected(boolean value)
    {
        saveBoolean(SYSTEM_OPTION_LAST_PROFILE_IS_CONNECTED, value);
    }

    public int getAirVPNMasterPasswordHashCode()
    {
        return getInt(AIRVPN_MASTER_PASSWORD_HASHCODE, AIRVPN_MASTER_PASSWORD_HASHCODE_DEFAULT);
    }

    public void setAirVPNMasterPasswordHashCode(int value)
    {
        saveInt(AIRVPN_MASTER_PASSWORD_HASHCODE, value);
    }

    public boolean isAirVPNRememberMe()
    {
        return getBoolean(SYSTEM_AIRVPN_REMEMBER_ME, SYSTEM_AIRVPN_REMEMBER_ME_DEFAULT);
    }

    public void setAirVPNRememberMe(boolean value)
    {
        saveBoolean(SYSTEM_AIRVPN_REMEMBER_ME, value);
    }

    public String getAirVPNProtocol()
    {
        return getString(AIRVPN_PROTOCOL, AIRVPN_PROTOCOL_DEFAULT);
    }

    public void setAirVPNProtocol(String value)
    {
        saveString(AIRVPN_PROTOCOL, value);
    }

    public int getAirVPNPort()
    {
        return getInt(AIRVPN_PORT, AIRVPN_PORT_DEFAULT);
    }

    public void setAirVPNPort(int value)
    {
        saveInt(AIRVPN_PORT, value);
    }

    public String getAirVPNIPVersion()
    {
        return getString(AIRVPN_IP_VERSION, AIRVPN_IP_VERSION_DEFAULT);
    }

    public void setAirVPNIPVersion(String value)
    {
        saveString(AIRVPN_IP_VERSION, value);
    }

    public String getAirVPNTLSMode()
    {
        return getString(AIRVPN_TLS_MODE, AIRVPN_TLS_MODE_DEFAULT);
    }

    public void setAirVPNTLSMode(String value)
    {
        saveString(AIRVPN_TLS_MODE, value);
    }

    public String getAirVPNDefaultProtocol()
    {
        return getString(AIRVPN_DEFAULT_PROTOCOL, AIRVPN_PROTOCOL_DEFAULT);
    }

    public void setAirVPNDefaultProtocol(String value)
    {
        saveString(AIRVPN_DEFAULT_PROTOCOL, value);
    }

    public int getAirVPNDefaultPort()
    {
        return getInt(AIRVPN_DEFAULT_PORT, AIRVPN_PORT_DEFAULT);
    }

    public void setAirVPNDefaultPort(int value)
    {
        saveInt(AIRVPN_DEFAULT_PORT, value);
    }

    public String getAirVPNDefaultIPVersion()
    {
        return getString(AIRVPN_DEFAULT_IP_VERSION, AIRVPN_IP_VERSION_DEFAULT);
    }

    public void setAirVPNDefaultIPVersion(String value)
    {
        saveString(AIRVPN_DEFAULT_IP_VERSION, value);
    }

    public String getAirVPNDefaultTLSMode()
    {
        return getString(AIRVPN_DEFAULT_TLS_MODE, AIRVPN_TLS_MODE_DEFAULT);
    }

    public void setAirVPNDefaultTLSMode(String value)
    {
        saveString(AIRVPN_DEFAULT_TLS_MODE, value);
    }

    public String getAirVPNCipher()
    {
        return getString(AIRVPN_CIPHER, AIRVPN_CIPHER_DEFAULT);
    }

    public void setAirVPNCipher(String value)
    {
        saveString(AIRVPN_CIPHER, value);
    }

    public String getAirVPNSortBy()
    {
        return getString(AIRVPN_SERVER_SORT_BY, AIRVPN_SERVER_SORT_BY_DEFAULT);
    }

    public void setAirVPNSortBy(String value)
    {
        saveString(AIRVPN_SERVER_SORT_BY, value);
    }

    public String getAirVPNSortMode()
    {
        return getString(AIRVPN_SERVER_SORT_MODE, AIRVPN_SERVER_SORT_MODE_DEFAULT);
    }

    public void setAirVPNSortMode(String value)
    {
        saveString(AIRVPN_SERVER_SORT_MODE, value);
    }

    public String getAirVPNQuickConnectMode()
    {
        return getString(AIRVPN_QUICK_CONNECT_MODE, AIRVPN_QUICK_CONNECT_MODE_DEFAULT);
    }

    public void setAirVPNQuickConnectMode(String value)
    {
        saveString(AIRVPN_QUICK_CONNECT_MODE, value);
    }

    public void setAirVPNForbidQuickConnectionToUserCountry(boolean value)
    {
        saveBoolean(AIRVPN_FORBID_QUICK_CONNECTION_TO_USER_COUNTRY, value);
    }

    public boolean isAirVPNForbidQuickConnectionToUserCountry()
    {
        return getBoolean(AIRVPN_FORBID_QUICK_CONNECTION_TO_USER_COUNTRY, AIRVPN_FORBID_QUICK_CONNECTION_TO_USER_COUNTRY_DEFAULT);
    }
    public String getAirVPNCustomBootstrap()
    {
        return getString(AIRVPN_CUSTOM_BOOTSTRAP_SERVERS, AIRVPN_CUSTOM_BOOTSTRAP_SERVERS_DEFAULT);
    }

    public void setAirVPNCustomBootstrap(String value)
    {
        saveString(AIRVPN_CUSTOM_BOOTSTRAP_SERVERS, value);
    }

    public ArrayList<String> getAirVPNCustomBootstrapList()
    {
        ArrayList<String> list = new ArrayList<String>();
        String[] valArray = null;
        String prefVal = "";

        prefVal = getString(AIRVPN_CUSTOM_BOOTSTRAP_SERVERS, AIRVPN_CUSTOM_BOOTSTRAP_SERVERS_DEFAULT);

        valArray = prefVal.split(DEFAULT_SPLIT_SEPARATOR);

        for(String item : valArray)
            list.add(item);

        return list;
    }

    public void setAirVPNCustomBootstrapList(ArrayList<String> value)
    {
        String prefVal = "";

        for(String item : value)
        {
            if(!prefVal.equals(""))
                prefVal += DEFAULT_SPLIT_SEPARATOR;

            prefVal += item;
        }

        if(prefVal != null)
            saveString(AIRVPN_CUSTOM_BOOTSTRAP_SERVERS, prefVal);
    }

    public String getLastOpenVPNProfile()
    {
        byte[] b64 = null;
        String profile = getString(SYSTEM_OPTION_LAST_PROFILE, "");

        if(!profile.equals(""))
        {
            b64 = Base64.decode(profile, Base64.NO_WRAP);

            return new String(b64, StandardCharsets.UTF_8);
        }
        else
            return "";
    }

    public void setLastOpenVPNProfile(String value)
    {
        if(value != null)
        {
            String profile = Base64.encodeToString(value.getBytes(StandardCharsets.UTF_8), Base64.NO_WRAP);

            saveString(SYSTEM_OPTION_LAST_PROFILE, profile);
        }
    }

    public HashMap<String, String> getSystemLastProfileInfo()
    {
        HashMap<String, String> pData = null;
        String info, profile = "";
        String[] items = null, entry = null;
        byte[] b64 = null;

        profile = getString(SYSTEM_OPTION_LAST_PROFILE_INFO, "");

        if(!profile.equals(""))
        {
            b64 = Base64.decode(profile, Base64.NO_WRAP);

            info = new String(b64, StandardCharsets.UTF_8);

            if(!info.equals(""))
            {
                pData = new HashMap<>();

                items = info.split("\\|");

                for(String item : items)
                {
                    entry = item.split(":");

                    if(entry.length == 2)
                        pData.put(entry[0], entry[1]);
                }
            }
        }

        return pData;
    }

    public void setSystemLastProfileInfo(HashMap<String, String> value)
    {
        String info = "";

        for(Map.Entry<String, String> item : value.entrySet())
        {
            if(!info.equals(""))
                info += "|";

            info += item.getKey() + ":" + item.getValue();
        }

        if(info != null)
            saveString(SYSTEM_OPTION_LAST_PROFILE_INFO, Base64.encodeToString(info.getBytes(StandardCharsets.UTF_8), Base64.NO_WRAP));
    }

    public boolean areLocalNetworksExcluded()
    {
        return getBoolean(SYSTEM_OPTION_EXCLUDE_LOCAL_NETWORKS, SYSTEM_OPTION_EXCLUDE_LOCAL_NETWORKS_DEFAULT);
    }

    public void setExcludeLocalNetworks(boolean value)
    {
        saveBoolean(SYSTEM_OPTION_EXCLUDE_LOCAL_NETWORKS, value);
    }

    public boolean isSystemPauseVpnWhenScreenIsOff()
    {
        return getBoolean(SYSTEM_OPTION_PAUSE_VPN_WHEN_SCREEN_IS_OFF, SYSTEM_OPTION_PAUSE_VPN_WHEN_SCREEN_IS_OFF_DEFAULT);
    }

    public void setSystemPauseVpnWhenScreenIsOff(boolean value)
    {
        saveBoolean(SYSTEM_OPTION_PAUSE_VPN_WHEN_SCREEN_IS_OFF, value);
    }

    public ArrayList<String> getAirVPNServerWhitelist()
    {
        ArrayList<String> list = new ArrayList<String>();
        String[] valArray = null;
        String prefVal = "";

        prefVal = getString(AIRVPN_SERVER_WHITELIST, AIRVPN_SERVER_WHITELIST_DEFAULT);

        if(!prefVal.equals(""))
        {
            valArray = prefVal.split(DEFAULT_SPLIT_SEPARATOR);

            for(String item : valArray)
                list.add(item);
        }

        return list;
    }

    public void setAirVPNServerWhitelist(ArrayList<String> value)
    {
        String prefVal = "";

        for(String item : value)
        {
            if(!prefVal.equals(""))
                prefVal += DEFAULT_SPLIT_SEPARATOR;

            prefVal += item;
        }

        if(prefVal != null)
            saveString(AIRVPN_SERVER_WHITELIST, prefVal);
    }

    public boolean isAirVPNServerWhitelisted(String name)
    {
        String prefVal = "";

        prefVal = getString(AIRVPN_SERVER_WHITELIST, AIRVPN_SERVER_WHITELIST_DEFAULT);

        return prefVal.contains(name);
    }

    public ArrayList<String> getAirVPNServerBlacklist()
    {
        ArrayList<String> list = new ArrayList<String>();
        String[] valArray = null;
        String prefVal = "";

        prefVal = getString(AIRVPN_SERVER_BLACKLIST, AIRVPN_SERVER_BLACKLIST_DEFAULT);

        if(!prefVal.equals(""))
        {
            valArray = prefVal.split(DEFAULT_SPLIT_SEPARATOR);

            for(String item : valArray)
                list.add(item);
        }

        return list;
    }

    public void setAirVPNServerBlacklist(ArrayList<String> value)
    {
        String prefVal = "";

        for(String item : value)
        {
            if(!prefVal.equals(""))
                prefVal += DEFAULT_SPLIT_SEPARATOR;

            prefVal += item;
        }

        if(prefVal != null)
            saveString(AIRVPN_SERVER_BLACKLIST, prefVal);
    }

    public boolean isAirVPNServerBlacklisted(String name)
    {
        String prefVal = "";

        prefVal = getString(AIRVPN_SERVER_BLACKLIST, AIRVPN_SERVER_BLACKLIST_DEFAULT);

        return prefVal.contains(name);
    }

    public ArrayList<String> getAirVPNCountryWhitelist()
    {
        ArrayList<String> list = new ArrayList<String>();
        String[] valArray = null;
        String prefVal = "";

        prefVal = getString(AIRVPN_COUNTRY_WHITELIST, AIRVPN_COUNTRY_WHITELIST_DEFAULT);

        if(!prefVal.equals(""))
        {
            valArray = prefVal.split(DEFAULT_SPLIT_SEPARATOR);

            for(String item : valArray)
                list.add(item);
        }

        return list;
    }

    public void setAirVPNCountryWhitelist(ArrayList<String> value)
    {
        String prefVal = "";

        for(String item : value)
        {
            if(!prefVal.equals(""))
                prefVal += DEFAULT_SPLIT_SEPARATOR;

            prefVal += item;
        }

        if(prefVal != null)
            saveString(AIRVPN_COUNTRY_WHITELIST, prefVal);
    }

    public boolean isAirVPNCountryWhitelisted(String name)
    {
        String prefVal = "";

        prefVal = getString(AIRVPN_COUNTRY_WHITELIST, AIRVPN_COUNTRY_WHITELIST_DEFAULT);

        return prefVal.contains(name);
    }

    public ArrayList<String> getAirVPNCountryBlacklist()
    {
        ArrayList<String> list = new ArrayList<String>();
        String[] valArray = null;
        String prefVal = "";

        prefVal = getString(AIRVPN_COUNTRY_BLACKLIST, AIRVPN_COUNTRY_BLACKLIST_DEFAULT);

        if(!prefVal.equals(""))
        {
            valArray = prefVal.split(DEFAULT_SPLIT_SEPARATOR);

            for(String item : valArray)
                list.add(item);
        }

        return list;
    }

    public void setAirVPNCountryBlacklist(ArrayList<String> value)
    {
        String prefVal = "";

        for(String item : value)
        {
            if(!prefVal.equals(""))
                prefVal += DEFAULT_SPLIT_SEPARATOR;

            prefVal += item;
        }

        if(prefVal != null)
            saveString(AIRVPN_COUNTRY_BLACKLIST, prefVal);
    }

    public boolean isAirVPNCountryBlacklisted(String name)
    {
        String prefVal = "";

        prefVal = getString(AIRVPN_COUNTRY_BLACKLIST, AIRVPN_COUNTRY_BLACKLIST_DEFAULT);

        return prefVal.contains(name);
    }

    public String dump()
    {
        String varDump = "";

        varDump += String.format("%s: %s\n", AIRVPN_DEFAULT_PROTOCOL, getString(AIRVPN_DEFAULT_PROTOCOL, AIRVPN_PROTOCOL_DEFAULT));
        varDump += String.format("%s: %s\n", AIRVPN_PROTOCOL, getString(AIRVPN_PROTOCOL, AIRVPN_PROTOCOL_DEFAULT));
        varDump += String.format(Locale.getDefault(),"%s: %d\n", AIRVPN_DEFAULT_PORT, getInt(AIRVPN_DEFAULT_PORT, AIRVPN_PORT_DEFAULT));
        varDump += String.format(Locale.getDefault(),"%s: %d\n", AIRVPN_PORT, getInt(AIRVPN_PORT, AIRVPN_PORT_DEFAULT));
        varDump += String.format("%s: %s\n", AIRVPN_DEFAULT_IP_VERSION, getString(AIRVPN_DEFAULT_IP_VERSION, AIRVPN_IP_VERSION_DEFAULT));
        varDump += String.format("%s: %s\n", AIRVPN_IP_VERSION, getString(AIRVPN_IP_VERSION, AIRVPN_IP_VERSION_DEFAULT));
        varDump += String.format("%s: %s\n", AIRVPN_DEFAULT_TLS_MODE, getString(AIRVPN_DEFAULT_TLS_MODE, AIRVPN_TLS_MODE_DEFAULT));
        varDump += String.format("%s: %s\n", AIRVPN_TLS_MODE, getString(AIRVPN_TLS_MODE, AIRVPN_TLS_MODE_DEFAULT));
        varDump += String.format("%s: %s\n", AIRVPN_QUICK_CONNECT_MODE, getString(AIRVPN_QUICK_CONNECT_MODE, AIRVPN_QUICK_CONNECT_MODE_DEFAULT));
        varDump += String.format("%s: %s\n", AIRVPN_CIPHER, getString(AIRVPN_CIPHER, AIRVPN_CIPHER_DEFAULT));
        varDump += String.format("%s: %s\n", AIRVPN_FORBID_QUICK_CONNECTION_TO_USER_COUNTRY, getBoolean(AIRVPN_FORBID_QUICK_CONNECTION_TO_USER_COUNTRY, AIRVPN_FORBID_QUICK_CONNECTION_TO_USER_COUNTRY_DEFAULT));
        varDump += String.format("%s: %s\n", AIRVPN_CUSTOM_BOOTSTRAP_SERVERS, getString(AIRVPN_CUSTOM_BOOTSTRAP_SERVERS, AIRVPN_CUSTOM_BOOTSTRAP_SERVERS_DEFAULT));
        varDump += String.format("%s: %s\n", AIRVPN_SERVER_WHITELIST, getString(AIRVPN_SERVER_WHITELIST, AIRVPN_SERVER_WHITELIST_DEFAULT));
        varDump += String.format("%s: %s\n", AIRVPN_SERVER_BLACKLIST, getString(AIRVPN_SERVER_BLACKLIST, AIRVPN_SERVER_BLACKLIST_DEFAULT));
        varDump += String.format("%s: %s\n", AIRVPN_COUNTRY_WHITELIST, getString(AIRVPN_COUNTRY_WHITELIST, AIRVPN_COUNTRY_WHITELIST_DEFAULT));
        varDump += String.format("%s: %s\n", AIRVPN_COUNTRY_BLACKLIST, getString(AIRVPN_COUNTRY_BLACKLIST, AIRVPN_COUNTRY_BLACKLIST_DEFAULT));
        varDump += String.format("%s: %s\n", SYSTEM_OPTION_VPN_LOCK, getBoolean(SYSTEM_OPTION_VPN_LOCK, SYSTEM_OPTION_VPN_LOCK_DEFAULT));
        varDump += String.format("%s: %s\n", SYSTEM_OPTION_VPN_RECONNECT, getBoolean(SYSTEM_OPTION_VPN_RECONNECT, SYSTEM_OPTION_VPN_RECONNECT_DEFAULT));
        varDump += String.format("%s: %s\n", SYSTEM_OPTION_VPN_RECONNECTION_RETRIES, getString(SYSTEM_OPTION_VPN_RECONNECTION_RETRIES, SYSTEM_OPTION_VPN_RECONNECTION_RETRIES_DEFAULT));
        varDump += String.format("%s: %s\n", SYSTEM_OPTION_DNS_OVERRIDE_ENABLE, getBoolean(SYSTEM_OPTION_DNS_OVERRIDE_ENABLE, SYSTEM_OPTION_DNS_OVERRIDE_ENABLE_DEFAULT));
        varDump += String.format("%s: %s\n", SYSTEM_OPTION_DNS_CUSTOM, getString(SYSTEM_OPTION_DNS_CUSTOM, SYSTEM_OPTION_DNS_CUSTOM_DEFAULT));
        varDump += String.format("%s: %s\n", SYSTEM_OPTION_PROXY_ENABLE, getBoolean(SYSTEM_OPTION_PROXY_ENABLE, SYSTEM_OPTION_PROXY_ENABLE_DEFAULT));
        varDump += String.format("%s: %s\n", SYSTEM_OPTION_PERSISTENT_NOTIFICATION, getBoolean(SYSTEM_OPTION_PERSISTENT_NOTIFICATION, SYSTEM_OPTION_PERSISTENT_NOTIFICATION_DEFAULT));
        varDump += String.format("%s: %s\n", SYSTEM_OPTION_NOTIFICATION_SOUND, getBoolean(SYSTEM_OPTION_NOTIFICATION_SOUND, SYSTEM_OPTION_NOTIFICATION_SOUND_DEFAULT));
        varDump += String.format("%s: %s\n", SYSTEM_OPTION_SHOW_MESSAGE_DIALOGS, getBoolean(SYSTEM_OPTION_SHOW_MESSAGE_DIALOGS, SYSTEM_OPTION_SHOW_MESSAGE_DIALOGS_DEFAULT));
        varDump += String.format("%s: %s\n", SYSTEM_CUSTOM_MTU, getString(SYSTEM_CUSTOM_MTU, SYSTEM_CUSTOM_MTU_DEFAULT));
        varDump += String.format("%s: %s\n", SYSTEM_OPTION_APPLICATION_FILTER_TYPE, getString(SYSTEM_OPTION_APPLICATION_FILTER_TYPE, SYSTEM_OPTION_APPLICATION_FILTER_TYPE_DEFAULT));
        varDump += String.format("%s: %s\n", SYSTEM_OPTION_APPLICATION_FILTER, getString(SYSTEM_OPTION_APPLICATION_FILTER, SYSTEM_OPTION_APPLICATION_FILTER_DEFAULT));
        varDump += String.format("%s: %s\n", SYSTEM_OPTION_APPLICATION_LANGUAGE, getString(SYSTEM_OPTION_APPLICATION_LANGUAGE, SYSTEM_OPTION_APPLICATION_LANGUAGE_DEFAULT));
        varDump += String.format("%s: %s\n", SYSTEM_OPTION_FIRST_RUN, getBoolean(SYSTEM_OPTION_FIRST_RUN, SYSTEM_OPTION_FIRST_RUN_DEFAULT));
        varDump += String.format("%s: %s\n", SYSTEM_OPTION_RESTORE_LAST_PROFILE, getBoolean(SYSTEM_OPTION_RESTORE_LAST_PROFILE, SYSTEM_OPTION_RESTORE_LAST_PROFILE_DEFAULT));
        varDump += String.format("%s: %s\n", SYSTEM_OPTION_LAST_PROFILE_IS_CONNECTED, getBoolean(SYSTEM_OPTION_LAST_PROFILE_IS_CONNECTED, SYSTEM_OPTION_LAST_PROFILE_IS_CONNECTED_DEFAULT));
        varDump += String.format("%s: %s\n", SYSTEM_OPTION_EXCLUDE_LOCAL_NETWORKS, getBoolean(SYSTEM_OPTION_EXCLUDE_LOCAL_NETWORKS, SYSTEM_OPTION_EXCLUDE_LOCAL_NETWORKS_DEFAULT));
        varDump += String.format("%s: %s\n", SYSTEM_OPTION_PAUSE_VPN_WHEN_SCREEN_IS_OFF, getBoolean(SYSTEM_OPTION_PAUSE_VPN_WHEN_SCREEN_IS_OFF, SYSTEM_OPTION_PAUSE_VPN_WHEN_SCREEN_IS_OFF_DEFAULT));
        varDump += String.format("%s: %s\n", SYSTEM_AIRVPN_REMEMBER_ME, getBoolean(SYSTEM_AIRVPN_REMEMBER_ME, SYSTEM_AIRVPN_REMEMBER_ME_DEFAULT));
        varDump += String.format("%s: %s\n", OVPN3_OPTION_TLS_MIN_VERSION, getString(OVPN3_OPTION_TLS_MIN_VERSION, OVPN3_OPTION_TLS_MIN_VERSION_DEFAULT));
        varDump += String.format("%s: %s\n", OVPN3_OPTION_PROTOCOL, getString(OVPN3_OPTION_PROTOCOL, OVPN3_OPTION_PROTOCOL_DEFAULT));
        varDump += String.format("%s: %s\n", OVPN3_OPTION_IPV6, getString(OVPN3_OPTION_IPV6, OVPN3_OPTION_IPV6_DEFAULT));
        varDump += String.format("%s: %s\n", OVPN3_OPTION_TIMEOUT, getString(OVPN3_OPTION_TIMEOUT, OVPN3_OPTION_TIMEOUT_DEFAULT));
        varDump += String.format("%s: %s\n", OVPN3_OPTION_TUN_PERSIST, getBoolean(OVPN3_OPTION_TUN_PERSIST, OVPN3_OPTION_TUN_PERSIST_DEFAULT));
        varDump += String.format("%s: %s\n", OVPN3_OPTION_COMPRESSION_MODE, getString(OVPN3_OPTION_COMPRESSION_MODE, OVPN3_OPTION_COMPRESSION_MODE_DEFAULT));
        varDump += String.format("%s: %s\n", OVPN3_OPTION_SYNCHRONOUS_DNS_LOOKUP, getBoolean(OVPN3_OPTION_SYNCHRONOUS_DNS_LOOKUP, OVPN3_OPTION_SYNCHRONOUS_DNS_LOOKUP_DEFAULT));
        varDump += String.format("%s: %s\n", OVPN3_OPTION_AUTOLOGIN_SESSIONS, getBoolean(OVPN3_OPTION_AUTOLOGIN_SESSIONS, OVPN3_OPTION_AUTOLOGIN_SESSIONS_DEFAULT));
        varDump += String.format("%s: %s\n", OVPN3_OPTION_DISABLE_CLIENT_CERT, getBoolean(OVPN3_OPTION_DISABLE_CLIENT_CERT, OVPN3_OPTION_DISABLE_CLIENT_CERT_DEFAULT));
        varDump += String.format("%s: %s\n", OVPN3_OPTION_SSL_DEBUG_LEVEL, getString(OVPN3_OPTION_SSL_DEBUG_LEVEL, OVPN3_OPTION_SSL_DEBUG_LEVEL_DEFAULT));
        varDump += String.format("%s: %s\n", OVPN3_OPTION_DEFAULT_KEY_DIRECTION, getString(OVPN3_OPTION_DEFAULT_KEY_DIRECTION, OVPN3_OPTION_DEFAULT_KEY_DIRECTION_DEFAULT));
        varDump += String.format("%s: %s\n", OVPN3_OPTION_FORCE_AES_CBC_CIPHERSUITES, getBoolean(OVPN3_OPTION_FORCE_AES_CBC_CIPHERSUITES, OVPN3_OPTION_FORCE_AES_CBC_CIPHERSUITES_DEFAULT));
        varDump += String.format("%s: %s\n", OVPN3_OPTION_TLS_CERT_PROFILE, getString(OVPN3_OPTION_TLS_CERT_PROFILE, OVPN3_OPTION_TLS_CERT_PROFILE_DEFAULT));
        varDump += String.format("%s: %s\n", OVPN3_OPTION_PROXY_HOST, getString(OVPN3_OPTION_PROXY_HOST, OVPN3_OPTION_PROXY_HOST_DEFAULT));
        varDump += String.format("%s: %s\n", OVPN3_OPTION_PROXY_PORT, getString(OVPN3_OPTION_PROXY_PORT, OVPN3_OPTION_PROXY_PORT_DEFAULT));
        varDump += String.format("%s: %s\n", OVPN3_OPTION_PROXY_ALLOW_CLEARTEXT_AUTH, getBoolean(OVPN3_OPTION_PROXY_ALLOW_CLEARTEXT_AUTH, OVPN3_OPTION_PROXY_ALLOW_CLEARTEXT_AUTH_DEFAULT));
        varDump += String.format("%s: %s\n", OVPN3_OPTION_CUSTOM_DIRECTIVES, getString(OVPN3_OPTION_CUSTOM_DIRECTIVES, OVPN3_OPTION_CUSTOM_DIRECTIVES_DEFAULT));

        return varDump;
    }

    public String getString(String key, String defValue)
    {
        return appPrefs.getString(key, defValue).trim();
    }

    public void saveString(String key, String value)
    {
        prefsEditor.putString(key, value);

        prefsEditor.apply();
    }

    public int getInt(String key, int defValue)
    {
        int returnValue = 0;
        String value = "";

        value = appPrefs.getString(key, "");

        if(value.equals(""))
            returnValue = defValue;
        else
        {
            try
            {
                returnValue = Integer.parseInt(value);
            }
            catch(NumberFormatException e)
            {
                returnValue = 0;
            }
        }

        return returnValue;
    }

    public void saveInt(String key, int value)
    {
        prefsEditor.putString(key, String.format(Locale.getDefault(), "%d", value));

        prefsEditor.apply();
    }

    public long getLong(String key, long defValue)
    {
        long returnValue = 0;
        String value = "";

        value = appPrefs.getString(key, "");

        if(value.equals(""))
            returnValue = defValue;
        else
        {
            try
            {
                returnValue = Long.parseLong(value);
            }
            catch(NumberFormatException e)
            {
                returnValue = 0;
            }
        }

        return returnValue;
    }

    public void saveLong(String key, long value)
    {
        prefsEditor.putString(key, String.format(Locale.getDefault(), "%d", value));

        prefsEditor.apply();
    }

    public boolean getBoolean(String key, boolean defValue)
    {
        boolean returnValue = false;
        String value = "";

        value = appPrefs.getString(key, "");

        if(value.equals(""))
            returnValue = defValue;
        else if(value.equals("true"))
            returnValue = true;
        else
            returnValue = false;

        return returnValue;
    }

    public void saveBoolean(String key, boolean value)
    {
        String val = "";

        if(value)
            val = "true";
        else
            val = "false";

        prefsEditor.putString(key, val);

        prefsEditor.apply();
    }
}
