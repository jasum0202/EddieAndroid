// <eddie_source_header>
// This file is part of Eddie/AirVPN software.
// Copyright (C)2014-2018 AirVPN (support@airvpn.org) / https://airvpn.org
//
// Eddie is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Eddie is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Eddie. If not, see <http://www.gnu.org/licenses/>.
// </eddie_source_header>
//
// 20 June 2018 - author: ProMIND - initial release. Based on revised code from com.eddie.android. (a tribute to the 1859 Perugia uprising occurred on 20 June 1859 and in memory of those brave inhabitants who fought for the liberty of Perugia)

package org.airvpn.eddie;

import org.w3c.dom.Document;

import java.util.ArrayList;

public class EddieEvent
{
    static ArrayList<EddieEventListener> listenerArrayList = null;

    public EddieEvent()
    {
        if(listenerArrayList == null)
            listenerArrayList = new ArrayList<EddieEventListener>();
    }

    public boolean addListener(EddieEventListener listener)
    {
        if(listenerArrayList == null)
            return false;

        if(listenerArrayList.contains(listener))
            return false;

        listenerArrayList.add(listener);

        return true;
    }

    public boolean removeListener(EddieEventListener listener)
    {
        if(listenerArrayList == null)
            return false;

        if(!listenerArrayList.contains(listener))
            return false;

        listenerArrayList.remove(listener);

        return true;
    }

    public synchronized void onVpnConnectionDataChanged(OpenVPNConnectionData connectionData)
    {
        if(listenerArrayList == null)
            return;

        ArrayList<EddieEventListener> localList = (ArrayList<EddieEventListener>)listenerArrayList.clone();

        for(EddieEventListener listener : localList)
        {
            if(listener != null)
                listener.onVpnConnectionDataChanged(connectionData);
        }
    }

    public synchronized void onVpnStatusChanged(VPN.Status status, String message)
    {
        if(listenerArrayList == null)
            return;

        ArrayList<EddieEventListener> localList = (ArrayList<EddieEventListener>)listenerArrayList.clone();

        for(EddieEventListener listener : localList)
        {
            if(listener != null)
                listener.onVpnStatusChanged(status, message);
        }
    }

    public synchronized void onVpnAuthFailed(OpenVPNEvent oe)
    {
        if(listenerArrayList == null)
            return;

        ArrayList<EddieEventListener> localList = (ArrayList<EddieEventListener>)listenerArrayList.clone();

        for(EddieEventListener listener : localList)
        {
            if(listener != null)
                listener.onVpnAuthFailed(oe);
        }
    }

    public synchronized void onVpnError(OpenVPNEvent oe)
    {
        if(listenerArrayList == null)
            return;

        ArrayList<EddieEventListener> localList = (ArrayList<EddieEventListener>)listenerArrayList.clone();

        for(EddieEventListener listener : localList)
        {
            if(listener != null)
                listener.onVpnError(oe);
        }
    }

    public synchronized void onMasterPasswordChanged()
    {
        if(listenerArrayList == null)
            return;

        ArrayList<EddieEventListener> localList = (ArrayList<EddieEventListener>)listenerArrayList.clone();

        for(EddieEventListener listener : localList)
        {
            if(listener != null)
                listener.onMasterPasswordChanged();
        }
    }

    public synchronized void onAirVPNLogin()
    {
        if(listenerArrayList == null)
            return;

        ArrayList<EddieEventListener> localList = (ArrayList<EddieEventListener>)listenerArrayList.clone();

        for(EddieEventListener listener : localList)
        {
            if(listener != null)
                listener.onAirVPNLogin();
        }
    }

    public synchronized void onAirVPNLoginFailed()
    {
        if(listenerArrayList == null)
            return;

        ArrayList<EddieEventListener> localList = (ArrayList<EddieEventListener>)listenerArrayList.clone();

        for(EddieEventListener listener : localList)
        {
            if(listener != null)
                listener.onAirVPNLoginFailed();
        }
    }

    public synchronized void onAirVPNLogout()
    {
        if(listenerArrayList == null)
            return;

        ArrayList<EddieEventListener> localList = (ArrayList<EddieEventListener>)listenerArrayList.clone();

        for(EddieEventListener listener : localList)
        {
            if(listener != null)
                listener.onAirVPNLogout();
        }
    }

    public synchronized void onAirVPNUserDataChanged()
    {
        if(listenerArrayList == null)
            return;

        ArrayList<EddieEventListener> localList = (ArrayList<EddieEventListener>)listenerArrayList.clone();

        for(EddieEventListener listener : localList)
        {
            if(listener != null)
                listener.onAirVPNUserDataChanged();
        }
    }

    public synchronized void onAirVPNUserProfileReceived(Document document)
    {
        if(listenerArrayList == null)
            return;

        ArrayList<EddieEventListener> localList = (ArrayList<EddieEventListener>)listenerArrayList.clone();

        for(EddieEventListener listener : localList)
        {
            if(listener != null)
                listener.onAirVPNUserProfileReceived(document);
        }
    }

    public synchronized void onAirVPNUserProfileDownloadError()
    {
        if(listenerArrayList == null)
            return;

        ArrayList<EddieEventListener> localList = (ArrayList<EddieEventListener>)listenerArrayList.clone();

        for(EddieEventListener listener : localList)
        {
            if(listener != null)
                listener.onAirVPNUserProfileDownloadError();
        }
    }

    public synchronized void onAirVPNUserProfileChanged()
    {
        if(listenerArrayList == null)
            return;

        ArrayList<EddieEventListener> localList = (ArrayList<EddieEventListener>)listenerArrayList.clone();

        for(EddieEventListener listener : localList)
        {
            if(listener != null)
                listener.onAirVPNUserProfileChanged();
        }
    }

    public synchronized void onAirVPNManifestReceived(Document document)
    {
        if(listenerArrayList == null)
            return;

        ArrayList<EddieEventListener> localList = (ArrayList<EddieEventListener>)listenerArrayList.clone();

        for(EddieEventListener listener : localList)
        {
            if(listener != null)
                listener.onAirVPNManifestReceived(document);
        }
    }

    public synchronized void onAirVPNManifestDownloadError()
    {
        if(listenerArrayList == null)
            return;

        ArrayList<EddieEventListener> localList = (ArrayList<EddieEventListener>)listenerArrayList.clone();

        for(EddieEventListener listener : localList)
        {
            if(listener != null)
                listener.onAirVPNManifestDownloadError();
        }
    }

    public synchronized void onAirVPNManifestChanged()
    {
        if(listenerArrayList == null)
            return;

        ArrayList<EddieEventListener> localList = (ArrayList<EddieEventListener>)listenerArrayList.clone();

        for(EddieEventListener listener : localList)
        {
            if(listener != null)
                listener.onAirVPNManifestChanged();
        }
    }

    public synchronized void onAirVPNIgnoredDocumentRequest()
    {
        if(listenerArrayList == null)
            return;

        ArrayList<EddieEventListener> localList = (ArrayList<EddieEventListener>)listenerArrayList.clone();

        for(EddieEventListener listener : localList)
        {
            if(listener != null)
                listener.onAirVPNIgnoredDocumentRequest();
        }
    }

    public synchronized void onCancelConnection()
    {
        if(listenerArrayList == null)
            return;

        ArrayList<EddieEventListener> localList = (ArrayList<EddieEventListener>)listenerArrayList.clone();

        for(EddieEventListener listener : localList)
        {
            if(listener != null)
                listener.onCancelConnection();
        }
    }
}

