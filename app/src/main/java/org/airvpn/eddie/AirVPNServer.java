// <eddie_source_header>
// This file is part of Eddie/AirVPN software.
// Copyright (C)2014-2018 AirVPN (support@airvpn.org) / https://airvpn.org
//
// Eddie is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Eddie is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Eddie. If not, see <http://www.gnu.org/licenses/>.
// </eddie_source_header>
//
// 12 October 2018 - author: ProMIND - initial release. (a tribute to the 1859 Perugia uprising occurred on 20 June 1859 and in memory of those brave inhabitants who fought for the liberty of Perugia)

package org.airvpn.eddie;

import android.content.Context;

import java.util.ArrayList;
import java.util.HashMap;

public class AirVPNServer
{
    public final int WORST_SCORE = 99999999;

    private CountryContinent countryContinent = null;
    private Context appContext = null;
    private SupportTools supportTools = null;

    private String name;
    private String continent;
    private String countryCode;
    private String region;
    private String location;
    private int bandWidth;
    private int maxBandWidth;
    private int users;
    private boolean supportIPv4;
    private boolean supportIPv6;
    private HashMap<Integer, String> entryIPv4;
    private HashMap<Integer, String> entryIPv6;
    private String exitIPv4;
    private String exitIPv6;
    private String warningOpen;
    private String warningClosed;
    private int scoreBase;
    private int score;
    private boolean supportCheck;
    private int group;
    private ArrayList<Integer> tlsCiphers;
    private ArrayList<Integer> tlsSuiteCiphers;
    private ArrayList<Integer> dataCiphers;

    AirVPNServer(Context c)
    {
        appContext = c;

        supportTools = new SupportTools(appContext);

        name = "";
        continent = "";
        countryCode = "";
        region = "";
        location = "";
        bandWidth = 0;
        maxBandWidth = 0;
        users = 0;
        supportIPv4 = false;
        supportIPv6 = false;
        entryIPv4 = null;
        entryIPv6 = null;
        exitIPv4 = "";
        exitIPv6 = "";
        warningOpen = "";
        warningClosed = "";
        scoreBase = 0;
        score = WORST_SCORE;
        supportCheck = false;
        group = -1;
        tlsCiphers = null;
        tlsSuiteCiphers = null;
        dataCiphers = null;

        countryContinent = new CountryContinent(c);
    }

    public String getName()
    {
        return name;
    }

    public void setName(String n)
    {
        name = n;
    }

    public String getContinent()
    {
        return continent;
    }

    public void setContinent(String c)
    {
        continent = c;
    }

    public String getCountryCode()
    {
        return countryCode;
    }

    public void setCountryCode(String c)
    {
        countryCode = c;

        continent = countryContinent.getCountryContinent(c);
    }

    public String getRegion()
    {
        return region;
    }

    public void setRegion(String r)
    {
        region = r;
    }

    public String getLocation()
    {
        return location;
    }

    public void setLocation(String l)
    {
        location = l;
    }

    public int getBandWidth()
    {
        return bandWidth;
    }

    public long getEffectiveBandWidth()
    {
        return (2 * (bandWidth * 8));
    }

    public void setBandWidth(int b)
    {
        bandWidth = b;
    }

    public int getMaxBandWidth()
    {
        return maxBandWidth;
    }

    public void setMaxBandWidth(int b)
    {
        maxBandWidth = b;
    }

    public int getUsers()
    {
        return users;
    }

    public void setUsers(int u)
    {
        users = u;
    }

    public boolean getSupportIPv4()
    {
        return supportIPv4;
    }

    public void setSupportIPv4(boolean s)
    {
        supportIPv4 = s;
    }

    public boolean getSupportIPv6()
    {
        return supportIPv6;
    }

    public void setSupportIPv6(boolean s)
    {
        supportIPv6 = s;
    }

    public HashMap<Integer, String> getEntryIPv4()
    {
        return entryIPv4;
    }

    public void setEntryIPv4(HashMap<Integer, String> l)
    {
        entryIPv4 = l;
    }

    public void setEntryIPv4(int entry, String IPv4)
    {
        if(entryIPv4 == null)
            entryIPv4 = new HashMap<Integer, String>();

        entryIPv4.put(entry, IPv4);
    }

    public HashMap<Integer, String> getEntryIPv6()
    {
        return entryIPv6;
    }

    public void setEntryIPv6(HashMap<Integer, String> l)
    {
        entryIPv6 = l;
    }

    public void setEntryIPv6(int entry, String IPv6)
    {
        if(entryIPv6 == null)
            entryIPv6 = new HashMap<Integer, String>();

        entryIPv6.put(entry, IPv6);
    }

    public String getExitIPv4()
    {
        return exitIPv4;
    }

    public void setExitIPv4(String e)
    {
        exitIPv4 = e;
    }

    public String getExitIPv6()
    {
        return exitIPv6;
    }

    public void setExitIPv6(String e)
    {
        exitIPv6 = e;
    }

    public String getWarningOpen()
    {
        return warningOpen;
    }

    public void setWarningOpen(String w)
    {
        warningOpen = w;
    }

    public String getWarningClosed()
    {
        return warningClosed;
    }

    public void setWarningClosed(String w)
    {
        warningClosed = w;
    }

    public int getScoreBase()
    {
        return scoreBase;
    }

    public void setScoreBase(int s)
    {
        scoreBase = s;
    }

    public boolean getSupportCheck()
    {
        return supportCheck;
    }

    public void setSupportCheck(boolean s)
    {
        supportCheck = s;
    }

    public int getLoad()
    {
        return supportTools.getLoad(bandWidth, maxBandWidth);
    }

    public ArrayList<Integer> getTlsCiphers()
    {
        if(tlsCiphers == null)
            tlsCiphers = new ArrayList<Integer>();

        return tlsCiphers;
    }

    public void setTlsCiphers(ArrayList<Integer> list)
    {
        tlsCiphers = list;
    }

    public boolean hasTlsCipher(int c)
    {
        if(tlsCiphers == null)
            return false;

        return tlsCiphers.contains(c);
    }

    public boolean hasTlsCipher(ArrayList<Integer> cipherList)
    {
        boolean serverHasCipher = false;

        for (int c : cipherList)
        {
            if(hasTlsCipher(c))
                serverHasCipher = true;
        }

        return serverHasCipher;
    }

    public ArrayList<Integer> getTlsSuiteCiphers()
    {
        if(tlsSuiteCiphers == null)
            tlsSuiteCiphers = new ArrayList<Integer>();

        return tlsSuiteCiphers;
    }

    public void setTlsSuiteCiphers(ArrayList<Integer> list)
    {
        tlsSuiteCiphers = list;
    }

    public boolean hasTlsSuiteCipher(int c)
    {
        if(tlsSuiteCiphers == null)
            return false;

        return tlsSuiteCiphers.contains(c);
    }

    public boolean hasTlsSuiteCipher(ArrayList<Integer> cipherList)
    {
        boolean serverHasCipher = false;

        for (int c : cipherList)
        {
            if(hasTlsSuiteCipher(c))
                serverHasCipher = true;
        }

        return serverHasCipher;
    }

    public ArrayList<Integer> getDataCiphers()
    {
        if(dataCiphers == null)
            dataCiphers = new ArrayList<Integer>();

        return dataCiphers;
    }

    public void setDataCiphers(ArrayList<Integer> list)
    {
        dataCiphers = list;
    }

    public boolean hasDataCipher(int c)
    {
        if(dataCiphers == null)
            return false;

        return dataCiphers.contains(c);
    }

    public boolean hasDataCipher(ArrayList<Integer> cipherList)
    {
        if(cipherList == null || cipherList.isEmpty())
            return true;

        boolean serverHasCipher = false;

        for (int c : cipherList)
        {
            if(hasDataCipher(c))
                serverHasCipher = true;
        }

        return serverHasCipher;
    }

    public void computeServerScore()
    {
        double s = 0;

        if(maxBandWidth == 0 || !warningOpen.isEmpty() || !warningClosed.isEmpty())
        {
            score = WORST_SCORE;

            return;
        }

        s = (double)scoreBase / AirVPNManifest.getSpeedFactor();
        s += (double)getLoad() * AirVPNManifest.getLoadFactor();
        s += (double)users * AirVPNManifest.getUserFactor();

        score = (int)s;
    }

    public int getScore()
    {
        return score;
    }

    public void setScore(int s)
    {
        score = s;
    }

    public boolean isAvailable()
    {
        return warningClosed.isEmpty();
    }

    public boolean isAvailable(ArrayList<Integer> cipherList)
    {
        return warningClosed.isEmpty() & hasDataCipher(cipherList);
    }
}
