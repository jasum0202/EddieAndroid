# AirVPN Eddie 2.4 for Android

* Free and open source OpenVPN GUI based on "OpenVPN 3.3 AirVPN"

* ChaCha20-Poly1305, AES-CBC and AES-GCM support on both OpenVPN Control and Data channels

* The only Android application officially developed by AirVPN

* Robust, best effort prevention of traffic leaks outside the VPN tunnel

* Battery-conscious application

* Low RAM footprint

* Ergonomic and friendly interface

* Ability to start and connect the application at device boot

* Option to define which apps must have traffic inside or outside the VPN tunnel through white and black list

* Localization in simplified and traditional Chinese, Danish, English, French, German, 

* Italian, Portuguese, Russian, Spanish, Turkish

* Full integration with AirVPN

* Enhanced security thanks to locally stored encrypted data through master password

* Quick one-tap connection and smart, fully automated server selection

* Smart server selection with custom settings

* Manual server selection

* Smart attempts to bypass OpenVPN blocks featuring protocol and server fail-over

* Full Android TV compatibility including D-Pad support. Mouse emulation is not required.

* Enhancements aimed to increase accessibility and comfort to visually impaired persons

* AirVPN servers sorting options

* Customizable "Favorite" and "Forbidden" servers and countries

* OpenVPN mimetype support to import profiles from external applications

* Multiple OpenVPN profile support. The app now imports and manages multiple OpenVPN profiles

* Support for custom bootstrap servers

* Support for favorite and forbidden countries

* AirVPN broadcast messages support

* User's subscription expiration date is shown in login/connection information

* The app is aware of concurrent VPN use. In case another app is granted VPN access, Eddie acts accordingly and releases VPN resources

* Optional local networks access. In such case, local network devices are exempted from the VPN and can be accessed within the local devices

* Localization override. User can choose the default language and localization from one of the available ones

* Favorite and forbidden lists can be emptied with a single tap

* VPN Lock can now be disabled or enabled from settings

* VPN reconnection in case of unexpected OpenVPN disconnection. (It requires VPN Lock to be disabled)

* User can generate an OpenVPN profile for any AirVPN server or country and save it in OpenVPN profile manager

* Server scoring algorithm implementing the latest AirVPN balancing factors in order to determine the best server for quick connection

* Network name and extra information are shown along with network type
Device network status management 

* The only Android application officially developed by AirVPN


With AirVPN you can keep your Internet traffic hidden from the eyes of your ISP and from any malicious entity wiretapping your line, connect safely even via a public Internet hotspot, unblock geo-restricted websites, bypass web sites blocks and protect the integrity of your communications.

Thanks to AirVPN, Eddie protects your Android device traffic. Eddie can be used even with any other VPN service based on OpenVPN.

To build AirVPN Eddie for Android you need Android Studio or gradle and Android sdk properly installed on your system

