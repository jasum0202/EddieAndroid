LOCAL_PATH:= $(call my-dir)/

include $(CLEAR_VARS)

LOCAL_LDLIBS := -lz 
# TEMP
LOCAL_STATIC_LIBRARIES := liblzo_static liblz4_static libmbedtls_static

LOCAL_CFLAGS= -funwind-tables -DHAVE_CONFIG_H -DTARGET_ABI=\"${TARGET_ABI}\" -DUSE_MBEDTLS=1 -DHAVE_LZO -DHAVE_LZ4 -DUSE_ASIO
LOCAL_CXXFLAGS += -std=c++1y

LOCAL_C_INCLUDES := lzo/include lz4/lib mbedtls mbedtls/include openvpn3/client openvpn3 asio/include boost_1_68_0/include

LOCAL_CPP_FEATURES += exceptions rtti

LOCAL_MODULE = ovpn3

LOCAL_SRC_FILES:= client/ovpncli.cpp

#ifneq ($(TARGET_ARCH),mips)
#LOCAL_SRC_FILES+=src/openvpn/breakpad.cpp
#endif

include $(BUILD_SHARED_LIBRARY)
