#!/bin/sh

# make eddie library and dependencies [ProMIND]

PLATFORM=`uname`
NDK_BUILD=ndk-build
NDK_BUILD_OPTIONS="V=0"
NDK_PATH=`which $NDK_BUILD 2>/dev/null`
LIB_PATH="../app/src/main/jniLibs"

#
# Edit paths according to your system and configuration
#

export NDK_PROJECT_PATH=.
LINUX_NDK_PATH=/opt/android-sdk-linux/ndk-bundle/
DARWIN_NDK_PATH=~/Library/Developer/Xamarin/android-sdk-macosx/ndk-bundle/

############################################################################

echo "System is $PLATFORM"

if [[ -z "$NDK_PATH" ]]; then
    case $PLATFORM in
    Linux)
        NDK_PATH=$LINUX_NDK_PATH
        break
        ;;
    Darwin)
        NDK_PATH=$DARWIN_NDK_PATH
        break
        ;;
    *)
        echo "Unknown platform $PLATFORM"
        break
        exit
        ;;
    esac

    echo "NDK path: $NDK_PATH"
else
    echo "$NDK_BUILD is in system path"
    NDK_PATH=
fi

echo "Project path: $NDK_PROJECT_PATH"

if [[ $1 == "clean" ]]; then
    echo
    echo "Cleaning project"
    echo

    $NDK_PATH$NDK_BUILD clean

    echo
    echo "Done"
    
    exit
fi

echo
echo "Making eddie native libraries"
echo "Output path: $LIB_PATH"
echo

"$NDK_PATH$NDK_BUILD" $NDK_BUILD_OPTIONS NDK_LIBS_OUT=$LIB_PATH NDK_APPLICATION_MK=jni/Application.mk

echo
echo "Done"
echo "Libraries have been written in $LIB_PATH"
