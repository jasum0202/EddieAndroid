// <eddie_source_header>
// This file is part of Eddie/AirVPN software.
// Copyright (C) 2014-2018 AirVPN (support@airvpn.org) / https://airvpn.org
//
// Eddie is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Eddie is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Eddie. If not, see <http://www.gnu.org/licenses/>.
// </eddie_source_header>

// Version 1.2 - 7 August 2018, ProMIND
//
// Removed Client map and multiple instances of client at each connection. It
// now uses a single instance and make sure it is deleted before creating a
// new one.
//
// All API functions now return a qualified result EddieLibraryResult
//
// Version 1.3 - 4 September 2018, ProMIND
//
// Added JNI support
//
// Version 1.3.2 - 22 March 2019, ProMIND
//
// Added OpenVPNInfo() and OpenVPNCopyright()

#include <unistd.h>
#include <string.h>

#include "stdafx.h"
#include "api.h"

#include "breakpad.h"
#include "client.h"
#include "utils.h"

#include "client/ovpncli.hpp"

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING_EDDIE_NAMESPACE

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

static Client *openVPNClient = nullptr;

static bool g_initialized = false;

EddieLibraryResult libResult;
std::string logMessage;

#ifdef __JNI
jobject jniLibResult = nullptr;
jobject jniTransportStats = nullptr;
#endif

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#ifdef __cplusplus
extern "C" {
#endif

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

const char *Name()
{
    const char *c = EDDIE_LIBRARY_NAME;

	return c;
}

#ifdef __JNI

JNIEXPORT jstring JNICALL Java_org_airvpn_eddie_EddieLibrary_name(JNIEnv *env, jobject obj)
{
    return env->NewStringUTF(Name());
}

#endif

const char *Version()
{
    const char *c = EDDIE_LIBRARY_VERSION;

	return c;
}

#ifdef __JNI

JNIEXPORT jstring JNICALL Java_org_airvpn_eddie_EddieLibrary_version(JNIEnv *env, jobject obj)
{
    return env->NewStringUTF(Version());
}

#endif

const char *QualifiedName()
{
    const char *c = EDDIE_LIBRARY_COMPLETE_NAME;

	return c;
}

#ifdef __JNI

JNIEXPORT jstring JNICALL Java_org_airvpn_eddie_EddieLibrary_qualifiedName(JNIEnv *env, jobject obj)
{
    return env->NewStringUTF(QualifiedName());
}

#endif

const char *ReleaseDate()
{
    const char *c = EDDIE_LIBRARY_RELEASE_DATE;

	return c;
}

#ifdef __JNI

JNIEXPORT jstring JNICALL Java_org_airvpn_eddie_EddieLibrary_releaseDate(JNIEnv *env, jobject obj)
{
    return env->NewStringUTF(ReleaseDate());
}

#endif

int ApiLevel()
{
	return EDDIE_LIBRARY_API_LEVEL;
}

#ifdef __JNI

JNIEXPORT jint JNICALL Java_org_airvpn_eddie_EddieLibrary_apiLevel(JNIEnv *env, jobject obj)
{
    return ApiLevel();
}

#endif

const char *Architecture()
{
    const char *c = EDDIE_LIBRARY_ARCHITECTURE;

	return c;
}

#ifdef __JNI

JNIEXPORT jstring JNICALL Java_org_airvpn_eddie_EddieLibrary_architecture(JNIEnv *env, jobject obj)
{
    return env->NewStringUTF(Architecture());
}

#endif

const char *Platform()
{
    const char *c = EDDIE_LIBRARY_PLATFORM;

	return c;
}

#ifdef __JNI

JNIEXPORT jstring JNICALL Java_org_airvpn_eddie_EddieLibrary_platform(JNIEnv *env, jobject obj)
{
    return env->NewStringUTF(Platform());
}

#endif

EddieLibraryResult InitOpenVPN()
{
	try
	{
		utils::log_debug("Initializing OpenVPN3");

		if(g_initialized == true)
        {
            libResult.code = LR_OPENVPN_ALREADY_INITIALIZED;
            libResult.description = "InitOpenVPN(): OpenVPN3 is already initialized";

            return libResult;
        }

		if(breakpad_init() == false)
        {
            libResult.code = LR_BREAKPAD_INITIALIZATION_ERROR;
            libResult.description = "InitOpenVPN(): Breakpad initialization failed";

            return libResult;
        }

		Client::init();

		utils::log_debug("OpenVPN3 initialization complete");

		g_initialized = true;

        libResult.code = LR_SUCCESS;
        libResult.description = LIBRARY_RESULT_OK;
	}
	catch(std::exception &e)
	{
        logMessage = utils::format("InitOpenVPN() failed: %s", e.what());

		utils::log_error(logMessage);

        libResult.code = LR_EXCEPTION_ERROR;
        libResult.description = logMessage.c_str();
    }
	catch(...)
	{
		logMessage = "InitOpenVPN(): Unknown exception";

		utils::log_error(logMessage);

        libResult.code = LR_EXCEPTION_ERROR;
        libResult.description = logMessage.c_str();
	}

	return libResult;
}

#ifdef __JNI

JNIEXPORT jobject JNICALL Java_org_airvpn_eddie_EddieLibrary_initOpenVPN(JNIEnv *env, jobject obj)
{
    if(setJniLibResult(env, InitOpenVPN()))
        return jniLibResult;
    else
        return nullptr;
}

#endif

EddieLibraryResult CleanUpOpenVPN()
{
	try
	{
		utils::log_debug("Cleaning up OpenVPN3");

		if(g_initialized == false)
        {
            libResult.code = LR_OPENVPN_NOT_INITIALIZED;
            libResult.description = "CleanUpOpenVPN(): OpenVPN3 is not initialized";

            return libResult;
        }

		Client::cleanup();

		breakpad_cleanup();

		utils::log_debug("OpenVPN3 clean up completed");

		g_initialized = false;

        libResult.code = LR_SUCCESS;
        libResult.description = LIBRARY_RESULT_OK;
	}
	catch(std::exception &e)
	{
		logMessage = utils::format("CleanUpOpenVPN() failed: %s", e.what());

		utils::log_error(logMessage);

        libResult.code = LR_EXCEPTION_ERROR;
        libResult.description = logMessage.c_str();
	}
	catch(...)
	{
		logMessage = "CleanUpOpenVPN(): Unknown exception";

		utils::log_error(logMessage);

        libResult.code = LR_EXCEPTION_ERROR;
        libResult.description = logMessage.c_str();
	}

	return libResult;
}

#ifdef __JNI

JNIEXPORT jobject JNICALL Java_org_airvpn_eddie_EddieLibrary_cleanUpOpenVPN(JNIEnv *env, jobject obj)
{
    jobject retObj = nullptr;

    if(jniTransportStats != nullptr)
        env->DeleteGlobalRef(jniTransportStats);

    if(setJniLibResult(env, CleanUpOpenVPN()))
    {
        memcpy(&retObj, jniLibResult, sizeof(_jobject));

        env->DeleteGlobalRef(jniLibResult);

        jniLibResult = nullptr;

        return retObj;
    }
    else
        return nullptr;
}

#endif

#ifdef __JNI
EddieLibraryResult CreateOpenVPNClient(JNIEnv *env, jobject callbackClient)
#else
EddieLibraryResult CreateOpenVPNClient(ovpn3_client_callback *callbackClient)
#endif
{
	try
	{
		utils::log_debug("CreateOpenVPNClient()");

#ifndef __JNI
		if(callbackClient == nullptr)
        {
            libResult.code = LR_OPENVPN_POINTER_IS_NULL;
            libResult.description = "CreateOpenVPNClient(): OpenVPN3 client callback is null";

            return libResult;
        }
#endif

		if(g_initialized == false)
        {
            libResult.code = LR_OPENVPN_NOT_INITIALIZED;
            libResult.description = "CreateOpenVPNClient(): OpenVPN3 is not initialized";

            return libResult;
        }

        if(openVPNClient != nullptr)
            DisposeOpenVPNClient();

#ifdef __JNI
        openVPNClient = new Client(env, callbackClient);
#else
        openVPNClient = new Client(callbackClient);
#endif

        if(openVPNClient != nullptr)
        {
            libResult.code = LR_SUCCESS;
            libResult.description = LIBRARY_RESULT_OK;

            openVPNClient->logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, "Successfully created a new OpenVPN client");

            // openVPNClient->logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, utils::format("Free memory: %d bytes", GetAvailableMemory()));
        }
        else
        {
            libResult.code = LR_FAILED_TO_CREATE_OPENVPN_CLIENT;
            libResult.description = "CreateOpenVPNClient(): Failed to create a new OpenVPN client";
        }
	}
	catch(std::exception &e)
	{
		logMessage = utils::format("CreateOpenVPNClient() failed: %s", e.what());

		utils::log_error(logMessage);

        libResult.code = LR_EXCEPTION_ERROR;
        libResult.description = logMessage.c_str();
	}
	catch(...)
	{
		logMessage = "CreateOpenVPNClient(): Unknown exception";

		utils::log_error(logMessage);

        libResult.code = LR_EXCEPTION_ERROR;
        libResult.description = logMessage.c_str();
	}

	return libResult;
}

#ifdef __JNI

JNIEXPORT jobject JNICALL Java_org_airvpn_eddie_EddieLibrary_createOpenVPNClient(JNIEnv *env, jobject obj, jobject clientCallback)
{
    if(setJniLibResult(env, CreateOpenVPNClient(env, clientCallback)))
        return jniLibResult;
    else
        return nullptr;
}

#endif

EddieLibraryResult DisposeOpenVPNClient()
{
	try
	{
		utils::log_debug("DisposeOpenVPNClient()");

		if(g_initialized == false)
        {
            libResult.code = LR_OPENVPN_NOT_INITIALIZED;
            libResult.description = "DisposeOpenVPNClient(): OpenVPN3 is not initialized";

            return libResult;
        }

        if(openVPNClient == nullptr)
        {
            libResult.code = LR_OPENVPN_POINTER_IS_NULL;
            libResult.description = "DisposeOpenVPNClient(): OpenVPN3 client is null";

            return libResult;
        }

        openVPNClient->logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, "Disposing current OpenVPN client");

        delete openVPNClient;

        openVPNClient = nullptr;

        libResult.code = LR_SUCCESS;
        libResult.description = LIBRARY_RESULT_OK;
	}
	catch(std::exception &e)
	{
		logMessage = utils::format("DisposeOpenVPNClient() failed: %s", e.what());

		utils::log_error(logMessage);

        libResult.code = LR_EXCEPTION_ERROR;
        libResult.description = logMessage.c_str();
	}
	catch(...)
	{
		logMessage = "DisposeOpenVPNClient(): Unknown exception";

		utils::log_error(logMessage);

        libResult.code = LR_EXCEPTION_ERROR;
        libResult.description = logMessage.c_str();
	}

	return libResult;
}

#ifdef __JNI

JNIEXPORT jobject JNICALL Java_org_airvpn_eddie_EddieLibrary_disposeOpenVPNClient(JNIEnv *env, jobject obj, jobject callbackObject)
{
    libResult = setCallbackObject(env, callbackObject);

     if(libResult.code != LR_SUCCESS)
        return nullptr;

    if(setJniLibResult(env, DisposeOpenVPNClient()))
        return jniLibResult;
    else
        return nullptr;
}

#endif

EddieLibraryResult StartOpenVPNClient()
{
	try
	{
		utils::log_debug("StartOpenVPNClient()");

        if(openVPNClient == nullptr)
        {
            libResult.code = LR_OPENVPN_POINTER_IS_NULL;
            libResult.description = "StartOpenVPNClient(): OpenVPN3 client is null";

            return libResult;
        }

		logMessage = "starting openVPNClient";

        utils::log_debug(logMessage);

        openVPNClient->logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, logMessage);

		libResult = openVPNClient->start();
	}
	catch(std::exception &e)
	{
        if(openVPNClient != nullptr)
            openVPNClient->logEvent(EDDIE_EVENT_TYPE_ERROR, EDDIE_EVENT_NAME_TAG, utils::format("StartOpenVPNClient() failed: %s", e.what()));

		logMessage = utils::format("StartOpenVPNClient() failed: %s", e.what());

		utils::log_error(logMessage);

        libResult.code = LR_EXCEPTION_ERROR;
        libResult.description = logMessage.c_str();
	}
	catch(...)
	{
        if(openVPNClient != nullptr)
            openVPNClient->logEvent(EDDIE_EVENT_TYPE_ERROR, EDDIE_EVENT_NAME_TAG, "Unknown error in StartOpenVPNClient()");

		logMessage = "StartOpenVPNClient(): Unknown exception";

		utils::log_error(logMessage);

        libResult.code = LR_EXCEPTION_ERROR;
        libResult.description = logMessage.c_str();
	}

	return libResult;
}

#ifdef __JNI

JNIEXPORT jobject JNICALL Java_org_airvpn_eddie_EddieLibrary_startOpenVPNClient(JNIEnv *env, jobject obj, jobject callbackObject)
{
    libResult = setCallbackObject(env, callbackObject);

     if(libResult.code != LR_SUCCESS)
        return nullptr;

    if(setJniLibResult(env, StartOpenVPNClient()))
        return jniLibResult;
    else
        return nullptr;
}

#endif

EddieLibraryResult StopOpenVPNClient()
{
	try
	{
		utils::log_debug("StopOpenVPNClient()");

        if(openVPNClient == nullptr)
        {
            libResult.code = LR_OPENVPN_POINTER_IS_NULL;
            libResult.description = "StopOpenVPNClient(): OpenVPN3 client is null";

            return libResult;
        }

        logMessage = "Stopping OpenVPN client";

		utils::log_debug(logMessage);

        openVPNClient->logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, logMessage);

		openVPNClient->stop();

        logMessage = "OpenVPN client stopped";

		utils::log_debug(logMessage);

        openVPNClient->logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, logMessage);

        libResult.code = LR_SUCCESS;
        libResult.description = LIBRARY_RESULT_OK;
	}
	catch(std::exception &e)
	{
        logMessage = utils::format("StopOpenVPNClient() failed: %s", e.what());

        if(openVPNClient != nullptr)
            openVPNClient->logEvent(EDDIE_EVENT_TYPE_ERROR, EDDIE_EVENT_NAME_TAG, logMessage);

		utils::log_error(logMessage);

        libResult.code = LR_EXCEPTION_ERROR;
        libResult.description = logMessage.c_str();
	}
	catch(...)
	{
        if(openVPNClient != nullptr)
            openVPNClient->logEvent(EDDIE_EVENT_TYPE_ERROR, EDDIE_EVENT_NAME_TAG, "Unknown error in StopOpenVPNClient()");

		logMessage = "StopOpenVPNClient(): Unknown exception";

		utils::log_error(logMessage);

        libResult.code = LR_EXCEPTION_ERROR;
        libResult.description = logMessage.c_str();
	}

	return libResult;
}

#ifdef __JNI

JNIEXPORT jobject JNICALL Java_org_airvpn_eddie_EddieLibrary_stopOpenVPNClient(JNIEnv *env, jobject obj, jobject callbackObject)
{
    libResult = setCallbackObject(env, callbackObject);

     if(libResult.code != LR_SUCCESS)
        return nullptr;

    if(setJniLibResult(env, StopOpenVPNClient()))
        return jniLibResult;
    else
        return nullptr;
}

#endif

EddieLibraryResult PauseOpenVPNClient(const char *reason)
{
	try
	{
		utils::log_debug("PauseOpenVPNClient()");

        if(openVPNClient == nullptr)
        {
            libResult.code = LR_OPENVPN_POINTER_IS_NULL;
            libResult.description = "PauseOpenVPNClient(): OpenVPN3 client is null";

            return libResult;
        }

		logMessage = "Pausing OpenVPN client";

        utils::log_debug(logMessage);

        openVPNClient->logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, logMessage);

		openVPNClient->pause(reason != nullptr ? reason : "");

		logMessage = "OpenVPN client paused";

        utils::log_debug(logMessage);

        openVPNClient->logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, logMessage);

        libResult.code = LR_SUCCESS;
        libResult.description = LIBRARY_RESULT_OK;
	}
	catch(std::exception &e)
	{
        logMessage = utils::format("PauseOpenVPNClient() failed: %s", e.what());

        if(openVPNClient != nullptr)
            openVPNClient->logEvent(EDDIE_EVENT_TYPE_ERROR, EDDIE_EVENT_NAME_TAG, logMessage);

		utils::log_error(logMessage);

        libResult.code = LR_EXCEPTION_ERROR;
        libResult.description = logMessage.c_str();
	}
	catch(...)
	{
		logMessage = "PauseOpenVPNClient(): Unknown exception";

        if(openVPNClient != nullptr)
            openVPNClient->logEvent(EDDIE_EVENT_TYPE_ERROR, EDDIE_EVENT_NAME_TAG, logMessage);

		utils::log_error(logMessage);

        libResult.code = LR_EXCEPTION_ERROR;
        libResult.description = logMessage.c_str();
	}

	return libResult;
}

#ifdef __JNI

JNIEXPORT jobject JNICALL Java_org_airvpn_eddie_EddieLibrary_pauseOpenVPNClient(JNIEnv *env, jobject obj, jobject callbackObject, jstring reason)
{
    const char *s;

    libResult = setCallbackObject(env, callbackObject);

     if(libResult.code != LR_SUCCESS)
        return nullptr;

    s = env->GetStringUTFChars(reason, NULL);

    if(setJniLibResult(env, PauseOpenVPNClient(s)))
        return jniLibResult;
    else
        return nullptr;
}

#endif

EddieLibraryResult ResumeOpenVPNClient()
{
	try
	{
		utils::log_debug("ResumeOpenVPNClient()");

        if(openVPNClient == nullptr)
        {
            libResult.code = LR_OPENVPN_POINTER_IS_NULL;
            libResult.description = "ResumeOpenVPNClient(): OpenVPN3 client is null";

            return libResult;
        }

		logMessage = "Resuming OpenVPN client";

        utils::log_debug(logMessage);

        openVPNClient->logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, logMessage);

		openVPNClient->resume();

        logMessage = "OpenVPN client resumed";

		utils::log_debug(logMessage);

        openVPNClient->logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, logMessage);

        libResult.code = LR_SUCCESS;
        libResult.description = LIBRARY_RESULT_OK;
	}
	catch(std::exception &e)
	{
		logMessage = utils::format("ResumeOpenVPNClient() failed: %s", e.what());

        if(openVPNClient != nullptr)
            openVPNClient->logEvent(EDDIE_EVENT_TYPE_ERROR, EDDIE_EVENT_NAME_TAG, logMessage);

		utils::log_error(logMessage);

        libResult.code = LR_EXCEPTION_ERROR;
        libResult.description = logMessage.c_str();
	}
	catch(...)
	{
		logMessage = "ResumeOpenVPNClient(): Unknown exception";

        if(openVPNClient != nullptr)
            openVPNClient->logEvent(EDDIE_EVENT_TYPE_ERROR, EDDIE_EVENT_NAME_TAG, logMessage);

		utils::log_error(logMessage);

        libResult.code = LR_EXCEPTION_ERROR;
        libResult.description = logMessage.c_str();
	}

	return libResult;
}

#ifdef __JNI

JNIEXPORT jobject JNICALL Java_org_airvpn_eddie_EddieLibrary_resumeOpenVPNClient(JNIEnv *env, jobject obj, jobject callbackObject)
{
    libResult = setCallbackObject(env, callbackObject);

     if(libResult.code != LR_SUCCESS)
        return nullptr;

    if(setJniLibResult(env, ResumeOpenVPNClient()))
        return jniLibResult;
    else
        return nullptr;
}

#endif

EddieLibraryResult GetOpenVPNClientTransportStats(ovpn3_transport_stats *stats)
{
	try
	{
        if(openVPNClient == nullptr)
        {
            libResult.code = LR_OPENVPN_POINTER_IS_NULL;
            libResult.description = "GetOpenVPNClientTransportStats(): OpenVPN3 client is null";

            return libResult;
        }

        if(stats == nullptr)
        {
            libResult.code = LR_OPENVPN_TRANSPORT_STATS_POINTER_IS_NULL;
            libResult.description = "GetOpenVPNClientTransportStats(): stats is null";

            return libResult;
        }

        openVPNClient->getTransportStats(*stats);

        libResult.code = LR_SUCCESS;
        libResult.description = LIBRARY_RESULT_OK;
	}
	catch(std::exception &e)
	{
		logMessage = utils::format("GetOpenVPNClientTransportStats() failed: %s", e.what());

        if(openVPNClient != nullptr)
            openVPNClient->logEvent(EDDIE_EVENT_TYPE_ERROR, EDDIE_EVENT_NAME_TAG, logMessage);

		utils::log_error(logMessage);

        libResult.code = LR_EXCEPTION_ERROR;
        libResult.description = logMessage.c_str();
	}
	catch(...)
	{
		logMessage = "GetOpenVPNClientTransportStats(): Unknown exception";

        if(openVPNClient != nullptr)
            openVPNClient->logEvent(EDDIE_EVENT_TYPE_ERROR, EDDIE_EVENT_NAME_TAG, logMessage);

		utils::log_error(logMessage);

        libResult.code = LR_EXCEPTION_ERROR;
        libResult.description = logMessage.c_str();
	}

	return libResult;
}

#ifdef __JNI

JNIEXPORT jobject JNICALL Java_org_airvpn_eddie_EddieLibrary_getOpenVPNClientTransportStats(JNIEnv *env, jobject obj)
{
    ovpn3_transport_stats tstats;
    EddieLibraryResult result;
    static jfieldID fcode, fdesc, fbin, fbout, fpin, fpout, flpr;
    jstring jDescription;

    result = GetOpenVPNClientTransportStats(&tstats);

     if(result.code != LR_SUCCESS)
        return nullptr;

    env->ExceptionClear();

    if(jniTransportStats == nullptr)
    {
        jclass cls = env->FindClass("org/airvpn/eddie/OpenVPNTransportStats");

        if(cls == nullptr)
        {
            utils::log_debug("getOpenVPNClientTransportStats(): Can't find OpenVPNTransportStats class");

            return nullptr;
        }

        jmethodID constructorID = env->GetMethodID(cls, "<init>","()V");

        if(constructorID == nullptr)
        {
            env->DeleteLocalRef(cls);

            return nullptr;
        }

        jobject statObject = env->NewObject(cls, constructorID);

        if(statObject == nullptr)
        {
            env->DeleteLocalRef(cls);

            return nullptr;
        }

        jniTransportStats = env->NewGlobalRef(statObject);

        env->DeleteLocalRef(statObject);

        fcode = env->GetFieldID(cls, "resultCode", "I");
        fdesc = env->GetFieldID(cls, "resultDescription", "Ljava/lang/String;");
        fbin = env->GetFieldID(cls, "bytesIn", "J");
        fbout = env->GetFieldID(cls, "bytesOut", "J");
        fpin = env->GetFieldID(cls, "packetsIn", "J");
        fpout = env->GetFieldID(cls, "packetsOut", "J");
        flpr = env->GetFieldID(cls, "lastPacketReceived", "I");

        if(fcode == nullptr ||
           fdesc == nullptr ||
           fbin == nullptr ||
           fbout == nullptr ||
           fpin == nullptr ||
           fpout == nullptr ||
           flpr == nullptr)
            return nullptr;

        env->DeleteLocalRef(cls);
    }

    jDescription = env->NewStringUTF(result.description);

    env->SetIntField(jniTransportStats, fcode, result.code);
    env->SetObjectField(jniTransportStats, fdesc, jDescription);
    env->SetLongField(jniTransportStats, fbin, tstats.bytes_in);
    env->SetLongField(jniTransportStats, fbout, tstats.bytes_out);
    env->SetLongField(jniTransportStats, fpin, tstats.packets_in);
    env->SetLongField(jniTransportStats, fpout, tstats.packets_out);
    env->SetIntField(jniTransportStats, flpr, tstats.last_packet_received);

    env->DeleteLocalRef(jDescription);

    return jniTransportStats;
}

#endif

EddieLibraryResult LoadProfileToOpenVPNClient(const char *filename)
{
	try
	{
		utils::log_debug("LoadProfileToOpenVPNClient()");

        if(filename == nullptr)
        {
            libResult.code = LR_PROFILE_FILENAME_IS_NULL;
            libResult.description = "LoadProfileToOpenVPNClient(): filename is null";

            return libResult;
        }

		utils::log_debug(utils::format("LoadProfileToOpenVPNClient('%s')", filename));

        if(openVPNClient == nullptr)
        {
            libResult.code = LR_OPENVPN_POINTER_IS_NULL;
            libResult.description = "LoadProfileToOpenVPNClient(): OpenVPN3 client is null";

            return libResult;
        }

        if(openVPNClient != nullptr)
            openVPNClient->logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, utils::format("Load '%s' profile to OpenVPN client", filename));

        libResult = openVPNClient->loadProfileFile(filename);
	}
	catch(std::exception &e)
	{
		logMessage = utils::format("LoadProfileToOpenVPNClient() failed: %s", e.what());

        if(openVPNClient != nullptr)
            openVPNClient->logEvent(EDDIE_EVENT_TYPE_ERROR, EDDIE_EVENT_NAME_TAG, logMessage);

		utils::log_error(logMessage);

        libResult.code = LR_EXCEPTION_ERROR;
        libResult.description = logMessage.c_str();
	}
	catch(...)
	{
		logMessage = "LoadProfileToOpenVPNClient(): Unknown exception";

        if(openVPNClient != nullptr)
            openVPNClient->logEvent(EDDIE_EVENT_TYPE_ERROR, EDDIE_EVENT_NAME_TAG, logMessage);

		utils::log_error(logMessage);

        libResult.code = LR_EXCEPTION_ERROR;
        libResult.description = logMessage.c_str();
	}

	return libResult;
}

#ifdef __JNI

JNIEXPORT jobject JNICALL Java_org_airvpn_eddie_EddieLibrary_loadProfileToOpenVPNClient(JNIEnv *env, jobject obj, jobject callbackObject, jstring filename)
{
    const char *s;

    libResult = setCallbackObject(env, callbackObject);

     if(libResult.code != LR_SUCCESS)
        return nullptr;

    s = env->GetStringUTFChars(filename, NULL);

    if(setJniLibResult(env, LoadProfileToOpenVPNClient(s)))
        return jniLibResult;
    else
        return nullptr;
}

#endif

EddieLibraryResult LoadStringProfileToOpenVPNClient(const char *str)
{
	try
	{
		utils::log_debug("LoadStringProfileToOpenVPNClient()");

        if(str == nullptr)
        {
            libResult.code = LR_PROFILE_STRING_IS_NULL;
            libResult.description = "LoadStringProfileToOpenVPNClient(): string is null";

            return libResult;
        }

        if(openVPNClient == nullptr)
        {
            libResult.code = LR_OPENVPN_POINTER_IS_NULL;
            libResult.description = "LoadProfileToOpenVPNClient(): OpenVPN3 client is null";

            return libResult;
        }

        libResult = openVPNClient->loadProfileString(str);
	}
	catch(std::exception &e)
	{
		logMessage = utils::format("LoadStringProfileToOpenVPNClient() failed: %s", e.what());

        if(openVPNClient != nullptr)
            openVPNClient->logEvent(EDDIE_EVENT_TYPE_ERROR, EDDIE_EVENT_NAME_TAG, logMessage);

		utils::log_error(logMessage);

        libResult.code = LR_EXCEPTION_ERROR;
        libResult.description = logMessage.c_str();
	}
	catch(...)
	{
		logMessage = "LoadStringProfileToOpenVPNClient(): Unknown exception";

        if(openVPNClient != nullptr)
            openVPNClient->logEvent(EDDIE_EVENT_TYPE_ERROR, EDDIE_EVENT_NAME_TAG, logMessage);

		utils::log_error(logMessage);

        libResult.code = LR_EXCEPTION_ERROR;
        libResult.description = logMessage.c_str();
	}

	return libResult;
}

#ifdef __JNI

JNIEXPORT jobject JNICALL Java_org_airvpn_eddie_EddieLibrary_loadStringProfileToOpenVPNClient(JNIEnv *env, jobject obj, jobject callbackObject, jstring profile)
{
    const char *s;

    libResult = setCallbackObject(env, callbackObject);

     if(libResult.code != LR_SUCCESS)
        return nullptr;

    s = env->GetStringUTFChars(profile, NULL);

    if(setJniLibResult(env, LoadStringProfileToOpenVPNClient(s)))
        return jniLibResult;
    else
        return nullptr;
}

#endif

EddieLibraryResult SetOpenVPNClientOption(const char *option, const char *value)
{
	try
	{
		utils::log_debug("SetOpenVPNClientOption()");

        if(option == nullptr)
        {
            libResult.code = LR_OPENVPN_OPTION_NAME_IS_NULL;
            libResult.description = "SetOpenVPNClientOption(): Option name is null";

            return libResult;
        }

        if(value == nullptr)
        {
            libResult.code = LR_OPENVPN_OPTION_VALUE_IS_NULL;
            libResult.description = "SetOpenVPNClientOption(): Option value is null";

            return libResult;
        }

		utils::log_debug(utils::format("SetOpenVPNClientOption() '%s:%s'", option, value));

        if(openVPNClient == nullptr)
        {
            libResult.code = LR_OPENVPN_POINTER_IS_NULL;
            libResult.description = "SetOpenVPNClientOption(): OpenVPN3 client is null";

            return libResult;
        }

        openVPNClient->logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, utils::format("OpenVPN client: option '%s' set to '%s'", option, value));

        libResult = openVPNClient->setOption(option, value);
	}
	catch(std::exception &e)
	{
		logMessage = utils::format("SetOpenVPNClientOption() failed: %s", e.what());

        if(openVPNClient != nullptr)
            openVPNClient->logEvent(EDDIE_EVENT_TYPE_ERROR, EDDIE_EVENT_NAME_TAG, logMessage);

		utils::log_error(logMessage);

        libResult.code = LR_EXCEPTION_ERROR;
        libResult.description = logMessage.c_str();
	}
	catch(...)
	{
		logMessage = "SetOpenVPNClientOption(): Unknown exception";

        if(openVPNClient != nullptr)
            openVPNClient->logEvent(EDDIE_EVENT_TYPE_ERROR, EDDIE_EVENT_NAME_TAG, logMessage);

		utils::log_error(logMessage);

        libResult.code = LR_EXCEPTION_ERROR;
        libResult.description = logMessage.c_str();
	}

	return libResult;
}

#ifdef __JNI

JNIEXPORT jobject JNICALL Java_org_airvpn_eddie_EddieLibrary_setOpenVPNClientOption(JNIEnv *env, jobject obj, jobject callbackObject, jstring option, jstring value)
{
    const char *opt, *val;

    libResult = setCallbackObject(env, callbackObject);

     if(libResult.code != LR_SUCCESS)
        return nullptr;

    opt = env->GetStringUTFChars(option, NULL);
    val = env->GetStringUTFChars(value, NULL);

    if(setJniLibResult(env, SetOpenVPNClientOption(opt, val)))
        return jniLibResult;
    else
        return nullptr;
}

#endif

unsigned long long GetAvailableMemory()
{
    return sysconf(_SC_AVPHYS_PAGES) * sysconf(_SC_PAGE_SIZE);
}

#ifdef __JNI

bool setJniLibResult(JNIEnv *env, EddieLibraryResult result)
{
    jobject obj;
    static jfieldID fcode, fdesc;
    jstring jDescription;

    if(jniLibResult == nullptr)
    {
        env->ExceptionClear();

        jclass cls = env->FindClass("org/airvpn/eddie/EddieLibraryResult");

        if(cls == nullptr)
        {
            utils::log_debug("setJniLibResult(): Can't find EddieLibraryResult class");

            return false;
        }

        jmethodID constructorID = env->GetMethodID(cls, "<init>","()V");

        if(constructorID == nullptr)
            return false;

        obj = env->NewObject(cls, constructorID);

        if(obj == nullptr)
            return false;

        jniLibResult = env->NewGlobalRef(obj);

        env->DeleteLocalRef(obj);

        fcode = env->GetFieldID(cls, "code", "I");
        fdesc = env->GetFieldID(cls, "description", "Ljava/lang/String;");

        env->DeleteLocalRef(cls);
    }

    if(jniLibResult == nullptr || fcode == nullptr || fdesc == nullptr)
        return false;

    jDescription = env->NewStringUTF(result.description);

    env->SetIntField(jniLibResult, fcode, result.code);
    env->SetObjectField(jniLibResult, fdesc, jDescription);

    env->DeleteLocalRef(jDescription);

    return true;
}

EddieLibraryResult setCallbackObject(JNIEnv *env, jobject callbackObject)
{
    if(openVPNClient == nullptr)
    {
        libResult.code = LR_OPENVPN_POINTER_IS_NULL;
        libResult.description = "CreateOpenVPNClient(): OpenVPN3 client is null";

        return libResult;
    }

    openVPNClient->setJniCallbackObject(callbackObject);

    libResult.code = LR_SUCCESS;
    libResult.description = LIBRARY_RESULT_OK;

    return libResult;
}

JNIEXPORT jobject JNICALL Java_org_airvpn_eddie_EddieLibrary_setCallback(JNIEnv *env, jobject obj, jobject callbackObject)
{
    if(setJniLibResult(env, setCallbackObject(env, callbackObject)))
        return jniLibResult;
    else
        return nullptr;
}

#endif

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#ifdef __cplusplus
}
#endif

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
