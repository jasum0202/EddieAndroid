// <eddie_source_header>
// This file is part of Eddie/AirVPN software.
// Copyright (C) 2014-2018 AirVPN (support@airvpn.org) / https://airvpn.org
//
// Eddie is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Eddie is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Eddie. If not, see <http://www.gnu.org/licenses/>.
// </eddie_source_header>

// Version 1.1 - 20 July 2018, ProMIND
//
// Added Client::Impl::logEvent method
// Added Client::Impl::logConfig method
// Added Client::Impl::logCredentials method

// Version 1.2 - 7 August 2018, ProMIND
//
// Changed return values in some functions in order to comply to API 4

#include "stdafx.h"
#include "client.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "client/ovpncli.hpp"
#include "constants.h"
#include "openvpn/common/arraysize.hpp"
#include "openvpn/client/clievent.hpp"
#include "openvpn/client/cliconstants.hpp"
#include "openvpn/options/merge.hpp"
#include "openvpn/error/error.hpp"
#include "options.h"
#include "utils.h"

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

EDDIE_NAMESPACE_BEGIN

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

class Client::Impl : public openvpn::ClientAPI::OpenVPNClient
{
    typedef openvpn::ClientAPI::OpenVPNClient BaseClass;

    typedef std::unordered_map<std::string, std::string> OptionsMap;

    // Construction
    public:
#ifdef __JNI
        Impl(JNIEnv *env, jobject callbackClient);
#else
        Impl(ovpn3_client_callback *callbackClient);
#endif
        virtual ~Impl();

        const std::string & getOption(const std::string &name, const std::string &defValue = constants::empty_string) const;
        EddieLibraryResult setOption(const std::string &name, const std::string &value, bool check = false);

        template <typename T> T getOptionValue(const std::string &name) const;
        template <typename T> T getOptionValue(const std::string &name, const T &defValue) const;

        // Operations

        void initOptions();

        void applyConfig(openvpn::ClientAPI::Config &config);
        void applyCredentials(openvpn::ClientAPI::ProvideCreds &credentials);

        static void eddie_log_debug(const std::string &message);
        static void eddie_log_error(const std::string &message);
        static void ovpn3_log_debug(const std::string &message);
        static void ovpn3_log_error(const std::string &message);
        void logEvent(int level, const char *name, std::string info);

#ifdef __JNI
        void setupJavaVM(JNIEnv *e);
        void setJniCallbackObject(jobject o);
        void setJniCallbackInterface(ovpn3_client_callback_jni *callbackClient);
        JNIEnv *getThreadJniEnv();
#endif
        // OpenVPNClient interface

        virtual void log(const openvpn::ClientAPI::LogInfo &li) EDDIE_OVERRIDE;
        virtual bool socket_protect(int socket, std::string remote, bool ipv6) EDDIE_OVERRIDE;
        virtual bool pause_on_connection_timeout() EDDIE_OVERRIDE;
        virtual void event(const openvpn::ClientAPI::Event &e) EDDIE_OVERRIDE;
        virtual void external_pki_cert_request(openvpn::ClientAPI::ExternalPKICertRequest&) EDDIE_OVERRIDE;
        virtual void external_pki_sign_request(openvpn::ClientAPI::ExternalPKISignRequest&) EDDIE_OVERRIDE;

        // TunBuilderBase overrides

        // Tun builder methods, loosely based on the Android VpnService.Builder
        // abstraction.  These methods comprise an abstraction layer that
        // allows the OpenVPN C++ core to call out to external methods for
        // establishing the tunnel, adding routes, etc.

        // All methods returning bool use the return
        // value to indicate success (true) or fail (false).
        // tun_builder_new() should be called first, then arbitrary setter methods,
        // and finally tun_builder_establish to return the socket descriptor
        // for the session.  IP addresses are pre-validated before being passed to
        // these methods.
        // This interface is based on Android's VpnService.Builder.

        // Callback to construct a new tun builder
        // Should be called first.

        virtual bool tun_builder_new() EDDIE_OVERRIDE;

        // Optional callback that indicates OSI layer, should be 2 or 3.
        // Defaults to 3.

        virtual bool tun_builder_set_layer(int layer) EDDIE_OVERRIDE;

        // Callback to set address of remote server
        // Never called more than once per tun_builder session.

        virtual bool tun_builder_set_remote_address(const std::string &address, bool ipv6) EDDIE_OVERRIDE;

        // Callback to add network address to VPN interface
        // May be called more than once per tun_builder session

        virtual bool tun_builder_add_address(const std::string &address, int prefix_length, const std::string &gateway, bool ipv6, bool net30) EDDIE_OVERRIDE;

        // Optional callback to set default value for route metric.
        // Guaranteed to be called before other methods that deal
        // with routes such as tun_builder_add_route and
        // tun_builder_reroute_gw.  Route metric is ignored
        // if < 0.

        virtual bool tun_builder_set_route_metric_default(int metric) EDDIE_OVERRIDE;

        // Callback to reroute default gateway to VPN interface.
        // ipv4 is true if the default route to be added should be IPv4.
        // ipv6 is true if the default route to be added should be IPv6.
        // flags are defined in RGWFlags (rgwflags.hpp).
        // Never called more than once per tun_builder session.

        virtual bool tun_builder_reroute_gw(bool ipv4, bool ipv6, unsigned int flags) EDDIE_OVERRIDE;

        // Callback to add route to VPN interface
        // May be called more than once per tun_builder session
        // metric is optional and should be ignored if < 0

        virtual bool tun_builder_add_route(const std::string &address, int prefix_length, int metric, bool ipv6) EDDIE_OVERRIDE;

        // Callback to exclude route from VPN interface
        // May be called more than once per tun_builder session
        // metric is optional and should be ignored if < 0

        virtual bool tun_builder_exclude_route(const std::string &address, int prefix_length, int metric, bool ipv6) EDDIE_OVERRIDE;

        // Callback to add DNS server to VPN interface
        // May be called more than once per tun_builder session
        // If reroute_dns is true, all DNS traffic should be routed over the
        // tunnel, while if false, only DNS traffic that matches an added search
        // domain should be routed.
        // Guaranteed to be called after tun_builder_reroute_gw.

        virtual bool tun_builder_add_dns_server(const std::string &address, bool ipv6) EDDIE_OVERRIDE;

        // Callback to add search domain to DNS resolver
        // May be called more than once per tun_builder session
        // See tun_builder_add_dns_server above for description of
        // reroute_dns parameter.
        // Guaranteed to be called after tun_builder_reroute_gw.

        virtual bool tun_builder_add_search_domain(const std::string &domain) EDDIE_OVERRIDE;

        // Callback to set MTU of the VPN interface
        // Never called more than once per tun_builder session.

        virtual bool tun_builder_set_mtu(int mtu) EDDIE_OVERRIDE;

        // Callback to set the session name
        // Never called more than once per tun_builder session.

        virtual bool tun_builder_set_session_name(const std::string &name) EDDIE_OVERRIDE;

        // Callback to add a host which should bypass the proxy
        // May be called more than once per tun_builder session

        virtual bool tun_builder_add_proxy_bypass(const std::string &bypass_host) EDDIE_OVERRIDE;

        // Callback to set the proxy "Auto Config URL"
        // Never called more than once per tun_builder session.

        virtual bool tun_builder_set_proxy_auto_config_url(const std::string &url) EDDIE_OVERRIDE;

        // Callback to set the HTTP proxy
        // Never called more than once per tun_builder session.

        virtual bool tun_builder_set_proxy_http(const std::string &host, int port) EDDIE_OVERRIDE;

        // Callback to set the HTTPS proxy
        // Never called more than once per tun_builder session.

        virtual bool tun_builder_set_proxy_https(const std::string &host, int port) EDDIE_OVERRIDE;

        // Callback to add Windows WINS server to VPN interface.
        // WINS server addresses are always IPv4.
        // May be called more than once per tun_builder session.
        // Guaranteed to be called after tun_builder_reroute_gw.

        virtual bool tun_builder_add_wins_server(const std::string &address) EDDIE_OVERRIDE;

        // Optional callback that indicates whether IPv6 traffic should be
        // blocked, to prevent unencrypted IPv6 packet leakage when the
        // tunnel is IPv4-only, but the local machine has IPv6 connectivity
        // to the internet.  Enabled by "block-ipv6" config var.

        virtual bool tun_builder_set_block_ipv6(bool block_ipv6) EDDIE_OVERRIDE;

        // Optional callback to set a DNS suffix on tun/tap adapter.
        // Currently only implemented on Windows, where it will
        // set the "Connection-specific DNS Suffix" property on
        // the TAP driver.

        virtual bool tun_builder_set_adapter_domain_suffix(const std::string &name) EDDIE_OVERRIDE;

        // Callback to establish the VPN tunnel, returning a file descriptor
        // to the tunnel, which the caller will henceforth own.  Returns -1
        // if the tunnel could not be established.
        // Always called last after tun_builder session has been configured.

        virtual int tun_builder_establish() EDDIE_OVERRIDE;

        // Return true if tun interface may be persisted, i.e. rolled
        // into a new session with properties untouched.  This method
        // is only called after all other tests of persistence
        // allowability succeed, therefore it can veto persistence.
        // If persistence is ultimately enabled,
        // tun_builder_establish_lite() will be called.  Otherwise,
        // tun_builder_establish() will be called.

        virtual bool tun_builder_persist() EDDIE_OVERRIDE;

        // Indicates a reconnection with persisted tun state.

        virtual void tun_builder_establish_lite() EDDIE_OVERRIDE;

        // Indicates that tunnel is being torn down.
        // If disconnect == true, then the teardown is occurring
        // prior to final disconnect.

        virtual void tun_builder_teardown(bool disconnect) EDDIE_OVERRIDE;

    private:
        openvpn::ClientAPI::ConnectionInfo connectionInfo;

        void dumpConfig(openvpn::ClientAPI::Config &config);
        void dumpCredentials(openvpn::ClientAPI::ProvideCreds &credentials);
        void logConfig(openvpn::ClientAPI::Config &config);
        void logCredentials(openvpn::ClientAPI::ProvideCreds &credentials);
        bool logFilter(std::string s);
        bool logFilter(char *cs);
        int getEventType(char *name);
        bool findErrorInString(std::string s);
        bool isUdpPartialSendError(std::string s);
        openvpn::Error::Type getOpenVPNErrorInString(std::string s);
        bool strcontains(const char *haystack, const char *needle);
#ifdef __JNI
    private:
        JavaVM *javaVM;
        jobject jniCallbackObject;

        bool setJniIntField(jclass jniClass, jobject jniObject, const char *fieldName, int value);
        bool setJniStringField(jclass jniClass, jobject jniObject, const char *fieldName, std::string value);
        bool setJniStringField(jclass jniClass, jobject jniObject, const char *fieldName, const char *value);
        bool setJniObjectField(jclass jniClass, jobject jniObject, const char *fieldName, jobject value);
        bool isJniCallbackValid();
#endif
    // OpenVPNClient overrides

    protected:
        virtual void connect_attach() EDDIE_OVERRIDE;
        virtual void connect_pre_run() EDDIE_OVERRIDE;
        virtual void connect_run() EDDIE_OVERRIDE;
        virtual void connect_session_stop() EDDIE_OVERRIDE;

    // class members

    private:
#ifdef __JNI
        ovpn3_client_callback_jni *openVPNClientJniCallbackInterface;
#else
        ovpn3_client_callback openVPNClientCallbackInterface;
#endif
        OptionsMap m_options;
        EddieLibraryResult libResult;
        std::string logMessage;
};

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#ifdef __JNI

Client::Impl::Impl(JNIEnv *env, jobject callbackClient)
{
    javaVM = nullptr;
    jniCallbackObject = nullptr;
    openVPNClientJniCallbackInterface = nullptr;

    setupJavaVM(env);

    setJniCallbackObject(callbackClient);

    initOptions();
}

#else

Client::Impl::Impl(ovpn3_client_callback *callbackClient)
{
    // IMPORTANT: ovpn3_client structure MUST be copied (the caller could deallocate after constructing a Client object)
    memcpy(&openVPNClientCallbackInterface, callbackClient, sizeof(ovpn3_client_callback));

    initOptions();
}

#endif

Client::Impl::~Impl()
{
#ifdef __JNI
    JNIEnv *jniEnv = getThreadJniEnv();

    if(jniCallbackObject != nullptr && jniEnv != nullptr)
    {
        if(jniEnv->GetObjectRefType(jniCallbackObject) == JNIGlobalRefType)
        {
            utils::log_debug("Deleting global reference of jniCallbackObject");

            jniEnv->DeleteGlobalRef(jniCallbackObject);
        }
        else
            utils::log_debug("jniCallbackObject is not a global reference");
    }

    if(openVPNClientJniCallbackInterface != nullptr)
    {
        utils::log_debug("Deleting jniCallbackObject");

        delete openVPNClientJniCallbackInterface;
    }
#endif
}

const std::string & Client::Impl::getOption(const std::string &name, const std::string &defValue) const
{
    OptionsMap::const_iterator i = m_options.find(name);

    if(i != m_options.end())
        return i->second;

    return defValue;
}

EddieLibraryResult Client::Impl::setOption(const std::string &name, const std::string &value, bool check)
{
    if(check && m_options.find(name) == m_options.end())
    {
        logMessage = utils::format("Client::Impl::setOption(): Unknown option '%s'", name.c_str());

        logEvent(EDDIE_EVENT_TYPE_ERROR, EDDIE_EVENT_NAME_TAG, logMessage);

        libResult.code = LR_OPENVPN_UNKNOWN_OPTION;
        libResult.description = logMessage.c_str();
    }
    else
    {
        m_options[name] = value;

        libResult.code = LR_SUCCESS;
        libResult.description = LIBRARY_RESULT_OK;
    }

    return libResult;
}

template <typename T> T Client::Impl::getOptionValue(const std::string &name) const
{
    return utils::from_string<T>(getOption(name));
}

template <typename T> T Client::Impl::getOptionValue(const std::string &name, const T &defValue) const
{
    // Do not check for empty strings here since it could be a "good" value, only returns defValue when a key doesn't exist

    OptionsMap::const_iterator i = m_options.find(name);
    if(i != m_options.end())
        return utils::from_string<T>(i->second);

    return defValue;
}

void Client::Impl::initOptions()
{
    setOption(EDDIE_OVPN3_OPTION_TLS_VERSION_MIN, EDDIE_OVPN3_OPTION_TLS_VERSION_MIN_DEFAULT);
    setOption(EDDIE_OVPN3_OPTION_PROTOCOL, EDDIE_OVPN3_OPTION_PROTOCOL_DEFAULT);
    setOption(EDDIE_OVPN3_OPTION_IPV6, EDDIE_OVPN3_OPTION_IPV6_DEFAULT);
    setOption(EDDIE_OVPN3_OPTION_TIMEOUT, EDDIE_OVPN3_OPTION_TIMEOUT_DEFAULT);
    setOption(EDDIE_OVPN3_OPTION_TUN_PERSIST, EDDIE_OVPN3_OPTION_TUN_PERSIST_DEFAULT);
    setOption(EDDIE_OVPN3_OPTION_COMPRESSION_MODE, EDDIE_OVPN3_OPTION_COMPRESSION_MODE_DEFAULT);
    setOption(EDDIE_OVPN3_OPTION_USERNAME, EDDIE_OVPN3_OPTION_USERNAME_DEFAULT);
    setOption(EDDIE_OVPN3_OPTION_PASSWORD, EDDIE_OVPN3_OPTION_PASSWORD_DEFAULT);
    setOption(EDDIE_OVPN3_OPTION_SYNCHRONOUS_DNS_LOOKUP, EDDIE_OVPN3_OPTION_SYNCHRONOUS_DNS_LOOKUP_DEFAULT);
    setOption(EDDIE_OVPN3_OPTION_AUTOLOGIN_SESSIONS, EDDIE_OVPN3_OPTION_AUTOLOGIN_SESSIONS_DEFAULT);
    setOption(EDDIE_OVPN3_OPTION_DISABLE_CLIENT_CERT, EDDIE_OVPN3_OPTION_DISABLE_CLIENT_CERT_DEFAULT);
    setOption(EDDIE_OVPN3_OPTION_SSL_DEBUG_LEVEL, EDDIE_OVPN3_OPTION_SSL_DEBUG_LEVEL_DEFAULT);
    setOption(EDDIE_OVPN3_OPTION_PRIVATE_KEY_PASSWORD, EDDIE_OVPN3_OPTION_PRIVATE_KEY_PASSWORD_DEFAULT);
    setOption(EDDIE_OVPN3_OPTION_DEFAULT_KEY_DIRECTION, EDDIE_OVPN3_OPTION_DEFAULT_KEY_DIRECTION_DEFAULT);
    setOption(EDDIE_OVPN3_OPTION_FORCE_AES_CBC_CIPHERSUITES, EDDIE_OVPN3_OPTION_FORCE_AES_CBC_CIPHERSUITES_DEFAULT);
    setOption(EDDIE_OVPN3_OPTION_TLS_CERT_PROFILE, EDDIE_OVPN3_OPTION_TLS_CERT_PROFILE_DEFAULT);
    setOption(EDDIE_OVPN3_OPTION_PROXY_HOST, EDDIE_OVPN3_OPTION_PROXY_HOST_DEFAULT);
    setOption(EDDIE_OVPN3_OPTION_PROXY_PORT, EDDIE_OVPN3_OPTION_PROXY_PORT_DEFAULT);
    setOption(EDDIE_OVPN3_OPTION_PROXY_USERNAME, EDDIE_OVPN3_OPTION_PROXY_USERNAME_DEFAULT);
    setOption(EDDIE_OVPN3_OPTION_PROXY_PASSWORD, EDDIE_OVPN3_OPTION_PROXY_PASSWORD_DEFAULT);
    setOption(EDDIE_OVPN3_OPTION_PROXY_ALLOW_CLEARTEXT_AUTH, EDDIE_OVPN3_OPTION_PROXY_ALLOW_CLEARTEXT_AUTH_DEFAULT);
}

void Client::Impl::applyConfig(openvpn::ClientAPI::Config &config)
{
    config.protoOverride = getOption(EDDIE_OVPN3_OPTION_PROTOCOL);
    config.connTimeout = getOptionValue<int>(EDDIE_OVPN3_OPTION_TIMEOUT);
    config.compressionMode = getOption(EDDIE_OVPN3_OPTION_COMPRESSION_MODE);
    config.ipv6 = getOption(EDDIE_OVPN3_OPTION_IPV6);
    config.privateKeyPassword = getOption(EDDIE_OVPN3_OPTION_PRIVATE_KEY_PASSWORD);
    config.tlsVersionMinOverride = getOption(EDDIE_OVPN3_OPTION_TLS_VERSION_MIN);
    config.tlsCertProfileOverride = getOption(EDDIE_OVPN3_OPTION_TLS_CERT_PROFILE);
    config.disableClientCert = getOptionValue<bool>(EDDIE_OVPN3_OPTION_DISABLE_CLIENT_CERT);
    config.proxyHost = getOption(EDDIE_OVPN3_OPTION_PROXY_HOST);
    config.proxyPort = getOption(EDDIE_OVPN3_OPTION_PROXY_PORT);
    config.proxyUsername = getOption(EDDIE_OVPN3_OPTION_PROXY_USERNAME);
    config.proxyPassword = getOption(EDDIE_OVPN3_OPTION_PROXY_PASSWORD);
    config.proxyAllowCleartextAuth = getOptionValue<bool>(EDDIE_OVPN3_OPTION_PROXY_ALLOW_CLEARTEXT_AUTH);
    config.defaultKeyDirection = getOptionValue<int>(EDDIE_OVPN3_OPTION_DEFAULT_KEY_DIRECTION);
    config.forceAesCbcCiphersuites = getOptionValue<bool>(EDDIE_OVPN3_OPTION_FORCE_AES_CBC_CIPHERSUITES);
    config.sslDebugLevel = getOptionValue<int>(EDDIE_OVPN3_OPTION_SSL_DEBUG_LEVEL);
    config.autologinSessions = getOptionValue<bool>(EDDIE_OVPN3_OPTION_AUTOLOGIN_SESSIONS);
    config.tunPersist = getOptionValue<bool>(EDDIE_OVPN3_OPTION_TUN_PERSIST);
    config.synchronousDnsLookup = getOptionValue<bool>(EDDIE_OVPN3_OPTION_SYNCHRONOUS_DNS_LOOKUP);

    dumpConfig(config);

    logConfig(config);
}

void Client::Impl::applyCredentials(openvpn::ClientAPI::ProvideCreds &credentials)
{
    credentials.username = getOption(EDDIE_OVPN3_OPTION_USERNAME);
    credentials.password = getOption(EDDIE_OVPN3_OPTION_PASSWORD);

    dumpCredentials(credentials);

    logCredentials(credentials);
}

void Client::Impl::dumpConfig(openvpn::ClientAPI::Config &config)
{
    eddie_log_debug("config.protoOverride: " + config.protoOverride);
    eddie_log_debug("config.connTimeout: " + utils::to_string(config.connTimeout));
    eddie_log_debug("config.compressionMode: " + config.compressionMode);
    eddie_log_debug("config.ipv6: " + config.ipv6);
    eddie_log_debug("config.privateKeyPassword: ********");
    eddie_log_debug("config.tlsVersionMinOverride: " + config.tlsVersionMinOverride);
    eddie_log_debug("config.tlsCertProfileOverride: " + config.tlsCertProfileOverride);
    eddie_log_debug("config.disableClientCert: " + utils::to_string(config.disableClientCert));
    eddie_log_debug("config.proxyHost: " + config.proxyHost);
    eddie_log_debug("config.proxyPort: " + config.proxyPort);
    eddie_log_debug("config.proxyUsername: ********");
    eddie_log_debug("config.proxyPassword: ********");
    eddie_log_debug("config.proxyAllowCleartextAuth: " + utils::to_string(config.proxyAllowCleartextAuth));
    eddie_log_debug("config.defaultKeyDirection: " + utils::to_string(config.defaultKeyDirection));
    eddie_log_debug("config.forceAesCbcCiphersuites: " + utils::to_string(config.forceAesCbcCiphersuites));
    eddie_log_debug("config.sslDebugLevel: " + utils::to_string(config.sslDebugLevel));
    eddie_log_debug("config.autologinSessions: " + utils::to_string(config.autologinSessions));
    eddie_log_debug("config.tunPersist: " + utils::to_string(config.tunPersist));
    eddie_log_debug("config.synchronousDnsLookup: " + utils::to_string(config.synchronousDnsLookup));
}

void Client::Impl::logConfig(openvpn::ClientAPI::Config &config)
{
    logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, "config.protoOverride: " + config.protoOverride);
    logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, "config.connTimeout: " + utils::to_string(config.connTimeout));
    logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, "config.compressionMode: " + config.compressionMode);
    logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, "config.ipv6: " + config.ipv6);
    logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, "config.privateKeyPassword: ********");
    logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, "config.tlsVersionMinOverride: " + config.tlsVersionMinOverride);
    logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, "config.tlsCertProfileOverride: " + config.tlsCertProfileOverride);
    logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, "config.disableClientCert: " + utils::to_string(config.disableClientCert));
    logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, "config.proxyHost: " + config.proxyHost);
    logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, "config.proxyPort: " + config.proxyPort);
    logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, "config.proxyUsername: ********");
    logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, "config.proxyPassword: ********");
    logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, "config.proxyAllowCleartextAuth: " + utils::to_string(config.proxyAllowCleartextAuth));
    logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, "config.defaultKeyDirection: " + utils::to_string(config.defaultKeyDirection));
    logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, "config.forceAesCbcCiphersuites: " + utils::to_string(config.forceAesCbcCiphersuites));
    logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, "config.sslDebugLevel: " + utils::to_string(config.sslDebugLevel));
    logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, "config.autologinSessions: " + utils::to_string(config.autologinSessions));
    logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, "config.tunPersist: " + utils::to_string(config.tunPersist));
    logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, "config.synchronousDnsLookup: " + utils::to_string(config.synchronousDnsLookup));
}

void Client::Impl::dumpCredentials(openvpn::ClientAPI::ProvideCreds &Credentials)
{
    eddie_log_debug("credentials.username: ********");
    eddie_log_debug("credentials.password: ********");
}

void Client::Impl::logCredentials(openvpn::ClientAPI::ProvideCreds &credentials)
{
    logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, "credentials.username: ********");
    logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, "credentials.password: ********");
}

void Client::Impl::log(const openvpn::ClientAPI::LogInfo &li)
{
    ovpn3_log_debug(li.text);

    logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, li.text);
}

#ifdef __JNI

void Client::Impl::setupJavaVM(JNIEnv *e)
{
    e->GetJavaVM(&javaVM);
}

void Client::Impl::setJniCallbackObject(jobject o)
{
    jmethodID mid = nullptr;
    jclass cls = nullptr;

    JNIEnv *jniEnv = getThreadJniEnv();

    if(jniEnv == nullptr)
    {
        jniCallbackObject = nullptr;

        return;
    }

    if(o == nullptr)
    {
        utils::log_debug("callback object is null. Events will be turned off.");

        if(jniCallbackObject != nullptr)
        {
            if(jniEnv->GetObjectRefType(jniCallbackObject) == JNIGlobalRefType)
                jniEnv->DeleteGlobalRef(jniCallbackObject);
        }

        jniCallbackObject = nullptr;

        return;
    }

    if(o == jniCallbackObject)
    {
        utils::log_debug("callback object has not changed");

        return;
    }

    utils::log_debug("Setting a new callback object");

    if(jniCallbackObject != nullptr)
    {
        if(jniEnv->GetObjectRefType(jniCallbackObject) == JNIGlobalRefType)
            jniEnv->DeleteGlobalRef(jniCallbackObject);
    }

    jniCallbackObject = jniEnv->NewGlobalRef(o);

    cls = jniEnv->GetObjectClass(jniCallbackObject);

    if(openVPNClientJniCallbackInterface != nullptr)
        delete openVPNClientJniCallbackInterface;

    openVPNClientJniCallbackInterface = new ovpn3_client_callback_jni();

    // Client callbacks

    openVPNClientJniCallbackInterface->socket_protect = jniEnv->GetMethodID(cls, "onSocketProtect", "(I)Z");
	openVPNClientJniCallbackInterface->on_event = jniEnv->GetMethodID(cls, "onEvent", "(Ljava/lang/Object;)V");

	// TUN callbacks

	openVPNClientJniCallbackInterface->tun_builder_new = jniEnv->GetMethodID(cls, "onTunBuilderNew", "()Z");
	openVPNClientJniCallbackInterface->tun_builder_set_layer = jniEnv->GetMethodID(cls, "onTunBuilderSetLayer", "(I)Z");
	openVPNClientJniCallbackInterface->tun_builder_set_remote_address = jniEnv->GetMethodID(cls, "onTunBuilderSetRemoteAddress", "(Ljava/lang/String;Z)Z");
	openVPNClientJniCallbackInterface->tun_builder_add_address = jniEnv->GetMethodID(cls, "onTunBuilderAddAddress", "(Ljava/lang/String;ILjava/lang/String;ZZ)Z");
	openVPNClientJniCallbackInterface->tun_builder_set_route_metric_default = jniEnv->GetMethodID(cls, "onTunBuilderSetRouteMetricDefault", "(I)Z");
	openVPNClientJniCallbackInterface->tun_builder_reroute_gw = jniEnv->GetMethodID(cls, "onTunBuilderRerouteGW", "(ZZI)Z");
	openVPNClientJniCallbackInterface->tun_builder_add_route = jniEnv->GetMethodID(cls, "onTunBuilderAddRoute", "(Ljava/lang/String;IIZ)Z");
	openVPNClientJniCallbackInterface->tun_builder_exclude_route = jniEnv->GetMethodID(cls, "onTunBuilderExcludeRoute", "(Ljava/lang/String;IIZ)Z");
	openVPNClientJniCallbackInterface->tun_builder_add_dns_server = jniEnv->GetMethodID(cls, "onTunBuilderAddDNSServer", "(Ljava/lang/String;Z)Z");
	openVPNClientJniCallbackInterface->tun_builder_add_search_domain = jniEnv->GetMethodID(cls, "onTunBuilderAddSearchDomain", "(Ljava/lang/String;)Z");
	openVPNClientJniCallbackInterface->tun_builder_set_mtu = jniEnv->GetMethodID(cls, "onTunBuilderSetMTU", "(I)Z");
	openVPNClientJniCallbackInterface->tun_builder_set_session_name = jniEnv->GetMethodID(cls, "onTunBuilderSetSessionName", "(Ljava/lang/String;)Z");
	openVPNClientJniCallbackInterface->tun_builder_add_proxy_bypass = jniEnv->GetMethodID(cls, "onTunBuilderAddProxyBypass", "(Ljava/lang/String;)Z");
	openVPNClientJniCallbackInterface->tun_builder_set_proxy_auto_config_url = jniEnv->GetMethodID(cls, "onTunBuilderSetProxyAutoConfigUrl", "(Ljava/lang/String;)Z");
	openVPNClientJniCallbackInterface->tun_builder_set_proxy_http = jniEnv->GetMethodID(cls, "onTunBuilderSetProxyHttp", "(Ljava/lang/String;I)Z");
	openVPNClientJniCallbackInterface->tun_builder_set_proxy_https = jniEnv->GetMethodID(cls, "onTunBuilderSetProxyHttps", "(Ljava/lang/String;I)Z");
	openVPNClientJniCallbackInterface->tun_builder_add_wins_server = jniEnv->GetMethodID(cls, "onTunBuilderAddWinsServer", "(Ljava/lang/String;)Z");
	openVPNClientJniCallbackInterface->tun_builder_set_block_ipv6 = jniEnv->GetMethodID(cls, "onTunBuilderSetBlockIPV6", "(Z)Z");
	openVPNClientJniCallbackInterface->tun_builder_set_adapter_domain_suffix = jniEnv->GetMethodID(cls, "onTunBuilderSetAdapterDomainSuffix", "(Ljava/lang/String;)Z");
	openVPNClientJniCallbackInterface->tun_builder_establish = jniEnv->GetMethodID(cls, "onTunBuilderEstablish", "()I");
	openVPNClientJniCallbackInterface->tun_builder_persist = jniEnv->GetMethodID(cls, "onTunBuilderPersist", "()Z");
	openVPNClientJniCallbackInterface->tun_builder_establish_lite = jniEnv->GetMethodID(cls, "onTunBuilderEstablishLite", "()V");
	openVPNClientJniCallbackInterface->tun_builder_teardown = jniEnv->GetMethodID(cls, "onTunBuilderTeardown", "(Z)V");

	// Connection callbacks

	openVPNClientJniCallbackInterface->connect_attach = jniEnv->GetMethodID(cls, "onConnectAttach", "()V");
	openVPNClientJniCallbackInterface->connect_pre_run = jniEnv->GetMethodID(cls, "onConnectPreRun", "()V");
	openVPNClientJniCallbackInterface->connect_run = jniEnv->GetMethodID(cls, "onConnectRun", "()V");
	openVPNClientJniCallbackInterface->connect_session_stop = jniEnv->GetMethodID(cls, "onConnectSessionStop", "()V");
}

void Client::Impl::setJniCallbackInterface(ovpn3_client_callback_jni *callbackClient)
{
    openVPNClientJniCallbackInterface = callbackClient;
}

bool Client::Impl::setJniIntField(jclass jniClass, jobject jniObject, const char *fieldName, int value)
{
    JNIEnv *jniEnv = getThreadJniEnv();

    if(jniEnv == nullptr || jniClass == nullptr || jniObject == nullptr)
    {
        if(jniEnv == nullptr)
            utils::log_debug("setJniStringField(): jniEnv is null");

        if(jniClass == nullptr)
            utils::log_debug("setJniStringField(): jniClass is null");

        if(jniObject == nullptr)
            utils::log_debug("setJniStringField(): jniObject is null");

        return false;
    }

    jfieldID fieldId = jniEnv->GetFieldID(jniClass, fieldName, "I");

    if(fieldId == nullptr)
        return false;

    jniEnv->SetIntField(jniObject, fieldId, value);

    return true;
}

bool Client::Impl::setJniStringField(jclass jniClass, jobject jniObject, const char *fieldName, std::string value)
{
    return setJniStringField(jniClass, jniObject, fieldName, value.c_str());
}

bool Client::Impl::setJniStringField(jclass jniClass, jobject jniObject, const char *fieldName, const char *value)
{
    JNIEnv *jniEnv = getThreadJniEnv();
    jstring jValue;

    if(jniEnv == nullptr || jniClass == nullptr || jniObject == nullptr)
    {
        if(jniEnv == nullptr)
            utils::log_debug("setJniStringField(): jniEnv is null");

        if(jniClass == nullptr)
            utils::log_debug("setJniStringField(): jniClass is null");

        if(jniObject == nullptr)
            utils::log_debug("setJniStringField(): jniObject is null");

        return false;
    }

    jfieldID fieldId = jniEnv->GetFieldID(jniClass, fieldName, "Ljava/lang/String;");

    if(fieldId == nullptr)
        return false;

    jValue = jniEnv->NewStringUTF(value);

    jniEnv->SetObjectField(jniObject, fieldId, jValue);

    jniEnv->DeleteLocalRef(jValue);

    return true;
}

bool Client::Impl::setJniObjectField(jclass jniClass, jobject jniObject, const char *fieldName, jobject value)
{
    JNIEnv *jniEnv = getThreadJniEnv();

    if(jniEnv == nullptr || jniClass == nullptr || jniObject == nullptr)
    {
        if(jniEnv == nullptr)
            utils::log_debug("setJniStringField(): jniEnv is null");

        if(jniClass == nullptr)
            utils::log_debug("setJniStringField(): jniClass is null");

        if(jniObject == nullptr)
            utils::log_debug("setJniStringField(): jniObject is null");

        return false;
    }

    jfieldID fieldId = jniEnv->GetFieldID(jniClass, fieldName, "Ljava/lang/Object;");

    if(fieldId == nullptr)
        return false;

    jniEnv->SetObjectField(jniObject, fieldId, value);

    return true;
}

JNIEnv *Client::Impl::getThreadJniEnv()
{
    JNIEnv *currentThreadEnv;
    JavaVMAttachArgs args;

    if(javaVM == nullptr)
    {
        utils::log_error("Client::Impl::getThreadJniEnv(): javaVM is null");

        return nullptr;
    }

    args.version = JNI_VERSION_1_6;
    args.name = NULL;
    args.group = NULL;

    javaVM->AttachCurrentThread(&currentThreadEnv, &args);

    return currentThreadEnv;
}

#endif

bool Client::Impl::socket_protect(int socket, std::string remote, bool ipv6)
{
#ifdef __JNI
    if(!isJniCallbackValid())
        return false;
#endif

#ifdef __JNI
    if(!openVPNClientJniCallbackInterface->socket_protect)
#else
    if(!openVPNClientCallbackInterface.socket_protect)
#endif
    {
        logMessage = "Client::Impl::socket_protect(): socket_protect is not implemented";

        eddie_log_error(logMessage);

        logEvent(EDDIE_EVENT_TYPE_ERROR, EDDIE_EVENT_NAME_TAG, logMessage);

        return false;
    }

    logMessage = utils::format("socket_protect(socket=%d)", socket);

    eddie_log_debug(logMessage);

    logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, logMessage);

#ifdef __JNI
    JNIEnv *jniEnv = getThreadJniEnv();

    if(jniEnv == nullptr)
        return false;

    return jniEnv->CallBooleanMethod(jniCallbackObject, openVPNClientJniCallbackInterface->socket_protect, socket);
#else
    return EDDIE_SUCCEEDED(openVPNClientCallbackInterface.socket_protect(socket));
#endif
}

bool Client::Impl::pause_on_connection_timeout()
{
    return false;
}

void Client::Impl::event(const openvpn::ClientAPI::Event &e)
{
    ovpn3_connection_data connection_data;
    openvpn::Error::Type openVPNNameError, openVPNInfoError;
    std::string message, info;

#ifdef __JNI
    if(!isJniCallbackValid())
        return;
#endif

    if(e.fatal)
        message = "FATAL ERROR";
    else if(e.error)
        message = "ERROR";
    else
        message = "EVENT";

    message += ": " + e.name;

    if(!e.info.empty())
        message += " - " + e.info;

    if(e.fatal || e.error)
        ovpn3_log_error(message);
    else
        ovpn3_log_debug(message);

#ifdef __JNI
    if(openVPNClientJniCallbackInterface->on_event)
#else
    if(openVPNClientCallbackInterface.on_event)
#endif
    {
        ovpn3_event ce;

        EDDIE_ZEROMEMORY(&ce, sizeof(ovpn3_event));

		if(e.fatal)
			ce.type = EDDIE_EVENT_TYPE_FATAL_ERROR;
		else if(e.error)
			ce.type = EDDIE_EVENT_TYPE_ERROR;
		else if(isUdpPartialSendError(e.name) || isUdpPartialSendError(e.info))
			ce.type = UDP_PARTIAL_SEND_ERROR;
		else if(findErrorInString(e.name) || findErrorInString(e.info))
			ce.type = EDDIE_EVENT_TYPE_ERROR;
        else
            ce.type = getEventType((char *)e.name.c_str());

        openVPNNameError = getOpenVPNErrorInString(e.name);
        openVPNInfoError = getOpenVPNErrorInString(e.info);

        if(openVPNNameError != openvpn::Error::SUCCESS || openVPNInfoError != openvpn::Error::SUCCESS)
        {
            if(openVPNNameError != openvpn::Error::TLS_AUTH_FAIL &&
               openVPNNameError != openvpn::Error::AUTH_FAILED &&
               openVPNInfoError != openvpn::Error::TLS_AUTH_FAIL &&
               openVPNInfoError != openvpn::Error::AUTH_FAILED)
                ce.type = EDDIE_EVENT_TYPE_FORMAL_WARNING;
            else
                ce.type = openvpn::ClientEvent::AUTH_FAILED;

            info = e.name + " " + e.info;

            ce.info = info.c_str();

            if(openVPNNameError != openvpn::Error::SUCCESS)
                ce.name = openvpn::Error::name(openVPNNameError);
            else
                ce.name = openvpn::Error::name(openVPNInfoError);

            ce.data = nullptr;
        }
        else if(ce.type == openvpn::ClientEvent::Type::CONNECTED)
        {
            connectionInfo = connection_info();

            EDDIE_ZEROMEMORY(&connection_data, sizeof(ovpn3_connection_data));

            connection_data.defined = connectionInfo.defined;
            connection_data.user = connectionInfo.user.c_str();
            connection_data.serverHost = connectionInfo.serverHost.c_str();
            connection_data.serverPort = connectionInfo.serverPort.c_str();
            connection_data.serverProto = connectionInfo.serverProto.c_str();
            connection_data.serverIp = connectionInfo.serverIp.c_str();
            connection_data.vpnIp4 = connectionInfo.vpnIp4.c_str();
            connection_data.vpnIp6 = connectionInfo.vpnIp6.c_str();
            connection_data.gw4 = connectionInfo.gw4.c_str();
            connection_data.gw6 = connectionInfo.gw6.c_str();
            connection_data.clientIp = connectionInfo.clientIp.c_str();
            connection_data.tunName = connectionInfo.tunName.c_str();

            ce.data = &connection_data;
        }
        else
        {
            ce.name = e.name.c_str();
            ce.info = e.info.c_str();
            ce.data = nullptr;
        }

#ifdef __JNI
        JNIEnv *jniEnv;
        jclass cls = nullptr;
        jmethodID constructorID = nullptr;
        jobject event = nullptr;
        jobject connectionData = nullptr;

        jniEnv = getThreadJniEnv();

        if(jniEnv == nullptr)
        {
            utils::log_debug("Client::Impl::event(): jniEnv is null");

            return;
        }

        // OpenVPN3ConnectionData

        if(ce.type == openvpn::ClientEvent::Type::CONNECTED)
        {
            jniEnv->ExceptionClear();

            cls = jniEnv->FindClass("org/airvpn/eddie/OpenVPNConnectionData");

            if(cls == nullptr)
            {
                utils::log_debug("Client::Impl::event(): Can't find OpenVPNConnectionData class");

                return;
            }

            constructorID = jniEnv->GetMethodID(cls, "<init>","()V");

            connectionData = jniEnv->NewObject(cls, constructorID);

            if(connectionData != nullptr)
            {
                if(!setJniIntField(cls, connectionData, "defined", connection_data.defined))
                    return;

                if(!setJniStringField(cls, connectionData, "user", connection_data.user))
                    return;

                if(!setJniStringField(cls, connectionData, "serverHost", connection_data.serverHost))
                    return;

                if(!setJniStringField(cls, connectionData, "serverPort", connection_data.serverPort))
                    return;

                if(!setJniStringField(cls, connectionData, "serverProto", connection_data.serverProto))
                    return;

                if(!setJniStringField(cls, connectionData, "serverIp", connection_data.serverIp))
                    return;

                if(!setJniStringField(cls, connectionData, "vpnIp4", connection_data.vpnIp4))
                    return;

                if(!setJniStringField(cls, connectionData, "vpnIp6", connection_data.vpnIp6))
                    return;

                if(!setJniStringField(cls, connectionData, "gw4", connection_data.gw4))
                    return;

                if(!setJniStringField(cls, connectionData, "gw6", connection_data.gw6))
                    return;

                if(!setJniStringField(cls, connectionData, "clientIp", connection_data.clientIp))
                    return;

                if(!setJniStringField(cls, connectionData, "tunName", connection_data.tunName))
                    return;
            }

            jniEnv->DeleteLocalRef(cls);
        }
        else
            connectionData = nullptr;

        // OpenVPN3Event

        jniEnv->ExceptionClear();

        cls = jniEnv->FindClass("org/airvpn/eddie/OpenVPNEvent");

        if(cls == nullptr)
        {
            if(connectionData != nullptr)
                jniEnv->DeleteLocalRef(connectionData);

            utils::log_debug("Client::Impl::event(): Can't find OpenVPNEvent class");

            return;
        }

        constructorID = jniEnv->GetMethodID(cls, "<init>","()V");

        event = jniEnv->NewObject(cls, constructorID);

        if(event == nullptr)
        {
            utils::log_debug("Client::Impl::event(): Can't create a OpenVPNEvent object");

            return;
        }

        if(!setJniIntField(cls, event, "type", ce.type))
            return;

        if(!setJniStringField(cls, event, "name", ce.name))
            return;

        if(!setJniStringField(cls, event, "info", ce.info))
            return;

        if(!setJniObjectField(cls, event, "data", connectionData))
            return;

        jniEnv->CallVoidMethod(jniCallbackObject, openVPNClientJniCallbackInterface->on_event, event);

        if(connectionData != nullptr)
            jniEnv->DeleteLocalRef(connectionData);

        jniEnv->DeleteLocalRef(event);

        jniEnv->DeleteLocalRef(cls);
#else
        openVPNClientCallbackInterface.on_event(&ce);
#endif
    }
}

bool Client::Impl::findErrorInString(std::string s)
{
    std::transform(s.begin(), s.end(), s.begin(), ::tolower);

    return (s.find("error") != std::string::npos) | (s.find("exception") != std::string::npos);
}

bool Client::Impl::isUdpPartialSendError(std::string s)
{
    std::transform(s.begin(), s.end(), s.begin(), ::tolower);

    return (s.find("udp partial send error") != std::string::npos);
}

openvpn::Error::Type Client::Impl::getOpenVPNErrorInString(std::string s)
{
    int i;
    openvpn::Error::Type type=openvpn::Error::UNDEF;

    for(i=0; i <= openvpn::Error::Type::N_ERRORS && type == openvpn::Error::SUCCESS; i++)
    {
        if(s.find(openvpn::Error::name((openvpn::Error::Type)i)) != std::string::npos)
            type = (openvpn::Error::Type)i;
    }

    if(s.find("TCP recv EOF") != std::string::npos)
        type = openvpn::Error::NETWORK_EOF_ERROR;

    if(s.find("TCP recv error") != std::string::npos)
        type = openvpn::Error::NETWORK_RECV_ERROR;

    return type;
}

int Client::Impl::getEventType(char *name)
{
    int i, type = EDDIE_EVENT_TYPE_ERROR;

    for(i=0; i <= openvpn::ClientEvent::Type::N_TYPES && type == EDDIE_EVENT_TYPE_ERROR; i++)
    {
        if(strcmp(openvpn::ClientEvent::event_name((openvpn::ClientEvent::Type)i), name) == 0)
            type = i;
    }

    return type;
}

void Client::Impl::external_pki_cert_request(openvpn::ClientAPI::ExternalPKICertRequest &req)
{
    logMessage = "external_pki_cert_request()";

    eddie_log_debug(logMessage);

    logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, logMessage);
}

void Client::Impl::external_pki_sign_request(openvpn::ClientAPI::ExternalPKISignRequest &req)
{
    logMessage = "external_pki_sign_request()";

    eddie_log_debug(logMessage);

    logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, logMessage);
}

void Client::Impl::connect_attach()
{
    logMessage = "connect_attach()";

    eddie_log_debug(logMessage);

    logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, logMessage);

    BaseClass::connect_attach();

#ifdef __JNI
    if(!isJniCallbackValid())
        return;
#endif

#ifdef __JNI
    JNIEnv *jniEnv = getThreadJniEnv();

    if(jniEnv == nullptr)
        return;

    if(openVPNClientJniCallbackInterface->connect_attach)
        jniEnv->CallVoidMethod(jniCallbackObject, openVPNClientJniCallbackInterface->connect_attach);
#else
    if(openVPNClientCallbackInterface.connect_attach)
        openVPNClientCallbackInterface.connect_attach();
#endif
}

void Client::Impl::connect_pre_run()
{
    logMessage = "connect_pre_run()";

    eddie_log_debug(logMessage);

    logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, logMessage);

    BaseClass::connect_pre_run();

#ifdef __JNI
    if(!isJniCallbackValid())
        return;
#endif

#ifdef __JNI
    JNIEnv *jniEnv = getThreadJniEnv();

    if(jniEnv == nullptr)
        return;

    if(openVPNClientJniCallbackInterface->connect_pre_run)
        jniEnv->CallVoidMethod(jniCallbackObject, openVPNClientJniCallbackInterface->connect_pre_run);
#else
    if(openVPNClientCallbackInterface.connect_pre_run)
        openVPNClientCallbackInterface.connect_pre_run();
#endif
}

void Client::Impl::connect_run()
{
    logMessage = "connect_run()";

    eddie_log_debug(logMessage);

    logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, logMessage);

    BaseClass::connect_run();

#ifdef __JNI
    if(!isJniCallbackValid())
        return;
#endif

#ifdef __JNI
    JNIEnv *jniEnv = getThreadJniEnv();

    if(jniEnv == nullptr)
        return;

    if(openVPNClientJniCallbackInterface->connect_run)
        jniEnv->CallVoidMethod(jniCallbackObject, openVPNClientJniCallbackInterface->connect_run);
#else
    if(openVPNClientCallbackInterface.connect_run)
        openVPNClientCallbackInterface.connect_run();
#endif
}

void Client::Impl::connect_session_stop()
{
    logMessage = "connect_session_stop()";

    eddie_log_debug(logMessage);

    logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, logMessage);

    BaseClass::connect_session_stop();

#ifdef __JNI
    if(!isJniCallbackValid())
        return;
#endif

#ifdef __JNI
    JNIEnv *jniEnv = getThreadJniEnv();

    if(jniEnv == nullptr)
        return;

    if(openVPNClientJniCallbackInterface->connect_session_stop)
        jniEnv->CallVoidMethod(jniCallbackObject, openVPNClientJniCallbackInterface->connect_session_stop);
#else
    if(openVPNClientCallbackInterface.connect_session_stop)
        openVPNClientCallbackInterface.connect_session_stop();
#endif
}

bool Client::Impl::tun_builder_new()
{
#ifdef __JNI
    if(!isJniCallbackValid())
        return false;
#endif

#ifdef __JNI
    if(!openVPNClientJniCallbackInterface->tun_builder_new)
#else
    if(!openVPNClientCallbackInterface.tun_builder_new)
#endif
    {
        logMessage = "tun_builder_new() is not implemented";

        eddie_log_error(logMessage);

        logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, logMessage);

        return false;
    }

    logMessage = "tun_builder_new()";

    eddie_log_debug(logMessage);

    logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, logMessage);

#ifdef __JNI
    JNIEnv *jniEnv = getThreadJniEnv();

    if(jniEnv == nullptr)
        return false;

    return jniEnv->CallBooleanMethod(jniCallbackObject, openVPNClientJniCallbackInterface->tun_builder_new);
#else
    return EDDIE_SUCCEEDED(openVPNClientCallbackInterface.tun_builder_new());
#endif
}

bool Client::Impl::tun_builder_set_layer(int layer)
{
#ifdef __JNI
    if(!isJniCallbackValid())
        return false;
#endif

#ifdef __JNI
    if(!openVPNClientJniCallbackInterface->tun_builder_set_layer)
#else
    if(!openVPNClientCallbackInterface.tun_builder_set_layer)
#endif
    {
        logMessage = "tun_builder_set_layer() is not implemented";

        eddie_log_error(logMessage);

        logEvent(EDDIE_EVENT_TYPE_ERROR, EDDIE_EVENT_NAME_TAG, logMessage);

        return false;
    }

    logMessage = utils::format("tun_builder_set_layer(layer=%d)", layer);

    eddie_log_debug(logMessage);

    logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, logMessage);

#ifdef __JNI
    JNIEnv *jniEnv = getThreadJniEnv();

    if(jniEnv == nullptr)
        return false;

    return jniEnv->CallBooleanMethod(jniCallbackObject, openVPNClientJniCallbackInterface->tun_builder_set_layer, layer);
#else
    return EDDIE_SUCCEEDED(openVPNClientCallbackInterface.tun_builder_set_layer(layer));
#endif
}

bool Client::Impl::tun_builder_set_remote_address(const std::string &address, bool ipv6)
{
#ifdef __JNI
    if(!isJniCallbackValid())
        return false;
#endif

#ifdef __JNI
    if(!openVPNClientJniCallbackInterface->tun_builder_set_remote_address)
#else
    if(!openVPNClientCallbackInterface.tun_builder_set_remote_address)
#endif
    {
        logMessage = "tun_builder_set_remote_address() is not implemented";

        eddie_log_error(logMessage);

        logEvent(EDDIE_EVENT_TYPE_ERROR, EDDIE_EVENT_NAME_TAG, logMessage);

        return false;
    }

    logMessage = utils::format("tun_builder_set_remote_address(address=%s, ipv6=%s)", address.c_str(), utils::to_string(ipv6).c_str());

    eddie_log_debug(logMessage);

    logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, logMessage);

#ifdef __JNI
    JNIEnv *jniEnv = getThreadJniEnv();
    jstring jAddress;
    bool retval;

    if(jniEnv == nullptr)
        return false;

    jAddress = jniEnv->NewStringUTF(address.c_str());

    retval = jniEnv->CallBooleanMethod(jniCallbackObject, openVPNClientJniCallbackInterface->tun_builder_set_remote_address,
                jAddress,
                ipv6);

    jniEnv->DeleteLocalRef(jAddress);

    return retval;
#else
    return EDDIE_SUCCEEDED(openVPNClientCallbackInterface.tun_builder_set_remote_address(address.c_str(), EDDIE_FLAG_CAST(ipv6)));
#endif
}

bool Client::Impl::tun_builder_add_address(const std::string &address, int prefix_length, const std::string &gateway, /*optional*/ bool ipv6, bool net30)
{
#ifdef __JNI
    if(!isJniCallbackValid())
        return false;
#endif

#ifdef __JNI
    if(!openVPNClientJniCallbackInterface->tun_builder_add_address)
#else
    if(!openVPNClientCallbackInterface.tun_builder_add_address)
#endif
    {
        logMessage = "tun_builder_add_address() is not implemented";

        eddie_log_error(logMessage);

        logEvent(EDDIE_EVENT_TYPE_ERROR, EDDIE_EVENT_NAME_TAG, logMessage);

        return false;
    }

    logMessage = utils::format("tun_builder_add_address(address=%s, prefix_length=%d, gateway=%s, ipv6=%s, net30=%s)", address.c_str(), prefix_length, gateway.c_str(), utils::to_string(ipv6).c_str(), utils::to_string(net30).c_str());

    eddie_log_debug(logMessage);

    logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, logMessage);

#ifdef __JNI
    JNIEnv *jniEnv = getThreadJniEnv();
    jstring jAddress, jGateway;
    bool retval;

    if(jniEnv == nullptr)
        return false;

    jAddress = jniEnv->NewStringUTF(address.c_str());
    jGateway = jniEnv->NewStringUTF(gateway.c_str());

    retval = jniEnv->CallBooleanMethod(jniCallbackObject, openVPNClientJniCallbackInterface->tun_builder_add_address,
                jAddress,
                prefix_length,
                jGateway,
                ipv6,
                net30);

    jniEnv->DeleteLocalRef(jAddress);
    jniEnv->DeleteLocalRef(jGateway);

    return retval;
#else
    return EDDIE_SUCCEEDED(openVPNClientCallbackInterface.tun_builder_add_address(address.c_str(), prefix_length, gateway.c_str(), EDDIE_FLAG_CAST(ipv6), EDDIE_FLAG_CAST(net30)));
#endif
}

bool Client::Impl::tun_builder_set_route_metric_default(int metric)
{
#ifdef __JNI
    if(!isJniCallbackValid())
        return false;
#endif

#ifdef __JNI
    if(!openVPNClientJniCallbackInterface->tun_builder_set_route_metric_default)
#else
    if(!openVPNClientCallbackInterface.tun_builder_set_route_metric_default)
#endif
    {
        logMessage = "tun_builder_set_route_metric_default() is not implemented";

        eddie_log_error(logMessage);

        logEvent(EDDIE_EVENT_TYPE_ERROR, EDDIE_EVENT_NAME_TAG, logMessage);

        return false;
    }

    logMessage = utils::format("tun_builder_set_route_metric_default(metric=%d)", metric);

    eddie_log_debug(logMessage);

    logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, logMessage);

#ifdef __JNI
    JNIEnv *jniEnv = getThreadJniEnv();

    if(jniEnv == nullptr)
        return false;

    return jniEnv->CallBooleanMethod(jniCallbackObject, openVPNClientJniCallbackInterface->tun_builder_set_route_metric_default, metric);
#else
    return EDDIE_SUCCEEDED(openVPNClientCallbackInterface.tun_builder_set_route_metric_default(metric));
#endif
}

bool Client::Impl::tun_builder_reroute_gw(bool ipv4, bool ipv6, unsigned int flags)
{
#ifdef __JNI
    if(!isJniCallbackValid())
        return false;
#endif

#ifdef __JNI
    if(!openVPNClientJniCallbackInterface->tun_builder_reroute_gw)
#else
    if(!openVPNClientCallbackInterface.tun_builder_reroute_gw)
#endif
    {
        logMessage = "tun_builder_reroute_gw() is not implemented";

        eddie_log_error(logMessage);

        logEvent(EDDIE_EVENT_TYPE_ERROR, EDDIE_EVENT_NAME_TAG, logMessage);

        return false;
    }

    logMessage = utils::format("tun_builder_reroute_gw(ipv4=%s, ipv6=%s, flags=%d)", utils::to_string(ipv4).c_str(), utils::to_string(ipv6).c_str(), flags);

    eddie_log_debug(logMessage);

    logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, logMessage);

#ifdef __JNI
    JNIEnv *jniEnv = getThreadJniEnv();

    if(jniEnv == nullptr)
        return false;

    return jniEnv->CallBooleanMethod(jniCallbackObject, openVPNClientJniCallbackInterface->tun_builder_reroute_gw, ipv4, ipv6, flags);
#else
    return EDDIE_SUCCEEDED(openVPNClientCallbackInterface.tun_builder_reroute_gw(EDDIE_FLAG_CAST(ipv4), EDDIE_FLAG_CAST(ipv6), flags));
#endif
}

bool Client::Impl::tun_builder_add_route(const std::string &address, int prefix_length, int metric, bool ipv6)
{
#ifdef __JNI
    if(!isJniCallbackValid())
        return false;
#endif

#ifdef __JNI
    if(!openVPNClientJniCallbackInterface->tun_builder_add_route)
#else
    if(!openVPNClientCallbackInterface.tun_builder_add_route)
#endif
    {
        logMessage = "tun_builder_add_route() is not implemented";

        eddie_log_error(logMessage);

        logEvent(EDDIE_EVENT_TYPE_ERROR, EDDIE_EVENT_NAME_TAG, logMessage);

        return false;
    }

    logMessage = utils::format("tun_builder_add_route(address=%s, prefix_length=%d, metric=%d, ipv6=%s)", address.c_str(), prefix_length, metric, utils::to_string(ipv6).c_str());

    eddie_log_debug(logMessage);

    logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, logMessage);

#ifdef __JNI
    JNIEnv *jniEnv = getThreadJniEnv();
    jstring jAddress;
    bool retval;

    if(jniEnv == nullptr)
        return false;

    jAddress = jniEnv->NewStringUTF(address.c_str());

    retval = jniEnv->CallBooleanMethod(jniCallbackObject, openVPNClientJniCallbackInterface->tun_builder_add_route,
                jAddress,
                prefix_length,
                metric,
                ipv6);

    jniEnv->DeleteLocalRef(jAddress);

    return retval;
#else
    return EDDIE_SUCCEEDED(openVPNClientCallbackInterface.tun_builder_add_route(address.c_str(), prefix_length, metric, EDDIE_FLAG_CAST(ipv6)));
#endif
}

bool Client::Impl::tun_builder_exclude_route(const std::string &address, int prefix_length, int metric, bool ipv6)
{
#ifdef __JNI
    if(!isJniCallbackValid())
        return false;
#endif

#ifdef __JNI
    if(!openVPNClientJniCallbackInterface->tun_builder_exclude_route)
#else
    if(!openVPNClientCallbackInterface.tun_builder_exclude_route)
#endif
    {
        logMessage = "tun_builder_exclude_route() is not implemented";

        eddie_log_error(logMessage);

        logEvent(EDDIE_EVENT_TYPE_ERROR, EDDIE_EVENT_NAME_TAG, logMessage);

        return false;
    }

    logMessage = utils::format("tun_builder_exclude_route(address=%s, prefix_length=%d, metric=%d, ipv6=%s)", address.c_str(), prefix_length, metric, utils::to_string(ipv6).c_str());

    eddie_log_debug(logMessage);

    logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, logMessage);

#ifdef __JNI
    JNIEnv *jniEnv = getThreadJniEnv();
    jstring jAddress;
    bool retval;

    if(jniEnv == nullptr)
        return false;

    jAddress = jniEnv->NewStringUTF(address.c_str());

    retval = jniEnv->CallBooleanMethod(jniCallbackObject, openVPNClientJniCallbackInterface->tun_builder_exclude_route,
                jAddress,
                prefix_length,
                metric,
                ipv6);

    jniEnv->DeleteLocalRef(jAddress);

    return retval;
#else
    return EDDIE_SUCCEEDED(openVPNClientCallbackInterface.tun_builder_exclude_route(address.c_str(), prefix_length, metric, EDDIE_FLAG_CAST(ipv6)));
#endif
}

bool Client::Impl::tun_builder_add_dns_server(const std::string &address, bool ipv6)
{
#ifdef __JNI
    if(!isJniCallbackValid())
        return false;
#endif

#ifdef __JNI
    if(!openVPNClientJniCallbackInterface->tun_builder_add_dns_server)
#else
    if(!openVPNClientCallbackInterface.tun_builder_add_dns_server)
#endif
    {
        logMessage = "tun_builder_add_dns_server() is not implemented";

        eddie_log_error(logMessage);

        logEvent(EDDIE_EVENT_TYPE_ERROR, EDDIE_EVENT_NAME_TAG, logMessage);

        return false;
    }

    logMessage = utils::format("tun_builder_add_dns_server(address=%s, ipv6=%s)", address.c_str(), utils::to_string(ipv6).c_str());

    eddie_log_debug(logMessage);

    logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, logMessage);

#ifdef __JNI
    JNIEnv *jniEnv = getThreadJniEnv();
    jstring jAddress;
    bool retval;

    if(jniEnv == nullptr)
        return false;

    jAddress = jniEnv->NewStringUTF(address.c_str());

    retval = jniEnv->CallBooleanMethod(jniCallbackObject, openVPNClientJniCallbackInterface->tun_builder_add_dns_server,
                jAddress,
                ipv6);

    jniEnv->DeleteLocalRef(jAddress);

    return retval;
#else
    return EDDIE_SUCCEEDED(openVPNClientCallbackInterface.tun_builder_add_dns_server(address.c_str(), EDDIE_FLAG_CAST(ipv6)));
#endif
}

bool Client::Impl::tun_builder_add_search_domain(const std::string &domain)
{
#ifdef __JNI
    if(!isJniCallbackValid())
        return false;
#endif

#ifdef __JNI
    if(!openVPNClientJniCallbackInterface->tun_builder_add_search_domain)
#else
    if(!openVPNClientCallbackInterface.tun_builder_add_search_domain)
#endif
    {
        logMessage = "tun_builder_add_search_domain() is not implemented";

        eddie_log_error(logMessage);

        logEvent(EDDIE_EVENT_TYPE_ERROR, EDDIE_EVENT_NAME_TAG, logMessage);

        return false;
    }

    logMessage = utils::format("tun_builder_add_search_domain(domain=%s)", domain.c_str());

    eddie_log_debug(logMessage);

    logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, logMessage);

#ifdef __JNI
    JNIEnv *jniEnv = getThreadJniEnv();
    jstring jDomain;
    bool retval;

    if(jniEnv == nullptr)
        return false;

    jDomain = jniEnv->NewStringUTF(domain.c_str());

    retval = jniEnv->CallBooleanMethod(jniCallbackObject, openVPNClientJniCallbackInterface->tun_builder_add_search_domain, jDomain);

    jniEnv->DeleteLocalRef(jDomain);

    return retval;
#else
    return EDDIE_SUCCEEDED(openVPNClientCallbackInterface.tun_builder_add_search_domain(domain.c_str()));
#endif
}

bool Client::Impl::tun_builder_set_mtu(int mtu)
{
#ifdef __JNI
    if(!isJniCallbackValid())
        return false;
#endif

#ifdef __JNI
    if(!openVPNClientJniCallbackInterface->tun_builder_set_mtu)
#else
    if(!openVPNClientCallbackInterface.tun_builder_set_mtu)
#endif
    {
        logMessage = "tun_builder_set_mtu() is not implemented";

        eddie_log_error(logMessage);

        logEvent(EDDIE_EVENT_TYPE_ERROR, EDDIE_EVENT_NAME_TAG, logMessage);

        return false;
    }

    logMessage = utils::format("tun_builder_set_mtu(mtu=%d)", mtu);

    eddie_log_debug(logMessage);

    logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, logMessage);

#ifdef __JNI
    JNIEnv *jniEnv = getThreadJniEnv();

    if(jniEnv == nullptr)
        return false;

    return jniEnv->CallBooleanMethod(jniCallbackObject, openVPNClientJniCallbackInterface->tun_builder_set_mtu, mtu);
#else
    return EDDIE_SUCCEEDED(openVPNClientCallbackInterface.tun_builder_set_mtu(mtu));
#endif
}

bool Client::Impl::tun_builder_set_session_name(const std::string &name)
{
#ifdef __JNI
    if(!isJniCallbackValid())
        return false;
#endif

#ifdef __JNI
    if(!openVPNClientJniCallbackInterface->tun_builder_set_session_name)
#else
    if(!openVPNClientCallbackInterface.tun_builder_set_session_name)
#endif
    {
        logMessage = "tun_builder_set_session_name() is not implemented";

        eddie_log_error(logMessage);

        logEvent(EDDIE_EVENT_TYPE_ERROR, EDDIE_EVENT_NAME_TAG, logMessage);

        return false;
    }

    logMessage = utils::format("tun_builder_set_session_name(name=%s)", name.c_str());

    eddie_log_debug(logMessage);

    logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, logMessage);

#ifdef __JNI
    JNIEnv *jniEnv = getThreadJniEnv();
    jstring jName;
    bool retval;

    if(jniEnv == nullptr)
        return false;

    jName = jniEnv->NewStringUTF(name.c_str());

    retval = jniEnv->CallBooleanMethod(jniCallbackObject, openVPNClientJniCallbackInterface->tun_builder_set_session_name, jName);

    jniEnv->DeleteLocalRef(jName);

    return retval;
#else
    return EDDIE_SUCCEEDED(openVPNClientCallbackInterface.tun_builder_set_session_name(name.c_str()));
#endif
}

bool Client::Impl::tun_builder_add_proxy_bypass(const std::string &bypass_host)
{
#ifdef __JNI
    if(!isJniCallbackValid())
        return false;
#endif

#ifdef __JNI
    if(!openVPNClientJniCallbackInterface->tun_builder_add_proxy_bypass)
#else
    if(!openVPNClientCallbackInterface.tun_builder_add_proxy_bypass)
#endif
    {
        logMessage = "tun_builder_add_proxy_bypass() is not implemented";

        eddie_log_error(logMessage);

        logEvent(EDDIE_EVENT_TYPE_ERROR, EDDIE_EVENT_NAME_TAG, logMessage);

        return false;
    }

    logMessage = utils::format("tun_builder_add_proxy_bypass(bypass_host=%s)", bypass_host.c_str());

    eddie_log_debug(logMessage);

    logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, logMessage);

#ifdef __JNI
    JNIEnv *jniEnv = getThreadJniEnv();
    jstring jBypassHost;
    bool retval;

    if(jniEnv == nullptr)
        return false;

    jBypassHost = jniEnv->NewStringUTF(bypass_host.c_str());

    retval = jniEnv->CallBooleanMethod(jniCallbackObject, openVPNClientJniCallbackInterface->tun_builder_add_proxy_bypass, jBypassHost);

    jniEnv->DeleteLocalRef(jBypassHost);

    return retval;
#else
    return EDDIE_SUCCEEDED(openVPNClientCallbackInterface.tun_builder_add_proxy_bypass(bypass_host.c_str()));
#endif
}

bool Client::Impl::tun_builder_set_proxy_auto_config_url(const std::string &url)
{
#ifdef __JNI
    if(!isJniCallbackValid())
        return false;
#endif

#ifdef __JNI
    if(!openVPNClientJniCallbackInterface->tun_builder_set_proxy_auto_config_url)
#else
    if(!openVPNClientCallbackInterface.tun_builder_set_proxy_auto_config_url)
#endif
    {
        logMessage = "tun_builder_set_proxy_auto_config_url() is not implemented";

        eddie_log_error(logMessage);

        logEvent(EDDIE_EVENT_TYPE_ERROR, EDDIE_EVENT_NAME_TAG, logMessage);

        return false;
    }

    logMessage = utils::format("tun_builder_set_proxy_auto_config_url(url=%s)", url.c_str());

    eddie_log_debug(logMessage);

    logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, logMessage);

#ifdef __JNI
    JNIEnv *jniEnv = getThreadJniEnv();
    jstring jUrl;
    bool retval;

    if(jniEnv == nullptr)
        return false;

    jUrl = jniEnv->NewStringUTF(url.c_str());

    retval =  jniEnv->CallBooleanMethod(jniCallbackObject, openVPNClientJniCallbackInterface->tun_builder_set_proxy_auto_config_url, jUrl);

    jniEnv->DeleteLocalRef(jUrl);

    return retval;

#else
    return EDDIE_SUCCEEDED(openVPNClientCallbackInterface.tun_builder_set_proxy_auto_config_url(url.c_str()));
#endif
}

bool Client::Impl::tun_builder_set_proxy_http(const std::string &host, int port)
{
#ifdef __JNI
    if(!isJniCallbackValid())
        return false;
#endif

#ifdef __JNI
    if(!openVPNClientJniCallbackInterface->tun_builder_set_proxy_http)
#else
    if(!openVPNClientCallbackInterface.tun_builder_set_proxy_http)
#endif
    {
        logMessage = "tun_builder_set_proxy_http() is not implemented";

        eddie_log_error(logMessage);

        logEvent(EDDIE_EVENT_TYPE_ERROR, EDDIE_EVENT_NAME_TAG, logMessage);

        return false;
    }

    logMessage = utils::format("tun_builder_set_proxy_http(host=%s, port=%d)", host.c_str(), port);

    eddie_log_debug(logMessage);

    logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, logMessage);

#ifdef __JNI
    JNIEnv *jniEnv = getThreadJniEnv();
    jstring jHost;
    bool retval;

    if(jniEnv == nullptr)
        return false;

    jHost = jniEnv->NewStringUTF(host.c_str());

    retval = jniEnv->CallBooleanMethod(jniCallbackObject, openVPNClientJniCallbackInterface->tun_builder_set_proxy_http,
                jHost,
                port);

    jniEnv->DeleteLocalRef(jHost);

    return retval;
#else
    return EDDIE_SUCCEEDED(openVPNClientCallbackInterface.tun_builder_set_proxy_http(host.c_str(), port));
#endif
}

bool Client::Impl::tun_builder_set_proxy_https(const std::string &host, int port)
{
#ifdef __JNI
    if(!isJniCallbackValid())
        return false;
#endif

#ifdef __JNI
    if(!openVPNClientJniCallbackInterface->tun_builder_set_proxy_https)
#else
    if(!openVPNClientCallbackInterface.tun_builder_set_proxy_https)
#endif
    {
        logMessage = "tun_builder_set_proxy_https() is not implemented";

        eddie_log_error(logMessage);

        logEvent(EDDIE_EVENT_TYPE_ERROR, EDDIE_EVENT_NAME_TAG, logMessage);

        return false;
    }

    logMessage = utils::format("tun_builder_set_proxy_https(host=%s, port=%d)", host.c_str(), port);

    eddie_log_debug(logMessage);

    logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, logMessage);

#ifdef __JNI
    JNIEnv *jniEnv = getThreadJniEnv();
    jstring jHost;
    bool retval;

    if(jniEnv == nullptr)
        return false;

    jHost = jniEnv->NewStringUTF(host.c_str());

    retval = jniEnv->CallBooleanMethod(jniCallbackObject, openVPNClientJniCallbackInterface->tun_builder_set_proxy_https,
                jHost,
                port);

    jniEnv->DeleteLocalRef(jHost);

    return retval;
#else
    return EDDIE_SUCCEEDED(openVPNClientCallbackInterface.tun_builder_set_proxy_https(host.c_str(), port));
#endif
}

bool Client::Impl::tun_builder_add_wins_server(const std::string &address)
{
#ifdef __JNI
    if(!isJniCallbackValid())
        return false;
#endif

#ifdef __JNI
    if(!openVPNClientJniCallbackInterface->tun_builder_add_wins_server)
#else
    if(!openVPNClientCallbackInterface.tun_builder_add_wins_server)
#endif
    {
        logMessage = "tun_builder_add_wins_server() is not implemented";

        eddie_log_error(logMessage);

        logEvent(EDDIE_EVENT_TYPE_ERROR, EDDIE_EVENT_NAME_TAG, logMessage);

        return false;
    }

    logMessage = utils::format("tun_builder_add_wins_server(address=%s)", address.c_str());

    eddie_log_debug(logMessage);

    logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, logMessage);

#ifdef __JNI
    JNIEnv *jniEnv = getThreadJniEnv();
    jstring jAddress;
    bool retval;

    if(jniEnv == nullptr)
        return false;

    jAddress = jniEnv->NewStringUTF(address.c_str());

    retval = jniEnv->CallBooleanMethod(jniCallbackObject, openVPNClientJniCallbackInterface->tun_builder_add_wins_server, jAddress);

    jniEnv->DeleteLocalRef(jAddress);

    return retval;
#else
    return EDDIE_SUCCEEDED(openVPNClientCallbackInterface.tun_builder_add_wins_server(address.c_str()));
#endif
}

bool Client::Impl::tun_builder_set_block_ipv6(bool block_ipv6)
{
#ifdef __JNI
    if(!isJniCallbackValid())
        return false;
#endif

#ifdef __JNI
    if(!openVPNClientJniCallbackInterface->tun_builder_set_block_ipv6)
#else
    if(!openVPNClientCallbackInterface.tun_builder_set_block_ipv6)
#endif
    {
        logMessage = "tun_builder_set_block_ipv6() is not implemented";

        eddie_log_error(logMessage);

        logEvent(EDDIE_EVENT_TYPE_ERROR, EDDIE_EVENT_NAME_TAG, logMessage);

        return false;
    }

    logMessage = utils::format("tun_builder_set_block_ipv6(block_ipv6=%s)", utils::to_string(block_ipv6).c_str());

    eddie_log_debug(logMessage);

    logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, logMessage);

#ifdef __JNI
    JNIEnv *jniEnv = getThreadJniEnv();

    if(jniEnv == nullptr)
        return false;

    return jniEnv->CallBooleanMethod(jniCallbackObject, openVPNClientJniCallbackInterface->tun_builder_set_block_ipv6, block_ipv6);
#else
    return EDDIE_SUCCEEDED(openVPNClientCallbackInterface.tun_builder_set_block_ipv6(EDDIE_FLAG_CAST(block_ipv6)));
#endif
}

bool Client::Impl::tun_builder_set_adapter_domain_suffix(const std::string &name)
{
#ifdef __JNI
    if(!isJniCallbackValid())
        return false;
#endif

#ifdef __JNI
    if(!openVPNClientJniCallbackInterface->tun_builder_set_adapter_domain_suffix)
#else
    if(!openVPNClientCallbackInterface.tun_builder_set_adapter_domain_suffix)
#endif
    {
        logMessage = "tun_builder_set_adapter_domain_suffix() is not implemented";

        eddie_log_error(logMessage);

        logEvent(EDDIE_EVENT_TYPE_ERROR, EDDIE_EVENT_NAME_TAG, logMessage);

        return false;
    }

    logMessage = utils::format("tun_builder_set_adapter_domain_suffix(name=%s)", name.c_str());

    eddie_log_debug(logMessage);

    logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, logMessage);

#ifdef __JNI
    JNIEnv *jniEnv = getThreadJniEnv();
    jstring jName;
    bool retval;

    if(jniEnv == nullptr)
        return false;

    jName = jniEnv->NewStringUTF(name.c_str());

    retval = jniEnv->CallBooleanMethod(jniCallbackObject, openVPNClientJniCallbackInterface->tun_builder_set_adapter_domain_suffix, jName);

    jniEnv->DeleteLocalRef(jName);

    return retval;
#else
    return EDDIE_SUCCEEDED(openVPNClientCallbackInterface.tun_builder_set_adapter_domain_suffix(name.c_str()));
#endif
}

int Client::Impl::tun_builder_establish()
{
#ifdef __JNI
    if(!isJniCallbackValid())
        return 0;
#endif

#ifdef __JNI
    if(!openVPNClientJniCallbackInterface->tun_builder_establish)
#else
    if(!openVPNClientCallbackInterface.tun_builder_establish)
#endif
    {
        logMessage = "tun_builder_establish() is not implemented";

        eddie_log_error(logMessage);

        logEvent(EDDIE_EVENT_TYPE_ERROR, EDDIE_EVENT_NAME_TAG, logMessage);

        return 0;
    }

    logMessage = "tun_builder_establish()";

    eddie_log_debug(logMessage);

    logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, logMessage);

#ifdef __JNI
    JNIEnv *jniEnv = getThreadJniEnv();

    if(jniEnv == nullptr)
        return 0;

    return jniEnv->CallIntMethod(jniCallbackObject, openVPNClientJniCallbackInterface->tun_builder_establish);
#else
    return openVPNClientCallbackInterface.tun_builder_establish();
#endif
}

bool Client::Impl::tun_builder_persist()
{
#ifdef __JNI
    if(!isJniCallbackValid())
        return false;
#endif

#ifdef __JNI
    if(!openVPNClientJniCallbackInterface->tun_builder_persist)
#else
    if(!openVPNClientCallbackInterface.tun_builder_persist)
#endif
    {
        logMessage = "tun_builder_persist() is not implemented";

        eddie_log_error(logMessage);

        logEvent(EDDIE_EVENT_TYPE_ERROR, EDDIE_EVENT_NAME_TAG, logMessage);

        return false;
    }

    logMessage = "tun_builder_persist()";

    eddie_log_debug(logMessage);

    logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, logMessage);

#ifdef __JNI
    JNIEnv *jniEnv = getThreadJniEnv();

    if(jniEnv == nullptr)
        return false;

    return jniEnv->CallBooleanMethod(jniCallbackObject, openVPNClientJniCallbackInterface->tun_builder_persist);
#else
    return EDDIE_SUCCEEDED(openVPNClientCallbackInterface.tun_builder_persist());
#endif
}

void Client::Impl::tun_builder_establish_lite()
{
#ifdef __JNI
    if(!isJniCallbackValid())
        return;
#endif

#ifdef __JNI
    if(!openVPNClientJniCallbackInterface->tun_builder_establish_lite)
#else
    if(!openVPNClientCallbackInterface.tun_builder_establish_lite)
#endif
    {
        logMessage = "tun_builder_establish_lite() is not implemented";

        eddie_log_error(logMessage);

        logEvent(EDDIE_EVENT_TYPE_ERROR, EDDIE_EVENT_NAME_TAG, logMessage);

        return;
    }

    logMessage = "tun_builder_establish_lite()";

    eddie_log_debug(logMessage);

    logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, logMessage);

#ifdef __JNI
    JNIEnv *jniEnv = getThreadJniEnv();

    if(jniEnv == nullptr)
        return;

    jniEnv->CallVoidMethod(jniCallbackObject, openVPNClientJniCallbackInterface->tun_builder_establish_lite);
#else
    openVPNClientCallbackInterface.tun_builder_establish_lite();
#endif
}

void Client::Impl::tun_builder_teardown(bool disconnect)
{
#ifdef __JNI
    if(!isJniCallbackValid())
        return;
#endif

#ifdef __JNI
    if(!openVPNClientJniCallbackInterface->tun_builder_teardown)
#else
    if(!openVPNClientCallbackInterface.tun_builder_teardown)
#endif
    {
        logMessage = "tun_builder_teardown() is not implemented";

        eddie_log_error(logMessage);

        logEvent(EDDIE_EVENT_TYPE_ERROR, EDDIE_EVENT_NAME_TAG, logMessage);

        return;
    }

    logMessage = utils::format("tun_builder_teardown(disconnect=%s)", utils::to_string(disconnect).c_str());

    eddie_log_debug(logMessage);

    logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, logMessage);

#ifdef __JNI
    JNIEnv *jniEnv = getThreadJniEnv();

    if(jniEnv == nullptr)
        return;

    jniEnv->CallVoidMethod(jniCallbackObject, openVPNClientJniCallbackInterface->tun_builder_teardown, disconnect);
#else
    openVPNClientCallbackInterface.tun_builder_teardown(EDDIE_FLAG_CAST(disconnect));
#endif
}

void Client::Impl::eddie_log_debug(const std::string &message)
{
    utils::log_debug(utils::format("%s: %s", EDDIE_EVENT_NAME_TAG, message.c_str()));
}

void Client::Impl::eddie_log_error(const std::string &message)
{
    utils::log_error(utils::format("%s: %s", EDDIE_EVENT_NAME_TAG, message.c_str()));
}

void Client::Impl::ovpn3_log_debug(const std::string &message)
{
    utils::log_debug(utils::format("%s: %s", EDDIE_EVENT_NAME_TAG,  message.c_str()));
}

void Client::Impl::ovpn3_log_error(const std::string &message)
{
    utils::log_error(utils::format("%s: %s", EDDIE_EVENT_NAME_TAG, message.c_str()));
}

//
// Send a log event to the host. [ProMIND]
//

void Client::Impl::logEvent(int level, const char *name, std::string info)
{
#ifdef __JNI
    if(!isJniCallbackValid())
        return;
#endif

#ifdef __JNI
    if(openVPNClientJniCallbackInterface->on_event)
#else
    if(openVPNClientCallbackInterface.on_event)
#endif
    {
        openvpn::Error::Type openVPNNameError, openVPNInfoError;
        ovpn3_event ce;
        std::string evInfo;

        EDDIE_ZEROMEMORY(&ce, sizeof(ovpn3_event));

        ce.type = level;

        utils::trim(info);

        /* if(findErrorInString(name) || findErrorInString(info))
			ce.type = EDDIE_EVENT_TYPE_ERROR; */

        openVPNNameError = getOpenVPNErrorInString(name);
        openVPNInfoError = getOpenVPNErrorInString(info);

        if(openVPNNameError != openvpn::Error::SUCCESS || openVPNInfoError != openvpn::Error::SUCCESS)
        {
            ce.type = EDDIE_EVENT_TYPE_FORMAL_WARNING;

            evInfo = name;
            evInfo += " " + info;

            ce.info = evInfo.c_str();

            if(openVPNNameError != openvpn::Error::SUCCESS)
                ce.name = openvpn::Error::name(openVPNNameError);
            else
                ce.name = openvpn::Error::name(openVPNInfoError);

            ce.data = nullptr;
        }
        else
        {
            ce.name = name;
            ce.info = info.c_str();
            ce.data = nullptr;
        }

        if(logFilter(ce.name) && logFilter(info))
        {
#ifdef __JNI
            JNIEnv *jniEnv = nullptr;
            jclass cls = nullptr;
            jmethodID constructorID = nullptr;
            jobject event = nullptr;

            jniEnv = getThreadJniEnv();

            if(jniEnv == nullptr)
                return;

            jniEnv->ExceptionClear();

            cls = jniEnv->FindClass("org/airvpn/eddie/OpenVPNEvent");

            if(cls == nullptr)
            {
                utils::log_debug("Client::Impl::logEvent(): Can't find OpenVPNEvent class");

                return;
            }

            constructorID = jniEnv->GetMethodID(cls, "<init>","()V");

            event = jniEnv->NewObject(cls, constructorID);

            if(event == nullptr)
            {
                utils::log_debug("Client::Impl::logEvent(): Can't create a OpenVPNEvent object");

                return;
            }

            if(!setJniIntField(cls, event, "type", ce.type))
                return;

            if(!setJniStringField(cls, event, "name", ce.name))
                return;

            if(!setJniStringField(cls, event, "info", ce.info))
                return;

            if(!setJniObjectField(cls, event, "data", nullptr))
                return;

            jniEnv->CallVoidMethod(jniCallbackObject, openVPNClientJniCallbackInterface->on_event, event);

            jniEnv->DeleteLocalRef(event);

            jniEnv->DeleteLocalRef(cls);
#else
            openVPNClientCallbackInterface.on_event(&ce);
#endif
        }
    }
}

#ifdef __JNI

bool Client::Impl::isJniCallbackValid()
{
    JNIEnv *jniEnv;
    bool result = false;

    if(openVPNClientJniCallbackInterface != nullptr && jniCallbackObject != nullptr)
    {
        jniEnv = getThreadJniEnv();

        if(jniEnv != nullptr)
        {
            if(jniEnv->GetObjectRefType(jniCallbackObject) == JNIGlobalRefType)
                result = true;
            else
            {
		        utils::log_error("Client::Impl::isJniCallbackValid() jniCallbackObjectis not a valid object");

                result = false;
            }
        }
        else
        {
		    utils::log_error("Client::Impl::isJniCallbackValid() Cannot get a valid JNIEnv");

            result = false;
        }
    }
    else
    {
        if(openVPNClientJniCallbackInterface == nullptr)
		    utils::log_error("Client::Impl::openVPNClientJniCallbackInterface is null");

        if(jniCallbackObject == nullptr)
		    utils::log_error("Client::Impl::jniCallbackObject is null. Events are turned off.");

        result = false;
    }

    return result;
}

#endif
//
// Check whether a string can be sent to the log or not. It simply
// evaluates the presence of "forbidden" keys in the string [ProMIND]
//

bool Client::Impl::logFilter(char *cs)
{
    std::string s = cs;

    return logFilter(s);
}

bool Client::Impl::logFilter(std::string s)
{
    bool result = true;
    int key_count = 0;
    std::string key[] = {"password", "username"};

    key_count = sizeof(key) / sizeof(key[0]);

    std::transform(s.begin(), s.end(), s.begin(), ::tolower);

    for(int i=0; i < key_count; i++)
    {
        if(s.find(key[i]) != std::string::npos)
            result = false;
    }

    return result;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


#ifdef __JNI

Client::Client(JNIEnv *env, jobject callbackClient) : openVpn3Impl(new Impl(env, callbackClient))
{
}

#else

Client::Client(ovpn3_client_callback *callbackClient) : openVpn3Impl(new Impl(callbackClient))
{
}

#endif

Client::~Client()
{
#ifdef __JNI
    JNIEnv *jniEnv;

    jniEnv = openVpn3Impl->getThreadJniEnv();
#endif

    utils::log_debug("Releasing Client::Impl");

    openVpn3Impl.reset();
}

#ifdef __JNI

void Client::setJniCallbackObject(jobject o)
{
    openVpn3Impl->setJniCallbackObject(o);
}

#endif

void Client::getTransportStats(ovpn3_transport_stats &stats) const
{
    EDDIE_ZEROMEMORY(&stats, sizeof(ovpn3_transport_stats));

    openvpn::ClientAPI::TransportStats ts = openVpn3Impl->transport_stats();

    stats.bytes_in = ts.bytesIn;
    stats.bytes_out = ts.bytesOut;
    stats.packets_in = ts.packetsIn;
    stats.packets_out = ts.packetsOut;
    stats.last_packet_received = ts.lastPacketReceived;
}

EddieLibraryResult Client::setOption(const std::string &name, const std::string &value)
{
    return openVpn3Impl->setOption(name, value, true);
}

void Client::init()
{
    Impl::init_process();
}

void Client::cleanup()
{
    Impl::uninit_process();
}

EddieLibraryResult Client::loadProfileFile(const std::string &filename)
{
    openvpn::ProfileMerge pm(filename.c_str(), "ovpn", "", openvpn::ProfileMerge::FOLLOW_FULL, openvpn::ProfileParseLimits::MAX_LINE_SIZE, openvpn::ProfileParseLimits::MAX_PROFILE_SIZE);

    if(pm.status() == openvpn::ProfileMerge::MERGE_SUCCESS)
        libResult = loadProfileString(pm.profile_content());
    else
    {
        logMessage = utils::format("Client::loadProfileFile(): merge config error: '%s' ('%s')", pm.status_string(), pm.error().c_str());

        openVpn3Impl->logEvent(EDDIE_EVENT_TYPE_ERROR, EDDIE_EVENT_NAME_TAG, logMessage);

		utils::log_error(logMessage);

        libResult.code = LR_OPENVPN_PROFILE_ERROR;
        libResult.description = logMessage.c_str();
    }

    return libResult;
}

EddieLibraryResult Client::loadProfileString(const std::string &str)
{
    std::string profile = utils::trim_copy(str);

    if(profile.empty() == false)
    {
        openVpnProfile = profile;

        libResult.code = LR_SUCCESS;
        libResult.description = LIBRARY_RESULT_OK;
    }
    else
    {
        logMessage = "Client::loadProfileString(): Profile is empty";

        openVpn3Impl->logEvent(EDDIE_EVENT_TYPE_ERROR, EDDIE_EVENT_NAME_TAG, logMessage);

		utils::log_error(logMessage);

        libResult.code = LR_OPENVPN_PROFILE_ERROR;
        libResult.description = logMessage.c_str();
    }

    return libResult;
}

EddieLibraryResult Client::start()
{
    logMessage = "Client::start(): Loading profile";

    Impl::eddie_log_debug(logMessage);

    openVpn3Impl->logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, logMessage);

    libResult = applyProfiles();

    if(libResult.code != LR_SUCCESS)
        return libResult;

    logMessage = "Client::start(): Profile loaded. Connecting to server";

    Impl::eddie_log_debug(logMessage);

    openVpn3Impl->logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, logMessage);

    openvpn::ClientAPI::Status connectStatus = openVpn3Impl->connect();

    if(!connectStatus.error)
    {
        libResult.code = LR_SUCCESS;
        libResult.description = LIBRARY_RESULT_OK;
    }
    else
    {
        logMessage = "Client::start(): OpenVPN Connection error";

        if(!connectStatus.status.empty())
            logMessage += ": " + connectStatus.status;

        openVpn3Impl->logEvent(EDDIE_EVENT_TYPE_ERROR, EDDIE_EVENT_NAME_TAG, logMessage);

        utils::log_error(logMessage);

        libResult.code = LR_OPENVPN_CONNECTION_ERROR;
        libResult.description = logMessage.c_str();
    }

	return libResult;
}

void Client::stop()
{
    openVpn3Impl->stop();
}

void Client::pause(const std::string &reason)
{
    openVpn3Impl->pause(reason);
}

void Client::resume()
{
    openVpn3Impl->resume();
}

EddieLibraryResult Client::applyProfiles()
{
    if(openVpnProfile == "")
    {
        logMessage = "Client::applyProfiles(): Profile is empty";

        openVpn3Impl->logEvent(EDDIE_EVENT_TYPE_ERROR, EDDIE_EVENT_NAME_TAG, logMessage);

        libResult.code = LR_OPENVPN_PROFILE_IS_EMPTY;
        libResult.description = logMessage.c_str();

        return libResult;
    }

    // Default params

    //std::string peer_info;
    //bool eval = false;
    //bool self_test = false;
    //bool merge = false;
    //bool version = false;
    std::string epki_cert_fn;
    //std::string epki_ca_fn;
    //std::string epki_key_fn;

    // Create the config from the loaded profiles

    openvpn::ClientAPI::Config config;
    config.guiVersion = EDDIE_LIBRARY_NAME;
    config.guiVersion += " ";
    config.guiVersion += EDDIE_LIBRARY_VERSION;
    config.info = true;
    config.altProxy = false;
    config.dco = false;
    config.googleDnsFallback = false;
    //config.serverOverride = server;
    //config.gremlinConfig = gremlin;

    config.content = openVpnProfile;

    openVpn3Impl->applyConfig(config);

    if(!epki_cert_fn.empty())
        config.externalPkiAlias = "epki"; // dummy

    openvpn::ClientAPI::EvalConfig eval = openVpn3Impl->eval_config(config);

    if(eval.error)
    {
        logMessage = "Client::applyProfiles(): Eval config error (" + eval.message + ")";

        openVpn3Impl->logEvent(EDDIE_EVENT_TYPE_ERROR, EDDIE_EVENT_NAME_TAG, logMessage);

        libResult.code = LR_OPENVPN_CONFIG_EVAL_ERROR;
        libResult.description = logMessage.c_str();

        return libResult;
    }

    if(!eval.autologin)
    {
        logMessage = "Client::applyProfiles(): Loading credentials";

        Impl::eddie_log_debug(logMessage);

        openVpn3Impl->logEvent(EDDIE_EVENT_TYPE_MESSAGE, EDDIE_EVENT_NAME_TAG, logMessage);

        openvpn::ClientAPI::ProvideCreds credentials;

        credentials.response = "";
        credentials.dynamicChallengeCookie = "";
        credentials.replacePasswordWithSessionID = true;
        credentials.cachePassword = false;

        openVpn3Impl->applyCredentials(credentials);

        if(credentials.username.empty() || credentials.password.empty())
        {
            logMessage = "Client::applyProfiles(): Credentials required";

            openVpn3Impl->logEvent(EDDIE_EVENT_TYPE_ERROR, EDDIE_EVENT_NAME_TAG, logMessage);

            libResult.code = LR_OPENVPN_CREDS_ERROR;
            libResult.description = logMessage.c_str();

            return libResult;
        }

        openvpn::ClientAPI::Status credentials_status = openVpn3Impl->provide_creds(credentials);

        if(credentials_status.error)
        {
            logMessage = "Client::applyProfiles(): Credentials error (" + credentials_status.message + ")";

            openVpn3Impl->logEvent(EDDIE_EVENT_TYPE_ERROR, EDDIE_EVENT_NAME_TAG, logMessage);

            libResult.code = LR_OPENVPN_CONFIG_EVAL_ERROR;
            libResult.description = logMessage.c_str();

            return libResult;
        }
    }

    // external PKI
    if(!epki_cert_fn.empty())
    {
        /*
        client.epki_cert = read_text_utf8(epki_cert_fn);
        if(!epki_ca_fn.empty())
            client.epki_ca = read_text_utf8(epki_ca_fn);
#if defined(USE_MBEDTLS)
        if(!epki_key_fn.empty())
        {
            const std::string epki_key_txt = read_text_utf8(epki_key_fn);
            client.epki_ctx.parse(epki_key_txt, "EPKI", privateKeyPassword);
        }
        else
            OPENVPN_THROW_EXCEPTION("--epki-key must be specified");
#endif
        */
    }

    libResult.code = LR_SUCCESS;
    libResult.description = LIBRARY_RESULT_OK;

    return libResult;
}

void Client::logEvent(int level, const char *name, std::string info)
{
    openVpn3Impl->logEvent(level, name, info);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

EDDIE_NAMESPACE_END

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
