// <eddie_source_header>
// This file is part of Eddie/AirVPN software.
// Copyright (C) 2014-2018 AirVPN (support@airvpn.org) / https://airvpn.org
//
// Eddie is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Eddie is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Eddie. If not, see <http://www.gnu.org/licenses/>.
// </eddie_source_header>

#ifndef EDDIE_ANDROID_NATIVE_MACROS_H
#define EDDIE_ANDROID_NATIVE_MACROS_H

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#define EDDIE_LIBRARY_NAME				"Eddie Library"
#define EDDIE_LIBRARY_VERSION			"1.3.2"
#define EDDIE_LIBRARY_COMPLETE_NAME		"Eddie Library 1.3.2"
#define EDDIE_LIBRARY_RELEASE_DATE		"22 March 2019"
#define EDDIE_LIBRARY_API_LEVEL			6

#ifdef __arm__
#define EDDIE_LIBRARY_ARCHITECTURE      "armeabi-v7a (Arm 32 bit)"
#elif defined(__aarch64__)
#define EDDIE_LIBRARY_ARCHITECTURE      "arm64-v8a (Arm 64 bit)"
#elif defined(__i386__)
#define EDDIE_LIBRARY_ARCHITECTURE      "x86 (32 bit)"
#elif defined(__x86_64__)
#define EDDIE_LIBRARY_ARCHITECTURE      "x86-64 (64 bit)"
#else
#define EDDIE_LIBRARY_ARCHITECTURE      "Unknown"
#endif

#ifdef __ANDROID__
#define EDDIE_LIBRARY_PLATFORM          "Android"
#elif defined(__linux__)
#define EDDIE_LIBRARY_PLATFORM          "Linux"
#elif defined( __FreeBSD__)
#define EDDIE_LIBRARY_PLATFORM          "FreeBSD"
#else
#define EDDIE_LIBRARY_PLATFORM          "Unknown"
#endif

#define EDDIE_ZEROMEMORY(dest, len)		memset((dest), 0, (len))

#define EDDIE_OVERRIDE					override

#define EDDIE_LOG_TAG					"org.airvpn.eddie.native"

#define EDDIE_NAMESPACE_NAME			eddie
#define EDDIE_NAMESPACE_BEGIN			namespace EDDIE_NAMESPACE_NAME {
#define EDDIE_NAMESPACE_END			    }
#define USING_EDDIE_NAMESPACE		    using namespace EDDIE_NAMESPACE_NAME;

#define EDDIE_SUCCEEDED(v)				((v) >= 0)
#define EDDIE_FAILED(v)					((v) < 0)

#define EDDIE_BOOL_CAST(v)				((v) ? true : false)
#define EDDIE_FLAG_CAST(v)				((v) ? 1 : 0)

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#endif // EDDIE_ANDROID_NATIVE_MACROS_H
