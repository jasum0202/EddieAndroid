// <eddie_source_header>
// This file is part of Eddie/AirVPN software.
// Copyright (C) 2014-2018 AirVPN (support@airvpn.org) / https://airvpn.org
//
// Eddie is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Eddie is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Eddie. If not, see <http://www.gnu.org/licenses/>.
// </eddie_source_header>

// Version 1.2 - 20 July 2018, ProMIND
//
// Added new ovpn3 event types macros
// Added ovpn event name macros
// Added eddie event name macros
//
// Version 1.3 - 4 September 2018, ProMIND
//
// Added JNI support
//
// Version 1.3.2 - 22 March 2019, ProMIND
//
// Added OpenVPNInfo() and OpenVPNCopyright()

#ifdef __JNI
    #include <jni.h>
#endif

#ifndef EDDIE_ANDROID_NATIVE_API_H
#define EDDIE_ANDROID_NATIVE_API_H

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Version 1.1

#define UDP_PARTIAL_SEND_ERROR          -5
#define EDDIE_EVENT_TYPE_FATAL_ERROR    -4
#define EDDIE_EVENT_TYPE_ERROR			-3
#define EDDIE_EVENT_TYPE_FORMAL_WARNING -2
#define EDDIE_EVENT_TYPE_MESSAGE		-1

#define OVPN3_EVENT_NAME_TAG    	    "OpenVPN3"
#define EDDIE_EVENT_NAME_TAG    	    "Eddie Library"

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#ifdef __cplusplus
extern "C" {
#endif

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Version 1.2

#define LIBRARY_RESULT_OK       "Ok"
#define LIBRARY_RESULT_ERROR    "Error"

typedef enum
{
    LR_ERROR = -1,
    LR_SUCCESS,
    LR_EXCEPTION_ERROR,
    LR_OPENVPN_NOT_INITIALIZED,
    LR_OPENVPN_ALREADY_INITIALIZED,
    LR_BREAKPAD_INITIALIZATION_ERROR,
    LR_OPENVPN_POINTER_IS_NULL,
    LR_FAILED_TO_CREATE_OPENVPN_CLIENT,
    LR_OPENVPN_TRANSPORT_STATS_POINTER_IS_NULL,
    LR_PROFILE_FILENAME_IS_NULL,
    LR_PROFILE_STRING_IS_NULL,
    LR_OPENVPN_OPTION_NAME_IS_NULL,
    LR_OPENVPN_OPTION_VALUE_IS_NULL,
    LR_OPENVPN_UNKNOWN_OPTION,
    LR_OPENVPN_PROFILE_IS_EMPTY,
    LR_OPENVPN_PROFILE_ERROR,
    LR_OPENVPN_CONFIG_EVAL_ERROR,
    LR_OPENVPN_CREDS_ERROR,
    LR_OPENVPN_CONNECTION_ERROR
}  LibResult;

typedef struct
{
    int code;
    const char *description;
} EddieLibraryResult;

typedef struct ovpn3_event_t
{
	int type;				// OVPN3_EVENT_TYPE_*
	const char *name;		// event name
	const char *info;		// additional event infomations
	const void *data;		// event payload (if any)
} ovpn3_event;

typedef struct ovpn3_connection_data_t
{
	int defined;
	const char *user;
	const char *serverHost;
	const char *serverPort;
	const char *serverProto;
	const char *serverIp;
	const char *vpnIp4;
	const char *vpnIp6;
	const char *gw4;
	const char *gw6;
	const char *clientIp;
	const char *tunName;
} ovpn3_connection_data;

typedef struct ovpn3_transport_stats_t
{
	long long bytes_in;
	long long bytes_out;
	long long packets_in;
	long long packets_out;
	int last_packet_received;			// number of binary milliseconds (1/1024th of a second) since last packet was received, or -1 if undefined
} ovpn3_transport_stats;

#ifdef __JNI

typedef struct ovpn3_client_callback_jni_t
{
	// Client callbacks

	jmethodID socket_protect; // (I)Z
	jmethodID on_event; // (Ljava/lang/Object;)V (ovpn3_event)

	// TUN callbacks

	jmethodID tun_builder_new; // ()Z
	jmethodID tun_builder_set_layer; // (I)Z
	jmethodID tun_builder_set_remote_address; // (Ljava/lang/String;Z)Z
	jmethodID tun_builder_add_address; // (Ljava/lang/String;ILjava/lang/String;ZZ)Z
	jmethodID tun_builder_set_route_metric_default; // (I)Z
	jmethodID tun_builder_reroute_gw; // (ZZI)Z
	jmethodID tun_builder_add_route; // (Ljava/lang/String;IIZ)Z
	jmethodID tun_builder_exclude_route; // (Ljava/lang/String;IIZ)Z
	jmethodID tun_builder_add_dns_server; // (Ljava/lang/String;Z)Z
	jmethodID tun_builder_add_search_domain; // (Ljava/lang/String;)Z
	jmethodID tun_builder_set_mtu; // (I)Z
	jmethodID tun_builder_set_session_name; // (Ljava/lang/String;)Z
	jmethodID tun_builder_add_proxy_bypass; // (Ljava/lang/String;)Z
	jmethodID tun_builder_set_proxy_auto_config_url; // (Ljava/lang/String;)Z
	jmethodID tun_builder_set_proxy_http; // (Ljava/lang/String;I)Z
	jmethodID tun_builder_set_proxy_https; // (Ljava/lang/String;I)Z
	jmethodID tun_builder_add_wins_server; // (Ljava/lang/String;)Z
	jmethodID tun_builder_set_block_ipv6; // (Z)Z
	jmethodID tun_builder_set_adapter_domain_suffix; // (Ljava/lang/String;)Z
	jmethodID tun_builder_establish; // ()I
	jmethodID tun_builder_persist; // ()Z
	jmethodID tun_builder_establish_lite; // ()V
	jmethodID tun_builder_teardown; // (Z)V

	// Connection callbacks

	jmethodID connect_attach; // ()V
	jmethodID connect_pre_run; // ()V
	jmethodID connect_run; // ()V
	jmethodID connect_session_stop; // ()V
} ovpn3_client_callback_jni;

#else

typedef struct ovpn3_client_callback_t
{
	// Client callbacks

	int (* socket_protect)(int socket);
	void (* on_event)(ovpn3_event *e);

	// TUN callbacks

	int (* tun_builder_new)();
	int (* tun_builder_set_layer)(int layer);
	int (* tun_builder_set_remote_address)(const char *address, int ipv6);
	int (* tun_builder_add_address)(const char *address, int prefix_length, const char *gateway, int ipv6, int net30);
	int (* tun_builder_set_route_metric_default)(int metric);
	int (* tun_builder_reroute_gw)(int ipv4, int ipv6, unsigned int flags);
	int (* tun_builder_add_route)(const char *address, int prefix_length, int metric, int ipv6);
	int (* tun_builder_exclude_route)(const char *address, int prefix_length, int metric, int ipv6);
	int (* tun_builder_add_dns_server)(const char *address, int ipv6);
	int (* tun_builder_add_search_domain)(const char *domain);
	int (* tun_builder_set_mtu)(int mtu);
	int (* tun_builder_set_session_name)(const char *name);
	int (* tun_builder_add_proxy_bypass)(const char *bypass_host);
	int (* tun_builder_set_proxy_auto_config_url)(const char *url);
	int (* tun_builder_set_proxy_http)(const char *host, int port);
	int (* tun_builder_set_proxy_https)(const char *host, int port);
	int (* tun_builder_add_wins_server)(const char *address);
	int (* tun_builder_set_block_ipv6)(int block_ipv6);
	int (* tun_builder_set_adapter_domain_suffix)(const char *name);
	int (* tun_builder_establish)();
	int (* tun_builder_persist)();
	void (* tun_builder_establish_lite)();
	void (* tun_builder_teardown)(int disconnect);

	// Connection callbacks

	void (* connect_attach)();
	void (* connect_pre_run)();
	void (* connect_run)();
	void (* connect_session_stop)();
} ovpn3_client_callback;

#endif

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

const char *Name();
const char *Version();
const char *QualifiedName();
const char *ReleaseDate();
int ApiLevel();
const char *Architecture();
const char *Platform();

EddieLibraryResult InitOpenVPN();
EddieLibraryResult CleanUpOpenVPN();

#ifdef __JNI
EddieLibraryResult CreateOpenVPNClient(JNIEnv *env, jobject clientCallback);
#else
EddieLibraryResult CreateOpenVPNClient(ovpn3_client_callback *callbackClient);
#endif

EddieLibraryResult DisposeOpenVPNClient(void);
EddieLibraryResult StartOpenVPNClient(void);
EddieLibraryResult StopOpenVPNClient(void);
EddieLibraryResult PauseOpenVPNClient(const char *reason);
EddieLibraryResult ResumeOpenVPNClient(void);
EddieLibraryResult GetOpenVPNClientTransportStats(ovpn3_transport_stats *stats);
EddieLibraryResult LoadProfileToOpenVPNClient(const char *filename);
EddieLibraryResult LoadStringProfileToOpenVPNClient(const char *str);
EddieLibraryResult SetOpenVPNClientOption(const char *option, const char *value);

#ifdef __JNI

bool setJniLibResult(JNIEnv *env, EddieLibraryResult result);
EddieLibraryResult setCallbackObject(JNIEnv *env, jobject callbackObject);

// EddieLibrary class

JNIEXPORT jstring JNICALL Java_org_airvpn_eddie_EddieLibrary_name(JNIEnv *env, jobject obj);
JNIEXPORT jstring JNICALL Java_org_airvpn_eddie_EddieLibrary_version(JNIEnv *env, jobject obj);
JNIEXPORT jstring JNICALL Java_org_airvpn_eddie_EddieLibrary_qualifiedName(JNIEnv *env, jobject obj);
JNIEXPORT jstring JNICALL Java_org_airvpn_eddie_EddieLibrary_releaseDate(JNIEnv *env, jobject obj);
JNIEXPORT jint JNICALL Java_org_airvpn_eddie_EddieLibrary_apiLevel(JNIEnv *env, jobject obj);
JNIEXPORT jstring JNICALL Java_org_airvpn_eddie_EddieLibrary_architecture(JNIEnv *env, jobject obj);
JNIEXPORT jstring JNICALL Java_org_airvpn_eddie_EddieLibrary_platform(JNIEnv *env, jobject obj);
JNIEXPORT jstring JNICALL Java_org_airvpn_eddie_EddieLibrary_openVPNInfo(JNIEnv *env, jobject obj);
JNIEXPORT jstring JNICALL Java_org_airvpn_eddie_EddieLibrary_openVPNCopyright(JNIEnv *env, jobject obj);

JNIEXPORT jobject JNICALL Java_org_airvpn_eddie_EddieLibrary_initOpenVPN(JNIEnv *env, jobject obj);
JNIEXPORT jobject JNICALL Java_org_airvpn_eddie_EddieLibrary_cleanUpOpenVPN(JNIEnv *env, jobject obj);

JNIEXPORT jobject JNICALL Java_org_airvpn_eddie_EddieLibrary_createOpenVPNClient(JNIEnv *env, jobject obj, jobject clientCallback);
JNIEXPORT jobject JNICALL Java_org_airvpn_eddie_EddieLibrary_disposeOpenVPNClient(JNIEnv *env, jobject obj, jobject callbackObject);
JNIEXPORT jobject JNICALL Java_org_airvpn_eddie_EddieLibrary_startOpenVPNClient(JNIEnv *env, jobject obj, jobject callbackObject);
JNIEXPORT jobject JNICALL Java_org_airvpn_eddie_EddieLibrary_stopOpenVPNClient(JNIEnv *env, jobject obj, jobject callbackObject);
JNIEXPORT jobject JNICALL Java_org_airvpn_eddie_EddieLibrary_pauseOpenVPNClient(JNIEnv *env, jobject obj, jobject callbackObject, jstring reason);
JNIEXPORT jobject JNICALL Java_org_airvpn_eddie_EddieLibrary_resumeOpenVPNClient(JNIEnv *env, jobject obj, jobject callbackObject);
JNIEXPORT jobject JNICALL Java_org_airvpn_eddie_EddieLibrary_getOpenVPNClientTransportStats(JNIEnv *env, jobject obj);
JNIEXPORT jobject JNICALL Java_org_airvpn_eddie_EddieLibrary_loadProfileToOpenVPNClient(JNIEnv *env, jobject obj, jobject callbackObject, jstring filename);
JNIEXPORT jobject JNICALL Java_org_airvpn_eddie_EddieLibrary_loadStringProfileToOpenVPNClient(JNIEnv *env, jobject obj, jobject callbackObject, jstring profile);
JNIEXPORT jobject JNICALL Java_org_airvpn_eddie_EddieLibrary_setOpenVPNClientOption(JNIEnv *env, jobject obj, jobject callbackObject, jstring option, jstring value);

// OpenVPNTunnel class

JNIEXPORT jobject JNICALL Java_org_airvpn_eddie_EddieLibrary_setCallback(JNIEnv *env, jobject obj, jobject caller);

#endif

unsigned long long GetAvailableMemory(void);

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#ifdef __cplusplus
}
#endif

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#endif // EDDIE_ANDROID_NATIVE_API_H

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
