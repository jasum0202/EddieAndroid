# AirVPN Eddie for Android - Native Library

In order to make Eddie's native library, you need the following projects:

* OpenVPN3 - AirVPN fork (https://github.com/AirVPN/openvpn3)

* MbedTLS (https://github.com/ARMmbed/mbedtls)

* Asio (https://github.com/chriskohlhoff/asio)

* Boost (https://www.boost.org)

* LZ4 (https://github.com/lz4/lz4)

* LZO (http://www.oberhumer.com/opensource/lzo)

* Breakpad (https://chromium.googlesource.com/breakpad/breakpad)


The directories of the above dependencies are already created in this project tree as "placeholders" or "reminders". Where applicable, inside these directories you can find the relative Android.mk file.


To build the library, you need to run one the below scripts, according to your system:

* Unix/Linux/OSX - makelibrary.sh

* Windows - build.bat


The above scripts may need some customization according to your system, environment or paths. You can edit them and change settings according to your needs. In order to make the native library, you need NDK to be properly installed in your system.

